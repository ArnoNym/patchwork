package ai;

import ai.training.utils.DecoderUtils;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DecoderUtilsTest {
    private boolean[][] matrix_7x7_1;
    private boolean[][] matrix_7x7_2;

    private boolean[][] matrix1;
    private boolean[][] matrix2;

    @Before
    public void setup() {
        matrix_7x7_1 = new boolean[7][7];
        matrix_7x7_2 = new boolean[7][7];
        matrix_7x7_2[0][0] = true;
        matrix_7x7_2[1][1] = true;
        matrix_7x7_2[2][2] = true;
        matrix_7x7_2[3][3] = true;
        matrix_7x7_2[4][4] = true;
        matrix_7x7_2[5][5] = true;
        matrix_7x7_2[6][6] = true;

        matrix1 = new boolean[5][5];
        matrix2 = new boolean[5][5];
        matrix2[0][0] = true;
        matrix2[1][1] = true;
        matrix2[2][2] = true;
        matrix2[3][3] = true;
        matrix2[4][4] = true;
    }

    @Test
    public void decodeSpecialPatch() {
        assertEquals(0, DecoderUtils.decodeSpecialPatch(false), 0.0);
        assertEquals(1, DecoderUtils.decodeSpecialPatch(true), 0.0);
    }

    @Test
    public void clampBetweenZeroAndOne() {
        assertEquals(1, DecoderUtils.clampBetweenZeroAndOne(1, 0, 1), 0.0);
        assertEquals(0, DecoderUtils.clampBetweenZeroAndOne(0, 0, 1), 0.0);

        assertEquals(1, DecoderUtils.clampBetweenZeroAndOne(0.8, 0, 0.8), 0.0);
        assertEquals(0, DecoderUtils.clampBetweenZeroAndOne(0, 0, 0.8), 0.0);

        assertEquals(1, DecoderUtils.clampBetweenZeroAndOne(8, 0, 8), 0.0);
        assertEquals(0, DecoderUtils.clampBetweenZeroAndOne(0, 0, 8), 0.0);

        assertEquals(1, DecoderUtils.clampBetweenZeroAndOne(1, -1, 1), 0.0);
        assertEquals(0.5, DecoderUtils.clampBetweenZeroAndOne(0, -1, 1), 0.0);
        assertEquals(0, DecoderUtils.clampBetweenZeroAndOne(-1, -1, 1), 0.0);

        assertEquals(1, DecoderUtils.clampBetweenZeroAndOne(0.8, -0.8, 0.8), 0.0);
        assertEquals(0.5, DecoderUtils.clampBetweenZeroAndOne(0, -0.8, 0.8), 0.0);
        assertEquals(0, DecoderUtils.clampBetweenZeroAndOne(-0.8, -0.8, 0.8), 0.0);

        assertEquals(1, DecoderUtils.clampBetweenZeroAndOne(8, -8, 8), 0.0);
        assertEquals(0.5, DecoderUtils.clampBetweenZeroAndOne(0, -8, 8), 0.0);
        assertEquals(0, DecoderUtils.clampBetweenZeroAndOne(-8, -8, 8), 0.0);
    }
}
