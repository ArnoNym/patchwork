package ai.anneat;

import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Generation;
import ai.anneat.models.generation.Species;
import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.NodeGene;
import ai.anneat.models.genome.Pool;
import ai.anneat.publish.settings.Settings;
import ai.anneat.main.utils.collections.OrderedSet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class Setup {

    public static Pool createPool(int inputSize, int outputSize, int clientCount) {
        Settings settings = new Settings();
        settings.getPerformanceSettings().setMaxAgentCount(clientCount);
        return new Pool(inputSize, outputSize, settings);
    }

    public static Species createSpecies(int clientsCount) {
        Pool pool = createPool(10, 10, clientsCount);
        return new Species(Generation.init(pool).getAgentList());
    }

    public static OrderedSet<Species> createListWith1SpeciesWithClients(int clientsCount) {
        OrderedSet<Species> speciesList = new OrderedSet<>();
        speciesList.add(createSpecies(clientsCount));
        return speciesList;
    }

    public static OrderedSet<Species> create100SpeciesOnlyWithRepresentative(int speciesCount) {
        OrderedSet<Species> speciesList = new OrderedSet<>();
        for (int i = 0; i < speciesCount; i++) {
            speciesList.add(createSpecies(1));
        }
        return speciesList;
    }

    public static Genome createGenomeWithOneNodeInBetween(Pool pool) {
        Genome genome = new Genome(pool);
        ConnectionGene c = genome.addNewConnection(
                pool.getInputNodes().get(0),
                pool.getOutputNodes().get(0),
                null).orElseThrow();
        genome.addNewNodeWithCons(c);
        return genome;
    }

    public static Genome createGenomeWithOneNodeInBetweenAllWeightsOne(Pool pool) {
        Genome genome = Setup.createGenomeWithOneNodeInBetween(pool);
        for (ConnectionGene con : genome.getConnections()) {
            con.setWeight(1);
        }
        return genome;
    }

    public static Genome createGenomeWithOneNodeInBetween() {
        Pool pool = createPool(3, 1, 2);
        return createGenomeWithOneNodeInBetween(pool);
    }

    public static Genome createGenomeWith2In2OutWithEachConnectedWith2ConnectionsAnd1Node() {
        Pool pool = createPool(2, 2, 2);

        Set<ConnectionGene> connectionSet = new HashSet<>();

        Genome genome = new Genome(pool);

        NodeGene n1 = new NodeGene(101, 0.5, 0.1);
        genome.addNode(n1);
        ConnectionGene c1 = genome.addNewConnection(pool.getInputNodes().get(0), n1, null).orElseThrow();
        ConnectionGene c2 = genome.addNewConnection(n1, pool.getOutputNodes().get(0), null).orElseThrow();
        connectionSet.add(c1);
        connectionSet.add(c2);

        n1 = new NodeGene(102, 0.5, 0.5);
        genome.addNode(n1);
        c1 = genome.addNewConnection(pool.getInputNodes().get(1), n1, null).orElseThrow();
        c2 = genome.addNewConnection(n1, pool.getOutputNodes().get(1), null).orElseThrow();
        connectionSet.add(c1);
        connectionSet.add(c2);

        return new Genome(pool, connectionSet);
    }

    public static List<Agent> createClients(int clientCount) {
        return Generation.init(createPool(10, 10, clientCount)).getAgentList();
    }

    public static List<Species> createSpeciesListWith4SpeciesWithDifferentClientSizes() {
        List<Agent> agentList = createClients(10);

        assertEquals(10, agentList.size());

        List<Species> speciesList = new ArrayList<>();

        agentList.get(0).setScore(10);
        agentList.get(1).setScore(10);
        agentList.get(2).setScore(10);
        agentList.get(3).setScore(10);
        agentList.get(4).setScore(10);
        agentList.get(5).setScore(0);
        agentList.get(6).setScore(20);
        agentList.get(7).setScore(15);
        agentList.get(8).setScore(1);
        agentList.get(9).setScore(3);

        List<Agent> cl = new ArrayList<>();
        cl.add(agentList.get(0));
        cl.add(agentList.get(1));
        cl.add(agentList.get(2));
        cl.add(agentList.get(3));
        cl.add(agentList.get(4));
        cl.add(agentList.get(5));
        Species s1 = new Species(cl);
        s1.calculateScore(false);
        speciesList.add(s1);

        cl.clear();
        cl.add(agentList.get(6));
        Species s2 = new Species(cl);
        s2.calculateScore(false);
        speciesList.add(s2);

        cl.clear();
        cl.add(agentList.get(7));
        cl.add(agentList.get(8));
        Species s4 = new Species(cl);
        s4.calculateScore(false);
        speciesList.add(s4);

        cl.clear();
        cl.add(agentList.get(9));
        Species s6 = new Species(cl);
        s6.calculateScore(false);
        speciesList.add(s6);

        return speciesList;
    }

    public static List<Agent> createClientsWithEqualNodesInBetween(Pool pool,
                                                                   int clientCount,
                                                                   boolean connectionRandomWeight) {
        Double connectionWeight = connectionRandomWeight ? null : 1d;

        Genome genome = new Genome(pool);

        ConnectionGene c1 = genome.addNewConnection(pool.getInputNodes().get(1), pool.getOutputNodes().get(2), connectionWeight).orElseThrow();
        NodeGene n1 = genome.addNewNodeWithCons(c1).orElseThrow().getNode();
        genome.addNewConnection(pool.getInputNodes().get(0), n1, connectionWeight).orElseThrow();
        genome.addNewConnection(n1, pool.getOutputNodes().get(4), connectionWeight).orElseThrow();

        ConnectionGene c2 = genome.addNewConnection(pool.getInputNodes().get(3), pool.getOutputNodes().get(4), connectionWeight).orElseThrow();
        NodeGene n2 = genome.addNewNodeWithCons(c2).orElseThrow().getNode();
        genome.addNewConnection(pool.getInputNodes().get(2), n2, connectionWeight).orElseThrow();
        genome.addNewConnection(pool.getInputNodes().get(4), n2, connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(2), connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(3), connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(5), connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(6), connectionWeight).orElseThrow();

        genome.addNewConnection(pool.getInputNodes().get(1), pool.getOutputNodes().get(3), connectionWeight).orElseThrow();
        genome.addNewConnection(pool.getInputNodes().get(1), pool.getOutputNodes().get(4), connectionWeight).orElseThrow();
        genome.addNewConnection(pool.getInputNodes().get(1), pool.getOutputNodes().get(7), connectionWeight).orElseThrow();
        genome.addNewConnection(pool.getInputNodes().get(1), pool.getOutputNodes().get(9), connectionWeight).orElseThrow();

        Set<ConnectionGene> connectionSet = genome.getConnections();

        List<Agent> agentList = new ArrayList<>();
        agentList.add(new Agent(genome));
        for (int i = 1; i < clientCount; i++) {
            agentList.add(new Agent(new Genome(pool, connectionSet)));
        }
        return agentList;
    }

    public static List<Agent> createClientsWithEqualNodesInBetween_2(Pool pool,
                                                                     int clientCount,
                                                                     boolean connectionRandomWeight) {
        Double connectionWeight = connectionRandomWeight ? null : 1d;

        Genome genome = new Genome(pool);

        ConnectionGene c1 = genome.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), connectionWeight).orElseThrow();
        NodeGene n1 = genome.addNewNodeWithCons(c1).orElseThrow().getNode();
        genome.addNewConnection(pool.getInputNodes().get(5), n1, connectionWeight).orElseThrow();
        genome.addNewConnection(n1, pool.getOutputNodes().get(4), connectionWeight).orElseThrow();
        genome.addNewConnection(n1, pool.getOutputNodes().get(7), connectionWeight).orElseThrow();

        ConnectionGene c2 = genome.addNewConnection(pool.getInputNodes().get(3), pool.getOutputNodes().get(4), connectionWeight).orElseThrow();
        NodeGene n2 = genome.addNewNodeWithCons(c2).orElseThrow().getNode();
        genome.addNewConnection(pool.getInputNodes().get(0), n2, connectionWeight).orElseThrow();
        genome.addNewConnection(pool.getInputNodes().get(5), n2, connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(1), connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(7), connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(8), connectionWeight).orElseThrow();
        genome.addNewConnection(n2, pool.getOutputNodes().get(9), connectionWeight).orElseThrow();

        ConnectionGene c3 = genome.addNewConnection(pool.getInputNodes().get(2), pool.getOutputNodes().get(4), connectionWeight).orElseThrow();
        NodeGene n3 = genome.addNewNodeWithCons(c3).orElseThrow().getNode();
        genome.addNewConnection(pool.getInputNodes().get(0), n3, connectionWeight).orElseThrow();
        genome.addNewConnection(pool.getInputNodes().get(5), n3, connectionWeight).orElseThrow();
        genome.addNewConnection(n3, pool.getOutputNodes().get(3), connectionWeight).orElseThrow();
        genome.addNewConnection(n3, pool.getOutputNodes().get(6), connectionWeight).orElseThrow();
        genome.addNewConnection(n3, pool.getOutputNodes().get(7), connectionWeight).orElseThrow();
        genome.addNewConnection(n3, pool.getOutputNodes().get(9), connectionWeight).orElseThrow();

        Set<ConnectionGene> connectionSet = genome.getConnections();

        List<Agent> agentList = new ArrayList<>();
        agentList.add(new Agent(genome));
        for (int i = 1; i < clientCount; i++) {
            agentList.add(new Agent(new Genome(pool, connectionSet)));
        }
        return agentList;
    }
}
