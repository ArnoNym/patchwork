package ai.anneat.dataStructures;

import ai.anneat.Setup;
import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Species;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.Pool;
import ai.anneat.main.utils.selectors.ByScoreCalculator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class ByScoreControllerCalculatorTest {
    @Test(expected = IllegalArgumentException.class)
    public void zero_empty() {
        new ByScoreCalculator(0, new HashSet<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonZero_empty() {
        new ByScoreCalculator(2, new HashSet<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonZero_negativeScore() {
        Set<Species> speciesSet = new HashSet<>();
        Agent agent = new Agent(new Genome(new Pool(2, 2)));
        agent.setScore(-1);
        agent.setVanillaScore(-1);
        Species species = new Species(agent);
        species.calculateScore(false);
        speciesSet.add(species);
        assertTrue(species.getScore() < 0);
        new ByScoreCalculator(2, speciesSet);
    }

    @Test
    public void nonZero_zeroScore() {
        Set<Species> speciesSet = new HashSet<>();
        Agent agent = new Agent(new Genome(new Pool(2, 2)));
        agent.setScore(0);
        agent.setVanillaScore(0);
        Species species = new Species(agent);
        species.calculateScore(false);
        speciesSet.add(species);
        assertEquals(0, species.getScore(), 0.0);
        ByScoreCalculator calculator = new ByScoreCalculator(2, speciesSet);
        assertTrue(calculator.isTotalScoreZero());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonZero_one_doubleGet() {
        List<Species> speciesSet = new ArrayList<>();
        speciesSet.add(Setup.createSpecies(3));
        speciesSet.get(0).getAgents().get(0).setScore(13);
        speciesSet.get(0).calculateScore(false);
        ByScoreCalculator calculator = new ByScoreCalculator(2, speciesSet);
        assertEquals(2, calculator.getCount(speciesSet.get(0).getScore()));
        calculator.getCount(speciesSet.get(0).getScore());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonZero_multi_doubleGet() {
        List<Species> speciesSet = new ArrayList<>();

        speciesSet.add(Setup.createSpecies(1));
        speciesSet.get(0).getAgents().get(0).setScore(13);
        speciesSet.get(0).calculateScore(false);

        speciesSet.add(Setup.createSpecies(1));
        speciesSet.get(1).getAgents().get(0).setScore(13);
        speciesSet.get(1).calculateScore(false);

        ByScoreCalculator calculator = new ByScoreCalculator(6, speciesSet);
        assertEquals(3, calculator.getCount(speciesSet.get(0).getScore()));
        assertEquals(3, calculator.getCount(speciesSet.get(1).getScore()));
        calculator.getCount(speciesSet.get(0).getScore());
    }

    @Test
    public void nonZero_multi_getTooMuchScore() {
        List<Species> speciesSet = new ArrayList<>();

        speciesSet.add(Setup.createSpecies(2));
        speciesSet.get(0).getAgents().get(0).setScore(13);
        speciesSet.get(0).calculateScore(false);

        ByScoreCalculator calculator = new ByScoreCalculator(6, speciesSet);

        Species notInCalculatorSpecies = Setup.createSpecies(2);
        notInCalculatorSpecies.getAgents().get(0).setScore(236);
        notInCalculatorSpecies.calculateScore(false);

        assertEquals(6, calculator.getCount(notInCalculatorSpecies.getScore()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonZero_one_notInCalculator() {
        List<Species> speciesSet = new ArrayList<>();

        speciesSet.add(Setup.createSpecies(1));
        speciesSet.get(0).getAgents().get(0).setScore(12);
        speciesSet.get(0).calculateScore(false);

        ByScoreCalculator calculator = new ByScoreCalculator(6, speciesSet);

        Species notInCalculatorSpecies = Setup.createSpecies(1);
        notInCalculatorSpecies.getAgents().get(0).setScore(6);
        notInCalculatorSpecies.calculateScore(false);

        assertEquals(6, calculator.getCount(notInCalculatorSpecies.getScore()));
        calculator.getCount(speciesSet.get(0).getScore());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonZero_two_notInCalculator() {
        List<Species> speciesSet = new ArrayList<>();

        speciesSet.add(Setup.createSpecies(1));
        speciesSet.get(0).getAgents().get(0).setScore(12);
        speciesSet.get(0).calculateScore(false);

        speciesSet.add(Setup.createSpecies(1));
        speciesSet.get(1).getAgents().get(0).setScore(12);
        speciesSet.get(1).calculateScore(false);

        ByScoreCalculator calculator = new ByScoreCalculator(5, speciesSet);

        Species notInCalculatorSpecies = Setup.createSpecies(1);
        notInCalculatorSpecies.getAgents().get(0).setScore(6);
        notInCalculatorSpecies.calculateScore(false);

        assertEquals(1, calculator.getCount(notInCalculatorSpecies.getScore()));
        assertEquals(4, calculator.getCount(speciesSet.get(0).getScore()));
        calculator.getCount(speciesSet.get(1).getScore());
    }

    @Test
    public void nonZero_multi() {
        List<Species> speciesSet = new ArrayList<>();

        speciesSet.add(Setup.createSpecies(2));
        speciesSet.get(0).getAgents().get(0).setScore(12);
        speciesSet.get(0).calculateScore(false);

        speciesSet.add(Setup.createSpecies(2));
        speciesSet.get(1).getAgents().get(0).setScore(6);
        speciesSet.get(1).calculateScore(false);

        speciesSet.add(Setup.createSpecies(2));
        speciesSet.get(2).getAgents().get(0).setScore(12);
        speciesSet.get(2).calculateScore(false);

        speciesSet.add(Setup.createSpecies(2));
        speciesSet.get(3).getAgents().get(0).setScore(6);
        speciesSet.get(3).calculateScore(false);

        ByScoreCalculator calculator = new ByScoreCalculator(12, speciesSet);
        assertEquals(4, calculator.getCount(speciesSet.get(0).getScore()));
        assertEquals(2, calculator.getCount(speciesSet.get(1).getScore()));
        assertEquals(4, calculator.getCount(speciesSet.get(2).getScore()));
        assertEquals(2, calculator.getCount(speciesSet.get(3).getScore()));
    }
}
