package ai.anneat.dataStructures;
import ai.anneat.models.genome.NodeGene;
import ai.anneat.main.utils.collections.NodeNodeMap;
import org.junit.Test;

public class NodeNodeMapTest {
    @Test(expected = IllegalArgumentException.class)
    public void nullInput() {
        NodeNodeMap<Integer> nodeNodeMap = new NodeNodeMap<>();
        nodeNodeMap.put(null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullInput_1() {
        NodeNodeMap<Integer> nodeNodeMap = new NodeNodeMap<>();
        NodeGene from = new NodeGene(1, 0.5, 0.5);
        nodeNodeMap.put(from, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullInput_2() {
        NodeNodeMap<Integer> nodeNodeMap = new NodeNodeMap<>();
        NodeGene from = new NodeGene(1, 0.5, 0.5);
        NodeGene to = new NodeGene(2, 0.6, 0.2);;
        nodeNodeMap.put(from, to, null);
    }
}
