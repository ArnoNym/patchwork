package ai.anneat.dataStructures;

import ai.anneat.main.utils.collections.Matrix;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixTest {
    @Test
    public void test_1() {
        Matrix<Integer, Double> matrix = new Matrix<>();

        matrix.put(1, 2, 3d);
        matrix.put(3, 4, 5d);
        assertEquals(3d, matrix.get(1, 2), 0);
        assertEquals(5d, matrix.get(3, 4), 0);
        assertEquals(2, matrix.size());

        matrix.put(3, 4, 5d);
        assertEquals(2, matrix.size());
        matrix.put(4, 3, 5d);
        assertEquals(2, matrix.size());

        matrix.put(4, 3, 3d);
        assertEquals(2, matrix.size());
    }
}
