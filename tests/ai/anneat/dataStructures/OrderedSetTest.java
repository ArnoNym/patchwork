package ai.anneat.dataStructures;

import ai.anneat.models.calculation.Node;
import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.NodeGene;
import ai.anneat.main.utils.collections.OrderedSet;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class OrderedSetTest {
    @Test
    public void test_collection_constructor() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(set);

        assertEquals(set.size(), orderedSet.size());
        for (Integer integer : set) {
            assertTrue(orderedSet.contains(integer));
        }
    }

    @Test
    public void test_collectionAnOrderConstructor() {
        Set<Integer> set = new HashSet<>();
        set.add(30);
        set.add(1);
        set.add(20);

        OrderedSet<Integer> orderedSet = new OrderedSet<>(set, Comparator.comparingInt(Integer::intValue));

        orderedSet.add(22);
        orderedSet.add(7);

        assertEquals(5, orderedSet.size());

        for (int i = 1; i < orderedSet.size(); i++) {
            assertTrue(orderedSet.get(i-1) < orderedSet.get(i));
        }
    }

    @Test
    public void test_order_addAll() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(3);
        set.add(2);

        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));
        orderedSet.addAll(set);

        assertEquals(set.size(), orderedSet.size());
        for (Integer integer : set) {
            assertTrue(orderedSet.contains(integer));
        }
        for (int i = 1; i < orderedSet.size(); i++) {
            assertTrue(orderedSet.get(i-1) < orderedSet.get(i));
        }
    }

    @Test
    public void test_order_addAll_2() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(3);
        set.add(2);

        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));
        orderedSet.addAll(set);

        set.clear();
        set.add(5);
        set.add(22);
        set.add(-234);
        orderedSet.addAll(set);

        assertEquals(6, orderedSet.size());
        for (Integer integer : set) {
            assertTrue(orderedSet.contains(integer));
        }
        for (int i = 1; i < orderedSet.size(); i++) {
            assertTrue(orderedSet.get(i-1) <= orderedSet.get(i));
        }
    }

    @Test
    public void test_order_addAll_3() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(3);
        set.add(2);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(set, Comparator.comparingInt(Integer::intValue));
        assertTrue(orderedSet.containsAll(set));
        assertEquals(3, orderedSet.size());
        set.clear();
        set.add(5);
        set.add(-234);
        assertTrue(orderedSet.addAll(set));
        assertEquals(5, orderedSet.size());
        assertTrue(orderedSet.contains(5));
        assertTrue(orderedSet.containsAll(set));
    }

    @Test
    public void test_order_add() {
        Set<Integer> set = new HashSet<>();
        set.add(30);
        set.add(1);
        set.add(20);

        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));

        orderedSet.addAll(set);
        assertEquals(3, orderedSet.size());

        orderedSet.add(22);
        assertTrue(orderedSet.add(7));
        assertEquals(5, orderedSet.size());

        for (int i = 1; i < orderedSet.size(); i++) {
            assertTrue(orderedSet.get(i-1) < orderedSet.get(i));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_add_null() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));
        orderedSet.add(2);
        orderedSet.add(null);
    }

    @Test
    public void test_add_alreadyContains() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));
        assertTrue(orderedSet.add(2));
        assertTrue(orderedSet.add(3));
        assertTrue(orderedSet.add(53));
        assertFalse(orderedSet.add(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_addAll_null() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));
        orderedSet.add(2);
        List<Integer> integerList = new ArrayList<>();
        integerList.add(3);
        integerList.add(null);
        orderedSet.addAll(integerList);
    }

    @Test
    public void getComparator() {
        Comparator<Integer> comparator = Comparator.comparingInt(Integer::intValue);
        assertEquals(comparator, (new OrderedSet<>(comparator)).getComparator());
    }

    @Test
    public void setComparator() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(30);
        orderedSet.add(1);
        orderedSet.add(20);
        assertEquals(Integer.valueOf(30), orderedSet.get(0));
        assertEquals(Integer.valueOf(1), orderedSet.get(1));
        assertEquals(Integer.valueOf(20), orderedSet.get(2));
        orderedSet.setComparator(Comparator.comparingInt(Integer::intValue));
        assertEquals(Integer.valueOf(1), orderedSet.get(0));
        assertEquals(Integer.valueOf(20), orderedSet.get(1));
        assertEquals(Integer.valueOf(30), orderedSet.get(2));
    }

    @Test
    public void randomElement_0() {
        int id = 1;
        OrderedSet<ConnectionGene> list = new OrderedSet<>();
        ConnectionGene con1 = new ConnectionGene(id++, new NodeGene(id++, 0.1, 0.3), new NodeGene(id++, 0.9, 0.3));
        assertTrue(list.add(con1));
        assertTrue(list.contains(con1));
        ConnectionGene con2 = new ConnectionGene(id++, new NodeGene(id++, 0.1, 0.6), new NodeGene(id++, 0.9, 0.6));
        assertTrue(list.add(con2));
        assertTrue(list.contains(con2));
        ConnectionGene con3 = new ConnectionGene(id++, new NodeGene(id++, 0.1, 0.9), new NodeGene(id, 0.9, 0.9));
        assertTrue(list.add(con3));
        assertTrue(list.contains(con3));
        assertTrue(list.contains(list.randomElement()));
    }

    @Test
    public void randomElement_1() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        assertTrue(list.contains(orderedSet.randomElement()));
        assertTrue(orderedSet.contains(orderedSet.randomElement()));
    }

    @Test
    public void randomElement_noElements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        assertNull(orderedSet.randomElement());
    }

    @Test
    public void randomElementWithPredicate() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        Integer randomElement = orderedSet.randomElement(integer -> integer > 25);
        assertEquals(Integer.valueOf(30), randomElement);
    }

    @Test
    public void randomElementWithPredicate_noElements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        Integer randomElement = orderedSet.randomElement(integer -> integer > 25);
        assertNull(randomElement);
    }

    @Test
    public void removeLast() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(30);
        orderedSet.add(1);
        orderedSet.add(20);
        orderedSet.removeLast(2);
        assertEquals(1, orderedSet.size());
        assertEquals(Integer.valueOf(30), orderedSet.get(0));
    }

    @Test
    public void toArray() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(30);
        orderedSet.add(1);
        orderedSet.add(20);
        Object[] array = orderedSet.toArray();
        assertEquals(30, array[0]);
        assertEquals(1, array[1]);
        assertEquals(20, array[2]);
    }

    @Test
    public void containsAll() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        assertTrue(orderedSet.containsAll(list));
    }

    @Test(expected = IllegalStateException.class)
    public void addAll() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        assertTrue(orderedSet.addAll(0, list));
    }

    @Test
    public void removeAll() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        orderedSet.add(65);
        assertTrue(orderedSet.removeAll(list));
        assertFalse(orderedSet.containsAll(list));
        assertEquals(1, orderedSet.size());
        assertEquals(Integer.valueOf(65), orderedSet.get(0));
    }

    @Test(expected = IllegalStateException.class)
    public void retainAll() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        assertTrue(orderedSet.retainAll(list));
    }

    @Test
    public void clear() {
        List<Integer> list = new ArrayList<>();
        list.add(30);
        list.add(1);
        list.add(20);
        OrderedSet<Integer> orderedSet = new OrderedSet<>(list);
        assertFalse(orderedSet.isEmpty());
        orderedSet.clear();
        assertTrue(orderedSet.isEmpty());
    }

    @Test(expected = IllegalStateException.class)
    public void set() {
        (new OrderedSet<>()).set(0, 3);
    }

    @Test
    public void indexOf() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(30);
        orderedSet.add(1);
        orderedSet.add(20);
        assertEquals(1, orderedSet.indexOf(1));
    }

    @Test(expected = IllegalStateException.class)
    public void lastIndexOf() {
        (new OrderedSet<>()).lastIndexOf(2);
    }

    @Test
    public void listIterator() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(2);
        Iterator<Integer> it = orderedSet.iterator();
        ListIterator<Integer> lit = orderedSet.listIterator();
        for (int i = 0; i < orderedSet.size(); i++) {
            assertEquals(it.next(), lit.next());
        }
    }

    @Test(expected = IllegalStateException.class)
    public void listIterator_i() {
        (new OrderedSet<>()).listIterator(2);
    }

    @Test
    public void subList() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(30);
        orderedSet.add(1);
        orderedSet.add(20);
        orderedSet.add(202);
        orderedSet.add(201);
        List<Integer> sublist = orderedSet.subList(1, 4);
        assertEquals(Integer.valueOf(1), sublist.get(0));
        assertEquals(Integer.valueOf(20), sublist.get(1));
        assertEquals(Integer.valueOf(202), sublist.get(2));
    }

    @Test(expected = IllegalStateException.class)
    public void replaceAll() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(2);
        assertFalse(orderedSet.isEmpty());
        orderedSet.replaceAll(integer -> integer+1);
    }

    @Test
    public void sort() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(2);
        orderedSet.add(222);
        orderedSet.add(-232);
        orderedSet.add(-2);
        orderedSet.sort(Integer::compareTo);
        for (int i = 1; i < orderedSet.size(); i++) {
            assertTrue(orderedSet.get(i-1) < orderedSet.get(i));
        }
    }

    @Test(expected = IllegalStateException.class)
    public void sort_exception() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>(Comparator.comparingInt(Integer::intValue));
        orderedSet.sort((integer, t1) -> integer + t1 + 3);
        assertTrue(orderedSet.isEmpty());
    }

    @Test(expected = IllegalStateException.class)
    public void spliterator() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        assertNotNull(orderedSet.spliterator());
    }

    @Test(expected = IllegalStateException.class)
    public void removeIf() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        assertFalse(orderedSet.isEmpty());
        orderedSet.removeIf(integer -> integer > 0);
    }

    @Test
    public void stream() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(-3);
        assertFalse(orderedSet.isEmpty());
        List<Integer> filtered = orderedSet.stream().filter(integer -> integer > 0).collect(Collectors.toList());
        assertTrue(filtered.contains(3));
        assertFalse(filtered.contains(-3));
    }

    @Test(expected = IllegalStateException.class)
    public void parallelStream() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        assertFalse(orderedSet.isEmpty());
        assertNotNull(orderedSet.parallelStream());
    }

    @Test
    public void forEach() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(2);
        orderedSet.add(343);
        orderedSet.forEach(integer -> {
            int v = integer + 2;
            assertTrue(v > 3);
        });
    }

    @Test(expected = IllegalStateException.class)
    public void add_i_o() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(2);
        orderedSet.add(343);
        orderedSet.add(2, 23);
        assertFalse(orderedSet.isEmpty());
    }

    @Test
    public void remove() {
        OrderedSet<Node> orderedSet = new OrderedSet<>();

        Node n1 = new Node(0.1);
        Node n2 = new Node(0.2);
        Node n3 = new Node(0.3);

        orderedSet.add(n1);
        orderedSet.add(n2);
        orderedSet.add(n3);

        assertEquals(3, orderedSet.size());
        assertEquals(n2, orderedSet.remove(1));
        assertEquals(2, orderedSet.size());

        assertTrue(orderedSet.contains(n1));
        assertFalse(orderedSet.contains(n2));
        assertTrue(orderedSet.contains(n3));
    }

    @Test
    public void remove_object() {
        OrderedSet<Node> orderedSet = new OrderedSet<>();

        Node n1 = new Node(0.1);
        Node n2 = new Node(0.2);
        Node n3 = new Node(0.3);

        orderedSet.add(n1);
        orderedSet.add(n2);
        orderedSet.add(n3);

        assertEquals(3, orderedSet.size());
        assertTrue(orderedSet.remove(n2));
        assertEquals(2, orderedSet.size());

        assertTrue(orderedSet.contains(n1));
        assertFalse(orderedSet.contains(n2));
        assertTrue(orderedSet.contains(n3));
    }

    @Test(expected = IllegalStateException.class)
    public void toArray_T_T_generator() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(2);
        orderedSet.add(343);
        assertNotNull(orderedSet.toArray(i -> new Integer[0]));
    }

    @Test(expected = IllegalStateException.class)
    public void toArray_T() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(3);
        orderedSet.add(2);
        orderedSet.add(343);
        assertNotNull(orderedSet.toArray(new Integer[2]));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorDuplicates_1() {
        List<Integer> list = new ArrayList<>();
        list.add(4);
        list.add(2);
        list.add(-32);
        list.add(2);
        list.add(343);
        new OrderedSet<>(list);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorDuplicates_2() {
        List<Integer> list = new ArrayList<>();
        list.add(4);
        list.add(2);
        list.add(-32);
        list.add(2);
        list.add(343);
        new OrderedSet<>(list, Integer::compareTo);
    }

    @Test
    public void randomElementPercent_withZeroElements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        assertNull(orderedSet.randomElement(0.1));
    }

    @Test
    public void randomElementPercent_with10Elements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 10; i++) {
            orderedSet.add(i);
        }
        assertEquals(Integer.valueOf(1), orderedSet.randomElement(0.1));
    }

    @Test
    public void randomElementPercent_withOneElement() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        orderedSet.add(1);
        assertEquals(Integer.valueOf(1), orderedSet.randomElement(0.1));
    }

    @Test
    public void randomElementPercent_with20Elements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 20; i++) {
            orderedSet.add(i);
        }
        boolean got1 = false;
        boolean got2 = false;
        for (int i = 0; i < 1000; i++) {
            Integer ret = orderedSet.randomElement(0.1);
            got1 = got1 | ret.equals(1);
            got2 = got2 | ret.equals(2);
            if (got1 && got2) {
                break;
            }
        }
        assertTrue(got1);
        assertTrue(got2);
    }

    @Test
    public void randomElementPercent_with15Elements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 15; i++) {
            orderedSet.add(i);
        }
        boolean got1 = false;
        boolean got2 = false;
        for (int i = 0; i < 1000; i++) {
            Integer ret = orderedSet.randomElement(0.1);
            got1 = got1 | ret.equals(1);
            got2 = got2 | ret.equals(2);
            if (got1 && got2) {
                break;
            }
        }
        assertTrue(got1);
        assertTrue(got2);
    }

    @Test
    public void randomElementPercent_isZero() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 5; i++) {
            orderedSet.add(i);
        }
        assertNull(orderedSet.randomElement(0));
    }

    @Test
    public void randomElementPercent_isOne() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 3; i++) {
            orderedSet.add(i);
        }
        boolean got1 = false;
        boolean got2 = false;
        boolean got3 = false;
        for (int i = 0; i < 1000; i++) {
            Integer ret = orderedSet.randomElement(1);
            got1 = got1 | ret.equals(1);
            got2 = got2 | ret.equals(2);
            got3 = got3 | ret.equals(3);
            if (got1 && got2 && got3) {
                break;
            }
        }
        assertTrue(got1);
        assertTrue(got2);
        assertTrue(got3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void randomElementPercent_isSmallerZero() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 5; i++) {
            orderedSet.add(i);
        }
        orderedSet.randomElement(-0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void randomElementPercent_isBiggerOne() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 5; i++) {
            orderedSet.add(i);
        }
        orderedSet.randomElement(1.2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void twoRandomElementPercent_isSmallerZero() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 5; i++) {
            orderedSet.add(i);
        }
        orderedSet.randomElements(-0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void twoRandomElementPercent_isBiggerOne() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 5; i++) {
            orderedSet.add(i);
        }
        orderedSet.randomElements(1.2);
    }

    @Test
    public void twoRandomElementPercent_isEmpty() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        assertNull(orderedSet.randomElements(1));
    }

    @Test
    public void twoRandomElementPercent_twoElements() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 2; i++) {
            orderedSet.add(i);
        }
        Map.Entry<Integer, Integer> ret = orderedSet.randomElements(1);
        assertEquals(Integer.valueOf(1), ret.getKey());
        assertEquals(Integer.valueOf(2), ret.getValue());
    }

    @Test
    public void randomElementPercent_isVeryLow() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 10; i++) {
            orderedSet.add(i);
        }
        Map.Entry<Integer, Integer> ret = orderedSet.randomElements(0.001);
        assertEquals(Integer.valueOf(1), ret.getKey());
        assertEquals(Integer.valueOf(2), ret.getValue());
    }

    @Test
    public void randomElementPercent_best50Percent() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 10; i++) {
            orderedSet.add(i);
        }
        boolean got1 = false;
        boolean got2 = false;
        boolean got3 = false;
        boolean got4 = false;
        boolean got5 = false;
        for (int i = 0; i < 1000; i++) {
            Map.Entry<Integer, Integer> ret = orderedSet.randomElements(0.5);
            got1 = got1 | ret.getKey().equals(1) | ret.getValue().equals(1);
            got2 = got2 | ret.getKey().equals(2) | ret.getValue().equals(2);
            got3 = got3 | ret.getKey().equals(3) | ret.getValue().equals(3);
            got4 = got4 | ret.getKey().equals(4) | ret.getValue().equals(4);
            got5 = got5 | ret.getKey().equals(5) | ret.getValue().equals(5);
            if (got1 && got2 && got3 && got4 && got5) {
                break;
            }
        }
        assertTrue(got1);
        assertTrue(got2);
        assertTrue(got3);
        assertTrue(got4);
        assertTrue(got5);
    }

    @Test
    public void randomElementPercent_roundBest10Percent() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 11; i++) {
            orderedSet.add(i);
        }
        boolean got1 = false;
        boolean got2 = false;
        for (int i = 0; i < 1000; i++) {
            Map.Entry<Integer, Integer> ret = orderedSet.randomElements(0.1);
            got1 = got1 | ret.getKey().equals(1) | ret.getValue().equals(1);
            got2 = got2 | ret.getKey().equals(2) | ret.getValue().equals(2);
            if (got1 && got2) {
                break;
            }
        }
        assertTrue(got1);
        assertTrue(got2);
    }

    /*
    @Test
    public void randomElementPercent_isOne() {
        OrderedSet<Integer> orderedSet = new OrderedSet<>();
        for (int i = 1; i <= 10; i++) {
            orderedSet.add(i);
        }
        boolean got1 = false;
        boolean got2 = false;
        boolean got3 = false;
        for (int i = 0; i < 1000; i++) {
            Integer ret = orderedSet.randomElement(0.001);
            got1 = got1 | ret.equals(1);
            got2 = got2 | ret.equals(2);
            got3 = got3 | ret.equals(3);
        }
        assertTrue(got1);
        assertTrue(got2);
        assertTrue(got3);
    }
    */
}
