package ai.anneat.models.calculation;

import ai.anneat.Setup;
import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.Pool;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    @Test(expected = IllegalArgumentException.class)
    public void wrongInputLength() {
        Calculator calculator = new Calculator(new Genome(new Pool(5, 5)));
        calculator.calculate(new double[2]);
    }

    @Test
    public void calculator_1() {
        Genome genome = Setup.createGenomeWithOneNodeInBetween();
        for (ConnectionGene connectionGene : genome.getConnections()) {
            connectionGene.setWeight(100);
        }
        double[] input = new double[3];
        for (int i = 0; i < 3; i++) {
            input[i] = 1;
        }
        Calculator calculator = new Calculator(genome);
        assertEquals(1, calculator.calculate(input)[0], 0);

        double[] output = new double[100];
        output[0] = calculator.calculate(input)[0];
        for (int i = 1; i < 100; i++) {
            output[i] = calculator.calculate(input)[0];
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void calculator_2() {
        Genome genome = Setup.createGenomeWithOneNodeInBetween();
        for (ConnectionGene connectionGene : genome.getConnections()) {
            connectionGene.setWeight(0);
        }
        double[] input = new double[3];
        for (int i = 0; i < 3; i++) {
            input[i] = 1;
        }
        Calculator calculator = new Calculator(genome);
        assertEquals(0.5, calculator.calculate(input)[0], 0);
    }

    @Test
    public void calculator_3() {
        Genome genome = Setup.createGenomeWith2In2OutWithEachConnectedWith2ConnectionsAnd1Node();
        for (ConnectionGene connectionGene : genome.getConnections()) {
            connectionGene.setWeight(0);
        }
        double[] input = new double[2];
        for (int i = 0; i < 2; i++) {
            input[i] = 1;
        }
        Calculator calculator = new Calculator(genome);
        assertEquals(calculator.calculate(input)[0], calculator.calculate(input)[0], 0);
        assertEquals(calculator.calculate(input)[1], calculator.calculate(input)[1], 0);
        assertEquals(0.5, calculator.calculate(input)[0], 0);
        assertEquals(0.5, calculator.calculate(input)[1], 0);

        for (int i = 0; i < 1; i++) {
            Calculator newCalculator = new Calculator(genome);
            assertEquals(calculator, newCalculator);
        }
    }

    @Test
    public void calculator_4() {
        Genome genome = Setup.createGenomeWith2In2OutWithEachConnectedWith2ConnectionsAnd1Node();
        assertEquals(4, genome.getConnections().size());
        assertEquals(6 + 1, genome.getNodes().size());
        for (ConnectionGene connectionGene : genome.getConnections()) {
            connectionGene.setWeight(0.3);
        }
        double[] input = new double[2];
        input[0] = 0.1;
        input[1] = 0.3;
        Calculator calculator = new Calculator(genome);
        assertEquals(calculator.calculate(input)[0], calculator.calculate(input)[0], 0);
        assertEquals(calculator.calculate(input)[1], calculator.calculate(input)[1], 0);
        assertTrue(0.5 < calculator.calculate(input)[0]);
        assertTrue(0.5 < calculator.calculate(input)[1]);
        assertNotEquals(calculator.calculate(input)[0], calculator.calculate(input)[1]);
    }

    @Test
    public void calculator_oneConnectionDisabled() {
        Genome genome = Setup.createGenomeWith2In2OutWithEachConnectedWith2ConnectionsAnd1Node();
        for (ConnectionGene connectionGene : genome.getConnections()) {
            connectionGene.setWeight(-0.23);
        }
        genome.getConnections().get(0).setEnabled(false);
        double[] input = new double[2];
        input[0] = 0.1;
        input[1] = 0.3;
        Calculator calculator = new Calculator(genome);
        assertEquals(calculator.calculate(input)[0], calculator.calculate(input)[0], 0);
        assertEquals(calculator.calculate(input)[1], calculator.calculate(input)[1], 0);
        assertTrue(0.5 > calculator.calculate(input)[0]);
        assertNotEquals(0.5, calculator.calculate(input)[1]);
    }
}
