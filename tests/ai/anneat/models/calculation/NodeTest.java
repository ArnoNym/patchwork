package ai.anneat.models.calculation;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class NodeTest {
    @Test
    public void test_0() {
        Node from = new Node(0.2);
        from.setOutput(0);

        Node to = new Node(0.5);

        Connection c1 = new Connection(from, to, 1, true);
        to.add(c1);

        to.calculate();
        assertEquals(0.5, to.getOutput(), 0);

        double[] output = new double[100];
        output[0] = to.getOutput();
        for (int i = 1; i < 100; i++) {
            to.calculate();
            output[i] = to.getOutput();
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void test_1() {
        Node from = new Node(0.2);
        from.setOutput(0.943);

        Node to = new Node(0.5);

        Connection c1 = new Connection(from, to, 0.3, true);
        to.add(c1);

        to.calculate();
        assertTrue(0.5 < to.getOutput());

        double[] output = new double[100];
        output[0] = to.getOutput();
        for (int i = 1; i < 100; i++) {
            to.calculate();
            output[i] = to.getOutput();
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void test_2() {
        Node from = new Node(0.2);
        from.setOutput(0.1);

        Node to = new Node(0.5);

        Connection c1 = new Connection(from, to, 8, true);
        to.add(c1);

        to.calculate();
        assertTrue(0.5 < to.getOutput());

        double[] output = new double[100];
        output[0] = to.getOutput();
        for (int i = 1; i < 100; i++) {
            to.calculate();
            output[i] = to.getOutput();
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void test_3() {
        Node from = new Node(0.2);
        from.setOutput(0.1);

        Node to = new Node(0.5);

        Connection c1 = new Connection(from, to, 0.08, true);
        to.add(c1);

        to.calculate();
        assertTrue(0.5 < to.getOutput());

        double[] output = new double[100];
        output[0] = to.getOutput();
        for (int i = 1; i < 100; i++) {
            to.calculate();
            output[i] = to.getOutput();
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void test_negative_0() {
        Node from = new Node(0.2);
        from.setOutput(0.1);

        Node to = new Node(0.5);

        Connection c1 = new Connection(from, to, -0.08, true);
        to.add(c1);

        to.calculate();
        assertTrue(0.5 > to.getOutput());

        double[] output = new double[100];
        output[0] = to.getOutput();
        for (int i = 1; i < 100; i++) {
            to.calculate();
            output[i] = to.getOutput();
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void test_negative_1() {
        Node from = new Node(0.3);
        from.setOutput(0.1);

        Node to = new Node(0.5);

        Connection c1 = new Connection(from, to, -2238, true);
        to.add(c1);

        to.calculate();
        assertTrue(0.5 > to.getOutput());

        double[] output = new double[100];
        output[0] = to.getOutput();
        for (int i = 1; i < 100; i++) {
            to.calculate();
            output[i] = to.getOutput();
        }
        for (int i = 1; i < 100; i++) {
            assertEquals(output[i-1], output[i], 0);
        }
    }

    @Test
    public void testCompareTo_1() {
        Node n1 = new Node(0.2);
        Node n2 = new Node(0.2);
        assertEquals(n1.compareTo(n2), n2.compareTo(n1));
        assertEquals(0, n1.compareTo(n2));
    }

    @Test
    public void testCompareTo_2() {
        Node n1 = new Node(0.1);
        Node n2 = new Node(0.2);
        assertEquals(n1.compareTo(n2), -n2.compareTo(n1));
        assertTrue(0 > n1.compareTo(n2));
    }

    @Test
    public void testCompareTo_3() {
        Node n1 = new Node(0.32);
        Node n2 = new Node(0.2);
        assertEquals(n1.compareTo(n2), -n2.compareTo(n1));
        assertTrue(0 < n1.compareTo(n2));
    }

    @Test
    public void testCompareTo_4() {
        List<Node> nodes = new ArrayList<>();
        Node n1 = new Node(0.32);
        nodes.add(n1);
        Node n2 = new Node(0.2);
        nodes.add(n2);
        assertFalse(nodes.get(0).getX() < nodes.get(1).getX());
        nodes.sort(Node::compareTo);
        assertTrue(nodes.get(0).getX() < nodes.get(1).getX());
    }

    @Test
    public void testCompareTo_5() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node(0.32));
        nodes.add(new Node(0.2));
        nodes.add(new Node(0.72));
        nodes.add(new Node(0.42));
        nodes.add(new Node(0.22));
        nodes.add(new Node(0.1));
        nodes.add(new Node(0.9));
        nodes.add(new Node(0.2324222));
        nodes.sort(Node::compareTo);
        for (int i = 1; i < nodes.size(); i++) {
            assertTrue(nodes.get(i-1).getX() <= nodes.get(i).getX());
        }
    }
}
