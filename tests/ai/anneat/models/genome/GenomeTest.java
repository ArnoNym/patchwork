package ai.anneat.models.genome;

import ai.anneat.Setup;
import ai.anneat.main.constants.Constants;
import ai.anneat.models.generation.Agent;
import ai.anneat.main.utils.NodeConnectionWrapper;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class GenomeTest {

    @Test
    public void constructor_connections_empty() {
        Pool pool = new Pool(10, 10);
        Set<ConnectionGene> connections = new HashSet<>();
        Genome g1 = new Genome(pool, connections);
        assertEquals(0, g1.getConnections().size());
        assertEquals(21, g1.getNodes().size());
    }

    @Test
    public void testDistance_toSelf_1() {
        Genome g1 = new Genome(Setup.createPool(10, 10, 10));
        assertEquals(0.0, g1.distance(g1), 0);
    }

    @Test
    public void testDistance_toSelf_2() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Setup.createGenomeWithOneNodeInBetween(pool);
        g1.mutateWeights();
        g1.mutateNewConnection();
        g1.mutateConnectionToggle();
        g1.mutateNewNode();
        g1.mutateWeights();
        assertEquals(0d, g1.distance(g1), 0);
    }

    @Test
    public void testDistance_toSelf_3() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Genome.allInputAndOutputNodesConnected(pool);
        assertEquals(0d, g1.distance(g1), 0);
        g1.mutateWeights();
        g1.mutateNewConnection();
        g1.mutateConnectionToggle();
        g1.mutateNewNode();
        g1.mutateWeights();
        assertEquals(0d, g1.distance(g1), 0);
    }

    @Test
    public void testDistance_toOtherEqualGenome_1() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = new Genome(pool);
        Genome g2 = new Genome(pool);
        assertEquals(g1.getNodes().size(), g2.getNodes().size());
        assertEquals(0, g1.getConnections().size());
        assertEquals(0, g2.getConnections().size());
        assertEquals(0, g1.distance(g2), 0);
    }

    @Test
    public void testDistance_toOther_1() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = new Genome(pool);
        Genome g2 = new Genome(pool);
        g2.mutateNewConnection();
        assertEquals(1 * Constants.C2, g1.distance(g2), 0);
    }

    @Test
    public void testDistance_33() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Genome.allInputAndOutputNodes(pool);
        Genome g2 = Genome.allInputAndOutputNodes(pool);

        g2.mutateNewConnection();

        assertEquals(g1.getNodes().size(), g2.getNodes().size());
        assertEquals(0, g1.getConnections().size());
        assertEquals(1, g2.getConnections().size());

        g2.mutateNewNode();

        assertEquals(g1.getNodes().size() + 1, g2.getNodes().size());
        assertEquals(0, g1.getConnections().size());
        assertEquals(3, g2.getConnections().size());
        assertEquals(2, g2.getConnections().stream().filter(ConnectionGene::isEnabled).count());

        assertEquals(3 * pool.getSettings().getEvolutionSettings().getC2() / g2.getConnections().size(), g1.distance(g2), 0);
    }

    @Test
    public void testDistance_mutateNodeOutOfConnection() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = new Genome(pool);
        ConnectionGene con = g1.mutateNewConnection().orElseThrow();
        con.setWeight(1);
        Genome g2 = new Genome(pool);
        g2.addNewConnection(con.getFrom(), con.getTo(), 1d);
        g2.mutateNewNode();
        assertEquals(g1.getNodes().size() + 1, g2.getNodes().size());
        assertEquals(1, g1.getConnections().size());
        assertEquals(3, g2.getConnections().size());
        assertEquals(2, g2.getConnections().stream().filter(ConnectionGene::isEnabled).count());
        assertEquals(g1.distance(g2), g2.distance(g1), 0);
        assertEquals(2 * pool.getSettings().getEvolutionSettings().getC2() / g2.getConnections().size(), g1.distance(g2), 0);
    }

    @Test
    public void testDistance_4() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Setup.createGenomeWithOneNodeInBetween(pool);
        Genome g2 = Setup.createGenomeWithOneNodeInBetween(pool);
        g2.mutateNewNode();
        assertNotEquals(0, g1.distance(g2));
    }

    @Test
    public void testDistance_5() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Setup.createGenomeWithOneNodeInBetween(pool);
        Genome g2 = Setup.createGenomeWithOneNodeInBetween(pool);
        g2.mutateConnectionToggle();
        assertNotEquals(0, g1.distance(g2));
    }

    @Test
    public void testDistance_6() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Setup.createGenomeWithOneNodeInBetween(pool);
        Genome g2 = Setup.createGenomeWithOneNodeInBetween(pool);
        g2.mutateWeights();
        assertNotEquals(0, g1.distance(g2));
    }

    @Test
    public void testDistance_7() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Setup.createGenomeWithOneNodeInBetween(pool);
        Genome g2 = Setup.createGenomeWithOneNodeInBetween(pool);
        g2.mutateWeights();
        assertNotEquals(0, g1.distance(g2));
    }

    @Test
    public void testDistance_8() {
        Pool pool = Setup.createPool(10, 10, 10);

        Genome g1 = Setup.createGenomeWithOneNodeInBetweenAllWeightsOne(pool);

        Genome g2 = Setup.createGenomeWithOneNodeInBetweenAllWeightsOne(pool);
        ConnectionGene c_2_3 = g2.addNewConnection(
                g1.getNodes().get(1),
                g1.getNodes().get(g1.getNodes().size() - 1),
                1d).orElseThrow();
        assertEquals(c_2_3.getId(), g2.getHighestInnovationGeneId());

        Genome g3 = Setup.createGenomeWithOneNodeInBetweenAllWeightsOne(pool);
        g3.getConnections().add(c_2_3);
        ConnectionGene c_3 = g3.addNewConnection(
                g1.getNodes().get(2),
                g1.getNodes().get(g1.getNodes().size() - 3),
                1d).orElseThrow();
        assertEquals(c_3.getId(), g3.getHighestInnovationGeneId());

        assertNotEquals(0, g1.distance(g2));
        assertNotEquals(0, g1.distance(g3));
        assertNotEquals(0, g2.distance(g3));

        assertEquals(g1.distance(g2), g2.distance(g1), 0);
        assertEquals(g1.distance(g3), g3.distance(g1), 0);
        assertEquals(g2.distance(g2), g2.distance(g2), 0);

        double g1g2Distance = g1.distance(g2);
        double g1g3Distance = g1.distance(g3);
        assertTrue(g1g2Distance < g1g3Distance);
    }

    @Test
    public void test_distance_9() {
        Pool pool = new Pool(10, 10);

        List<Agent> agentList_1 = Setup.createClientsWithEqualNodesInBetween(
                pool, 4, true);
        assertEquals(4, agentList_1.size());

        List<Agent> agentList_2 = Setup.createClientsWithEqualNodesInBetween(
                pool, 6, true);
        assertEquals(6, agentList_2.size());

        for (int i = 1; i < agentList_1.size(); i++) {
            Genome g1 = agentList_1.get(i-1).getGenome();
            Genome g2 = agentList_1.get(i).getGenome();
            assertEquals(0, g1.distance(g2), 0);
        }
        for (int i = 1; i < agentList_2.size(); i++) {
            Genome g1 = agentList_2.get(i-1).getGenome();
            Genome g2 = agentList_2.get(i).getGenome();
            assertEquals(0, g1.distance(g2), 0);
        }
        Genome g1 = agentList_1.get(0).getGenome();
        Genome g2 = agentList_2.get(0).getGenome();
        assertTrue(g1.distance(g2) > 0);
    }

    @Test
    public void test_highest_innovationNumber() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = Setup.createGenomeWithOneNodeInBetween(pool);
        g1.mutateNewNode();
        g1.mutateNewNode();
        g1.mutateNewConnection();
        g1.mutateNewNode();
        g1.mutateNewConnection();
        g1.mutateNewConnection();
        g1.mutateNewConnection();
        int maxId = -Integer.MAX_VALUE;
        for (ConnectionGene connectionGene : g1.getConnections()) {
            if (maxId < connectionGene.getId()) {
                maxId = connectionGene.getId();
            }
        }
        assertEquals(maxId, g1.getHighestInnovationGeneId());
    }

    @Test
    public void mutateNewConnection_createNewNode() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = new Genome(pool);

        assertEquals(21, g1.getNodes().size());
        assertEquals(0, g1.getConnections().size());

        g1.mutateNewConnection();
        ConnectionGene con = g1.getConnection(0);

        assertEquals(21, g1.getNodes().size());
        assertEquals(1, g1.getConnections().size());

        g1.addNewNodeWithCons(con);

        assertEquals(22, g1.getNodes().size());
        assertEquals(3, g1.getConnections().size());
        assertEquals(2, g1.getConnections().stream().filter(ConnectionGene::isEnabled).count());
    }

    @Test
    public void mutateNewConnection_mutateNewNode() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = new Genome(pool);

        assertEquals(21, g1.getNodes().size());
        assertEquals(0, g1.getConnections().size());

        g1.mutateNewConnection();
        ConnectionGene oldCon = g1.getConnection(0);
        oldCon.setWeight(0.2);
        oldCon.setEnabled(true);

        assertEquals(21, g1.getNodes().size());
        assertEquals(1, g1.getConnections().size());

        NodeConnectionWrapper wrapper = g1.mutateNewNode().orElseThrow();

        assertEquals(22, g1.getNodes().size());
        assertEquals(3, g1.getConnections().size());
        assertEquals(2, g1.getConnections().stream().filter(ConnectionGene::isEnabled).count());

        ConnectionGene leftCon = wrapper.getLeftCon();
        ConnectionGene rightCon = wrapper.getRightCon();

        assertEquals(1, leftCon.getWeight(), 0);
        assertTrue(leftCon.isEnabled());

        assertEquals(oldCon.getWeight(), rightCon.getWeight(), 0);
        assertFalse(oldCon.isEnabled());
        assertTrue(rightCon.isEnabled());
    }

    @Test
    public void mutateNewConnection_cantMutateNewNodeOfDisabledConnection() {
        Pool pool = Setup.createPool(10, 10, 10);
        Genome g1 = new Genome(pool);
        g1.mutateNewConnection();
        ConnectionGene oldCon = g1.getConnection(0);
        oldCon.setWeight(0.2);
        oldCon.setEnabled(false);
        assertTrue(g1.mutateNewNode().isEmpty());
    }

    @Test
    public void crossOver_bothHaveGene() {
        int clientCount = 10;

        Pool pool = Setup.createPool(10, 10, clientCount);

        Genome g1 = new Genome(pool);
        g1.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);
        Genome g2 = new Genome(pool);
        g2.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);

        assertEquals(g1.getConnections().size(), g2.getConnections().size());
        assertEquals(g1.getConnections().get(0), g2.getConnections().get(0));

        Genome g = Genome.crossOver(g1, g2);

        assertEquals(g1.getConnections().size(), g.getConnections().size());
        assertEquals(g2.getConnections().size(), g.getConnections().size());
        assertEquals(g.getConnections().get(0), g1.getConnections().get(0));
        assertEquals(g.getConnections().get(0), g2.getConnections().get(0));
        assertEquals(g1.getNodes().size(), g.getNodes().size());
        assertEquals(g2.getNodes().size(), g.getNodes().size());
        assertEquals(g1.complexity(), g.complexity());
        assertEquals(g2.complexity(), g.complexity());
    }

    @Test
    public void crossOver_g1HasGene() {
        int clientCount = 10;

        Pool pool = Setup.createPool(10, 10, clientCount);

        Genome g1 = new Genome(pool);
        g1.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);
        Genome g2 = new Genome(pool);

        Genome g = Genome.crossOver(g1, g2);

        assertEquals(g1.getConnections().size(), g.getConnections().size());
        assertEquals(g1.getNodes().size(), g.getNodes().size());
        assertEquals(g2.getNodes().size(), g.getNodes().size());
        assertEquals(g1.complexity(), g.complexity());
    }

    @Test
    public void crossOver_g2HasGene() {
        int clientCount = 10;

        Pool pool = Setup.createPool(10, 10, clientCount);

        Genome g1 = new Genome(pool);
        Genome g2 = new Genome(pool);
        g2.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);

        Genome g = Genome.crossOver(g1, g2);

        assertTrue(g.getConnections().isEmpty());
        assertEquals(g1.getConnections().size(), g.getConnections().size());
        assertEquals(g1.getNodes().size(), g.getNodes().size());
        assertEquals(g2.getNodes().size(), g.getNodes().size());
        assertEquals(g1.complexity(), g.complexity());
    }

    @Test
    public void crossOver_bothHaveGeneWithNode() {
        for (int i = 0; i < 10; i++) {
            int clientCount = 10;

            Pool pool = Setup.createPool(10, 10, clientCount);

            Genome g1 = new Genome(pool);
            g1.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);
            g1.addNewNodeWithCons(g1.getConnection(0)).orElseThrow();
            assertFalse(g1.getConnection(0).isEnabled());
            assertTrue(g1.getConnection(1).isEnabled());
            assertTrue(g1.getConnection(2).isEnabled());
            Genome g2 = new Genome(pool);
            g2.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);
            g2.addNewNodeWithCons(g2.getConnection(0)).orElseThrow();
            assertFalse(g2.getConnection(0).isEnabled());
            assertTrue(g2.getConnection(1).isEnabled());
            assertTrue(g2.getConnection(2).isEnabled());

            assertEquals(g1.getConnection(0), g2.getConnection(0));
            assertEquals(g1.getConnection(1), g2.getConnection(1));
            assertEquals(g1.getConnection(2), g2.getConnection(2));

            Genome g = Genome.crossOver(g1, g2);

            assertEquals(g1.getConnections().size(), g.getConnections().size());
            assertEquals(g2.getConnections().size(), g.getConnections().size());
            assertEquals(g1.getNodes().size(), g.getNodes().size());
            assertEquals(g2.getNodes().size(), g.getNodes().size());
            System.out.println("g1.complexity()="+g1.complexity()+"g.complexity()="+g.complexity());
            assertEquals(g1.complexity(), g.complexity());
            assertEquals(g2.complexity(), g.complexity());
        }
    }

    @Test
    public void crossOver_g1HasGeneWithNode() {
        int clientCount = 10;

        Pool pool = Setup.createPool(10, 10, clientCount);

        Genome g1 = new Genome(pool);
        g1.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);
        g1.addNewNodeWithCons(g1.getConnection(0));
        Genome g2 = new Genome(pool);

        Genome g = Genome.crossOver(g1, g2);

        assertEquals(g1.getConnections().size(), g.getConnections().size());
        assertNotEquals(g2.getConnections().size(), g.getConnections().size());
        assertEquals(g1.getNodes().size(), g.getNodes().size());
        assertNotEquals(g2.getNodes().size(), g.getNodes().size());
        assertEquals(g1.complexity(), g.complexity());
        assertNotEquals(g2.complexity(), g.complexity());
    }

    @Test
    public void crossOver_g2HasGeneWithNode() {
        int clientCount = 10;

        Pool pool = Setup.createPool(10, 10, clientCount);

        Genome g1 = new Genome(pool);
        Genome g2 = new Genome(pool);
        g2.addNewConnection(pool.getInputNodes().get(0), pool.getOutputNodes().get(0), 1d);
        g2.addNewNodeWithCons(g2.getConnection(0));

        Genome g = Genome.crossOver(g1, g2);

        assertEquals(g1.getConnections().size(), g.getConnections().size());
        assertNotEquals(g2.getConnections().size(), g.getConnections().size());
        assertEquals(g1.getNodes().size(), g.getNodes().size());
        assertNotEquals(g2.getNodes().size(), g.getNodes().size());
        assertEquals(g1.complexity(), g.complexity());
        assertNotEquals(g2.complexity(), g.complexity());
    }

    @Test
    public void createAllInputNodeConnections() {
        int clientCount = 10;
        Pool pool = Setup.createPool(10, 10, clientCount);
        Genome g1 = new Genome(pool);
        assertEquals(0, g1.getConnections().size());
        g1.createAlInputToOutputNodesConnections();
        assertEquals(10 * 10, g1.getConnections().size());
    }

    @Test
    public void createAllInputNodeConnections_whenFirstAddingAnotherConnection() {
        int clientCount = 10;
        Pool pool = Setup.createPool(10, 10, clientCount);
        Genome g1 = new Genome(pool);
        g1.addNewConnection(pool.getInputNodes().get(2), pool.getOutputNodes().get(2), 1d);
        assertEquals(1, g1.getConnections().size());
        g1.createAlInputToOutputNodesConnections();
        assertEquals(10 * 10, g1.getConnections().size());
    }

    @Test
    public void redistributeClientsInSpecies() {
        Pool pool = Setup.createPool(10, 10, 10);

        List<Agent> agentList_1 = Setup.createClientsWithEqualNodesInBetween(pool, 4, false);
        assertEquals(4, agentList_1.size());

        List<Agent> agentList_2 = Setup.createClientsWithEqualNodesInBetween_2(pool, 6, false);
        assertEquals(6, agentList_2.size());

        assertNotEquals(0, agentList_1.get(0).distance(agentList_2.get(0)));
        System.out.println(agentList_1.get(0).distance(agentList_2.get(0)));
    }

    @Test
    public void mutateNewConnection() {
        Pool pool = new Pool(1, 1);

        Genome g1 = new Genome(pool);
        ConnectionGene g1_c1 = g1.mutateNewConnection().orElseThrow();
        g1_c1.setWeight(1d);

        Genome g2 = new Genome(pool);
        ConnectionGene g2_c1 = g2.addNewConnection(g1_c1.getFrom(), g1_c1.getTo(), 1d).orElseThrow();
        NodeGene n1 = new NodeGene(pool.nextNodeId(), 0.1, 0.9);
        NodeGene n2 = new NodeGene(pool.nextNodeId(), 0.9, 0.9);
        ConnectionGene g2_c2 = g2.addNewConnection(n1, n2, 1d).orElseThrow();
        NodeGene n3 = new NodeGene(pool.nextNodeId(), 0.1, 0.9);
        NodeGene n4 = new NodeGene(pool.nextNodeId(), 0.9, 0.9);
        ConnectionGene g2_c3 = g2.addNewConnection(n3, n4, 1d).orElseThrow();

        ConnectionGene g1_c3 = g1.addNewConnection(g2_c3.getFrom(), g2_c3.getTo(), 1d).orElseThrow();

        assertEquals(g1.distance(g2), g2.distance(g1), 0);
    }
}
