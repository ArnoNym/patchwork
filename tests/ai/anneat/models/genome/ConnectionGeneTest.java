package ai.anneat.models.genome;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ConnectionGeneTest {
    @Test
    public void testDefaultConstructor() {
        int id = 1;
        NodeGene from = new NodeGene(2, 0.1, 0.5);
        NodeGene to = new NodeGene(3, 0.9, 0.5);
        ConnectionGene con = new ConnectionGene(id, from, to);
        assertEquals(id, con.getId());
        assertEquals(from, con.getFrom());
        assertEquals(to, con.getTo());
    }

    @Test
    public void tesCopyConstructor() {
        int id = 1;
        NodeGene from = new NodeGene(2, 0.1, 0.5);
        NodeGene to = new NodeGene(3, 0.9, 0.5);
        ConnectionGene copyCon = new ConnectionGene(id, from, to);
        copyCon.setWeight(0.22323232);
        copyCon.setEnabled(false);

        ConnectionGene newCon = copyCon.copy();
        assertEquals(copyCon.getId(), newCon.getId());
        assertEquals(copyCon.getFrom(), newCon.getFrom());
        assertEquals(copyCon.getTo(), newCon.getTo());
        assertEquals(copyCon.getWeight(), newCon.getWeight(), 0);
        assertEquals(copyCon.isEnabled(), newCon.isEnabled());
    }

    @Test
    public void testToString() {
        int id = 1;
        NodeGene from = new NodeGene(2, 0.1, 0.5);
        NodeGene to = new NodeGene(3, 0.9, 0.5);
        ConnectionGene con = new ConnectionGene(id, from, to);
        assertNotNull(con.toString());
    }
}
