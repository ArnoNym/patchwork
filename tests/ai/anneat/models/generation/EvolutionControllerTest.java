package ai.anneat.models.generation;

import ai.anneat.Setup;
import ai.anneat.controller.EvolutionController;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.Pool;
import ai.anneat.publish.settings.Settings;
import ai.anneat.main.utils.collections.OrderedSet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EvolutionControllerTest {
    private Settings settings = new Settings();

    /*
    @Test
    public void shield() {
        List<Agent> agentList = Setup.createClients(10);
        List<Species> speciesList = new ArrayList<>();

        agentList.get(0).setScore(10);
        agentList.get(1).setScore(10);
        agentList.get(2).setScore(10);
        agentList.get(3).setScore(10);
        agentList.get(4).setScore(10);
        agentList.get(5).setScore(0);
        agentList.get(6).setScore(20);
        agentList.get(7).setScore(15);
        agentList.get(8).setScore(1);
        agentList.get(9).setScore(3);

        List<Agent> cl = new ArrayList<>();
        cl.add(agentList.get(0));
        cl.add(agentList.get(1));
        cl.add(agentList.get(2));
        cl.add(agentList.get(3));
        cl.add(agentList.get(4));
        cl.add(agentList.get(5));
        Species s1 = new Species(cl);
        s1.calculateScore();
        speciesList.add(s1);

        cl.clear();
        cl.add(agentList.get(6));
        Species s2 = new Species(cl);
        s2.calculateScore();
        speciesList.add(s2);

        cl.clear();
        cl.add(agentList.get(7));
        cl.add(agentList.get(8));
        Species s4 = new Species(cl);
        s4.calculateScore();
        speciesList.add(s4);

        cl.clear();
        cl.add(agentList.get(9));
        Species s6 = new Species(cl);
        s6.calculateScore();
        speciesList.add(s6);

        assertEquals(6, s1.size());
        assertEquals(1, s2.size());
        assertEquals(2, s4.size());
        assertEquals(1, s6.size());

        assertEquals(10, s1.getScore(), 0);
        assertEquals(20, s2.getScore(), 0);
        assertEquals(15, s4.getScore(), 0);
        assertEquals(3, s6.getScore(), 0);

        assertEquals(10, s1.getAllTimeAgentMaxScore(), 0);
        assertEquals(20, s2.getAllTimeAgentMaxScore(), 0);
        assertEquals(15, s4.getAllTimeAgentMaxScore(), 0);
        assertEquals(3, s6.getAllTimeAgentMaxScore(), 0);

        Evolution.shieldBestLowPopSpecies(speciesList);

        assertFalse(s1.isShielded());
        assertTrue(s2.isShielded());
        assertFalse(s4.isShielded());
        assertFalse(s6.isShielded());
    }
    */

    // kill ============================================================================================================

    @Test
    public void kill_emptySpeciesList() {
        EvolutionController.kill(new HashSet<>());
    }

    @Test
    public void kill_speciesWith2Clients() {
        int clients = 2;

        double survivorPercent = 0.000000001;

        Species species = Setup.createSpecies(clients);
        species.getAgents().get(0).getGenome().getPool().getSettings().getEvolutionSettings().setSurvivorsPercent(survivorPercent);
        Set<Species> speciesSet = new HashSet<>();
        speciesSet.add(species);

        EvolutionController.kill(speciesSet);

        assertEquals(1, species.size());
    }

    @Test
    public void kill_speciesWith5Clients() {
        int clients = 5;

        double survivorPercent = 0.8;
        settings.getEvolutionSettings().setSurvivorsPercent(survivorPercent);

        Species species = Setup.createSpecies(clients);
        assertEquals(clients, species.size());
        Set<Species> speciesSet = new HashSet<>();
        speciesSet.add(species);

        EvolutionController.kill(speciesSet);

        assertEquals(clients * survivorPercent, species.size(), 0);
    }

    @Test
    public void kill_speciesWith100Clients() {
        int clients = 100;

        double survivorPercent = 0.8;
        settings.getEvolutionSettings().setSurvivorsPercent(survivorPercent);

        Species species = Setup.createSpecies(clients);
        Set<Species> speciesSet = new HashSet<>();
        speciesSet.add(species);

        EvolutionController.kill(speciesSet);


        assertEquals(clients * survivorPercent, species.size(), 0);
    }

    @Test
    public void kill_speciesWith100Clients_mitKillableAufFalse() {
        int clients = 100;

        double survivorPercent = 0.8;
        settings.getEvolutionSettings().setSurvivorsPercent(survivorPercent);

        Species species = Setup.createSpecies(clients);

        for (int i = 0; i < 10; i++) {
            species.getAgents().get(i).setAllowKill(false);
        }

        Set<Species> speciesSet = new HashSet<>();
        speciesSet.add(species);

        EvolutionController.kill(speciesSet);


        assertEquals(clients * survivorPercent, species.size(), 0);
    }

    // removeExtinctSpecies ============================================================================================

    @Test
    public void removeExtinctSpecies_100SpeciesListWith1Representative() {
        int speciesSize = 100;

        OrderedSet<Species> speciesList = Setup.create100SpeciesOnlyWithRepresentative(speciesSize);
        assertEquals(speciesSize, speciesList.size());
        for (Species species : speciesList) {
            assertEquals(1, species.size());
            for (Agent agent : species) {
                agent.setAllowKill(true);
            }
        }

        EvolutionController.kill(speciesList);
        assertEquals(100, speciesList.size());
        assertEquals(0, speciesList.get(0).size());

        EvolutionController.removeExtinctSpecies(speciesList);
        assertEquals(speciesSize, speciesList.size());
        assertEquals(0, speciesList.size());
    }

    @Test
    public void dontRemoveExtinctSpecies_1SpeciesWith2Representative() {
        int clientCount = 2;

        OrderedSet<Species> speciesList = Setup.createListWith1SpeciesWithClients(clientCount);

        EvolutionController.removeExtinctSpecies(speciesList);

        assertFalse(speciesList.isEmpty());
    }

    @Test
    public void removeExtinctSpecies_1SpeciesWith0Representative() {
        OrderedSet<Species> speciesList = new OrderedSet<>();

        speciesList.add(new Species(new Agent(new Genome(new Pool(1, 1)))));

        EvolutionController.removeExtinctSpecies(speciesList);

        assertFalse(speciesList.isEmpty());
    }

    // redistributeClientsInSpecies ====================================================================================

    @Test
    public void redistributeClientsInSpecies() {
        Settings settings = new Settings();
        settings.getEvolutionSettings().setC1(1);
        settings.getEvolutionSettings().setC2(1);
        settings.getEvolutionSettings().setC3(1);
        settings.getEvolutionSettings().setCp(0.1);

        Pool pool = new Pool(10, 10, settings);

        List<Agent> agentList_1 = Setup.createClientsWithEqualNodesInBetween(pool, 4, false);
        assertEquals(4, agentList_1.size());

        List<Agent> agentList_2 = Setup.createClientsWithEqualNodesInBetween_2(pool, 6, false);
        assertEquals(6, agentList_2.size());

        List<Agent> agentList_all = new ArrayList<>(agentList_1);
        agentList_all.addAll(agentList_2);

        Species species = new Species(agentList_all);
        OrderedSet<Species> speciesList = new OrderedSet<>();
        speciesList.add(species);

        EvolutionController.redistributeAgentsInSpecies(speciesList);
        System.out.println(agentList_1.get(0).distance(agentList_2.get(0)));

        assertEquals(2, speciesList.size());

        Species s1 = speciesList.get(0);
        Species s2 = speciesList.get(1);

        assertEquals(4, Math.min(s1.size(), s2.size()));
        assertEquals(6, Math.max(s1.size(), s2.size()));
        assertEquals(10, s1.size() + s2.size());
    }
}