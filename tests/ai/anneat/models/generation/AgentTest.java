package ai.anneat.models.generation;

import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.Pool;
import org.junit.Test;

import static org.junit.Assert.*;

public class AgentTest {
    @Test
    public void constructor() {
        Pool pool = new Pool(5, 5);
        Agent agent = new Agent(new Genome(pool));

        int beforeComplexity = agent.complexity();
        assertEquals(0, beforeComplexity);

        double[] input = {2, 3, 2, 5, 5};

        double[] output = agent.calculate(input);

        agent.getGenome().mutateNewConnection();
        agent.getGenome().mutateNewConnection();
        agent.getGenome().mutateNewNode();
        agent.getGenome().mutateNewConnection();
        agent.getGenome().mutateNewNode();

        assertNotEquals(output, agent.calculate(input));
        assertTrue(beforeComplexity < agent.complexity());
    }

    @Test
    public void compareTo() {
        Pool pool = new Pool(3, 3);
        Agent agent1 = new Agent(new Genome(pool));
        agent1.setVanillaScore(1);
        Agent agent2 = new Agent(new Genome(pool));
        agent1.setVanillaScore(3);
        assertEquals(1, agent1.compareTo(agent2));
    }
}
