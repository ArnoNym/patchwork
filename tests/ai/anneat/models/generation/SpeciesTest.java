package ai.anneat.models.generation;

import ai.anneat.Setup;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.Pool;
import org.junit.Before;
import org.junit.Test;
import java.util.List;

import static org.junit.Assert.*;

public class SpeciesTest {
    private Pool pool;
    private Genome genome;
    private Agent agent;
    private Species species;

    @Before
    public void setup() {
        pool = new Pool(10, 10);
        genome = new Genome(pool);
        agent = new Agent(genome);
        species = new Species(agent);
    }

    @Test
    public void representativeConstructor() {
        assertEquals(1, species.size());
        assertTrue(species.getAgents().contains(agent));

        agent.setScore(0.5);
        species.calculateScore(false);
        assertEquals(0.5, species.getScore(), 0);

        species.reduceToRepresentative();
        assertEquals(1, species.size());
        assertTrue(species.getAgents().contains(agent));
    }

    @Test
    public void scoreDidntIncreaseCounter() {
        assertEquals(0, species.getScoreDidntIncreaseCounter());

        agent.setScore(0.5);

        pool.getSettings().getEvolutionSettings().setAllowGenerationsWithoutIncreaseInValue(5);

        for (int i = 0; i < 6; i++) {
            species.calculateScore(false);
            assertEquals(0.5, species.getScore(), 0);
            assertEquals(i, species.getScoreDidntIncreaseCounter());
            assertTrue(species.isAllowedToBreed());
        }
        species.calculateScore(false);
        assertEquals(0.5, species.getScore(), 0);
        assertEquals(6, species.getScoreDidntIncreaseCounter());
        assertFalse(species.isAllowedToBreed());
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_sameTwice() {
        Pool pool = new Pool(10, 10);
        Agent agent = new Agent(Setup.createGenomeWithOneNodeInBetween());
        (new Species(agent)).add(agent);
    }

    @Test
    public void add_veryEqualAgents() {
        Pool pool = new Pool(10, 10);
        List<Agent> agentList = Setup.createClientsWithEqualNodesInBetween(pool, 3, false);
        Species species = new Species(agentList.get(0));
        assertEquals(1, species.size());
        assertTrue(species.add(agentList.get(1)));
        assertEquals(2, species.size());
        assertTrue(species.add(agentList.get(2)));
        assertEquals(3, species.size());
    }

    @Test
    public void add_thenForceAdd_veryUnequalAgents() {
        int inputCount = 2;
        int outputCount = 2;

        Pool pool = new Pool(inputCount, outputCount);
        pool.getSettings().getPerformanceSettings().setMaxAgentCount(3);
        pool.getSettings().getEvolutionSettings().setC1(1);
        pool.getSettings().getEvolutionSettings().setC2(1);
        pool.getSettings().getEvolutionSettings().setC3(1);
        pool.getSettings().getEvolutionSettings().setCp(1);

        // a0
        Agent a0 = new Agent(Genome.allInputAndOutputNodesConnected(pool));
        Species species = new Species(a0);
        assertEquals(a0, species.getRepresentative());

        // a1
        Agent a1 = new Agent(Genome.allInputAndOutputNodesConnected(pool));
        assertEquals(inputCount * outputCount, a1.getGenome().getConnections().size());
        a1.getGenome().mutateNewNode().orElseThrow();
        assertEquals(inputCount * outputCount + 2, a1.getGenome().getConnections().size());
        System.out.println("Distance a0 to a1 = "+a0.distance(a1));

        assertFalse(species.add(a1));
        assertEquals(1, species.size());
        species.forceAdd(a1);
        assertEquals(2, species.size());

        // a2
        Agent a2 = new Agent(Genome.allInputAndOutputNodesConnected(pool));
        a2.getGenome().mutateNewNode().orElseThrow();
        assertEquals(inputCount * outputCount + 2, a2.getGenome().getConnections().size());
        a2.getGenome().mutateNewConnection().orElseThrow();
        assertEquals(inputCount * outputCount + 2 + 1, a2.getGenome().getConnections().size());
        System.out.println("Distance a0 to a2 = "+a0.distance(a2));

        assertFalse(species.add(a2));
        assertEquals(2, species.size());
        species.forceAdd(a2);
        assertEquals(3, species.size());

        assertEquals(a0.equals(a2), a2.equals(a0));
        assertEquals(a0.equals(a1), a1.equals(a0));
        assertEquals(a1.equals(a2), a2.equals(a1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_thenForceAdd_veryUnequalAgents_2() {
        Pool pool = new Pool(2, 2);
        pool.getSettings().getPerformanceSettings().setMaxAgentCount(3);

        Agent a0 = new Agent(new Genome(pool));
        a0.getGenome().createAlInputToOutputNodesConnections();
        Agent a1 = new Agent(new Genome(pool));
        a1.getGenome().createAlInputToOutputNodesConnections();

        Species species = new Species(a0);

        assertTrue(species.add(a1));
        species.forceAdd(a1);
    }
}
