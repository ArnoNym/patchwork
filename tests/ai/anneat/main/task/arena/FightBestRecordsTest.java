package ai.anneat.main.task.arena;

import ai.anneat.Setup;
import ai.anneat.models.calculation.Calculator;
import ai.anneat.models.genome.Pool;
import ai.anneat.main.utils.arena.FightRecords;
import ai.anneat.main.utils.arena.Gladiator;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FightBestRecordsTest {
    private List<Gladiator> gladiatorList;
    private int gladiatorCount;

    @Before
    public void setup() {
        gladiatorCount = 20;
        Pool pool = new Pool(3, 3);
        gladiatorList = new ArrayList<>();
        for (int i = 0; i < gladiatorCount; i++) {
            gladiatorList.add(new Gladiator(new Calculator(Setup.createGenomeWithOneNodeInBetween(pool))));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_addSameGladiatorRecord() {
        FightRecords fightRecords = new FightRecords();
        Gladiator g = gladiatorList.get(0);
        assertEquals(-1, fightRecords.getWinner(g.getCalculator(), g.getCalculator()));
    }

    @Test
    public void test_addTwoGladiatorRecord() {
        FightRecords fightRecords = new FightRecords();
        Gladiator g1 = gladiatorList.get(0);
        Gladiator g2 = gladiatorList.get(1);
        assertNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertEquals(-1, fightRecords.getWinner(g1.getCalculator(), g2.getCalculator()));
        fightRecords.addFightRecord(g1, g2);
        assertNotNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertEquals(0, fightRecords.getWinner(g1.getCalculator(), g2.getCalculator()));
        g1.setScore(1);
        g2.setScore(0);
        fightRecords.addFightRecord(g1, g2);
        assertNotNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertEquals(1, fightRecords.getWinner(g1.getCalculator(), g2.getCalculator()));
        fightRecords.removeUnusedRecords();
        assertNotNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertEquals(1, fightRecords.getWinner(g1.getCalculator(), g2.getCalculator()));
        fightRecords.removeUnusedRecords();
        fightRecords.removeUnusedRecords();
        assertNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertEquals(-1, fightRecords.getWinner(g1.getCalculator(), g2.getCalculator()));
    }

    @Test
    public void test_useOneOutOfTwoRecordsRecord() {
        FightRecords fightRecords = new FightRecords();
        Gladiator g1 = gladiatorList.get(0);
        Gladiator g2 = gladiatorList.get(1);
        Gladiator g3 = gladiatorList.get(2);
        Gladiator g4 = gladiatorList.get(3);
        fightRecords.addFightRecord(g1, g2);
        fightRecords.addFightRecord(g3, g4);
        fightRecords.removeUnusedRecords();
        assertNotNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertNotNull(fightRecords.getFightRecord(g3.getCalculator(), g4.getCalculator()));
        fightRecords.removeUnusedRecords();
        assertNotNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertNotNull(fightRecords.getFightRecord(g3.getCalculator(), g4.getCalculator()));
        fightRecords.removeUnusedRecords();
        fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator());
        fightRecords.removeUnusedRecords();
        assertNotNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertNull(fightRecords.getFightRecord(g3.getCalculator(), g4.getCalculator()));
        fightRecords.removeUnusedRecords();
        fightRecords.removeUnusedRecords();
        assertNull(fightRecords.getFightRecord(g1.getCalculator(), g2.getCalculator()));
        assertNull(fightRecords.getFightRecord(g3.getCalculator(), g4.getCalculator()));
    }
}
