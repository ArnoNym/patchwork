package ai;

import ai.training.utils.MatrixUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class MatrixUtilsTest {
    boolean[][] matrix;
    boolean[] row0;
    boolean[] row1;
    boolean[] row2;

    @Before
    public void setup() {
        matrix = new boolean[3][3];
        row0 = matrix[0];
        row1 = matrix[1];
        row2 = matrix[2];
    }

    public void setMatrixToTrue() {
        for (boolean[] booleans : matrix) {
            Arrays.fill(booleans, true);
        }
    }

    @Test
    public void testCalcMass_1() {
        assertEquals(0, MatrixUtils.calcMass(matrix));
        row0[0] = true;
        assertEquals(1, MatrixUtils.calcMass(matrix));
        row0[1] = true;
        row0[2] = true;
        row1[1] = true;
        row1[2] = true;
        row2[0] = true;
        row2[2] = true;
        assertEquals(7, MatrixUtils.calcMass(matrix));
    }

    @Test
    public void testOtherCalcMass_1() {
        assertEquals(0, MatrixUtils.calcMass(matrix));
        row0[0] = true;
        assertEquals(1, MatrixUtils.calcMass(matrix));
        row0[1] = true;
        row0[2] = true;
        row1[1] = true;
        row1[2] = true;
        row2[0] = true;
        row2[2] = true;
        assertEquals(7, MatrixUtils.calcMass(matrix, 0, 0, 3, 3));
    }

    @Test
    public void testCalcEdges_1() {
        int borderEdgesCount = 3+3+3+3;
        assertEquals(borderEdgesCount, MatrixUtils.calcEdges(matrix));
        row0[0] = true;
        assertEquals(borderEdgesCount, MatrixUtils.calcEdges(matrix));
        row1[1] = true;
        assertEquals(borderEdgesCount + 4, MatrixUtils.calcEdges(matrix));
        row1[2] = true;
        assertEquals(borderEdgesCount + 4, MatrixUtils.calcEdges(matrix));
    }

    @Test
    public void testCalcFullComponents_1() {
        assertEquals(0, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
        row0[0] = true;
        assertEquals(1, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
        row0[1] = true;
        assertEquals(1, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
        row2[1] = true;
        assertEquals(2, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
        row1[1] = true;
        assertEquals(1, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
    }

    @Test
    public void testCalcFullComponents_2() {
        row0[0] = true;
        row0[1] = true;
        row0[2] = true;
        assertEquals(1, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
        row2[0] = true;
        row2[1] = true;
        row2[2] = true;
        assertEquals(2, MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)));
    }

    @Test
    public void testCalcEmptyComponents_1() {
        setMatrixToTrue();
        assertEquals(0, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
        row0[0] = false;
        assertEquals(1, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
        row0[1] = false;
        assertEquals(1, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
        row2[1] = false;
        assertEquals(2, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
        row1[1] = false;
        assertEquals(1, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
    }

    @Test
    public void testCalcEmptyComponents_2() {
        setMatrixToTrue();
        row0[0] = false;
        row0[1] = false;
        row0[2] = false;
        assertEquals(1, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
        row2[0] = false;
        row2[1] = false;
        row2[2] = false;
        assertEquals(2, MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)));
    }

    @Test
    public void testCalcCloseTo7Times7() {
        matrix = new boolean[9][9];
        assertEquals(0, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[0][0] = true;
        assertEquals(1, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[0][8] = true;
        assertEquals(1, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[8][0] = true;
        assertEquals(1, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[8][8] = true;
        assertEquals(1, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[0][1] = true;
        matrix[0][2] = true;
        assertEquals(3, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[8][7] = true;
        matrix[8][6] = true;
        matrix[8][5] = true;
        matrix[7][7] = true;
        matrix[7][6] = true;
        assertEquals(6, MatrixUtils.calcCloseTo7Times7(matrix));
    }

    @Test
    public void testCalcCloseTo7Times7_2() {
        matrix = new boolean[9][9];
        assertEquals(0, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[0][0] = true;
        assertEquals(1, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[6][6] = true;
        assertEquals(2, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[5][6] = true;
        assertEquals(3, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[8][8] = true;
        assertEquals(3, MatrixUtils.calcCloseTo7Times7(matrix));
        matrix[7][8] = true;
        assertEquals(4, MatrixUtils.calcCloseTo7Times7(matrix));
    }
}
