package ai;

import ai.agents.PatchworkAgent;
import ai.hardcoded.benchmarks.BuyFirstAgent;
import ai.training.ShowMatch;
import ai.training.utils.TrainingUtils;
import ai.training.trainings.TurnBenchmarkTraining;
import controller.GameController;
import controller.IOController;
import model.Difficulty;
import model.Patch;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TurnBenchmarkTrainingTest {
    /*
    @Test
    public void getScoreDeterministicTest() {
        List<Patch> patches = (new IOController(null)).importCSV();

        PatchworkAgent agent = new NeatPatchworkAgent(new NeatAgent(doubles -> doubles));
        double benchmarkScore = TurnTraining.getScore(agent, patches);
        for (int i = 0; i < 10; i++) {
            double score = TurnTraining.getScore(agent, patches);
            assertEquals(benchmarkScore, score, 0.0);
        }
    }
    */

    @Test
    public void testDeterministic_1() {
        AiConstants.USE_DECISION_TREE = false;

        /*
        PatchworkAgent patchworkAgent = new StupidAgent();
        */
        PatchworkAgent patchworkAgent = (new IOController(null))
                .loadAgentMapFromPath("resources/"+AiConstants.AGENT_MAP+".ser")
                .orElseThrow()
                .get(Difficulty.HARDCORE);

        List<Patch> patches = TrainingUtils.resortPatches_2(new IOController(null).importCSV());

        GameController gameController = new GameController(
                patchworkAgent,
                patchworkAgent,
                new ArrayList<>(patches));
        ShowMatch.play(gameController, false, null);
        double score = TurnBenchmarkTraining.calcScore(gameController, AiConstants.TRAINING_PLAYER_1_NAME, 5).getV2();

        for (int i = 0; i < 10; i++) {
            gameController = new GameController(
                    patchworkAgent,
                    patchworkAgent,
                    new ArrayList<>(patches));

            ShowMatch.play(gameController, false, null);

            double newScore = TurnBenchmarkTraining.calcScore(gameController, AiConstants.TRAINING_PLAYER_1_NAME, 5).getV2();

            assertEquals(score, newScore, 0.0);
        }
    }

    @Test
    public void testDeterministic_2() {
        AiConstants.USE_DECISION_TREE = false;

        List<Patch> patches = TrainingUtils.resortPatches_2((new IOController(null).importCSV()));

        GameController gameController = new GameController(
                new BuyFirstAgent(),
                new BuyFirstAgent(),
                new ArrayList<>(patches));
        ShowMatch.play(gameController, false, null);
        double score = TurnBenchmarkTraining.calcScore(gameController, AiConstants.TRAINING_PLAYER_1_NAME, 5).getV2();

        for (int i = 0; i < 10; i++) {
            gameController = new GameController(
                    new BuyFirstAgent(),
                    new BuyFirstAgent(),
                    new ArrayList<>(patches));

            ShowMatch.play(gameController, false, null);
            assertEquals(score, TurnBenchmarkTraining.calcScore(gameController, AiConstants.TRAINING_PLAYER_1_NAME, 5).getV2(), 0.0);
        }
    }
}
