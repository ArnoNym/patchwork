package ai.internals;

import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.NodeGene;
import ai.anneat.models.genome.Pool;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataTest {
    @Test
    public void getConnection_notPresent() {
        Pool pool = new Pool(10, 10);
        assertEquals(10, pool.getInputNodes().size());
        assertEquals(10, pool.getOutputNodes().size());
        assertTrue(pool.getConnectionBetween(pool.getInputNodes().get(0), pool.getOutputNodes().get(0)).isEmpty());
        assertEquals(10, pool.getInputNodes().size());
        assertEquals(10, pool.getOutputNodes().size());
    }

    @Test
    public void putConnection_getConnection() {
        Pool pool = new Pool(10, 10);
        assertEquals(10, pool.getInputNodes().size());
        assertEquals(10, pool.getOutputNodes().size());
        NodeGene n1 = new NodeGene(pool.nextNodeId(), 0.5, 0.9);
        NodeGene n2 = new NodeGene(pool.nextNodeId(), 0.7, 0.1);
        ConnectionGene c1 = new ConnectionGene(pool.nextNodeId(), n1, n2);
        pool.putConnectionBetween(c1);
        assertTrue(pool.getConnectionBetween(n1, n2).isPresent());
        assertEquals(10, pool.getInputNodes().size());
        assertEquals(10, pool.getOutputNodes().size());
    }
}
