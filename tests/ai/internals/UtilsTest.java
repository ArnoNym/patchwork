package ai.internals;

import ai.anneat.main.utils.Utils;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UtilsTest {

    @Test
    public void test_getX_4() {
        double x = Utils.getX(1.3232, 1.53232321);
        assertTrue(1.3 < x);
        assertTrue(x < 1.5);
    }

    @Test
    public void test_getX_5() {
        double x = Utils.getX(1.333333333, 1.333333334);
        assertTrue(1.3333 < x);
        assertTrue(x < 1.333333334);
    }
}
