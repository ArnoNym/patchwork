package ai.agents;

import ai.AiConstants;
import ai.anneat.main.utils.collections.Tuple;
import ai.training.save.AiSaveController;
import ai.training.trainings.BlanketTraining;
import ai.training.utils.TrainingUtils;
import controller.IOController;
import model.Difficulty;
import model.Patch;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.*;

public class AgentDifficultyTest {
    public static final int PATCH_LISTS_COUNT = 100;

    public static final double TARGET_BLANKET_FILLED_PERCENT = 0.9;
    public static final double TARGET_BLANKET_SPECIAL_TOKEN_PERCENT = 0.5;

    public static final double TARGET_HARD_VS_MEDIUM_WIN_PERCENTAGE = 0.60;
    public static final double TARGET_MEDIUM_VS_SOFT_WIN_PERCENTAGE = 0.70;
    public static final double TARGET_HARD_VS_SOFT_WIN_PERCENTAGE = 0.90;

    private static List<List<Patch>> patchLists;
    private static Map<Difficulty, PatchworkAgent> patchworkAgentMap;

    @BeforeClass
    public static void setup() {
        patchLists = TrainingUtils.resortedPatches((new IOController(null)).importCSV(), PATCH_LISTS_COUNT);
        patchworkAgentMap = AiSaveController.loadAgentMap(AiConstants.AGENT_MAP);
    }

    /*
    @Test
    public void blanket() {
        AiConstants.PLACE_PATCH_AND_EXTRA_PATCH_AT_ONCE = false;

        List<Tuple<List<Patch>, Integer>> tuples = TrainingUtils.addSpecialTokenIndices(patchLists);
        double[] victoryPoint = new double[tuples.size()];
        for (int i = 0, tuplesSize = tuples.size(); i < tuplesSize; i++) {
            Tuple<List<Patch>, Integer> tuple = tuples.get(i);
            Set<Integer> extraPatchPatchIndex = new HashSet<>();
            BlanketTraining.initExtraPatchesPatchIndexSet(extraPatchPatchIndex);

            Tuple<Double, Double> scors = BlanketTraining.calcScore(gameStateAgent, tuple, extraPatchPatchIndex, false, false);
            victoryPoint[i] = scors.getV1();
        }

        double victoryPoints = Arrays.stream(victoryPoint).sum();
        victoryPoints /= tuples.size();
        victoryPoints -= (9*9) * 2;

        double expectedScore = (1 - TARGET_BLANKET_FILLED_PERCENT) * ((9*9)* (-2)) + TARGET_BLANKET_SPECIAL_TOKEN_PERCENT * 7;

        double standardDerivation = TrainingUtils.deviation(victoryPoint);

        System.out.println("BlanketAgentScore: "+victoryPoints+", ExpectedScore: "+expectedScore+", StandardDerivation: "+standardDerivation);
        assertTrue(victoryPoints > expectedScore);
    }
    */

    @Test
    public void hard_vs_medium() {
        AiConstants.USE_DECISION_TREE = true;

        Tuple<Integer, Integer> winsDrawsTuple = TrainingUtils.calcWinsDrawsTuple(
                patchLists,
                patchworkAgentMap.get(Difficulty.HARDCORE),
                patchworkAgentMap.get(Difficulty.MISSIONARY));
        printAndTest(winsDrawsTuple, "hard_vs_medium", TARGET_HARD_VS_MEDIUM_WIN_PERCENTAGE);
    }

    /*
    @Test
    public void medium_vs_hard() {
        AiConstants.USE_COMPUTE = false;

        Tuple<Integer, Integer> winsDrawsTuple = TrainingUtils.calcWinsDrawsTuple(
                patchLists,
                patchworkAgentMap.get(Difficulty.MISSIONARY),
                patchworkAgentMap.get(Difficulty.HARDCORE));
        printAndTest(winsDrawsTuple, "medium_vs_hard", 1 - TARGET_HARD_VS_MEDIUM_WIN_PERCENTAGE);
    }
    */

    @Test
    public void hard_vs_soft() {
        AiConstants.USE_DECISION_TREE = true;
        Tuple<Integer, Integer> winsDrawsTuple = TrainingUtils.calcWinsDrawsTuple(
                patchLists,
                patchworkAgentMap.get(Difficulty.HARDCORE),
                patchworkAgentMap.get(Difficulty.SOFTCORE));
        printAndTest(winsDrawsTuple, "hard_vs_soft", TARGET_HARD_VS_SOFT_WIN_PERCENTAGE);
    }

    @Test
    public void medium_vs_soft() {
        Tuple<Integer, Integer> winsDrawsTuple = TrainingUtils.calcWinsDrawsTuple(
                patchLists,
                patchworkAgentMap.get(Difficulty.MISSIONARY),
                patchworkAgentMap.get(Difficulty.SOFTCORE));
        printAndTest(winsDrawsTuple, "medium_vs_soft", TARGET_MEDIUM_VS_SOFT_WIN_PERCENTAGE);
    }

    public static void printAndTest(Tuple<Integer, Integer> winsDrawsTuple, String name, double targetWinPercentage) {
        double winPercentage = winsDrawsTuple.getV1() / (2d * patchLists.size());
        double drawPercentage = winsDrawsTuple.getV2() / (2d * patchLists.size());
        System.out.println(name+": [winPercentage: "+winPercentage+"], [drawPercentage: "+drawPercentage+"]");
        assertTrue(winPercentage > targetWinPercentage);
    }
}
