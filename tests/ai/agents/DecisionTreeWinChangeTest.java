package ai.agents;

import ai.AiConstants;
import ai.anneat.main.utils.collections.Tuple;
import ai.hardcoded.benchmarks.StupidAgent;
import ai.training.save.AiSaveController;
import ai.training.trainings.TurnBenchmarkTraining;
import ai.training.utils.TrainingUtils;
import controller.IOController;
import model.Difficulty;
import model.Patch;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class DecisionTreeWinChangeTest {
    private static final double COMPUTE_BETTER_MULT = 1.1d;

    private static List<List<Patch>> patchLists;
    private static PatchworkAgent benchmarkAgent;
    private static PatchworkAgent buyFirstAgent;

    @BeforeClass
    public static void setup() {
        List<Patch> patches = (new IOController(null)).importCSV();
        patchLists = new ArrayList<>();
        patchLists.add(TrainingUtils.resortPatches_(patches));
        patchLists.add(TrainingUtils.resortPatches_0(patches));
        patchLists.add(TrainingUtils.resortPatches_1(patches));
        patchLists.add(TrainingUtils.resortPatches_2(patches));
        patchLists.add(TrainingUtils.resortPatches_3(patches));

        benchmarkAgent = AiSaveController.loadAgentMap(AiConstants.TURN_AGENT_NAME).get(Difficulty.HARDCORE);

        buyFirstAgent = new StupidAgent();
    }

    @Test
    public void play() {
        double noScore = play(false);
        double computeScore = play(true);
        assert noScore * COMPUTE_BETTER_MULT <= computeScore;
    }

    public double play(boolean compute) {
        AiConstants.USE_DECISION_TREE = compute;

        String string = compute ? " Compute-" : "No-";

        double scoreSum = 0;
        for (List<Patch> patches : patchLists) {
            Tuple<Boolean, Double> tuple = TurnBenchmarkTraining.calcScore(patches, benchmarkAgent, buyFirstAgent, true, 1000);
            if (tuple.getV1()) {
                System.out.println(string+" Won!");
                scoreSum += tuple.getV2();
            } else {
                System.out.println(string+" Verloren :(");
            }
            System.out.println("Score: "+tuple.getV1());
        }
        System.out.println(string+"ScoreSum: "+scoreSum);

        return scoreSum;
    }
}
