package ai.decisionTree;

import ai.AiConstants;
import controller.GameBoardController;
import controller.GameController;
import controller.IOController;
import controller.PlayerController;
import model.*;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;

public class DecisionTreeRunnableTest {
    DecisionTree decisionTree;
    private GameBoard gameBoard;
    private GameState gameState;
    private GameController gameController;
    private GameBoardController gbc;
    private PlayerController playerController;
    private List<Patch> patches;
    private Function<double[],double[]> eval = in ->{
        double[] out = new double[1];
        out[0] += in[0];
        out[0] += in[1];
        out[0] += in[2]*3;
        out[0] += in[3]*5;
        out[0] += in[4]*3;
        out[0] += in[5] *-0.1;
        out[0] += in[6] * -1;
        out[0] += in[7]*0.1;
        out[0] += in[8]*5;
        out[0] += in[9]*-2;
        out[0] += in[10];
        out[0] += in[11];
        out[0] += in[12];
        out[0] += in[13];
        out[0] += in[14]*22;
        out[0] += in[15]*-2.5;
        out[0] += in[16]*-2;
        out[0] += in[17];
        out[0]+=3500;
        out[0] /= 1500;
        if(out[0] <= 0) out[0] = 0.00001;
        if(out[0] >= 1) out[0] = 0.99999;
        //System.out.println("Current Score: " + out[0]);
        return out;
    };
    /*@Before
    public void testDecisionTree(){
        List<Patch> patches = getPatches();
        for(int i = 0 ; i < patches.size();i++){
            //System.out.println("----" + (i + 1) + "---- " + "Price:" + patches.get(i).getCost() + "\n" + patches.get(i));
        }
        AIInformation info = new AIInformation(false,Difficulty.HARDCORE,SimulationSpeed.NOSTOP);
        GameState gameState = new GameState(new Player("Florian",info),new Player("Leon",null),new GameBoard(patches));
        Agent agent = new Agent(new NeatAgent(in -> new double[] {0.5}));
        decisionTree = new DecisionTree(agent, gameState);
    }
    @Before
    public void testCompute(){
        decisionTree.compute(0,8);
    }

    @Test
    public void testGetBestTurn(){
        decisionTree.getBestTurn();
    }*/

    private List<Patch> getPatches(){
        IOController io = new IOController(null);
        List<Patch> patches = io.importCSV();
        Collections.shuffle(patches);
        return patches;
    }

    @Test
    public void playGame(){
        AiConstants.USE_DECISION_TREE = false;

        AIInformation ai1 = new AIInformation(false,Difficulty.HARDCORE,SimulationSpeed.NOSTOP);
        AIInformation ai2 = new AIInformation(false,Difficulty.HARDCORE,SimulationSpeed.NOSTOP);
        Player p1 = new Player("Flo", ai1);
        Player p2 = new Player("NichtFlo", ai2);

        IOController io = new IOController(null);
        this.gameBoard = new GameBoard(getPatches());
        this.gameState = new GameState(p1,p2,gameBoard);
        this.gameController = new GameController();
        this.gameController.getGame().setGameState(gameState);
        this.gbc = this.gameController.getGameBoardController();
        this.playerController = this.gameController.getPlayerController();
        this.patches = gameBoard.getPatches();
        while(!gameController.getGame().getGameState().isFinished()){
            Player active = gameController.getGame().getGameState().getActivePlayer();
            System.out.println(active.getName() + " is active");
            Player passive = gameController.getGame().getGameState().getPassivePlayer();
            StringBuilder pos1 = new StringBuilder("----------------------------------------------------------");
            StringBuilder pos2 = new StringBuilder("----------------------------------------------------------");
            if(active.getName().startsWith("F")){
                pos1.setCharAt(active.getPos(),'F');
                pos2.setCharAt(passive.getPos(),'N');
                System.out.println(pos1);
                System.out.println(pos2);
            }
            else{
                pos1.setCharAt(passive.getPos(),'F');
                pos2.setCharAt(active.getPos(),'N');
                System.out.println(pos1);
                System.out.println(pos2);
            }

            gameController.getAiController().doTurn();
        }
        System.out.println("Game is over!");
        Player winner = PlayerController.calculateWinner(gameController.getGame().getGameState());
        Player looser1 = gameController.getGame().getGameState().getActivePlayer();
        Player looser2 = gameController.getGame().getGameState().getPassivePlayer();
        Player looser;
        if(looser1.equals(winner)){
            looser = looser2;
        }
        else{
            looser = looser1;
        }
        System.out.println("The winner is: " + winner.getName());
        System.out.println("Their score: " + PlayerController.calculateScore(winner));
        System.out.println("------");
        System.out.println(winner.getBlanket());
        System.out.println("--------------------");
        System.out.println("The looser is: " + looser.getName());
        System.out.println("Their score: " + PlayerController.calculateScore(looser));
        System.out.println("------");
        System.out.println(looser.getBlanket());

        AiConstants.USE_DECISION_TREE = true;
    }
}
