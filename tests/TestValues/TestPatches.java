package TestValues;

import model.Patch;
import Resources.Constants;

/**
 * PatchTuple Test values
 */
public class TestPatches {

    /**
     * gets the Special Patch
     * @return
     */
    public static Patch getSpectialPatch(){
        //return getSpectialPatch();
        return Constants.generateSpecialPatch(); //Ich denke mal, hier sollte ein SpecialPatch generiert
                                                // werden statt einer endlosrekursion Long
    }

    /**
     * gets Patch1 with the following Shape:
     * 	        0   *
     * 	        1	*	*   *
     * 	   	    2
     * 	   	    3
     * 	     	4
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch1(){
        boolean [] col0 = {true, false, false, false, false};
        boolean [] col1 = {true, true, true, false, false};
        boolean [] col2 = {false, false, false, false, false};
        boolean [] col3 = {false, false, false, false, false};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 2, 5 ,1);
        return p;
    }

    /**
     * gets Patch2 with the following Shape:
     * 	        0
     * 	        1		*
     * 	   	    2       *   *
     * 	   	    3       *   *
     * 	     	4
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch2(){
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, true, false, false, false};
        boolean [] col2 = {false, true, true, false, false};
        boolean [] col3 = {false, true, true, false, false};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 1, 2 ,0);
        return p;
    }

    /**
     * gets Patch3 with the following Shape:
     * 	        0
     * 	        1	                *
     * 	   	    2               *   *
     * 	   	    3               *
     * 	     	4
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch3(){
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, false, false, false, true};
        boolean [] col2 = {false, false, false, true, true};
        boolean [] col3 = {false, false, false, true, false};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 0, 2 ,0);
        return p;
    }

    /**
     * gets Patch4 with the following Shape:
     * 	        0
     * 	        1
     * 	   	    2
     * 	   	    3   *   *   *
     * 	     	4           *
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch4(){
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, true, false, false, false};
        boolean [] col2 = {false, true, false, false, false};
        boolean [] col3 = {true, true, true, false, false};
        boolean [] col4 = {true, false, true, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 4, 3 ,2);
        return p;
    }

    /**
     * gets Patch5 with the following Shape:
     * 	        0
     * 	        1       *   *
     * 	   	    2       *   *
     * 	   	    3
     * 	     	4
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch5(){
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, true, true, false, false};
        boolean [] col2 = {false, true, true, false, false};
        boolean [] col3 = {false, false, false, false, false};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 2, 5 ,2);
        return p;
    }

    /**
     * gets Patch6 with the following Shape:
     * 	        0
     * 	        1
     * 	   	    2
     * 	   	    3
     * 	     	4   *   *   *   *   *
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch6(){
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, false, false, false, false};
        boolean [] col2 = {false, false, false, false, false};
        boolean [] col3 = {false, false, false, false, false};
        boolean [] col4 = {true, true, true, true, true};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 1, 1 ,0);
        return p;
    }

    /**
     * gets Patch7 with the following Shape:
     * 	        0   *
     * 	        1	*   *
     * 	   	    2   *   *   *
     * 	   	    3
     * 	     	4
     * 	  	 	    0 	1 	2	3	4
     * @return Patch
     */
    public static Patch getPatch7(){
        boolean [] col0 = {true, false, false, false, false};
        boolean [] col1 = {true, true, false, false, false};
        boolean [] col2 = {true, true, true, false, false};
        boolean [] col3 = {false, false, false, false, false};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 2, 3 ,1);
        return p;
    }

}
