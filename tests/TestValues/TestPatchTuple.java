package TestValues;

import model.Patch;
import model.PatchTuple;
import model.Rotation;
import model.VecTwo;

/**
 * PatchTuple Test values
 */
public class TestPatchTuple {

    /**
     *  Constructs a PatchTuple with the shape of Patch1 and the Vector2(0,0)
     * @return PatchTuple
     */
    public static PatchTuple getPatchTuple1(){
        Patch p = TestPatches.getPatch1();
        VecTwo tlc = new VecTwo(0, 0);
        PatchTuple patchTuple = new PatchTuple(p, Rotation._0, tlc, false);
        return patchTuple;
    }
    /**
     *  Constructs a PatchTuple with the shape of Patch2 and the Vector2(5,5)
     * @return PatchTuple
     */
    public static PatchTuple getPatchTuple2(){
        Patch p = TestPatches.getPatch2();

        VecTwo tlc = new VecTwo(5, 5);
        PatchTuple patchTuple = new PatchTuple(p, Rotation._90, tlc, false);
        return patchTuple;
    }
    /**
     *  Constructs a PatchTuple with the shape of Patch3 and the Vector2(2,4)
     * @return PatchTuple
     */
    public static PatchTuple getPatchTuple3(){
        Patch p = TestPatches.getPatch3();

        VecTwo tlc = new VecTwo(2, 4);
        PatchTuple patchTuple = new PatchTuple(p, Rotation._0, tlc, false);
        return patchTuple;
    }
    /**
     *  Constructs a PatchTuple with the shape of Patch4 and the Vector2(3,1)
     * @return PatchTuple
     */
    public static PatchTuple getPatchTuple4(){

        Patch p = TestPatches.getPatch4();

        VecTwo tlc = new VecTwo(3, 1);
        PatchTuple patchTuple = new PatchTuple(p, Rotation._0, tlc, false);
        return patchTuple;
    }
    /**
     *  Constructs a PatchTuple with the shape of Patch5 and the Vector2(2,3)
     * @return PatchTuple
     */
    public static PatchTuple getPatchTuple5(){

        Patch p = TestPatches.getPatch5();

        VecTwo tlc = new VecTwo(2, 3);
        PatchTuple patchTuple = new PatchTuple(p, Rotation._0, tlc, false);
        return patchTuple;
    }
    /**
     *  Constructs a PatchTuple with the shape of Patch6 and the Vector2(0,3)
     * @return PatchTuple
     */
    public static PatchTuple getPatchTuple6(){

        Patch p = TestPatches.getPatch6();

        VecTwo tlc = new VecTwo(0, 3);
        PatchTuple patchTuple = new PatchTuple(p, Rotation._0, tlc, false);
        return patchTuple;
    }



}
