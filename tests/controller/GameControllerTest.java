package controller;

import ai.agents.GameStateAgent;
import ai.agents.NeatPatchworkAgent;
import ai.decisionTree.Turn;
import model.*;
import org.junit.Before;
import org.junit.Test;
import view.GUI;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the GameControllerTest
 */
public class GameControllerTest {

    private GameController gamecontroller;
    private Game game;
    private GameState gameState;
    private GameBoard gameBoard;
    private Player p1;

    /**
     * sets up the testing environment
     */
    @Before
    public void setUp() throws Exception {
        Player p1 = new Player("Spieler 1", null);
        Player p2 = new Player("Spieler 2", null);
        IOController io = new IOController(null);

        this.gameBoard = new GameBoard(io.importCSV());
        this.gameState = new GameState(p1, p2, this.gameBoard);

        this.game = new Game();
        this.game.setGameState(gameState);

        this.gamecontroller = new GameController();
        gamecontroller.setGame(game);
    }

    /**
     * tests the Game controller with gui constructor
     */
    @Test
    public void testGameControllerConstructorGUI() {
        GUI gui = null;
        GameController gameController = new GameController(gui);
        assertNotNull(gameController);
    }

    /**
     * tests the gamecontroller with patchwork agent constructor
     */
    @Test
    public void testGameControllerPatchworkAgent() {
        GameController patchworkAgentGameController = new GameController(
                new NeatPatchworkAgent(new GameStateAgent(doubles -> doubles)),
                new NeatPatchworkAgent(new GameStateAgent(doubles -> doubles)),
                gamecontroller.getGame().getGameState().getGameBoard().getPatches());
        assertNotNull(patchworkAgentGameController);
    }

    /**
     * tests if the gamecontroller with neatpatchworkagent works as intended
     */
    @Test
    public void testGameControllerWithAI() {
        GameController aiGameController = new GameController(
                new NeatPatchworkAgent(new GameStateAgent(doubles -> doubles)),
                this.gamecontroller.getGame().getGameState().getGameBoard().getPatches());
        assertNotNull(aiGameController.getGame());
    }

    /**
     * tests if the nextgamestate is being called correctly and if it puts it in the redo stack
     */
    @Test
    public void testNextGameState() {
        GameBoard gameBoard1 = gameBoard.clone();
        gameBoard1.setMeeplePos(2);
        GameState gameState1 = new GameState(this.gameState.getActivePlayer(),
                this.gameState.getPassivePlayer(),
                gameBoard1);
        GameBoard gameBoard2 = gameBoard.clone();
        gameBoard2.setMeeplePos(4);
        GameState gameState2 = new GameState(this.gameState.getPassivePlayer(),
                this.gameState.getActivePlayer(),
                gameBoard2);

        GameBoard gameBoard3 = gameBoard.clone();

        gameBoard3.setMeeplePos(6);
        GameState gameState3 = new GameState(this.gameState.getActivePlayer(),
                this.gameState.getPassivePlayer(),
                gameBoard2);

        GameBoard gameBoard4 = gameBoard.clone();

        gameBoard4.setMeeplePos(8);
        GameState gameState4 = new GameState(this.gameState.getActivePlayer(),
                this.gameState.getPassivePlayer(),
                gameBoard2);

        this.game.getUndoStack().add(gameState1);
        this.game.setGameState(gameState2);
        this.game.getRedoStack().add(gameState3);

        this.gamecontroller.nextGameState(gameState4);
        assertEquals(gameState4, this.game.getGameState());
        assertEquals(gameState2, this.game.getUndoStack().peek());
    }

    /**
     * tests if a highscore gets shown
     */
    @Test
    public void testShowHighScore() {
        this.game.getHighscore().put("pewlivecake", 11);
        this.game.getHighscore().put("xpc", 10);
        this.game.getHighscore().put("schrautt", 12);
        System.out.println(gamecontroller.showHighscore());
        assertNotNull(gamecontroller.showHighscore());
    }

    /**
     * tests the act method
     */
    @Test
    public void testAct() {
        AIInformation aiInformation = new AIInformation(true, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        p1 = new Player("AI", aiInformation);
        this.gamecontroller.getGame().getGameState().setActivePlayer(p1);
        this.gamecontroller.getGame().getGameState().getGameBoard().setMeeplePos(0);
        Turn turn = gamecontroller.getAiController().calculateHint();
        this.gamecontroller.act(turn);
        Blanket blanket = this.gamecontroller.getGame().getGameState().getPassivePlayer().getBlanket();
        String aiBlanket = " -   1   -   -   -   -   -   -   -  \n" +
                " -   1   -   -   -   -   -   -   -  \n" +
                " 1   1   1   -   -   -   -   -   -  \n" +
                " -   1   -   -   -   -   -   -   -  \n" +
                " -   1   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  ";
        System.out.println(blanket.toString().contains(aiBlanket));
    }


    /**
     * tests the getgame getter
     */
    @Test
    public void testGetGame() {
        assertNotNull(gamecontroller.getGame());
    }

    /**
     * tests the setgame method
     */
    @Test
    public void testSetGame() {
        Game game2 = new Game();
        Player p1 = new Player("Spieler 4", null);
        Player p2 = new Player("Spieler 5", null);
        IOController io = new IOController(null);

        GameBoard gb = new GameBoard(io.importCSV());
        GameState gs = new GameState(p1, p2, gb);
        game2.setGameState(gs);
        gamecontroller.setGame(game2);
        assertEquals(gamecontroller.getGame(), game2);

    }

    /**
     * tests the setgame method
     */
    @Test
    public void testGetAIController() {
        assertNotNull(gamecontroller.getAiController());
    }

    /**
     * tests the getblanketcontroller method
     */
    @Test
    public void testGetBlanketController() {
        assertNotNull(gamecontroller.getBlanketController());
    }

    /**
     * tests the getgameboardcontroller method
     */
    @Test
    public void testGetGameboardController() {
        assertNotNull(gamecontroller.getGameBoardController());
    }

    /**
     * tests the getplayercontroller method
     */
    @Test
    public void testGetPlayerController() {
        assertNotNull(gamecontroller.getPlayerController());
    }

    /**
     * tests the getgui method
     */
    @Test
    public void testGetGUI() {
        assertNull(gamecontroller.getGUI());
    }

    /**
     * tests the isMusik and the setMusik method
     */
    @Test
    public void testIsMusiksetMusik() {
        gamecontroller.setMusik(true);
        assertTrue(gamecontroller.isMusik());

        gamecontroller.setMusik(false);
        assertFalse(gamecontroller.isMusik());
    }
}
