package controller;
import model.*;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * tests the methods of the GameStateController
 */
public class GameStateControllerTest {

    private GameController gameController;
    private GameStateController gameStateController;
    private IOController ioController;

    /**
     * Creates a Testing-Environment
     */
    @Before
    public void setUp() {
        gameController = new GameController();

        gameStateController = new GameStateController(gameController);
        ioController = new IOController(gameController);
        Player player1 = new Player("Player1", null);
        Player player2 = new Player("Player1", null);

        List<Patch> patchList = ioController.importCSV();
        GameBoard gameBoard = new GameBoard(patchList);
        GameState gameState = new GameState(player1, player2, gameBoard);
        gameController.getGame().setGameState(gameState);
    }

    /**
     * Tests the undoMove method
     */
    @Test
    public void testUndoMove(){
        GameState old = gameController.getGame().getGameState();
        Player p1 = new Player("Person1", null);
        Player p2 = new Player("Person2", null);
        gameController.nextGameState(new GameState(p1, p2, new GameBoard(new ArrayList<>())));

        gameStateController.undoMove();
        assertEquals(old, gameController.getGame().getGameState());
    }

    /**
     * Tests the redoMove method
     */
    @Test
    public void testRedoMove(){
        GameState old = gameController.getGame().getGameState();
        Player p1 = new Player("Person1", null);
        Player p2 = new Player("Person2", null);
        GameState gameState = new GameState(p1, p2, new GameBoard(new ArrayList<>()));
        gameController.nextGameState(gameState);

        gameStateController.undoMove();
        gameStateController.redoMove();

        assertEquals(gameState, gameController.getGame().getGameState());
    }

    /**
     * Tests the redoMove method
     */
    @Test(expected = IllegalStateException.class)
    public void testRedoMoveException(){
        gameStateController.redoMove();
    }

    /**
     * Tests the undoMove method
     */
    @Test(expected = IllegalStateException.class)
    public void testUndoMoveException(){
        gameStateController.undoMove();
    }


    /**
     * Tests the undoPossible method
     */
    @Test
    public void testUndoPossibleTrue(){
        gameController.nextGameState(gameController.getGame().getGameState().clone());
        assertTrue(gameStateController.undoPossible());
    }

    /**
     * Tests the undoPossible method
     */
    @Test
    public void testUndoPossibleFalse(){
        assertFalse(gameStateController.undoPossible());
    }

    /**
     * Tests the redoPossible method
     */
    @Test
    public void testRedoPossibleTrue(){
        gameController.nextGameState(gameController.getGame().getGameState().clone());
        gameStateController.undoMove();
        assertTrue(gameStateController.redoPossible());
    }

    /**
     * Tests the redoPossible method
     */
    @Test
    public void testRedoPossibleFalse(){
        assertFalse(gameStateController.redoPossible());
    }

    /**
     * Tests the setUpGame method
     */
    @Test
    public void testSetUpGame(){
        Player p1 = new Player("Person1", null);
        Player p2 = new Player("Person2", null);
        gameStateController.setUpGame(p1, p2, ioController.importCSV());
    }

    /**
     * Tests the finishGame method
     */
    @Test(expected = IllegalStateException.class)
    public void testFinishGameNotFinished(){
        gameStateController.finishGame();
    }

    /**
     * Tests the finishGame method
     */
    @Test
    public void testFinishGame(){
        int lastPos = gameController.getGame().getGameState().getGameBoard()._LASTPOS;
        gameController.getGame().getGameState().getActivePlayer().setPos(lastPos);
        gameController.getGame().getGameState().getPassivePlayer().setPos(lastPos);

        gameController.getGame().getHighscore().put("Player1", -9999);

        gameStateController.finishGame();

        setUp();
        gameController.getGame().getGameState().getActivePlayer().setPos(lastPos);
        gameController.getGame().getGameState().getPassivePlayer().setPos(lastPos);
        gameController.getGame().getGameState().getActivePlayer().setHighscoreActivated(false);

        gameStateController.finishGame();
    }

    /**
     * Tests the swapPlayerRoles method
     */
    @Test
    public void testSwapPlayerRoles(){
        Player passive = gameController.getGame().getGameState().getPassivePlayer();
        gameStateController.swapPlayerRoles();
        assertEquals(passive, gameController.getGame().getGameState().getActivePlayer());
    }

    /**
     * Tests the isSpecialTokenAvailable method
     */
    @Test
    public void testIsSpecialTokenAvailableTrue(){
        assertTrue(GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()));

        gameController.getGame().getGameState().getActivePlayer().setSpecialToken(true);
        assertFalse(GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()));

        gameController.getGame().getGameState().getPassivePlayer().setSpecialToken(true);
        assertFalse(GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()));

        gameController.getGame().getGameState().getActivePlayer().setSpecialToken(false);
        assertFalse(GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()));
    }

//    @Test
//    public void integrationTest(){
//        GameController gameController = new GameController(null);
//        Game game = gameController.getGame();
//        GameStateController gameStateController = gameController.getGameStateController();
//        PlayerController playerController = gameController.getPlayerController();
//        GameBoardController gameBoardController = gameController.getGameBoardController();
//
//        Player raphael = new Player("Raphael", null);
//        Player david = new Player("David__", null);
//        List<Patch> patches = new LinkedList<>();
//        patches.add(TestPatches.getPatch1());patches.add(TestPatches.getPatch2()); patches.add(TestPatches.getPatch3());patches.add(TestPatches.getPatch4());patches.add(TestPatches.getPatch5());patches.add(TestPatches.getPatch6());patches.add(TestPatches.getPatch7());
//        gameStateController.setUpGame(raphael, david, patches);
//        show(game.getGameState());
//
//        while(game.getGameState() != null){
//            playerController.pass();
//            show(game.getGameState());
//        }
//        System.out.println(game.getHighscore());
//
//        /*
//        playerController.pass();show(game.getGameState());
//        playerController.pass(); show(game.getGameState());
//        gameStateController.undoMove();show(game.getGameState());
//        gameStateController.undoMove();show(game.getGameState());
//
//        gameStateController.redoMove();show(game.getGameState());
//        gameStateController.redoMove();show(game.getGameState());
//
//        gameStateController.undoMove();show(game.getGameState());
//
//        System.out.println(game.getGameState().getGameBoard().getPatches().size());
//        System.out.println(game.getGameState().getGameBoard().getPatches());
//        Patch p1 = gameBoardController.getAvailablePatches().get(1);
//        PatchTuple pt1 = new PatchTuple(p1, Rotation._90, new Vec2(4,4), true);
//        System.out.println(pt1);
//        playerController.takePatchAndInsert(pt1);show(game.getGameState());
//
//        System.out.println(game.getGameState().getGameBoard().getPatches().size());
//        System.out.println(game.getGameState().getGameBoard().getPatches());
//        */
//
//    }
//
//    @Test
//    public void show(GameState gameState){
//        System.out.println(gameState);
//        System.out.println("\n##############################################\n");
//    }
//
//    @Test
//    public void durchlauf(){
//
//    }

}
