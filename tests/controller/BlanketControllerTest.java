package controller;

import Resources.Constants;
import TestValues.TestPatches;
import ai.anneat.main.utils.collections.Tuple;
import model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.*;

/**
 * This class tests the methods
 * of the IOController
 */
public class BlanketControllerTest {
    private GameBoard gameBoard;
    private GameController gameController;
    private GameState gameState;
    private BlanketController blanketController;
    private List<Patch> patches;

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {
        Player p1 = new Player("Spieler 1", null);
        Player p2 = new Player("Spieler 2", null);

        IOController io = new IOController(null);

        this.gameBoard = new GameBoard(io.importCSV());
        this.gameState = new GameState(p1, p2, gameBoard);
        this.gameController = new GameController();
        this.gameController.getGame().setGameState(gameState);
        this.blanketController = this.gameController.getBlanketController();
        this.patches = gameBoard.getPatches();
    }

    /**
     * tests if a placeable patchTuple is actually placeable
     */
    @Test
    public void testPlacePatchTuple() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        VecTwo vecTwo = new VecTwo(0, 0);
        PatchTuple pt = new PatchTuple(patch, Rotation._0, vecTwo, false);

        List<PatchTuple> ptList = new ArrayList<>();
        ptList.add(pt);
        blanket.setPatchTuples(ptList);
        Blanket blanketCopy = blanket.clone();
        blanketController.placePatchTuple(pt.clone());
        assertEquals(blanketCopy, gameController.getGame().getGameState().getActivePlayer().getBlanket());
    }

    /**
     * tests if a placeable patch is actually placeable
     */
    @Test
    public void testCanPlacePatch() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        assertTrue(BlanketController.canPlacePatch(blanket, patch));

        List<PatchTuple> tuples = new ArrayList<>();
        // Create 4 x 4 True Patch at (0, 0)
        boolean[][] matrix = new boolean[5][5];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = true;
            }
        }
        Patch patchd = new Patch(matrix, 0, 0, 0);
        PatchTuple tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(0, 0), false);
        tuples.add(tuple);
        // Create 3 x 4 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = true;
            }
        }
        patchd = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(0, 4), false);
        tuples.add(tuple);
        // Create 4 x 3 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = true;
            }
        }
        patchd = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(4, 0), false);
        tuples.add(tuple);

        // Create 3 x 3 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = true;
            }
        }
        patchd = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(4, 4), false);
        tuples.add(tuple);
        gameController.getGame().getGameState().getActivePlayer().getBlanket().setPatchTuples(tuples);
        assertFalse(BlanketController.canPlacePatch(blanket, patchd));
    }

    /**
     * tests if all the placements of a patch is getting calculated correctly
     */
    @Test
    public void testCalculatePlacements() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);

        assertEquals(200, BlanketController.calculatePlacements(blanket, patch).size());
    }

    /**
     * tests if calculate first placement doesn't work
     */
    @Test
    public void testCalculateFirstPlacementCantPlace() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        VecTwo vecTwo = new VecTwo(0, 0);
        PatchTuple pt = new PatchTuple(patch, Rotation._0, vecTwo, false);
        Patch patchCopy = patch.clone();
        blanket = BlanketController.insertPatchTuple(blanket, pt);
        assertNull(BlanketController.calculateFirstPlacement(blanket, patchCopy));
    }

    /**
     * tests if insert Patchtuple works as intended
     */
    @Test
    public void testInsertPatchTuple() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        VecTwo vecTwo = new VecTwo(0, 0);
        PatchTuple pt = new PatchTuple(patch, Rotation._0, vecTwo, false);
        Blanket blanket1 = BlanketController.insertPatchTuple(blanket, pt);
        assertNotEquals(blanket, blanket1);
        assertEquals(1, blanket1.getPatchTuples().size());
    }

    /**
     * tests if it correctly shows that a special token is ready
     */
    @Test
    public void testSpecialTokenReady() {
        List<PatchTuple> tuples = new ArrayList<>();
        Blanket blanket = new Blanket();


        // Create 4 x 4 True Patch at (0, 0)
        boolean[][] matrix = new boolean[5][5];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = true;
            }
        }
        Patch patch = new Patch(matrix, 0, 0, 0);
        PatchTuple tuple = new PatchTuple(patch, Rotation._0, new VecTwo(0, 0), false);
        tuples.add(tuple);


        // Create 3 x 4 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = true;
            }
        }
        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(0, 4), false);
        tuples.add(tuple);


        // Create 4 x 3 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = true;
            }
        }
        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(4, 0), false);
        tuples.add(tuple);

        // Create 3 x 3 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = true;
            }
        }
        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(4, 4), false);
        tuples.add(tuple);
        gameController.getGame().getGameState().getActivePlayer().getBlanket().setPatchTuples(tuples);
        assertTrue(blanketController.specialTokenReady());
    }

    /**
     * tests the calculate first placement
     */
    @Test
    public void testCalculateFirstPlacement() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();
        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        assertNotNull(BlanketController.calculateFirstPlacement(blanket, patch));

    }

    /**
     * tests the canplacePatchTuple method when it isn't possible
     */
    @Test
    public void testCanPlacePatchTuple() {
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        VecTwo vecTwo = new VecTwo(0, 0);
        PatchTuple pt = new PatchTuple(patch, Rotation._0, vecTwo, false);
        ArrayList<PatchTuple> tuples = new ArrayList<>();
        tuples.add(pt);
        PatchTuple pt1 = pt.clone();
        blanket.setPatchTuples(tuples);
        assertFalse(BlanketController.canPlacePatchTuple(blanket, pt1));
    }


    /**
     * tests the integration
     */
    @Test
    public void integrationTest() {
        boolean[] col00 = {false, true, false, false, false};
        boolean[] col01 = {true, true, true, false, false};
        boolean[] col02 = {true, false, true, false, false};
        boolean[] col03 = {false, false, false, false, false};
        boolean[] col04 = {false, false, false, false, false};
        boolean[][] rows0 = {col00, col01, col02, col03, col04};
        Patch p0 = new Patch(rows0, 0, 0, 0);
        VecTwo v0 = new VecTwo(3, 3);
        PatchTuple pt0 = new PatchTuple(p0, Rotation._0, v0, true);

        boolean[] col10 = {false, false, false, false, false};
        boolean[] col11 = {false, false, false, false, false};
        boolean[] col12 = {false, false, true, false, false};
        boolean[] col13 = {false, false, true, false, false};
        boolean[] col14 = {false, false, true, false, false};
        boolean[][] rows1 = {col10, col11, col12, col13, col14};
        Patch p1 = new Patch(rows1, 0, 0, 0);
        VecTwo v1 = new VecTwo(3, 3);
        PatchTuple pt1 = new PatchTuple(p1, Rotation._0, v1, true);

        boolean[] col20 = {false, false, false, false, false};
        boolean[] col21 = {true, true, true, false, false};
        boolean[] col22 = {false, false, true, false, false};
        boolean[] col23 = {false, false, true, false, false};
        boolean[] col24 = {false, false, true, false, false};
        boolean[][] rows2 = {col20, col21, col22, col23, col24};
        Patch p2 = new Patch(rows2, 0, 0, 0);
        VecTwo v2 = new VecTwo(5, 6);
        PatchTuple pt2 = new PatchTuple(p2, Rotation._0, v2, true);
        List<PatchTuple> ls = new LinkedList<>();
        //ls.add(pt0);
        ls.add(pt1);
        // ls.add(pt2);

        Blanket testBlanket = new Blanket();
        testBlanket.setPatchTuples(ls);
        System.out.println(testBlanket);

        boolean[] col30 = {true, true, true, true, true};
        boolean[] col31 = {true, true, true, true, true};
        boolean[] col32 = {true, true, true, true, true};
        boolean[] col33 = {true, true, true, true, true};
        boolean[] col34 = {true, true, true, true, true};
        boolean[][] rows3 = {col30, col31, col32, col33, col34};


        Patch testPatch = new Patch(rows3, 0, 0, 0);
        PatchTuple testTuple = new PatchTuple(testPatch, Rotation._0, new VecTwo(0, 5), true);

        for (Tuple<Blanket, PatchTuple> t : BlanketController.calculateAllPossibleFollowupBlankets(testBlanket, testPatch)) {
            System.out.println(t.getV1());
            //System.out.println(t.getV2());
        }
    }

    /**
     * @author Leon
     */
    @Test
    public void calculatePlacements_deterministicTest() {
        Blanket blanket = new Blanket();
        Patch patch = (new IOController(null)).importCSV().get(0);
        System.out.println(patch.toString());
        for (int i = 0; i < 10; i++) {
            Collection<PatchTuple> patchTuples1 = BlanketController.calculatePlacements(blanket, patch);
            Collection<PatchTuple> patchTuples2 = BlanketController.calculatePlacements(blanket, patch);
            assertEquals(patchTuples1, patchTuples2);
        }
    }

    /**
     * @author Leon
     */
    @Test
    public void calculateFirstPlacement_deterministicTest() {
        Blanket blanket = new Blanket();
        Patch patch = (new IOController(null)).importCSV().get(0);
        System.out.println(patch.toString());
        for (int i = 0; i < 10; i++) {
            PatchTuple patchTuple1 = BlanketController.calculateFirstPlacement(blanket, patch);
            PatchTuple patchTuple2 = BlanketController.calculateFirstPlacement(blanket, patch);
            assertEquals(patchTuple1, patchTuple2);
        }
    }

    /**
     * tests the calculateAllPossibleFollowupBlankets(Blanket, Patch, boolean) method
     */
    @Test
    public void testcalculateAllPossibleFollowupBlankets() {
        boolean [] col0 = {true, true, true, true, true};
        boolean [] col1 = {true, true, true, true, true};
        boolean [] col2 = {true, true, true, true, true};
        boolean [] col3 = {true, true, true, true, true};
        boolean [] col4 = {true, true, true, true, true};
        boolean [] [] rows = {col0, col1, col2, col3, col4};
        Patch p = new Patch(rows, 1, 1 ,0);
        PatchTuple pt = new PatchTuple(p,Rotation._0,new VecTwo(0,0),false);
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean [] col00 = {true, true, true, true, false};
        boolean [] col11 = {true, true, true, true, false};
        boolean [] col22 = {true, true, true, true, false};
        boolean [] col33 = {true, true, true, true, false};
        boolean [] col44 = {true, true, true, true, false};
        boolean [] [] rows2 = {col00, col11, col22, col33, col44};
        Patch p2 = new Patch(rows2, 1, 1 ,0);
        PatchTuple pt2 = new PatchTuple(p2,Rotation._0,new VecTwo(0,5),false);


        Patch p3 = p2.rotatePatch(Rotation._90);
        PatchTuple pt3 = new PatchTuple(p3,Rotation._0,new VecTwo(5,0),false);

        boolean [] col000 = {true, true, true, true, false};
        boolean [] col111 = {true, true, true, true, false};
        boolean [] col222 = {true, true, true, true, false};
        boolean [] col333 = {true, true, false, false, false};
        boolean [] col444 = {false, false, false, false, false};
        boolean [] [] rows4 = {col000, col111, col222, col333, col444};
        Patch p4 = new Patch(rows4, 1, 1 ,0);
        PatchTuple pt4 = new PatchTuple(p4,Rotation._0,new VecTwo(5,5),false);

        blanketController.placePatchTuple(pt);
        blanketController.placePatchTuple(pt2);
        blanketController.placePatchTuple(pt3);
        blanketController.placePatchTuple(pt4);
        assertEquals(16, BlanketController.calculateAllPossibleFollowupBlankets(blanket,
                Constants.generateSpecialPatch(),true).size());
    }

    /**
     * tests the calculateAllPossibleFollowupBlankets(Blanket, Patch, Patch) method
     */
    @Test
    public void testCalculateAllPossibleFollowupBlankets() {
        Patch patch1 = TestPatches.getPatch1();
        Patch patch2 = TestPatches.getPatch2();
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();
        assertEquals(68607, BlanketController.calculateAllPossibleFollowupBlankets(blanket,patch1,patch2).size());
    }
}
