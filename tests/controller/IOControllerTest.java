package controller;

import ai.agents.GameStateAgent;
import ai.agents.NeatAgent;
import ai.agents.NeatPatchworkAgent;
import ai.agents.PatchworkAgent;
import ai.anneat.main.SerializableFunction;
import ai.anneat.models.NeatData;
import ai.anneat.publish.Neat;
import ai.anneat.publish.settings.Settings;
import ai.anneat.publish.task.Deterministic;
import ai.anneat.publish.task.SoloTask;
import ai.anneat.publish.task.Task;
import ai.training.settings.TurnBenchmarkSettings;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.*;

import java.util.*;

/**
 * This class tests the methods
 * of the IOController
 */
public class IOControllerTest {

    private GameController gameController;
    private IOController ioController;
    private String errorString;

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {
        errorString = "/\\0><\0" + '/';
        gameController = new GameController();
        ioController = new IOController(gameController);
        Player player1 = new Player("Player1", null);
        Player player2 = new Player("Player1", null);

        List<Patch> patchList = ioController.importCSV();
        GameBoard gameBoard = new GameBoard(patchList);
        GameState gameState = new GameState(player1, player2, gameBoard);
        gameController.getGame().setGameState(gameState);
    }

    /**
     * Tests the saveGame method
     */
    @Test
    public void testSaveGame() {
        ioController.saveGame();
    }

    /**
     * Tests the saveGame method with Exception
     */
    @Test
    public void testSaveGameException() {
        ioController.saveGame(errorString);
    }

    /**
     * Tests the saveGame method with null
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSaveGameNull() {
        ioController.saveGame(null);
    }

    /**
     * Tests the loadGame method
     */
    @Test
    public void testLoadGame() {
        ioController.saveGame();

        Game compareGame = ioController.loadGame();

        assertEquals(
                gameController.getGame(),
                compareGame
        );
    }

    /**
     * Tests the loadGame method with null
     */
    @Test(expected = IllegalArgumentException.class)
    public void testLoadGameNull() {
        ioController.saveGame();

        Game compareGame = ioController.loadGame(null);

        assertEquals(
                gameController.getGame(),
                compareGame
        );
    }

    /**
     * Tests the loadGame method with Error
     */
    @Test
    public void testLoadGameException() {
        ioController.loadGame(System.getProperty("user.dir") + "\\nope.ser");
    }


    /**
     * Tests the loadCSV method
     */
    @Test
    public void testLoadCSV() {
        assertNotNull(new IOController(new GameController()).importCSV());
        assertEquals(33, new IOController(new GameController()).importCSV().size());
    }

    /**
     * Tests the loadCSV method
     */
    @Test
    public void testLoadCSVPatternError() {
        String filepath = "resources/patchwork-piecesTestFehlerInPattern.csv";
        assertNull(new IOController(new GameController()).importCSV(filepath));
    }

    /**
     * Tests the loadCSV method
     */
    @Test
    public void testLoadCSVPatternErrorLength() {
        String filepath = "resources/patchwork-piecesTestFehlerInPatternLength.csv";
        assertNull(new IOController(new GameController()).importCSV(filepath));
    }

    /**
     * Tests the loadCSV method
     */
    @Test
    public void testLoadCSVPatternErrorInInteger() {
        String filepath = "resources/patchwork-piecesTestFehlerInInteger.csv";
        assertNull(new IOController(new GameController()).importCSV(filepath));
    }

    /**
     * Tests the loadCSV method
     */
    @Test
    public void testLoadCSVPatternErrorInSymbol() {
        String filepath = "resources/patchwork-piecesTestFehlerInSymbol.csv";
        assertNull(new IOController(new GameController()).importCSV(filepath));
    }

    /**
     * Tests the loadCSV method
     */
    @Test
    public void testLoadCSVPatternFileNotFound() {
        String filepath = "src/bruh.csv";
        assertNull(new IOController(new GameController()).importCSV(filepath));
    }

    /**
     * Tests the saveAgentMap
     */
    @Test
    public void testsaveAgentMap() {
        String filepath = "src/bruh.ser";
        SerializableFunction<double[], double[]> function1 = output -> (output[0] < 0.5 ? new double[]{1} : new double[]{0});
        GameStateAgent neatAgent = new GameStateAgent(function1);
        Map<Difficulty, PatchworkAgent> hashmap = new HashMap<>();
        hashmap.put(Difficulty.HARDCORE, new NeatPatchworkAgent(neatAgent));
        new IOController(new GameController()).saveAgentMapFromPath(hashmap, filepath);
    }

    /**
     * Tests the saveAgentMap with Exception
     */
    @Test
    public void testSaveAgentMapException() {
        String filepath = errorString;
        SerializableFunction<double[], double[]> function1 = output -> (output[0] < 0.5 ? new double[]{1} : new double[]{0});
        GameStateAgent neatAgent = new GameStateAgent(function1);
        Map<Difficulty, PatchworkAgent> hashmap = new HashMap<>();
        hashmap.put(Difficulty.HARDCORE, new NeatPatchworkAgent(neatAgent));
        new IOController(new GameController()).saveAgentMapFromPath(hashmap, filepath);
    }

    /**
     * Tests the saveAgentMap
     */
    @Test(expected = IllegalArgumentException.class)
    public void testsaveAgentMapToPathNullPath() {
        SerializableFunction<double[], double[]> function1 = output -> (output[0] < 0.5 ? new double[]{1} : new double[]{0});
        GameStateAgent gameStateAgent = new GameStateAgent(function1);
        Map<Difficulty, PatchworkAgent> hashmap = new HashMap<>();
        hashmap.put(Difficulty.HARDCORE, new NeatPatchworkAgent(gameStateAgent));
        new IOController(new GameController()).saveAgentMapFromPath(hashmap, null);
    }

    /**
     * Tests the saveAgentMap
     */
    @Test(expected = IllegalArgumentException.class)
    public void testsaveAgentMapToPathNullHashmap() {
        String filepath = "src/bruh.ser";
        SerializableFunction<double[], double[]> function1 = output -> (output[0] < 0.5 ? new double[]{1} : new double[]{0});
        GameStateAgent neatAgent = new GameStateAgent(function1);
        new IOController(new GameController()).saveAgentMapFromPath(null, filepath);
    }

    /**
     * Tests the saveAgentMap
     */
    @Test(expected = IllegalArgumentException.class)
    public void testsaveAgentMapToPathNullMap() {
        new IOController(new GameController()).saveAgentMapFromPath(null, null);
    }

    /**
     * Tests the loadAgentMap
     */
    @Test
    public void testloadAgentMap() {
        String filepath = "src/bruh.ser";
        SerializableFunction<double[], double[]> function1 = output -> (output[0] < 0.5 ? new double[]{1} : new double[]{0});
        GameStateAgent neatAgent = new GameStateAgent(function1);
        NeatPatchworkAgent agent = new NeatPatchworkAgent(neatAgent);
        Map<Difficulty, PatchworkAgent> hashmap = new HashMap<>();
        hashmap.put(Difficulty.HARDCORE, agent);
        new IOController(new GameController()).saveAgentMapFromPath(hashmap, filepath);
        Map<Difficulty, PatchworkAgent> hashmap1;
        hashmap1 = new IOController(new GameController()).loadAgentMapFromPath(filepath).orElseThrow();
        assertNotNull(hashmap1);
    }
    /**
     * Tests the loadAgentMap
     */
    @Test(expected = IllegalArgumentException.class)
    public void testloadAgentMapFromNullPath() {
        String filepath = null;
        SerializableFunction<double[], double[]> function1 = output -> (output[0] < 0.5 ? new double[]{1} : new double[]{0});
        GameStateAgent neatAgent = new GameStateAgent(function1);
        Map<Difficulty, PatchworkAgent> hashmap = new HashMap<>();
        hashmap.put(Difficulty.HARDCORE, new NeatPatchworkAgent(neatAgent));
        new IOController(new GameController()).saveAgentMapFromPath(hashmap, filepath);
        Map<Difficulty, PatchworkAgent> hashmap1;
        hashmap1 = new IOController(new GameController()).loadAgentMapFromPath(filepath).orElseThrow();
        assertNotNull(hashmap1);
    }


    /**
     * tests if it correctly fails when there's no path
     */
    @Test(expected = IllegalArgumentException.class)
    public  void testNullPathloadAgentMap(){
        new IOController(new GameController()).loadAgentMapFromPath(null);
    }
    /**
     * tests if it correctly fails when the file doesn't match
     */
    @Test
    public  void testWrongPathloadAgentMap(){
        try {
            new IOController(new GameController()).loadAgentMapFromPath("resources/patchwork-pieces.csv");
        }
        catch (Exception ex){
            assertTrue(true);
        }
        assertFalse(false);
    }

    /**
     * Tests the save neat to Path
     */
    @Test
    public void testSaveNeatToPath() {
        Task task = new SoloTask(19, 1, Deterministic.YES_EXCEPTION, function -> 4d);

        Settings settings = new TurnBenchmarkSettings();
        NeatData neatData = new Neat(settings, task,1040d,null).getData();
        new IOController(new GameController()).saveNeatDataToPath("resources/neatTest_01.ser", neatData);
    }

    /**
     * Tests the save neat to Path with Exception
     */
    @Test
    public void testSaveNeatToPathException() {
        Task task = new SoloTask(19, 1, Deterministic.YES_EXCEPTION, function -> 4d);

        Settings settings = new TurnBenchmarkSettings();
        NeatData neatData = new Neat(settings, task,1040d,null).getData();
        new IOController(new GameController()).saveNeatDataToPath(errorString, neatData);
    }

    /**
     * Tests the loadNeatToPath
     */
    @Test
    public void testLoadNeatToPath() {
        assertNotNull(new IOController(new GameController()).loadNeatFromPath("resources/neatTest_01.ser"));
    }

    /**
     * Tests the loadNeatToPath with Exception
     */
    @Test
    public void testLoadNeatToPathException() {
        new IOController(new GameController()).loadNeatFromPath(errorString);
    }

    /**
     * @author Leon
     */
    @Test
    public void tesPatchOrderDeterministic() {
        IOController ioController = new IOController(null);
        List<Patch> patchList1 = ioController.importCSV();
        List<Patch> patchList2 = ioController.importCSV();
        assertEquals(patchList1, patchList2);
    }

    /**
     * Tests the load method
     */
    @Test
    public void testLoad() {
        testSave();
        String test = IOController.load(System.getProperty("user.dir") + "\\test_save.ser");
        System.out.println(test);
    }

    /**
     * Tests the load method with Exception
     */
    @Test
    public void testLoadException() {
        testSave();
        String test = IOController.load(System.getProperty("user.dir") + "\\nope.ser");
        System.out.println(test);
    }

    /**
     * Tests the save method
     */
    @Test
    public void testSave() {
        String path = System.getProperty("user.dir") + "\\test_save.ser";
        IOController.save("Test", path);
    }

    /**
     * Tests the save method with Exception
     */
    @Test
    public void testSaveException() {
        IOController.save("Test", errorString);
    }


}
