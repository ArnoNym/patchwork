package controller;

import ai.decisionTree.Turn;

import org.junit.Before;
import org.junit.Test;

import model.*;

import static org.junit.Assert.*;

import java.io.File;
import java.util.*;

/**
 * This class tests the methods
 * of the IOController
 */
public class AIControllerTest {

    private GameController gameController;
    private IOController ioController;
    private AIController aiController;

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {
        gameController = new GameController();
        ioController = new IOController(gameController);
        aiController = new AIController(gameController);

        Player player1 = new Player("Player1", null);
        Player player2 = new Player("Player1", null);

        List<Patch> patchList = ioController.importCSV();
        GameBoard gameBoard = new GameBoard(patchList);
        GameState gameState = new GameState(player1, player2, gameBoard);
        gameController.getGame().setGameState(gameState);
    }

    /**
     * Tests the Constructor
     */
    @Test
    public void testConstructor() {
        // File (or directory) with old name
        File file = new File("resources/agentMap.ser");
        // File (or directory) with new name
        File file2 = new File("resources/agentMap_temp.ser");

        // Rename file (or directory)
        boolean success = file.renameTo(file2);

        if (success) {
            AIController aiController = new AIController(new GameController());
        }
        else {
            System.out.println("Rename didnt work");
            return;
        }

        file.renameTo(new File("resources/agentMap.ser"));

    }

    /**
     * Tests the calculateHint method
     */
    @Test
    public void testCalculateHint() {
        Turn turn = aiController.calculateHint();
        String hintMove = " -  #  -  -  -  -  -  -  - \n" +
                " -  #  -  -  -  -  -  -  - \n" +
                " #  #  #  -  -  -  -  -  - \n" +
                " -  #  -  -  -  -  -  -  - \n" +
                " -  #  -  -  -  -  -  -  - \n" +
                " -  -  -  -  -  -  -  -  - \n" +
                " -  -  -  -  -  -  -  -  - \n" +
                " -  -  -  -  -  -  -  -  - \n" +
                " -  -  -  -  -  -  -  -  - ";
        assertTrue(turn.toString().contains(hintMove));
    }

    /**
     * Tests the do turn with Turn as parameter method
     */
    @Test
    public void testdoTurnWithParameter() {
        Turn turn = aiController.calculateHint();
        aiController.doTurn(turn);
        String hintMove = " -   1   -   -   -   -   -   -   -  \n" +
                " -   1   -   -   -   -   -   -   -  \n" +
                " 1   1   1   -   -   -   -   -   -  \n" +
                " -   1   -   -   -   -   -   -   -  \n" +
                " -   1   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  ";

        assertTrue(gameController.getGame().getGameState().getPassivePlayer().getBlanket().toString().contains(hintMove));
    }

    /**
     * Tests the doturn when player isn't an AI
     */
    @Test(expected = IllegalStateException.class)
    public void testdoTurn() {
        aiController.doTurn();
    }
}
