package controller;

import model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class tests the methods
 * of the GameBoardControllerTest
 */
public class GameBoardControllerTest {
    private GameBoard gameBoard;
    private GameController gameController;
    private GameState gameState;
    private GameBoardController gbc;
    private List<Patch> patches;

    /**
     * sets up the testing environment
     */
    @Before
    public void setUp() throws Exception {
        Player p1 = new Player("Spieler 1", null);
        Player p2 = new Player("Spieler 2", null);

        IOController io = new IOController(null);

        this.gameBoard = new GameBoard(io.importCSV());
        this.gameState = new GameState(p1, p2, gameBoard);
        this.gameController = new GameController();
        this.gameController.getGame().setGameState(gameState);
        this.gbc = this.gameController.getGameBoardController();
        this.patches = gameBoard.getPatches();
    }

    /**
     * tests if the list of the first 3 patches is actually the first three patches
     */
    @Test
    public void testGetAvailablePatchesFirstThree() {
        List<Patch> firstThreePatches = this.patches.stream().limit(3).collect(Collectors.toList());
        assertEquals(firstThreePatches, this.gbc.getAvailablePatches());
    }

    /**
     * tests if the list if the available patches are actually the patches after choosing an option
     */
    @Test
    public void testGetAvailablePatchesAfterChoosingAnOption() {
        List<Patch> subListPatches = new ArrayList<>(this.patches.subList(3, patches.size()));
        List<Patch> firstThreePatchesOfSubList = subListPatches.stream().limit(3).collect(Collectors.toList());
        this.gbc.removePatchAtIndex(2);
        this.gameBoard.setMeeplePos(2);
        assertEquals(firstThreePatchesOfSubList, gbc.getAvailablePatches());
    }

    /**
     * tests if the list if the available patches are actually the patches if it's the last 3 in the list
     */
    @Test
    public void testGetAvailablePatchesRemainingPatchesLessThan3() {
        int initialPatchSize = patches.size();
        List<Patch> subListPatches = new ArrayList<>(this.patches.subList(patches.size() - 1, patches.size()));
        for (int i = 0; i < initialPatchSize - 1; i++) {
            this.gbc.removePatchAtIndex(0);
        }
        assertEquals(subListPatches, gbc.getAvailablePatches());
    }

    /*
     * tests if the list if the available patches are actually the patches if it's the last 3 in the list
     *
    @Test
    public void testGetAvailablePatches_modulo() {
        patches = patches.
        gameBoard.setMeeplePos();
        List<Patch> patches1 = GameBoardController.getAvailablePatches(gameBoard);
        List<Patch> patches2 = GameBoardController.getAvailablePatches(gameBoard);

        List<Patch> subListPatches = new ArrayList<>(this.patches.subList(patches.size() - 1, patches.size()));
        for (int i = 0; i < initialPatchSize - 1; i++) {
            this.gbc.removePatchAtIndex(0);
        }
        assertEquals(subListPatches, );
    }
    */

    /**
     * tests if removing a patch works as intended
     */
    @Test
    public void testRemovePatchAtIndex() {
        int listSize = patches.size();
        this.gbc.removePatchAtIndex(2);
        assertEquals(listSize - 1, patches.size());
    }

    /**
     * tests if the number of remaining extrapatches are correct
     */
    @Test
    public void calcRemainingExtraPatches() {
        assertEquals(5, GameBoardController.calcExtraPatchesLeft(gameBoard));
    }

    /**
     * tests if the getcurrentpatchlist method actually returns the currentpatchlist
     */
    @Test
    public void testgetCurrentPatchList() {
        assertEquals(33, gameController.getGameBoardController().getCurrentPatchList().size());

        gameController.getGameBoardController().removePatchAtIndex(3);
        gameController.getGame().getGameState().getGameBoard().setMeeplePos(3);
        assertEquals(32, gameController.getGameBoardController().getCurrentPatchList().size());
    }

    /**
     * tests if the getcurrentpatchlist method actually returns the currentpatchlist
     */
    @Test
    public void testgetCurrentPatchListRemoveLastPatch() {
        assertEquals(33, gameController.getGameBoardController().getCurrentPatchList().size());

        gameController.getGameBoardController().removePatchAtIndex(32);
        gameController.getGame().getGameState().getGameBoard().setMeeplePos(32);
        boolean[] col1 = {true,false,false,false,false};
        boolean[] col2 = {true, true, true, true, false};
        boolean[] col3 = {true, false, false, false, false};
        boolean[] col4 = {false, false, false, false, false};
        boolean[] col5 = {false, false, false, false, false};
        boolean[][]patchMatrix = new boolean[][]{col1, col2, col3, col4, col5};


        for (Patch p: gameController.getGameBoardController().getCurrentPatchList()) {
            System.out.println(p);
        }
        assertArrayEquals(patchMatrix, gameController.getGameBoardController().getCurrentPatchList().iterator().next().getMatrix());
        assertEquals(32, gameController.getGameBoardController().getCurrentPatchList().size());
    }
}
