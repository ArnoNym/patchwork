package controller;

import Resources.Constants;
import ai.hardcoded.raphael.GreedyFirstFit;
import ai.hardcoded.raphael.StupidAI;
import model.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.*;

/**
 * This class tests the methods
 * of the PlayerControllerTest
 */
public class PlayerControllerTest {
    private GameBoard gameBoard;
    private GameController gameController;
    private GameState gameState;
    private GameBoardController gbc;
    private PlayerController playerController;
    private List<Patch> patches;
    private PatchTuple pt;

    /**
     * sets up the testing environment
     */
    @Before
    public void setUp() throws Exception {
        Player p1 = new Player("stupidAI", null);
        Player p2 = new Player("greedyFirstFit", null);

        IOController io = new IOController(null);
        this.gameBoard = new GameBoard(io.importCSV());


        //this.gameState = new GameState(p1,p2,randomised(gameBoard));
        this.gameState = new GameState(p1, p2, gameBoard);

        this.gameController = new GameController();
        this.gameController.getGame().setGameState(gameState);
        this.gbc = this.gameController.getGameBoardController();
        this.playerController = this.gameController.getPlayerController();
        this.patches = gameBoard.getPatches();

        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {true, false, false, false, true};
        boolean[][]rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5, 1);

        VecTwo tlc = new VecTwo(5, 5);
        this.pt = new PatchTuple(p, Rotation._0, tlc, true);
    }

    private GameBoard randomised(GameBoard gameBoard) {
        List<Patch> rand = Constants.shufflePatches(gameBoard.getPatches());
        return new GameBoard(rand);
    }

    //########################################################
    //unit Tests anfang
    /**
     * tests if it correctly calculates a score
     */
    @Test
    public void testCalculateScore() {
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, true};
        boolean[][]rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 2, 5, 1);

        VecTwo tlc = new VecTwo(5, 5);
        this.pt = new PatchTuple(p, Rotation._0, tlc, true);

        Player p1 = gameController.getGame().getGameState().getActivePlayer();
        p1.setButtonCount(10);
        List<PatchTuple> ptList = new LinkedList<>();
        ptList.add(pt);
        p1.getBlanket().setPatchTuples(ptList);
        p1.setSpecialToken(true);
        assertEquals(-135, PlayerController.calculateScore(p1));
    }

    /**
     * tests if patches are correctly inserted and taken
     */
    @Test
    public void testTakePatchAndInsert() {
        this.pt.setPatch(gameController.getGame().getGameState().getGameBoard().getPatches().iterator().next());
        assertFalse(playerController.takePatchAndInsert(this.pt, false,false));

        this.pt.setPatch(gameController.getGame().getGameState().getGameBoard().getPatches().iterator().next());
        gameController.getGame().getGameState().getActivePlayer().setButtonCount(0);
        assertFalse(playerController.takePatchAndInsert(this.pt, true,false));

        this.pt.setPatch(gameController.getGame().getGameState().getGameBoard().getPatches().iterator().next());
        gameController.getGame().getGameState().getActivePlayer().setButtonCount(100);
        gameController.getGame().getGameState().getGameBoard().getFields().get(2).setSpecialPatch(true);
        assertTrue(playerController.takePatchAndInsert(this.pt, true,true));
    }

    /**
     * tests if patches are correctly inserted and taken and there's a specialtoken
     */
    @Test
    public void testTakePatchAndInsertButSpecialToken() {
        gameController.getGame().getGameState().getActivePlayer().setButtonCount(100);
        //gameController.getGame().getGameState().getActivePlayer().setBlanket();

        List<PatchTuple> tuples = new ArrayList<>();
        Blanket blanket = new Blanket();


        // Create 4 x 4 True Patch at (0, 0)
        boolean[][] matrix = new boolean[5][5];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = true;
            }
        }
        Patch patch = new Patch(matrix, 0, 0, 0);
        PatchTuple tuple = new PatchTuple(patch, Rotation._0, new VecTwo(0, 0), false);
        tuples.add(tuple);


        // Create 3 x 4 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = true;
            }
        }
        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(0, 4), false);
        tuples.add(tuple);


        // Create 4 x 3 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = true;
            }
        }
        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(4, 0), false);
        tuples.add(tuple);

        // Create 3 x 3 True Patch
        matrix = new boolean[5][5];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = true;
            }
        }
        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(4, 4), false);
        tuples.add(tuple);

        blanket.setPatchTuples(tuples);
        gameController.getGame().getGameState().getActivePlayer().setBlanket(blanket);
        assertTrue(gameController.getBlanketController().specialTokenReady());

        gameController.getGame().getGameState().getGameBoard().getFields().get(2).setSpecialPatch(true);

        // Create 1 x 1 True Patch
        matrix = new boolean[5][5];
        matrix[0][0] = true;

        patch = new Patch(matrix, 0, 0, 0);
        tuple = new PatchTuple(patch, Rotation._0, new VecTwo(0, 7), false);

        assertTrue(GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()));
        playerController.takePatchAndInsert(tuple, true,true);
        assertFalse(GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()));
    }


    /**
     * tests if the takepatch and insert method correctly gives an illegalstateexception
     */
    @Test(expected = IllegalStateException.class)
    public void testTakePatchAndInsertButException() {
        gameController.getGame().getGameState().getGameBoard().setMeeplePos(10);
        playerController.takePatchAndInsert(this.pt, true,true);
    }


    /**
     * tests if the winner gets determined correctly
     */
    @Test
    public void testCalculateWinner() {
        gameController.getGame().getGameState().getActivePlayer().setButtonCount(9001);
        assertEquals(gameController.getGame().getGameState().getActivePlayer(),
                PlayerController.calculateWinner(gameController.getGame().getGameState()));
    }

    /**
     * tests if a buyable patch is buyable
     */
    @Test
    public void testCanBuyPatch() {
        assertTrue(playerController.canBuyPatch(pt.getPatch()));
    }


    /**
     * tests if the position of the player gets calc'ed correctly
     */
    @Test
    public void testCalcTime() {
        gameController.getGame().getGameState().getActivePlayer().setPos(10);
        assertEquals(42, PlayerController.calcTime(gameController.getGame().getGameState().getActivePlayer()));
    }

    /**
     * tests the takepatchandinsert method with just patchtuple as an argument
     */
    @Test
    public void testTakePatchAndInsertButOnlyOneArgument() {
        this.pt.setPatch(gameController.getGame().getGameState().getGameBoard().getPatches().iterator().next());
        playerController.takePatchAndInsert(this.pt);
    }

    /**
     * tests the calcIncomeButtonsLeftCount method
     */
    @Test
    public void calcIncomeButtonsLeftCount() {
        assertEquals(9,
                PlayerController.calcIncomeButtonsLeftCount(gameController.getGame().getGameState().getActivePlayer()));
    }

    /**
     * tests the isPassingExtraPatch if there's an extra Patch method
     */
    @Test
    public void testIsPassingExtraPatchTrue(){
        gameController.getGame().getGameState().getGameBoard().getFields().get(1).setSpecialPatch(true);
        assertTrue(playerController.isPassingExtraPatch(gameController.getGame().getGameState().getGameBoard().getPatches().iterator().next()));
    }

    /**
     * tests the isPassingExtraPatch if there isn't an extra Patch method
     */
    @Test
    public void testIsPassingExtraPatchFalse(){
        assertFalse(playerController.isPassingExtraPatch(gameController.getGame().getGameState().getGameBoard().getPatches().iterator().next()));
    }


    //unit tests ende
    //########################################################


}
