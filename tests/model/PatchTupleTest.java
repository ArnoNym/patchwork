package model;

import controller.GameController;
import controller.IOController;
import model.*;

import static org.hamcrest.CoreMatchers.instanceOf;

import org.junit.Before;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Raphael
 *
 *  test methods for the PatchTuple class
 */
public class PatchTupleTest {
    boolean[][] rows;
    GameController gameController;
    PatchTuple pt;

    /**
     * sets up the testing environment
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, true};
        this.rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5, 0);

        VecTwo tlc = new VecTwo(5, 5);
        this.pt = new PatchTuple(p, Rotation._0, tlc, true);
        System.out.println(p);
        System.out.println(pt);
        this.gameController = new GameController();
    }

    /**
     * tests contructor with null patch
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNullPatch() {
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, true};
        this.rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5, 0);

        VecTwo tlc = new VecTwo(5, 5);
        PatchTuple lul = new PatchTuple(null, Rotation._0, tlc, true);
    }

    /**
     * tests contructor with null rotation
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorRotationNull() {
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, true};
        this.rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5, 0);

        VecTwo tlc = new VecTwo(5, 5);
        PatchTuple lul = new PatchTuple(p, null, tlc, true);

    }

    /**
     * tests contructor with null VecTwo
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNullposTopLeftCorner() {
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, true};
        this.rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5, 0);

        VecTwo tlc = new VecTwo(5, 5);
        PatchTuple lul = new PatchTuple(p, Rotation._0, null, false);

    }

    /**
     * tests the getpatch method
     */
    @Test
    public void testGetPatch() {
        assertThat(pt.getPatch(), instanceOf(Patch.class));
    }

    /**
     * tests the setpatch method
     */
    @Test
    public void testSetPatch() {
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, true};
        boolean[] col2 = {false, false, false, false, false};
        boolean[] col3 = {false, false, true, false, false};
        boolean[] col4 = {false, false, false, true, true};

        boolean[][] rowsUno = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p1 = new Patch(rowsUno, 5, 5, 0);
        pt.setPatch(p1);
        assertEquals(p1, pt.getPatch());
    }

    /**
     * tests the setnullpatch method
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetNullPatch() {
        pt.setPatch(null);
    }

    /**
     * tests the getrotation method
     */
    @Test
    public void testGetRotation() {
        assertThat(pt.getRotation(), instanceOf(Rotation.class));
    }

    /**
     * tests the setrotation method
     */
    @Test
    public void testSetRotation() {
        pt.setRotation(Rotation._90);
        assertEquals(pt.getRotation(), Rotation._90);
    }

    /**
     * tests the setnullrotation method
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetNullRotation() {
        pt.setRotation(null);
    }

    /**
     * tests the getpostopleftcorner method
     */
    @Test
    public void testGetPosTopLeftCorner() {
        assertThat(pt.getPosTopLeftCorner(), instanceOf(VecTwo.class));
    }

    /**
     * tests the setpostopleftcorner method
     */
    @Test
    public void testSetPosTopLeftCorner() {
        VecTwo tlc = new VecTwo(1, 1);
        pt.setPosTopLeftCorner(tlc);
        assertEquals(pt.getVectorList().iterator().next(), tlc);
    }

    /**
     * tests the setpostopleftcorner with illegal argument
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetPosTopLeftCornerNull() {
        pt.setPosTopLeftCorner(null);
    }

    /**
     * tests the getIsflipped method
     */
    @Test
    public void testGetIsFlipped() {
        assertThat(pt.isFLipped(), instanceOf(boolean.class));
    }

    /**
     * tests the setIsflipped method
     */
    @Test
    public void testSetIsFlipped() {
        pt.setFLipped(false);
        assertEquals(pt.isFLipped(), false);
    }

    /**
     * tests the getvectorlist method
     */
    @Test
    public void testGetVectorList() {
        assertNotNull(pt.getVectorList());
        pt.setFLipped(true);
        assertNotNull(pt.getVectorList());
        pt.setFLipped(false);
        assertNotNull(pt.getVectorList());
        pt.setRotation(Rotation._0);
        assertNotNull(pt.getVectorList());
        pt.setRotation(Rotation._90);
        assertNotNull(pt.getVectorList());
        pt.setPosTopLeftCorner(new VecTwo(0, 0));
        assertNotNull(pt.getVectorList());
        pt.setPosTopLeftCorner(new VecTwo(3, 3));
        assertNotNull(pt.getVectorList());
    }

    /**
     * tests the toString method
     */
    @Test
    public void testToString() {
        assertNotNull(pt.toString());
    }

    /**
     * tests the getMatrixRepresentation method
     */
    @Test
    public void testgetMatrixRepresentation() {
        assertThat(pt.getMatrixRepresentation(), instanceOf(boolean[][].class));
    }

    /**
     * tests the clone method
     */
    @Test
    public void testClone() {
        PatchTuple toClone = pt.clone();
        assertTrue(toClone != pt);
        assertEquals(pt, toClone);
    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        IOController ioController = new IOController(null);
        List<Patch> patchList = ioController.importCSV();
        PatchTuple tuple1 = new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false);

        assertEquals(tuple1, tuple1);
        assertNotEquals(tuple1, null);
        assertNotEquals(tuple1, new Object());

        PatchTuple tuple2 = new PatchTuple(patchList.get(0), Rotation._90, new VecTwo(0, 0), false);
        assertNotEquals(tuple1, tuple2);
        assertNotEquals(tuple1.hashCode(), tuple2.hashCode());

        tuple2 = new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(1, 1), false);
        assertNotEquals(tuple1, tuple2);
        assertNotEquals(tuple1.hashCode(), tuple2.hashCode());

        tuple2 = new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false);
        assertEquals(tuple1, tuple2);
        assertEquals(tuple1.hashCode(), tuple2.hashCode());
    }

}
