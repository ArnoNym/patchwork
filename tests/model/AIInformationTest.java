package model;

import Resources.Constants;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the AIInformation
 */
public class AIInformationTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        AIInformation a2 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);

        assertTrue(a1.equals(a2) && a2.equals(a1));
        assertEquals(a1.hashCode(), a2.hashCode());
        assertTrue(a1.equals(a1) && a2.equals(a2));
        assertFalse(a1.equals(null));

        a1 = new AIInformation(true, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        a2 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);

        assertNotEquals(a1, a2);

        a2 = new AIInformation(false, Difficulty.MISSIONARY, SimulationSpeed.NOSTOP);
        assertNotEquals(a1, a2);

        a2 = new AIInformation(false, Difficulty.SOFTCORE, SimulationSpeed.NOSTOP);
        assertNotEquals(a1, a2);

        a2 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.CONFIRMATION);
        assertNotEquals(a1, a2);

        a2 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.DELAYED);
        assertNotEquals(a1, a2);
    }

    /**
     * Tests the setDifficulty method
     */
    @Test
    public void testSetDifficulty() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        a1.setDifficulty(Difficulty.SOFTCORE);
        assertEquals(a1.getDifficulty(), Difficulty.SOFTCORE);

        a1.setDifficulty(Difficulty.HARDCORE);
        assertEquals(a1.getDifficulty(), Difficulty.HARDCORE);

        a1.setDifficulty(Difficulty.MISSIONARY);
        assertEquals(a1.getDifficulty(), Difficulty.MISSIONARY);
    }

    /**
     * Tests the setDifficulty method with Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetDifficultyWithException() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        a1.setDifficulty(null);
    }

    /**
     * Tests the setSimulationSpeed method with Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetSimulationSpeedWithException() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        a1.setSimulationSpeed(null);
    }

    /**
     * Tests the setSimulationSpeed method
     */
    @Test
    public void testSetSimulationSpeed() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        a1.setSimulationSpeed(SimulationSpeed.DELAYED);
        assertEquals(a1.getSimulationSpeed(), SimulationSpeed.DELAYED);

        a1.setSimulationSpeed(SimulationSpeed.NOSTOP);
        assertEquals(a1.getSimulationSpeed(), SimulationSpeed.NOSTOP);

        a1.setSimulationSpeed(SimulationSpeed.CONFIRMATION);
        assertEquals(a1.getSimulationSpeed(), SimulationSpeed.CONFIRMATION);
    }

    /**
     * Tests the clone method
     */
    @Test
    public void testClone() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        AIInformation a2 = a1.clone();
        assertEquals(a1, a2);
    }

    /**
     * Tests the isProvidingFeedback method
     */
    @Test
    public void testIsProvidingFeedback() {
        AIInformation a1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
        a1.setProvidingFeedback(true);
        assertTrue(a1.isProvidingFeedback());
    }
}
