package model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the VecTwo
 */
public class VecTwoTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        VecTwo vecTwo1 = new VecTwo(0, 0);
        VecTwo vecTwo2 = new VecTwo(0, 0);

        assertEquals(vecTwo1, vecTwo1);
        assertEquals(vecTwo1.hashCode(), vecTwo2.hashCode());
        assertEquals(vecTwo1, vecTwo2);

        assertNotEquals(vecTwo1, null);
        assertNotEquals(vecTwo1, new Object());

        assertNotEquals(vecTwo1, new VecTwo(1, 0));
        assertNotEquals(vecTwo1, new VecTwo(0, 1));
    }

    /**
     * Tests the validVec2 method
     */
    @Test
    public void testValidVec2() {
        VecTwo vecTwo1 = new VecTwo(0, 0);
        assertTrue(vecTwo1.validVec2());

        vecTwo1 = new VecTwo(10, 0);
        assertFalse(vecTwo1.validVec2());

        vecTwo1 = new VecTwo(0, 10);
        assertFalse(vecTwo1.validVec2());

        vecTwo1 = new VecTwo(-1, 0);
        assertFalse(vecTwo1.validVec2());

        vecTwo1 = new VecTwo(0, -1);
        assertFalse(vecTwo1.validVec2());
    }

    /**
     * Tests the validVec2 method
     */
    @Test
    public void testEuclidianDistance() {
        VecTwo vecTwo1 = new VecTwo(0, 0);

        VecTwo vecTwo2 = new VecTwo(10, 2);
        assertNotNull(VecTwo.euclidanDistance(vecTwo1,vecTwo2));

    }

    /**
     * @author Leon
     */
    @Test
    public void testEuclideanDistance_2() {
        VecTwo vecTwo1 = new VecTwo(0, 0);
        VecTwo vecTwo2 = new VecTwo(8, 8);
        VecTwo vecTwo3 = new VecTwo(8, 0);
        VecTwo vecTwo4 = new VecTwo(0, 8);
        assertEquals(VecTwo.euclidanDistance(vecTwo1, vecTwo2), VecTwo.euclidanDistance(vecTwo3, vecTwo4), 0.0);
    }

    /**
     * @author Leon
     */
    @Test
    public void testEuclideanDistance_3() {
        VecTwo vecTwo1 = new VecTwo(0, 0);
        VecTwo vecTwo2 = new VecTwo(1, 1);
        double euc_1_2 = VecTwo.euclidanDistance(vecTwo1, vecTwo2);
        System.out.println(euc_1_2);
        VecTwo vecTwo3 = new VecTwo(7, 7);
        VecTwo vecTwo4 = new VecTwo(8, 8);
        double euc_3_4 = VecTwo.euclidanDistance(vecTwo3, vecTwo4);
        System.out.println(euc_3_4);
        assertEquals(euc_1_2, euc_3_4, 0.0);
    }

    /**
     * Tests the toString method
     */
    @Test
    public void testToString() {
        VecTwo vecTwo1 = new VecTwo(0, 0);
        assertEquals(vecTwo1.toString(), "(0,0)");
    }


}
