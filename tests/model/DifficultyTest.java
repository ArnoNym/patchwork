package model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This class tests the methods
 * of Difficulty
 */
public class DifficultyTest {
    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the getGuiName method
     */
    @Test
    public void testToString() {
        assertEquals(Difficulty.HARDCORE.getGuiName(), "Hardcore");
        assertEquals(Difficulty.GREEDY_FIRST_FIT.getGuiName(), "Softcore");
        assertEquals(Difficulty.MISSIONARY.getGuiName(), "Missionary");
        assertEquals(Difficulty.STUPID.getGuiName(), "Softcore");
        assertEquals(Difficulty.SOFTCORE.getGuiName(), "Softcore");
    }
}
