package model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the Player
 */
public class PlayerTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter", null);

        assertEquals(p1.hashCode(), p2.hashCode());
        assertEquals(p1, p2);

        assertNotEquals(p1, null);
        assertNotEquals(p1, new Object());

        p1.setSpecialToken(true);
        assertNotEquals(p1, p2);
        p1.setSpecialToken(false);

        p1.setButtonCount(1);
        p2.setButtonCount(0);
        assertNotEquals(p1, p2);
    }

    /**
     * Tests the Constructor
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor() {
        Player p1 = new Player(null, null);
    }
}
