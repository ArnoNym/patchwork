package model;

import controller.BlanketController;
import controller.IOController;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotEquals;

/**
 * This class tests the methods
 * of the Blanket
 */
public class BlanketTest {
    /**
     * sets up the testing environment
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * tests setpatchtuples method
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetPatchTuples(){
        Blanket blanket = new Blanket();
        blanket.setPatchTuples(null);
    }

    /**
     * tests the intmatrix representation
     */
    @Test(expected =  IllegalStateException.class)
    public void testIntMatrixRep(){
            Blanket blanket = new Blanket();

            boolean[][] patchMatrix = new boolean[5][5];
            for (boolean[] b : patchMatrix)
                Arrays.fill(b, true);
            Patch patch = new Patch(patchMatrix, 1, 1, 1);
            assertTrue(BlanketController.canPlacePatch(blanket, patch));

            List<PatchTuple> tuples = new ArrayList<>();
            // Create 4 x 4 True Patch at (0, 0)
            boolean[][] matrix = new boolean[5][5];
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    matrix[i][j] = true;
                }
            }
            Patch patchd = new Patch(matrix, 0, 0, 0);
            PatchTuple tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(0, 0), false);
            tuples.add(tuple);
            // Create 3 x 4 True Patch
            matrix = new boolean[5][5];
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 3; j++) {
                    matrix[i][j] = true;
                }
            }
            patchd = new Patch(matrix, 0, 0, 0);
            tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(0, 4), false);
            tuples.add(tuple);
            // Create 4 x 3 True Patch
            matrix = new boolean[5][5];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++) {
                    matrix[i][j] = true;
                }
            }
            patchd = new Patch(matrix, 0, 0, 0);
            tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(4, 0), false);
            tuples.add(tuple);

            // Create 3 x 3 True Patch
            matrix = new boolean[5][5];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    matrix[i][j] = true;
                }
            }
            patchd = new Patch(matrix, 0, 0, 0);
            tuple = new PatchTuple(patchd, Rotation._0, new VecTwo(4, 4), false);
            tuples.add(tuple);
            blanket.setPatchTuples(tuples);
            blanket.getIntMatrixRepresentation();
    }

    /**
     * @author Raphael
     */
    @Test
    public void integrationTest() {
        boolean [] col00 = {false, false, true, false, false};
        boolean [] col01 = {false, true, true, false, false};
        boolean [] col02 = {true, true, true, false, false};
        boolean [] col03 = {false, false, false, false, false};
        boolean [] col04 = {false, false, false, false, false};
        boolean [] [] rows0 = {col00, col01, col02, col03, col04};
        Patch p0 = new Patch(rows0, 0, 0, 0);
        VecTwo v0 = new VecTwo(0,2);
        PatchTuple pt0 = new PatchTuple(p0, Rotation._0, v0, false);

        boolean [] col10 = {false, false, false, false, false};
        boolean [] col11 = {false, false, false, false, false};
        boolean [] col12 = {false, false, true, false, false};
        boolean [] col13 = {false, false, true, false, false};
        boolean [] col14 = {false, false, true, false, false};
        boolean [] [] rows1 = {col10, col11, col12, col13, col14};
        Patch p1 = new Patch(rows1, 0, 0, 0);
        VecTwo v1 = new VecTwo(4,0);
        PatchTuple pt1 = new PatchTuple(p1, Rotation._0, v1, true);

        boolean [] col20 = {false, false, false, false, false};
        boolean [] col21 = {true, true, true, false, false};
        boolean [] col22 = {false, false, true, false, false};
        boolean [] col23 = {false, false, true, false, false};
        boolean [] col24 = {false, false, false, false, false};
        boolean [] [] rows2 = {col20, col21, col22, col23, col24};
        Patch p2 = new Patch(rows2, 0, 0, 0);
        VecTwo v2 = new VecTwo(6,6);
        PatchTuple pt2 = new PatchTuple(p2, Rotation._0, v2, true);

//        System.out.println(pt0);
//        System.out.println(pt1);
//        System.out.println(pt2);

        List<PatchTuple> ls = new LinkedList<>();
        ls.add(pt0); ls.add(pt1); ls.add(pt2);

        Blanket blankedTest = new Blanket();
        if (BlanketController.canPlacePatchTuple(blankedTest, pt0)){
            System.out.println("fehler");
        }
        blankedTest.setPatchTuples(ls);

        System.out.println(blankedTest);
    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        Blanket b1 = new Blanket();
        Blanket b2 = new Blanket();

        assertEquals(b1, b1);
        assertEquals(b1, b2);
        assertNotEquals(b1, null);
        assertNotEquals(b1, new Object());
    }

    /**
     * Tests the toString method
     */
    @Test
    public void testToString() {
        Blanket b1 = new Blanket();
        String compare =
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n";

        assertEquals(b1.toString(), compare);

        IOController ioController = new IOController(null);
        List<Patch> patchList = ioController.importCSV();
        List<PatchTuple> list = new ArrayList<>();
        list.add(new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false));
        b1.setPatchTuples(list);

        compare =
                " 1   1   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n";

        assertEquals(b1.toString(), compare);

        list.clear();
        int row = 0;
        int col = 0;
        while (true) {
            if (col > 6) {
                col = 0;
                row += 1;
            }
            if (row > 7) {
                break;
            }
            list.add(new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(row, col), false));
            col += 2;
        }

        compare =
                " 1   1   2   2   3   3   4   4   -  \n" +
                " 5   5   6   6   7   7   8   8   -  \n" +
                " 9   9  10  10  11  11  12  12   -  \n" +
                "13  13  14  14  15  15  16  16   -  \n" +
                "17  17  18  18  19  19  20  20   -  \n" +
                "21  21  22  22  23  23  24  24   -  \n" +
                "25  25  26  26  27  27  28  28   -  \n" +
                "29  29  30  30  31  31  32  32   -  \n" +
                " -   -   -   -   -   -   -   -   -  \n";
        assertEquals(compare, b1.toString());
    }

    /**
     * Tests the toString method
     */
    @Test
    public void testAddPatchTuple() {
        Blanket b1 = new Blanket();
        IOController ioController = new IOController(null);
        List<Patch> patchList = ioController.importCSV();
        b1.addPatchTuple(new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false));
        assertEquals(b1.getPatchTuples().size(), 1);
    }

    /**
     * Tests the calcIncome Method with PatchTuples in the blanket
     */
    @Test
    public void testCalcIncome(){
        Blanket blanket = new Blanket();
        IOController ioController = new IOController(null);
        List<Patch> patchList = ioController.importCSV();
        Patch patch = patchList.get(1);
        blanket.addPatchTuple(new PatchTuple(patch, Rotation._0, new VecTwo(0, 0), false));
        assertEquals(2, blanket.calcIncome());
    }

}