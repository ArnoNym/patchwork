package model;

import Resources.Constants;
import controller.IOController;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the Field
 */
public class FieldTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the toString method
     */
    @Test
    public void testToString() {
        Field field = new Field(false, false);
        assertEquals(field.toString(), "isButtonField: false, hasExtraPatch: false");
    }

    /**
     * Tests the clone method
     */
    @Test
    public void testClone() {
        Field field1 = new Field(false, false);
        Field field2 = field1.clone();

        assertEquals(field1, field2);
    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        Field field1 = new Field(false, false);
        Field field2 = new Field(false, false);

        assertEquals(field1, field1);
        assertEquals(field1.hashCode(), field2.hashCode());
        assertEquals(field1, field2);

        field2 = new Field(true, true);
        assertNotEquals(field1, field2);
        field2 = new Field(false, true);
        assertNotEquals(field1, field2);
        field2 = new Field(true, false);
        assertNotEquals(field1, field2);
        assertNotEquals(field1, null);
        assertNotEquals(field1, new Object());
    }


}












