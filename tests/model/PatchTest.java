package model;

import controller.IOController;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * test methods for the Patch class
 */
public class PatchTest {

    /**
     * tests the rotation
     */
    @Test
    public void testRotation_print_1() {
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, true, false, false, false};
        boolean [] col2 = {false, false, true, false, false};
        boolean [] col3 = {false, false, false, true, false};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5 , 0);
        System.out.println(p);
        Patch p1 = p.flipPatch().rotatePatch(Rotation._180);
        System.out.println(p1);
    }

    /**
     * @author Leon
     */
    @Test
    public void testFlipAndRotate_1() {
        boolean [] col0 = {false, false, false, false, false};
        boolean [] col1 = {false, true, false, false, false};
        boolean [] col2 = {false, false, true, false, true};
        boolean [] col3 = {true, true, true, true, true};
        boolean [] col4 = {false, false, false, false, false};
        boolean [] [] rows = {col0, col1, col2, col3, col4};

        Patch patch = new Patch(rows, 5, 5 , 0);
        Patch flippedPatch = patch.flipPatch();

        assertNotEquals(patch, flippedPatch);
        assertEquals(patch, flippedPatch.flipPatch());
        assertEquals(patch, flippedPatch.flipPatch().flipPatch().flipPatch());

        assertEquals(patch, patch.rotatePatch(Rotation._0));
        assertNotEquals(patch, patch.rotatePatch(Rotation._90));
        assertNotEquals(patch, patch.rotatePatch(Rotation._180));
        assertNotEquals(patch, patch.rotatePatch(Rotation._270));
        assertEquals(patch, patch.rotatePatch(Rotation._90).rotatePatch(Rotation._270));
        assertEquals(patch, patch.rotatePatch(Rotation._180).rotatePatch(Rotation._180));
    }

    /**
     * tests the getpngstring method
     */
    @Test
    public void testGetPngString(){
        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        patchMatrix[0][0] = false;
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        assertNotNull(patch.getPngString());
        System.out.println(patch.getPngString());
    }

    /**
     * tests the getmatrix method
     */
    @Test
    public void testGetMatrix(){
        boolean[][] patchMatrix = new boolean[5][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
        assertEquals(patchMatrix,patch.getMatrix());
    }

    /**
     * tests the patch constructor with an illegal argument
     */
    @Test(expected=IllegalArgumentException.class)
    public void testWrongRows(){
        boolean[][] patchMatrix = new boolean[4][5];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
    }

    /**
     * tests the patch constructor with an illegal argument
     */
    @Test(expected=IllegalArgumentException.class)
    public void testWrongLength(){
        boolean[][] patchMatrix = new boolean[5][4];
        for (boolean[] b : patchMatrix)
            Arrays.fill(b, true);
        Patch patch = new Patch(patchMatrix, 1, 1, 1);
    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {

        Patch p1 = new Patch(new boolean[5][5], 0, 0, 0);
        Patch p2 = new Patch(new boolean[5][5], 0, 0, 0);

        assertEquals(p1, p2);
        assertNotEquals(p1, null);
        assertNotEquals(p1, new Object());
        assertEquals(p1, p2);

        assertEquals(p1.hashCode(), p2.hashCode());
        p2 = new Patch(new boolean[5][5], 1, 0, 0);
        assertNotEquals(p1, p2);
        p2 = new Patch(new boolean[5][5], 0, 1, 0);
        assertNotEquals(p1, p2);
        p2 = new Patch(new boolean[5][5], 0, 0, 1);
        assertNotEquals(p1, p2);
        boolean[][] matrix = new boolean[5][5];
        matrix[0][0] = true;
        p2 = new Patch(matrix, 0, 0, 0);
        assertNotEquals(p1, p2);
    }

    /**
     * Tests the rotatePatch method with Error
     */
    @Test(expected = IllegalStateException.class)
    public void testRotatePatch() {
        Patch p1 = new Patch(new boolean[5][5], 0, 0, 0);
        p1.rotatePatch(null);
    }

}
