package model;

import Resources.Constants;
import controller.IOController;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the GameBoard
 */
public class GameBoardTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        GameBoard gb1 = new GameBoard(null);
        GameBoard gb2 = new GameBoard(null);

        assertTrue(gb1.equals(gb2) && gb2.equals(gb1));
        assertEquals(gb1.hashCode(), gb2.hashCode());

        assertNotEquals(gb1, null);
        assertNotEquals(gb1, new Object());
        assertEquals(gb1, gb1);

        gb1.setMeeplePos(1);
        gb2.setMeeplePos(2);
        assertNotEquals(gb1, gb2);

        gb1.getFields().get(0).setSpecialPatch(true);
        gb2.getFields().get(0).setSpecialPatch(false);
        assertNotEquals(gb1, gb2);

        List<Patch> patchList1 = new ArrayList<>();
        boolean[][] matrix1 = new boolean[5][5];
        patchList1.add(new Patch(matrix1, 0, 0, 0));
        gb1 = new GameBoard(patchList1);

        List<Patch> patchList2 = new ArrayList<>();
        boolean[][] matrix2 = new boolean[5][5];
        patchList2.add(new Patch(matrix2, 1, 1, 1));
        gb2 = new GameBoard(patchList2);

        assertNotEquals(gb1, gb2);
    }

    /**
     * Tests the toString method
     */
    @Test
    public void testToString() {
        IOController ioController = new IOController(null);
        GameBoard gb1 = new GameBoard(ioController.importCSV());
        assertEquals(gb1.toString(), gb1.toString());
    }

    /**
     * Tests the toString method with parameter
     */
    @Test
    public void testToStringWithParam() {
        IOController ioController = new IOController(null);
        GameBoard gb1 = new GameBoard(ioController.importCSV());
        assertEquals(gb1.toString(4), gb1.toString(4));
    }


}












