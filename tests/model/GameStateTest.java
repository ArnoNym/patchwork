package model;

import Resources.Constants;
import controller.GameController;
import controller.IOController;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the GameState
 */
public class GameStateTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());

        GameState g1 = new GameState(p1, p2, gameBoard);
        GameState g2 = new GameState(p1, p2, gameBoard);

        assertEquals(g1.hashCode(), g2.hashCode());
        assertEquals(g1, g2);

        g2.setActivePlayer(new Player("Long", null));
        g2.setPassivePlayer(p1);

        assertNotEquals(g1, g2);
        assertNotEquals(g1, null);
        assertNotEquals(g1, new Object());
    }

    /**
     * Tests the Constructor
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor() {
        Player p1 = new Player(null, null);
    }

    /**
     * Tests the Constructor
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNullArgument() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());

        GameState g1 = new GameState(null, p2, gameBoard);
    }

    /**
     * tests the toString method
     */
    @Test
    public void testTestToString() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter2", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());

        GameState g1 = new GameState(p1, p2, gameBoard);
        String peter = "active:\n" +
                "Peter at Pos 0 with 5 buttons";
        String peter2 = "inactive:\n" +
                "Peter2 at Pos 0 with 5 buttons";
        assertTrue(g1.toString().contains(peter) && g1.toString().contains(peter2));
    }

    /**
     * tests the toString finish method
     */
    @Test
    public void testTestToStringFinish() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter2", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());
        p1.setPos(gameBoard._LASTPOS);
        p2.setPos(gameBoard._LASTPOS);
        GameState g1 = new GameState(p1, p2, gameBoard);
        String result = "The game is finished!\n" +
                "Draw!";
        assertTrue(g1.toString().contains(result));
    }

    /**
     * tests the toString finish method if passive player wins
     */
    @Test
    public void testTestToStringFinishPassiveWin() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter2", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());
        p1.setPos(gameBoard._LASTPOS);
        p2.setPos(gameBoard._LASTPOS);
        p2.setButtonCount(1000000);
        GameState g1 = new GameState(p1, p2, gameBoard);
        String result = "The game is finished!\n" +
                "The winner is:\n" +
                "Peter2 with a score of 999838!\n" +
                "The looser is:\n" +
                "Peter with a score of -157!";
        assertTrue(g1.toString().contains(result));
    }

    /**
     * tests the toString finish method if active player wins
     */
    @Test
    public void testTestToStringFinishActiveWin() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter2", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());
        p1.setPos(gameBoard._LASTPOS);
        p2.setPos(gameBoard._LASTPOS);
        p1.setButtonCount(1000000);
        GameState g1 = new GameState(p1, p2, gameBoard);
        String result = "The winner is:\n" +
                "Peter with a score of 999838!\n" +
                "The looser is:\n" +
                "Peter2 with a score of -157!";
        assertTrue(g1.toString().contains(result));
    }

    /**
     * tests the clone method
     */
    @Test
    public void testClone_1() {
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Peter", null);
        IOController ioController = new IOController(null);
        GameBoard gameBoard = new GameBoard(ioController.importCSV());
        p1.setPos(0);
        p2.setPos(1);
        p2.setButtonCount(1000000);
        GameState g1 = new GameState(p1, p2, gameBoard);
        GameState cloned = g1.clone();
        assertEquals(g1, cloned);
    }

    /**
     * tests the clone method after a turn has passed
     */
    @Test
    public void testClone_2() {
        Player p1 = new Player("Peter_1", new AIInformation(false, Difficulty.SOFTCORE, SimulationSpeed.NOSTOP));
        Player p2 = new Player("Peter_2", new AIInformation(false, Difficulty.SOFTCORE, SimulationSpeed.NOSTOP));
        IOController ioController = new IOController(null);

        GameController gameController = new GameController();
        gameController.getGame().setGameState(new GameState(p1, p2, new GameBoard(ioController.importCSV())));
        GameState gameState = gameController.getGame().getGameState();

        GameState clonedGameState = gameState.clone();
        assertEquals(gameState, clonedGameState);

        //gameController.getAiController().doTurn();
        gameController.getPlayerController().pass();

        assertEquals(gameState, clonedGameState);
        assertNotEquals(gameState, gameController.getGame().getGameState());
    }
}
