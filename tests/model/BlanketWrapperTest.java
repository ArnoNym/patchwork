package model;

import controller.BlanketController;
import controller.IOController;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import static org.junit.Assert.*;
/**
 * This class tests the methods
 * of the BlanketWrapper
 */
public class BlanketWrapperTest {
    BlanketWrapper blanketWrapper;

    /**
     * setup of the testing environment
     */
    @Before
    public void setUp() throws Exception {
        Blanket blanket = new Blanket();
        boolean[] col0 = {false, false, false, false, false};
        boolean[] col1 = {true, true, true, false, false};
        boolean[] col2 = {false, false, true, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, true};
        boolean[][] rows = new boolean[][]{col0, col1, col2, col3, col4};

        Patch p = new Patch(rows, 5, 5, 0);

        VecTwo tlc = new VecTwo(5, 5);
        PatchTuple lul = new PatchTuple(p, Rotation._0, tlc, true);
        blanketWrapper = new BlanketWrapper(blanket, lul);
    }

    /**
     * tests the getpatchtuple method
     */
    @Test
    public void testGetPatchTuple() {
        assertNotNull(blanketWrapper.getPatchTuple());
    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        Blanket b1 = new Blanket();
        Blanket b2 = new Blanket();
        IOController ioController = new IOController(null);
        List<Patch> patchList = ioController.importCSV();
        PatchTuple t1 = new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false);

        BlanketWrapper w1 = new BlanketWrapper(b1, t1);
        BlanketWrapper w2 = new BlanketWrapper(b1, t1);

        assertEquals(w1.hashCode(), w2.hashCode());
        assertEquals(w1, w1);
        assertEquals(w1, w2);
        assertNotEquals(w1, null);
        assertNotEquals(w1, new Object());

        b1 = new Blanket();
        List<PatchTuple> list = new ArrayList<>();
        list.add(new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false));
        b1.setPatchTuples(list);

        list = new ArrayList<>();
        list.add(new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(0, 0), false));
        list.add(new PatchTuple(patchList.get(0), Rotation._0, new VecTwo(1, 0), false));
        b2.setPatchTuples(list);

        w1 = new BlanketWrapper(b1, t1);
        w2 = new BlanketWrapper(b2, t1);
        assertNotEquals(w1, w2);
    }
}
