package model;

import Resources.Constants;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This class tests the methods
 * of the Game
 */
public class GameTest {

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the hashCode and equals method
     */
    @Test
    public void testHashCodeAndEquals() {
        Game g1 = new Game();
        Game g2 = new Game();
        Player p1 = new Player("Peter", null);
        Player p2 = new Player("Gisela", null);
        GameBoard gb = new GameBoard(null);
        g1.getHighscore().put("Peter", 10);
        g2.getHighscore().put("Peter", 10);

        g1.setGameState(new GameState(p1, p2, gb));
        g2.setGameState(new GameState(p1, p2, gb));

        assertTrue(g1.equals(g2) && g2.equals(g1));
        assertEquals(g1.hashCode(), g2.hashCode());
        assertEquals(g1,g2);
        assertNotEquals(g1, null);
        assertNotEquals(g1, new Object());

        g1.setGameState(new GameState(p1, p1, gb));
        assertNotEquals(g1, g2);

        g1.getHighscore().put("Peter", 10);
        assertNotEquals(g1, g2);

        g1.getRedoStack().add(new GameState(p1, p1, gb));
        assertNotEquals(g1, g2);

        g1.getUndoStack().add(new GameState(p1, p1, gb));
        assertNotEquals(g1, g2);


    }


}












