package resources;

import static org.junit.Assert.*;

import Resources.Constants;
import TestValues.TestPatchTuple;
import ai.anneat.main.utils.collections.Tuple;
import controller.IOController;
import org.junit.Before;
import org.junit.Test;

import model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This class tests the methods
 * of the Constants
 */
public class ConstantsTest {

    /**
     * Tests the Constructor
     */
    @Test
    public void testConstructor() {
        Constants con = new Constants();
    }

    /**
     * Creates a Testing-Environment
     *
     * @throws Exception Gets thrown if creation goes wrong
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Tests the randomLizenz method
     */
    @Test
    public void testRandomLizenz() {
        String l1 = Constants.randomLizenz();
        String l2 = Constants.randomLizenz();
        assertNotEquals(l1, l2);
    }

    /**
     * Tests the statisticalAnalysis method
     */
    @Test
    public void testStatisticalAnalysis() {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);

        Tuple<Double, Double> tuple = Constants.statisticalAnalysis(ints);
        // Average
        assertEquals(tuple.getV1(), 2d, 0d);

        // Standard Deviation
        assertEquals(tuple.getV2(), 1d, 0d);
    }

    /**
     * Tests the shufflePatches method
     */
    @Test
    public void testShufflePatches() {
        IOController ioController = new IOController(null);
        List<Patch> patchList = ioController.importCSV();

        assertNotEquals(patchList, Constants.shufflePatches(patchList));
    }

    /**
     * Tests the shufflePatchTuples method
     */
    @Test
    public void testShufflePatchTuples() {
        List<PatchTuple> patchTuples = new ArrayList<>();
        patchTuples.add(TestPatchTuple.getPatchTuple1());
        patchTuples.add(TestPatchTuple.getPatchTuple2());
        patchTuples.add(TestPatchTuple.getPatchTuple3());
        patchTuples.add(TestPatchTuple.getPatchTuple4());
        patchTuples.add(TestPatchTuple.getPatchTuple5());
        patchTuples.add(TestPatchTuple.getPatchTuple6());
        List<PatchTuple> patchTuplesCopy = new ArrayList<>(patchTuples);
        assertNotEquals(Constants.shufflePatchTuples(patchTuples),patchTuplesCopy);
    }
}
