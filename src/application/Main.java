package application;
	
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import view.GUI;
import view.StartmenuController;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {


		new GUI(primaryStage);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
