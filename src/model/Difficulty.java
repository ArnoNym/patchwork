package model;

/**
 * @author not Raphael
 */
public enum Difficulty {
	HARDCORE("Hardcore"),
	MISSIONARY("Missionary"),
	SOFTCORE("Softcore"),
	STUPID("Softcore"),
	GREEDY_FIRST_FIT("Softcore");

	private final String guiName;

	/**
	 * @author not Raphael
	 */
	Difficulty(String guiName) {
		this.guiName = guiName;
	}

	/**
	 * @author not Raphael
	 */
	public String getGuiName() {
		return guiName;
	}
}

