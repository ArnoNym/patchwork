package model;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Raphael
 */
public class Field implements Serializable {

	private boolean buttonField;
	private boolean specialPatch;

	/**
	 * @param isButtonField -
	 * @param hasSpecialPatch -
	 * @author Raphael
	 */
	public Field(boolean isButtonField, boolean hasSpecialPatch){
		this.buttonField = isButtonField;
		this.specialPatch = hasSpecialPatch;
	}

	/**
	 * @author Raphael
	 */
	public boolean isButtonField() {
		return buttonField;
	}

	/**
	 * @author Raphael
	 */
	public boolean isSpecialPatch() {
		return specialPatch;
	}

	/**
	 * @author Raphael
	 */
	public void setSpecialPatch(boolean specialPatch) {
		this.specialPatch = specialPatch;
	}

	/**
	 * @author Leon
	 */
	@Override
	public String toString() {
		return "isButtonField: "+ buttonField+", hasExtraPatch: "+specialPatch;
	}

	/**
	 * @return cloned Field
	 * @author Raphael
	 */
	public Field clone(){
		return new Field (buttonField, specialPatch);
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Field field = (Field) obj;
		return buttonField == field.buttonField && specialPatch == field.specialPatch;
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public int hashCode() {
		return Objects.hash(buttonField, specialPatch);
	}
}
