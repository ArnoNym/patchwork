package model;

import Resources.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Raphael
 */
public class GameBoard implements Serializable {

	private int meeplePos;
	private List<Field> fields;
	private List<Patch> patches;
	public final int _LASTPOS;

	/**
	 * Constructs a new GameBoard with setup-values.
	 * player1Position = 0
	 * player2Position = 0
	 * meeplePosition = 0
	 * fields = constant for every Game
	 * @param patches list of all initial patches, the first patch is at index 0
	 * @author Raphael
	 */
	public GameBoard(List<Patch> patches){
		this.meeplePos = 0;
		this.patches = patches;
		 this.fields = Constants.generateTimeBoard();
		_LASTPOS = fields.size() - 1;
	}

	/**
	 * @author Raphael
	 */
	public int getMeeplePos() {
		return meeplePos;
	}

	/**
	 * @author Raphael
	 */
	public void setMeeplePos(int meeplePos) {
		this.meeplePos = meeplePos;
	}

	/**
	 * @author Raphael
	 */
	public List<Field> getFields() {
		return fields;
	}

	/**
	 * @author Raphael
	 */
	public List<Patch> getPatches() {
		return patches;
	}

	/**
	 * @return cloned GameBoard
	 * @author Raphael
	 */
	public GameBoard clone(){
		List<Patch> patches = new ArrayList<>();
		for (Patch patch : this.patches){
			patches.add(patch.clone());
		}
		GameBoard res = new GameBoard(patches);
		res.meeplePos = meeplePos;
		res.fields = fields.stream().map(Field::clone).collect(Collectors.toList());
		return res;
	}

	/**
	 * @author Leon
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("Patches: \n");
		for (int count = 0; count < 3; count++) {
			int patchIndex = (meeplePos + count) % (patches.size() - 1);
			stringBuilder.append(count).append(": \n").append(patches.get(patchIndex).toString());
		}
		return stringBuilder.toString();
	}

	/**
	 * @author Leon
	 */
	public String toString(int playerPos) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(toString()).append("\n");

		stringBuilder.append("Fields: \n");
		for (int count = 0; count < 5; count++) {
			int patchIndex = (playerPos + count) % (fields.size() - 1);
			stringBuilder.append(count).append(": ").append(fields.get(patchIndex).toString()).append("\n");
		}

		return stringBuilder.toString();
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GameBoard gameBoard = (GameBoard) o;
		return meeplePos == gameBoard.meeplePos && _LASTPOS == gameBoard._LASTPOS && fields.equals(gameBoard.fields) && patches.equals(gameBoard.patches);
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public int hashCode() {
		return Objects.hash(meeplePos, fields, patches, _LASTPOS);
	}
}
