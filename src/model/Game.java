package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Raphael
 */
public class Game implements Serializable {

	private final Map<String, Integer> highscore = new HashMap<>();
	private final GameStack redoStack = new GameStack();
	private final GameStack undoStack = new GameStack();
	private GameState gameState = null;

	/**
	 * @author Raphael
	 */
	public Map<String, Integer> getHighscore() {
		return highscore;
	}

	/**
	 * @author Raphael
	 */
	public GameStack getRedoStack() {
		return redoStack;
	}

	/**
	 * @author Raphael
	 */
	public GameStack getUndoStack() {
		return undoStack;
	}

	/**
	 * @author Raphael
	 */
	public GameState getGameState() {
		return gameState;
	}

	/**
	 * @author Raphael
	 */
	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Game game = (Game) obj;
		return Objects.equals(highscore, game.highscore) &&
				Objects.equals(redoStack, game.redoStack) &&
				Objects.equals(undoStack, game.undoStack) &&
				Objects.equals(gameState, game.gameState);
	}

	/**
	 * @author Raphael
	 */
	@Override
	public int hashCode() {
		return Objects.hash(highscore, redoStack, undoStack, gameState);
	}


}
