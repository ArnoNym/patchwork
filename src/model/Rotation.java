package model;

/**
 * All possible Rotations.
 */
public enum Rotation {

	_90(1),

	_180(2),

	_270(3),

	_0(4);

	private final int hashCode;

	Rotation(int hashCode) {
		this.hashCode = hashCode;
	}

	public int getHashCode() {
		return hashCode;
	}
}
