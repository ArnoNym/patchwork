package model;

import Resources.Constants;

import java.io.Serializable;

/**
 * 2d-Vector Class. Vector Format: (row, column).
 * @author Raphael
 */
public class VecTwo implements Serializable {
    private int row;
    private int col;

    /**
     * Constructs a new VecTwo
     * @param row
     * @param col
     * @author Raphael
     */
    public VecTwo(int row, int col){
        this.row = row;
        this.col = col;
    }

    /**
     * @return Row
     * @author Raphael
     */
    public int getRow(){
        return row;
    }

    /**
     * sets Row, doesnt check if valid.
     * @author Raphael
     */
    public void setRow(int row){
        this.row = row;
    }

    /**
     * @return Col
     * @author Raphael
     */
    public int getCol(){
        return col;
    }

    /**
     * sets Column, doesnt check if valid.
     * @author Raphael
     */
    public void setCol(int col){
        this.col = col;
    }

    /**
     * @return String-representation of this vec2
     * @author Raphael
     */
    public String toString(){
        return "(" + row + "," + col + ")";
    }

    /**
     * @return cloned Vec2
     * @author Raphael
     */
    public VecTwo clone(){
        return new VecTwo(row,col);
    }

    /**
     * @author Leon
     */
    @Override
    public int hashCode() {
        return row * (Constants.BLANKET_COLUMNS_COUNT + 1) + col;
    }

    /**
     * @param obj other VecTwo
     * @return true if obj equals this VecTwo
     * @author Raphael
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        VecTwo vecTwo = (VecTwo) obj;
        return row == vecTwo.row && col == vecTwo.col;
    }

    /**
     * checks if this vector is valid.
     * @return true if valid.
     * @author Raphael
     */
    public boolean validVec2(){
        return row < 9 && row >= 0 && col < 9 && col >= 0;
    }

    /**
     * @return euclidDistance between vt1 and vt2
     */
    public static double euclidanDistance(VecTwo vt1, VecTwo vt2){
        int distRow = vt1.getRow() - vt2.getRow();
        int distCol = vt1.getCol() - vt2.getCol();
        return Math.sqrt(Math.pow(distRow, 2) + Math.pow(distCol, 2));
    }

}
