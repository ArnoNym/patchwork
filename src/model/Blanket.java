package model;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.*;

/**
 * @author Raphael
 */
public class Blanket implements Serializable {

	private List<PatchTuple> patchTuples;

	/**
	 * New empty Blanket.
	 * @author Raphael
	 */
	public Blanket(){
		patchTuples = new ArrayList<>();
	}

	/**
	 * @return income that is generated by the patches on this blanket
	 * @author Leon
	 */
	public int calcIncome() {
		int income = 0;
		for (PatchTuple patchTuple : patchTuples) {
			income += patchTuple.getPatch().getIncome();
		}
		return income;
	}
	public int getBlanketAllPatchTime() {
		int time = 0;
		for (PatchTuple patchTuple : patchTuples) {
			time += patchTuple.getPatch().getTime();
		}
		return time;
	}
	public int getBlanketAllPatchCount() {
		int patchCount = 0;
		for (PatchTuple patchTuple : patchTuples) {
			patchCount +=1;
		}
		return patchCount;
	}
	public int getBlanketAllFullCells(){
		int intcells = 0;
		boolean [][] res = getBoolMatrixRepresentation();
		for (int row = 0; row < 9; row++){
			for (int col = 0; col < 9; col++){
				if (res [row][col] != true){
					intcells +=1;
				}
			}
		}
		return 81-intcells;
	}

	public int getBlanketAllEmptyCells(){

		return 81-getBlanketAllFullCells();
	}

	public Collection<PatchTuple> getPatchTuples() {
		return patchTuples;
	}

	public void setPatchTuples(List<PatchTuple> patchTuples) {
		if (patchTuples == null) throw new IllegalArgumentException();
		this.patchTuples = patchTuples;
	}

	/**
	 * Format_example (vertical ~ y, horizontal ~ x):
	 *
	 *	0	x	x	x		o	o	o
	 *	1	x	x					o	o
	 *	2	x	x		*	*	o	o
	 *	3				*	*	*
	 *	4			#		*
	 * 	5			#
	 * 	6			#	#	#
	 * 	7				#
	 * 	8
	 * 		0	1	2	3	4	5	6	7	8
	 *
	 *  (every empty field stands for a false-Value)
	 *
	 *	0	t	t	t		t	t	t
	 *	1	t	t					t	t
	 *	2	t	t		t	t	t	t
	 *	3				t	t	t
	 *	4			t		t
	 * 	5			t
	 * 	6			t	t	t
	 * 	7				t
	 * 	8
	 * 		0	1	2	3	4	5	6	7	8
	 *
	 * @return boolean[row][col]_matrix representing the blanked
	 * @author Raphael
	 */
	public boolean [][] getBoolMatrixRepresentation() {
		boolean [][] res = new boolean[9][9];
		int [][] intMatrix = getIntMatrixRepresentation();
		for (int row = 0; row < 9; row++){
			for (int col = 0; col < 9; col++){
				if (intMatrix [row][col] != 0){
					res [row][col] = true;
				}
			}
		}
		return res;
	}

	/**
	 * @author Raphael
	 */
	public int [][] getIntMatrixRepresentation(){
		int [][] res = new int[9][9];
		int current = 1;
		List<VecTwo> vecList;
		for (PatchTuple patchTuple : patchTuples){
			vecList = patchTuple.getVectorList();
			for(VecTwo vecTwo : vecList){
				int row = vecTwo.getRow();
				int col = vecTwo.getCol();
				if (res [row][col] == 0){
					res [row][col] = current;
				}
				else throw new IllegalStateException("overlapping patches");
			}
			current++;
		}
		return res;
	}

	/**
	 * So that null doesn't get added accidentally
	 *
	 * @param patchTuple to be added
	 * @author Leon
	 */
	public void addPatchTuple(@NotNull PatchTuple patchTuple) {
		patchTuples.add(patchTuple);
	}

	/**
	 * @return Representation of this Blanket.
	 * @author Raphael
	 */
	public String toString(){
		int [][] matrix = getIntMatrixRepresentation();

		StringBuilder res = new StringBuilder();
		/*
		res.append("[Cost: ").append(patchTuples.stream().mapToInt(patchTuple -> patchTuple.getPatch().getCost()).sum()).append("]");
		res.append(", ");
		res.append("[Time: ").append(patchTuples.stream().mapToInt(patchTuple -> patchTuple.getPatch().getTime()).sum()).append("]");
		res.append(", ");
		res.append("[Income: ").append(patchTuples.stream().mapToInt(patchTuple -> patchTuple.getPatch().getIncome()).sum()).append("]");
		res.append("\n");
		*/

		final int nine = 9;
		for (int [] row : matrix){
			for (int val : row){
				if (val != 0){
					if (val > nine) {
						res.append(val).append("  ");
					} else {
						res.append(" ").append(val).append("  ");
					}
				}
				else{
					res.append(" -  ");
				}
			}
			res.append("\n");
		}
		return res.toString();
	}

	/**
	 * @return cloned Blanket
	 * @author Raphael
	 */
	public Blanket clone(){
		ArrayList<PatchTuple> tmp = new ArrayList<>();
		for (PatchTuple pt : patchTuples){
			tmp.add(pt.clone());
		}
		Blanket res = new Blanket();
		res.patchTuples = tmp;
		return res;
	}
	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Blanket blanket = (Blanket) obj;
		return Objects.equals(patchTuples, blanket.patchTuples);
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public int hashCode() {
		return Objects.hash(patchTuples);
	}
}
