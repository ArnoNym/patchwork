package model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class PatchTuple implements Serializable {

	private Patch patch;
	private Rotation rotation;
	private VecTwo posTopLeftCorner;
	private boolean isFLipped;

	/**
	 * @param patch patch
	 * @param rotation Degree of clockwise rotation
	 * @param posTopLeftCorner top-left corner of the patch, top has higher prioritisation than most left
	 * @param isFlipped true = flipped, false = not flipped
	 * @author Raphael
	 */
	public PatchTuple (Patch patch, Rotation rotation, VecTwo posTopLeftCorner, boolean isFlipped){
		if (patch != null && rotation != null && posTopLeftCorner != null){
			this.patch = patch;
			this.rotation = rotation;
			this.posTopLeftCorner = posTopLeftCorner;
			this.isFLipped = isFlipped;
		}
		else{
			if (patch == null) throw new IllegalArgumentException("patch null");
			else if (rotation == null) throw new IllegalArgumentException("rotation null");
			else throw new IllegalArgumentException("posTopLeftCorner null");
		}
	}

	/**
	 * @author Raphael
	 */
	public Patch getPatch() {
		return patch;
	}

	/**
	 * @author Raphael
	 */
	public void setPatch(Patch patch){
		if (patch != null) this.patch = patch;
		else throw new IllegalArgumentException();
	}

	/**
	 * @author Raphael
	 */
	public Rotation getRotation() {
		return rotation;
	}

	/**
	 * @author Raphael
	 */
	public void setRotation(Rotation rotation){
		if (rotation != null) this.rotation = rotation;
		else throw new IllegalArgumentException();
	}

	/**
	 * @author Raphael
	 */
	public VecTwo getPosTopLeftCorner() {
		return posTopLeftCorner;
	}

	/**
	 * @author Raphael
	 */
	public void setPosTopLeftCorner (VecTwo posTopLeftCorner){
		if (posTopLeftCorner != null) this.posTopLeftCorner = posTopLeftCorner;
		else throw new IllegalArgumentException();
	}

	/**
	 * @author Raphael
	 */
	public boolean isFLipped() {
		return isFLipped;
	}

	/**
	 * @author Raphael
	 */
	public void setFLipped(boolean isFLipped){
		this.isFLipped = isFLipped;
	}

	/**
	 * Returns the fully evaluated Patch as a list.
	 * Firstly the Patch is flipped (if needed), afterwards its being rotated.
	 * For the final result we need to also consider the position-information -given by topLeftCorner -
	 * and shift the patch accordingly.
	 * @return List of all Positions of the current Patch.
	 * @author Raphael
	 */
	public List<VecTwo> getVectorList(){
		Patch tmp = patch;
		if (isFLipped) tmp = tmp.flipPatch();
		tmp = tmp.rotatePatch(rotation);
		//Vec2-List
		List<VecTwo> res = tmp.getVectorList();

		//finding top(1)-left(2)-field of the patch
		VecTwo topLeft = res.get(0);

		// I think this is not being used, ever (Kevin)
//		for (VecTwo vecTwo : res){
//			if (topLeft.getRow() > vecTwo.getRow()) topLeft = vecTwo;
//			//System.out.println(topLeft);
//			if (topLeft.getRow() == vecTwo.getRow() && topLeft.getCol() > vecTwo.getCol()) topLeft = vecTwo;
//		}

		int shiftRow = posTopLeftCorner.getRow() - topLeft.getRow();
		int shiftCol = posTopLeftCorner.getCol() - topLeft.getCol();

		//shifting coordinates of all patch-fields
		for (VecTwo vecTwo : res){
			vecTwo.setRow(vecTwo.getRow() + shiftRow);
			vecTwo.setCol(vecTwo.getCol() + shiftCol);
		}

		return res;
	}

	/**
	 * @return Representation of this PatchTuple inside the 9x9-Matrix
	 * @author Raphael
	 */
	public String toString(){
		StringBuilder res = new StringBuilder();

		res.append("[Cost: ").append(patch.getCost()).append("]");
		res.append(", ");
		res.append("[Time: ").append(patch.getTime()).append("]");
		res.append(", ");
		res.append("[Income: ").append(patch.getIncome()).append("]");
		res.append("\n");

		boolean [][] bArr = getMatrixRepresentation();

		for (boolean [] col : bArr){
			for (boolean bool : col){
				if (bool) res.append(" # ");
				else res.append(" - ");
			}
			res.append("\n");
		}

		return res.toString();
	}

	/**
	 * @author Raphael
	 */
	public boolean [] [] getMatrixRepresentation(){
		List<VecTwo> lst = getVectorList();
		boolean [][] res = new boolean[9][9];
		for (VecTwo vecTwo : lst){
			//tem.out.println(vecTwo.getRow());
			//System.out.println(vecTwo.getCol());
			res [vecTwo.getRow()] [vecTwo.getCol()] = true;
		}
		return res;
	}

	/**
	 * @return cloned PatchTuple
	 * @author Raphael
	 */
	public PatchTuple clone(){
		return new PatchTuple(patch.clone(), rotation, posTopLeftCorner.clone(), isFLipped);
	}

	/**
	 * @author Leon
	 */
	@Override
	public int hashCode() {
		//return rotation.getHashCode() + posTopLeftCorner.hashCode() + (isFLipped ? 293343 : 0);
		return Objects.hash(patch, rotation.getHashCode(), posTopLeftCorner, isFLipped);
	}

	/*
	public boolean equals(PatchTuple patchTuple){
		return	patch.equals(patchTuple.patch) &&
				rotation.equals(patchTuple.rotation) &&
				posTopLeftCorner.equals(patchTuple.posTopLeftCorner) &&
				isFLipped == patchTuple.isFLipped;
	}*/

	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		PatchTuple that = (PatchTuple) obj;
		return isFLipped == that.isFLipped && Objects.equals(patch, that.patch) && rotation == that.rotation && Objects.equals(posTopLeftCorner, that.posTopLeftCorner);
	}
}
