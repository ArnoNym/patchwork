package model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Raphael, Leon
 */
public class AIInformation implements Serializable {

	private boolean providingFeedback;
	private Difficulty difficulty;
	private SimulationSpeed simulationSpeed;

	/**
	 * @param providingFeedback -
	 * @param difficulty -
	 * @param simulationSpeed -
	 */
	public AIInformation(boolean providingFeedback, @Nullable Difficulty difficulty, @NotNull SimulationSpeed simulationSpeed){
		this.difficulty = difficulty;
		this.providingFeedback = providingFeedback;
		this.simulationSpeed = simulationSpeed;
	}

	/**
	 * @author Raphael
	 */
	public boolean isProvidingFeedback() {
		return providingFeedback;
	}

	/**
	 * @author Raphael
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}

	/**
	 * @author Raphael
	 */
	public SimulationSpeed getSimulationSpeed() {
		return simulationSpeed;
	}

	/**
	 * @author Raphael
	 */
	public void setProvidingFeedback(boolean providingFeedback){
		this.providingFeedback = providingFeedback;
	}

	/**
	 * @author Raphael
	 */
	public void setDifficulty(Difficulty difficulty){
		if (difficulty != null) this.difficulty = difficulty;
		else throw new IllegalArgumentException();
	}

	/**
	 * @author Raphael
	 */
	public void setSimulationSpeed(SimulationSpeed simulationSpeed){
		if (simulationSpeed != null) this.simulationSpeed = simulationSpeed;
		else throw new IllegalArgumentException();
	}

	/**
	 * @return cloned AIInformation
	 * @author Raphael
	 */

	public AIInformation clone(){
		return new AIInformation(providingFeedback, difficulty, simulationSpeed);
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		AIInformation that = (AIInformation) obj;
		return providingFeedback == that.providingFeedback && difficulty == that.difficulty && simulationSpeed == that.simulationSpeed;
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public int hashCode() {
		return Objects.hash(providingFeedback, difficulty, simulationSpeed);
	}
}
