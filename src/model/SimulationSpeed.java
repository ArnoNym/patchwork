package model;

/**
 * All possible Simulation Speeds.
 */
public enum SimulationSpeed {
	CONFIRMATION,
	DELAYED,
	NOSTOP
}
