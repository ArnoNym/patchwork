package model;

import ai.anneat.main.utils.collections.Tuple;

import java.util.Arrays;

/**
 * @author Leon
 */
public class BlanketWrapper extends Tuple<Blanket, PatchTuple> {

    /**
     * @author Leon
     */
    public BlanketWrapper(Blanket blanket, PatchTuple patchTuple) {
        super(blanket, patchTuple);
    }

    /**
     * @author Leon
     */
    public Blanket getBlanket() {
        return getV1();
    }

    /**
     * @author Leon
     */
    public PatchTuple getPatchTuple() {
        return getV2();
    }

    /**
     * @author not Raphael
     */
    @Override
    public int hashCode() {
        /*
        int hashCode = 0;
        int mult = 1;
        for (boolean[] array : getBlanket().getBoolMatrixRepresentation()) {
            hashCode += Arrays.hashCode(array) * mult;
            mult++;
        }
        return hashCode;
        */
        return Arrays.deepHashCode(getBlanket().getBoolMatrixRepresentation());
    }

    /**
     * @author not Raphael
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BlanketWrapper)) return false;
        BlanketWrapper other = (BlanketWrapper) obj;
        boolean[][] matr1 = getBlanket().getBoolMatrixRepresentation();
        boolean[][] matr2 = other.getBlanket().getBoolMatrixRepresentation();
        for (int i = 0; i < matr1.length; i++) {
            if (!Arrays.equals(matr1[i], matr2[i])) {
                return false;
            }
        }
        return true;
    }




}
