package model;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Raphael
 */
public class Player implements Serializable {

	private boolean hasSpecialToken;
	private int buttonCount;
	private int pos;
	private final String name;
	private boolean highscoreActivated;
	private Blanket blanket;
	private AIInformation aIInformation;

	/**
	 * Player-Constructor for setting up a player at the start of a new game.
	 * hasSpecialToken = false
	 * buttonCount = 5
	 * hasActivatedHighscore = true
	 * blanket = new Blanket()
	 * @param name - Name of the Player
	 * @param aIInformation - null if human
	 * @author Raphael
	 */
	public Player(String name, AIInformation aIInformation){
		if (name != null){
			this.hasSpecialToken = false;
			this.buttonCount = 5;
			this.pos = 0;
			this.name = name;
			this.highscoreActivated = true;
			this.blanket = new Blanket();
			this.aIInformation = aIInformation;
		}
		else throw new IllegalArgumentException();
	}

	/**
	 * @return true if hasSpecialToken
	 * @author Raphael
	 */
	public boolean hasSpecialToken() {
		return hasSpecialToken;
	}

	/**
	 * sets specialToken
	 * @param hasSpecialToken
	 * @author Raphael
	 */
	public void setSpecialToken(boolean hasSpecialToken) {
		this.hasSpecialToken = hasSpecialToken;
	}

	/**
	 * @author Raphael
	 */
	public int getButtonCount() {
		return buttonCount;
	}

	/**
	 * @author Raphael
	 */
	public int getPos() {
		return pos;
	}

	/**
	 * @author Raphael
	 */
	public void setPos(int pos) {
		this.pos = pos;
	}

	/**
	 * @author Raphael
	 */
	public void setButtonCount(int buttonCount) {
		this.buttonCount = buttonCount;
	}

	/**
	 * @author Raphael
	 */
	public String getName() {
		return name;
	}

	/**
	 * @author Raphael
	 */
	public boolean isHighscoreActivated() {
		return highscoreActivated;
	}

	/**
	 * @author Raphael
	 */
	public void setHighscoreActivated(boolean highscoreActivated) {
		this.highscoreActivated = highscoreActivated;
	}

	/**
	 * @author Raphael
	 */
	public Blanket getBlanket() {
		return blanket;
	}

	/**
	 * @author Raphael
	 */
	public void setBlanket (Blanket blanket){
		this.blanket = blanket;
	}

	/**
	 * @author Raphael
	 */
	public AIInformation getAIInformation() {
		return aIInformation;
	}

	/**
	 * @author Raphael
	 */
	public void setAIInformation(AIInformation aIInformation) {
		this.aIInformation = aIInformation;
	}

	/**
	 * @return cloned Player
	 * @author Raphael
	 */
	public Player clone(){
		Player res = new Player(name, aIInformation);
		res.hasSpecialToken = hasSpecialToken;
		res.buttonCount = buttonCount;
		res.pos = pos;
		res.blanket = this.blanket.clone();
		res.highscoreActivated = highscoreActivated;
		return res;
	}

	/**
	 * @author not Raphael, Leon or Kevin i guess.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Player player = (Player) o;
		return hasSpecialToken == player.hasSpecialToken && buttonCount == player.buttonCount && pos == player.pos && highscoreActivated == player.highscoreActivated && name.equals(player.name) && blanket.equals(player.blanket) && aIInformation.equals(player.aIInformation);
	}

	/**
	 * @author not Raphael, Leon or Kevin i guess.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(hasSpecialToken, buttonCount, name, highscoreActivated, blanket, aIInformation);
	}

	/**
	 * @author not Raphael
	 */
	public String toString(){
		return name + " at Pos " + pos + " with " + buttonCount + " buttons" + "\n" + blanket;
	}
}
