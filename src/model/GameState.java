package model;

import controller.PlayerController;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Raphael
 */
public class GameState implements Serializable {

	private Player activePlayer;
	private Player passivePlayer;
	private GameBoard gameBoard;

	/**
	 * Constructs a new GameState (start of a game).
	 * @param player1 starting PLayer
	 * @param player2 non-starting Player
	 * @param gameBoard -
	 * @author Raphael
	 */
	public GameState(Player player1, Player player2, GameBoard gameBoard) {
		if (player1 != null && player2 != null && gameBoard != null) {
			this.activePlayer = player1;
			this.passivePlayer = player2;
			this.gameBoard = gameBoard;
		}
		else throw new IllegalArgumentException("null values in initialisation");
	}

	/**
	 * @author Raphael
	 */
	public Player getActivePlayer() {
		return activePlayer;
	}

	/**
	 * @author Raphael
	 */
	public void setActivePlayer(Player activePlayer) {
		this.activePlayer = activePlayer;
	}

	/**
	 * @author Raphael
	 */
	public Player getPassivePlayer() {
		return passivePlayer;
	}

	/**
	 * @author Raphael
	 */
	public void setPassivePlayer(Player passivePlayer) {
		this.passivePlayer = passivePlayer;
	}

	/**
	 * @author Raphael
	 */
	public GameBoard getGameBoard() {
		return gameBoard;
	}

	/**
	 * @return true, if both Player are on the last field of the gameboard
	 * @author Raphael
	 */
	public boolean isFinished() {
		return (activePlayer.getPos() + passivePlayer.getPos()) / 2  == gameBoard._LASTPOS;
	}

	/**
	 * @return cloned GameState
	 * @author Raphael
	 */
	public GameState clone() {
		GameBoard gameBoard = this.gameBoard.clone();
		Player active = activePlayer.clone();
		Player passive = passivePlayer.clone();
		return new GameState(active, passive, gameBoard);
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GameState gameState = (GameState) o;
		return activePlayer.equals(gameState.activePlayer) && passivePlayer.equals(gameState.passivePlayer) && gameBoard.equals(gameState.gameBoard);
	}

	/**
	 * @author not Raphael
	 */
	@Override
	public int hashCode() {
		return Objects.hash(activePlayer, passivePlayer, gameBoard, activePlayer);
	}

	/**
	 * @author Raphael
	 */
	@Override
	public String toString(){
		String str = "";

		//str += gameBoard.toString(activePlayer.getPos()) + "\n";
		//str += gameBoard + "\n";
		str += "active:\n" + activePlayer.toString();
		str += "\n";
		str += "inactive:\n" + passivePlayer.toString();

		if (isFinished()) {
			str += "\n";
			str += "The game is finished!\n";

			Player winner = PlayerController.calculateWinner(this);
			if (winner == null) {
				str += "Draw!";
			} else {
				Player looser;
				if (activePlayer.equals(winner)) {
					looser = passivePlayer;
				} else {
					looser = activePlayer;
				}
				str += "The winner is:\n";
				str += winner.getName();
				str += " with a score of ";
				str += PlayerController.calculateScore(winner);
				str += "!\n";
				str += "The looser is:\n";
				str += looser.getName();
				str += " with a score of ";
				str += PlayerController.calculateScore(looser);
				str += "!\n";
			}
		}

		return str;
	}
}
