package view;

import controller.GameController;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Scene;
import javafx.scene.control.*;
import view.GUI;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import controller.*;


public class OptionsController extends AnchorPane{

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label Lizenz;

    @FXML
    private Label beschreibung;

    @FXML
    private ChoiceBox<String> MusicDropdown;

    @FXML
    private Slider MusicSlider;

    @FXML
    private ColorPicker ColorPicker;

    @FXML
    private CheckBox MaximizeWindow;

    @FXML
    private CheckBox SecretSettingCheckbox;

    @FXML
    private Button BackButton;

    @FXML
    private CheckBox PlayMusic;

    private GameController gameController;
    public boolean secret;

    /**
     * New OptionsController
     * @param gameController = Controller of the game
     * @author Marcel
     */
    public OptionsController(GameController gameController) {
        this.gameController = gameController;

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Options.fxml"));
            loader.setRoot(this);
            loader.setController(this);

            loader.load();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        gameController.getGUI().getWindow().setWidth(gameController.getGUI().widthSize);
        gameController.getGUI().getWindow().setHeight(gameController.getGUI().heightSize);

        gameController.getGUI().getWindow().widthProperty().addListener((obs, oldVal, newVal) -> gameController.getGUI().setPreferredWindowSize((Double) newVal, gameController.getGUI().heightSize));

        gameController.getGUI().getWindow().heightProperty().addListener((obs, oldVal, newVal) -> gameController.getGUI().setPreferredWindowSize(gameController.getGUI().widthSize, (Double) newVal));

    }



    @FXML
    void BackButtonPressed(ActionEvent event) {
        gameController.getGUI().backToStartmenu();
    }
    @FXML
    void stumm(ActionEvent event) {
        if (gameController.isMusik())
        {

        gameController.getGUI().stopMusic();
        gameController.setMusik(false);
        }
        else {
            gameController.getGUI().playMusic("rick.mp3");
            gameController.setMusik(true);
        }


    }

    /**
     * CheckboxListener for Maximizing the window
     * @param event = Event
     * @author Marcel
     */

    @FXML
    void MaximizeWindowChecked(ActionEvent event) {

            gameController.getGUI().maximizeWindow(true);
    }

    @FXML
    void SecretSettingCheckboxChecked(ActionEvent event) {

        Scene scene = new Scene(new OptionsController(gameController),1200,600);
        gameController.getGUI().setCssResource("./../view/texture/alternative.css");
        gameController.getGUI().getWindow().getScene().getStylesheets().add(getClass().getResource("./../view/texture/alternative.css").toExternalForm());
        gameController.getGUI().getWindow().setScene(scene);
        gameController.getGUI().getWindow().show();
        //Scene scene = new Scene(new OptionsController(gameController),1200,600);
        gameController.getGUI().getWindow().getScene().getStylesheets().add(getClass().getResource("./../view/texture/alternative.css").toExternalForm());
        gameController.getGUI().getWindow().setScene(scene);
        gameController.getGUI().getWindow().show();
    }

    /**
     * Music Changer DropdownMenu
     * @param event = Event
     * @author Marcel
     */
    @FXML
    void musicChanged(ActionEvent event) {
        String choice = this.MusicDropdown.getValue();

        if( choice.equals("No Music") ){

        }
        else if (choice.equals("Never Gonna Give You Up") ){
            gameController.getGUI().playMusic("rick.mp3");
        }
    }



        @FXML // This method is called by the FXMLLoader when initialization is complete
        void initialize() {
            assert BackButton != null : "fx:id=\"HomeButton\" was not injected: check your FXML file 'GameScreen.fxml'.";
            //assert settingsButton != null : "fx:id=\"HomeButton\" was not injected: check your FXML file 'GameScreen.fxml'.";


            ObservableList<String> AuswahlTypen = FXCollections.observableArrayList(
                    "No Music","Never Gonna Give You Up");

        }


}
