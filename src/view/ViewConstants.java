package view;

public abstract class ViewConstants {
    public static final double PATCHES_IN_SELECTION_SPACING = 14d;
    public static final double PATCH_ATTRIBUTE_SIZE = 30d;
}
