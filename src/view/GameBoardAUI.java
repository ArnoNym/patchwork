package view;

import controller.GameController;
import javafx.scene.control.Label;
import model.Blanket;

public class GameBoardAUI{

    private GameController gameController;

    /**
     * Updates Patch1Button, Patch2Button, Patch3Button in GameScreen
     * @author Marcel
     */
    public void updatePatchList(){
        int currentPatchPosition = gameController.getGUI().getGameScreenController().getCurrentPatchPosition();


        //Update the List of Patches that is shown

    }

    /**
     * Updates the position of the current meeple, hopefully.
     * @author Marcel
     */
    public void updateGameBoard(GameController gameController){
        gameController.getGUI().getGameScreenController().positionUpdate();

        /*
        Player currentPlayer = gameController.getGame().getGameState().getActivePlayer();
        String currentPlayerName = currentPlayer.getName();
        System.out.println(currentPlayerName);
        int currentPlayerPosition = currentPlayer.getPos();
        System.out.println(currentPlayerPosition);
        GameScreenController gameScreenController = gameController.getGUI().getGameScreenContoller();
        gameScreenController.positionUpdate(currentPlayerPosition);


         */
        //HBox position = gameController.getGUI().getGameScreenContoller().getPositionHBox(currentPlayerPosition);

        //position.getStyleClass().add("player-1");

    }

    public void updateBlanket(Blanket blanket){}

    public void updateName(GameController gameController){
            gameController.getGUI().getGameScreenController().nameUpdate();
    }
}
