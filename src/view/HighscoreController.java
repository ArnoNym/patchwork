package view;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.ResourceBundle;

import controller.GameController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import model.Game;
import model.GameState;
import model.Player;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.collections.ObservableList;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;


public class HighscoreController extends AnchorPane {
    private GameController gameController;
    @FXML
    private Button BACK;
    @FXML
    private TableColumn<Hightscore,String> Position;
    @FXML
    private TableColumn<Hightscore, String> Name;
    @FXML
    private TableColumn<Hightscore,String> Score;
    @FXML
    private TableView<Hightscore> table;
    @FXML
    private Map<String, Integer> highscore;

    private ObservableList<Hightscore> list;
    private Game game;
    private int count =1;

    /**
     * Show the HightScore
     * @param gameController = Aktuelles Game
     * @author Ziad
     */
    public HighscoreController(GameController gameController) {
        this.gameController = gameController;

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Highscore.fxml"));
            loader.setRoot(this);
            loader.setController(this);

            loader.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Go to Startmenu
     * @param event = Event
     * @author Ziad
     */
    @FXML
    void BackButtonPressed(ActionEvent event) {

        gameController.getGUI().backToStartmenu();
    }

    /**
     * Initialize
     * @author Ziad
     */
    @FXML
    public void initialize() {
        Game loadedGame = gameController.getIoController().loadGame();
        // We dont have a save file present
        if (loadedGame == null) {
            return;
        }

        highscore = gameController.getIoController().loadGame().getHighscore();

        ArrayList<Hightscore> arrayList = new ArrayList<>();
        highscore.forEach((name , score) -> {
                    Hightscore h = new Hightscore(name, score);
                    arrayList.add(h);
                });
        Collections.sort(arrayList);

        for (Hightscore h:arrayList) {
            h.setPosition(count);
            arrayList.set(count-1,h);
            count++; }
        list = FXCollections.observableArrayList();
        list.addAll(arrayList);
        table.setItems(list);

        Name.setCellValueFactory(row -> new SimpleStringProperty(row.getValue().getName()));
        Score.setCellValueFactory(row -> new SimpleStringProperty(Integer.toString(row.getValue().getScore())));
        Position.setCellValueFactory((row) ->
                new SimpleStringProperty(Integer.toString(row.getValue().getPosition())));
    }
}

 class Hightscore implements Comparable<Hightscore>{
     @ Override
     public int compareTo( Hightscore o) {
         return ( o.score - this.score );
     }
     private  String name ;
     private int position;
     private  int score ;
     public int getPosition() {
         return position;
     }
     public void setPosition(int position) {
         this.position = position;
     }
     public String getName() {
         return name;
     }
     public void setName(String name) {
         this.name = name;
     }
     public int getScore() {
         return score;
     }
     public void setScore(int score) {
         this.score = score;
     }
     public Hightscore(String name,int score) {
         this.name = name;
         this.score=score; }
     @Override
     public String toString() {
         return "" + score ;
     }

}

