package view;

import controller.GameController;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.File;
import java.util.Stack;
public class GUI {


    private GameController gameController;
    private GameBoardAUI gameBoardAUI;
    private GameScreenController gameScreenController;
    private HighscoreController highscoreController;
    private OptionsController optionsController;
    private SetupController setupController;
    private StartmenuController startmenuController;
    private Stage window;
    private Stack<Scene> scenes;
    public DoubleProperty minWidthProperty;
    public DoubleProperty minHeightProperty;
    public double widthSize;
    public double heightSize;
    public boolean maximized;
    private MediaPlayer mediaPlayer;
    boolean musik = true;
    private String cssResource;

    /**
     * New GUI
     * @param primaryStage = First shown window
     * @author Marcel
     */
    public GUI (Stage primaryStage){
        gameController = new GameController(this);
        gameBoardAUI = new GameBoardAUI();
        window = primaryStage;
        scenes = new Stack<>();
        cssResource = "/application/application.css";

        primaryStage.setWidth(1280);
        primaryStage.setHeight(720);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Patchwork");

        primaryStage.setOnCloseRequest(e -> {
            gameController.getIoController().saveGame();
        });

        if(gameController.getGUI().maximized){
            gameController.getGUI().maximizeWindow(false);
        }
        else {
            setPreferredWindowSize(1280,720);
        }


        showPane(new StartmenuController(gameController));

    }

    /**
     * Shows the pane
     * @author Marcel
     */
    public void showPane(Pane pane) {
        try {
            Scene scene = new Scene(pane,1280,720);
            scenes.push(scene);
            scene.getStylesheets().add(getClass().getResource(cssResource).toExternalForm());
            window.setScene(scene);
            window.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the pane
     * @author Marcel
     */
    public void showGameScreenController(GameController gameController) {
        GameScreenController gameScreenController = new GameScreenController(gameController);
        showPane(gameScreenController);
        gameScreenController.initAfterSceneAdded();
    }

    /**
     * Get window
     * @author Marcel
     */
    public Stage getWindow(){
        return window;
    }

    /**
     * Back to previous scene
     * @author Marcel
     */
    public void backToPrevious() {
        scenes.pop();
        Scene scene = scenes.pop();
        scenes.push(scene);

        window.setScene(scene);
        window.show();
    }

    /**
     * Back to startmenu
     * @author Marcel
     */
    public void backToStartmenu() {
        showPane(new StartmenuController(gameController));

        scenes.clear();
    }

    /**
     * Plays Music
     * @param music: Name of song in music
     * @author Marcel
     */
    public void playMusic(String music){

            String path = "src/view/music/" + music ;
            Media media = new Media(new File(path).toURI().toString());
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer.setAutoPlay(true);
            mediaPlayer.play();
            musik=false;
    }
    public void stopMusic(){

        mediaPlayer.stop();
        gameController.setMusik(false);
    }

    /**
     * Maximizes Window
     * @author Marcel
     */
    public void maximizeWindow(boolean maximizeNow) {

        if(maximizeNow){
            Stage stage = this.window;
            stage.setFullScreen(true);
            this.maximized = true;
        }
        if(maximized){
            Stage stage = this.window;
            stage.setFullScreen(true);
        }

    }

    /**
     * Maximizes Window
     * @param widthSize = Size of the window width
     * @param heightSize = Size of the window height
     * @author Marcel
     */
    public void setPreferredWindowSize(double widthSize, double heightSize){
        this.widthSize = widthSize;
        this.heightSize = heightSize;
    }


    /**
     * Finish Game function
     * @param winnerName  winner of the game
     * @param winnerScore score of winner
     * @param loserName  not the winner of the game
     * @param loserScore score of loser
     * @author Marcel
     */
    public void showFinishScreen(String winnerName, int winnerScore, String loserName, int loserScore){

    }

    /**
     * returns GameBoardAUI
     * @author Marcel
     */
    public GameBoardAUI getGameBoardAUI() { return this.gameBoardAUI; }

    /**
     * returns GameScreenController
     * @author Marcel
     */
    public GameScreenController getGameScreenController() { return this.gameScreenController; }

    /**
     * returns HighscoreController
     * @author Marcel
     */
    public HighscoreController getHighscoreController() { return this.highscoreController; }

    /**
     * returns OptionsController
     * @author Marcel
     */
    public OptionsController getOptionsController() { return this.optionsController; }

    /**
     * returns SetupController
     * @author Marcel
     */
    public SetupController getSetupController() { return this.setupController; }

    public void setGameScreenContoller(GameScreenController gsc){
        this.gameScreenController = gsc;
    }
    /**
     * returns StartmenuController
     * @author Marcel
     */
    public StartmenuController getStartmenuController() { return this.startmenuController; }


    /**
     * @param cssResource The css that gets set POG
     */
    public void setCssResource(String cssResource) {
        this.cssResource = cssResource;
    }
}
