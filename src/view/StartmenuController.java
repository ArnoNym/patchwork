package view;

import controller.GameController;
import controller.IOController;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import model.Game;

import javax.swing.event.ChangeListener;
import java.io.IOException;
import java.lang.System;

public class StartmenuController extends AnchorPane{


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="OptionsButton"
    private Button OptionsButton; // Value injected by FXMLLoader

    @FXML // fx:id="StartGameButton"
    private Button StartGameButton; // Value injected by FXMLLoader

    @FXML // fx:id="LoadGameButton"
    private Button LoadGameButton; // Value injected by FXMLLoader

    @FXML // fx:id="HighscoreButton"
    private Button HighscoreButton; // Value injected by FXMLLoader

    @FXML // fx:id="ContinueGameButton"
    private Button ContinueGameButton; // Value injected by FXMLLoader

    @FXML // fx:id="CloseButton"
    private Button CloseButton; // Value injected by FXMLLoader

    private GameController gameController;

    private System system;
    private Boolean b = true;




    /**
     * Start the Startmenu Screen
     * @param gameController = Aktuelles Game
     * @author Marcel
     */
    public StartmenuController(GameController gameController) {
        this.gameController = gameController;

        try {

                if (gameController.isMusik()&&gameController.getGUI().musik)
                {

                    gameController.getGUI().playMusic("rick.mp3");


                }



            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Startmenu.fxml"));
            loader.setRoot(this);
            loader.setController(this);

            loader.load();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if(gameController.getGUI().maximized){
            gameController.getGUI().maximizeWindow(true);
        }
        else {
            gameController.getGUI().getWindow().setWidth(gameController.getGUI().widthSize);
            gameController.getGUI().getWindow().setHeight(gameController.getGUI().heightSize);
        }

        gameController.getGUI().getWindow().widthProperty().addListener((obs, oldVal, newVal) -> gameController.getGUI().setPreferredWindowSize((Double) newVal, gameController.getGUI().heightSize));

        gameController.getGUI().getWindow().heightProperty().addListener((obs, oldVal, newVal) -> gameController.getGUI().setPreferredWindowSize(gameController.getGUI().widthSize, (Double) newVal));

    }


    /**
     * Close Game
     * @param event = Event
     * @author Yuriy
     */
    @FXML
    void CloseButtonPressed(ActionEvent event) {
        gameController.getIoController().saveGame();
        System.exit(0);
    }

    /**
     * Go to Highscore
     * @param event = Event
     * @author Marcel
     */
    @FXML
    void HighscoreButtonPressed(ActionEvent event) {
        gameController.getGUI().showPane(new HighscoreController(gameController));
    }

    /**
     * Load Game
     * @param event = Event
     * @author ???
     */
    @FXML
    void LoadGameButtonPressed(ActionEvent event) {
        // Used to load a save.ser file from the User
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Load Game");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("SER Files", "*.ser"));
            String path = fileChooser.showOpenDialog(gameController.getGUI().getWindow()).getAbsolutePath();
            if(path.equals("")) return;
            Game game = gameController.getIoController().loadGame(path);
            if(game == null) return;
            gameController.setGame(game);
            gameController.getGUI().showPane(new GameScreenController(gameController));
        }
        catch (Exception e)
        {
        }
    }



    /**
     * Go to Options
     * @param event = Event
     * @author Marcel
     */
    @FXML
    void OptionsButtonPressed(ActionEvent event) {
        gameController.getGUI().showPane(new OptionsController(gameController));
    }

    /**
     * Go to Setup Screen
     * @param event = Event
     * @author Marcel
     */
    @FXML
    void StartGameButtonPressed(ActionEvent event) {
        gameController.getGUI().showPane(new SetupController(gameController));
    }


    /**
     * Continue last Game
     * @param event = Event
     * @author ???
     */
    @FXML
    void ContinueGameButtonPressed(ActionEvent event) {
        gameController.setGame(gameController.getIoController().loadGame());
        gameController.getGUI().showGameScreenController(gameController);
    }

    /**
     * Initialize
     * @author Marcel
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert OptionsButton != null : "fx:id=\"OptionsButton\" was not injected: check your FXML file 'Startmenu.fxml'.";
        assert StartGameButton != null : "fx:id=\"StartGameButton\" was not injected: check your FXML file 'Startmenu.fxml'.";
        assert LoadGameButton != null : "fx:id=\"LoadGameButton\" was not injected: check your FXML file 'Startmenu.fxml'.";
        assert HighscoreButton != null : "fx:id=\"HighscoreButton\" was not injected: check your FXML file 'Startmenu.fxml'.";
        assert CloseButton != null : "fx:id=\"CloseButton\" was not injected: check your FXML file 'Startmenu.fxml'.";
        assert ContinueGameButton != null : "fx:id=\"ContinueGameButton\" was not injected: check your FXML file 'Startmenu.fxml'.";


    }
}



