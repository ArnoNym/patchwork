package view;


import java.net.URL;
import java.util.*;

import Resources.Constants;
import ai.decisionTree.Turn;
import controller.GameStateController;
import javafx.event.Event;
import controller.BlanketController;
import controller.GameController;
import controller.PlayerController;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import model.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import view.texture.GUIPatch;

import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;

import static view.ViewConstants.PATCH_ATTRIBUTE_SIZE;


public class GameScreenController extends AnchorPane {

    private final Pane[][] panes = new Pane[9][9];
    private final String LIGHTGREEN = "-fx-background-color: #598718";
    private final String RED = "-fx-background-color: #874418";
    private final String TRANSPARENT = "-fx-background-color: transparent";

    private final String activeColor = "-fx-background-color: #9c3d00;";
    private final String passiveColor = "-fx-background-color: #701300;";

    private final String highlightColor = "-fx-background-color: #598718;";

    private Turn hintTurn;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    @FXML
    private HBox Position0;
    @FXML
    private HBox Position1;
    @FXML
    private HBox Position2;
    @FXML
    private HBox Position3;
    @FXML
    private HBox Position4;
    @FXML
    private HBox Position5;
    @FXML
    private HBox Position6;
    @FXML
    private HBox Position7;
    @FXML
    private HBox Position8;
    @FXML
    private HBox Position9;
    @FXML
    private HBox Position10;
    @FXML
    private HBox Position11;
    @FXML
    private HBox Position12;
    @FXML
    private HBox Position13;
    @FXML
    private HBox Position14;
    @FXML
    private HBox Position15;
    @FXML
    private HBox Position16;
    @FXML
    private HBox Position17;
    @FXML
    private HBox Position18;
    @FXML
    private HBox Position19;
    @FXML
    private HBox Position20;
    @FXML
    private HBox Position21;
    @FXML
    private HBox Position22;
    @FXML
    private HBox Position23;
    @FXML
    private HBox Position24;
    @FXML
    private HBox Position25;
    @FXML
    private HBox Position26;
    @FXML
    private HBox Position27;
    @FXML
    private HBox Position28;
    @FXML
    private HBox Position29;
    @FXML
    private HBox Position30;
    @FXML
    private HBox Position31;
    @FXML
    private HBox Position32;
    @FXML
    private HBox Position33;
    @FXML
    private HBox Position34;
    @FXML
    private HBox Position35;
    @FXML
    private HBox Position36;
    @FXML
    private HBox Position37;
    @FXML
    private HBox Position38;
    @FXML
    private HBox Position39;
    @FXML
    private HBox Position40;
    @FXML
    private HBox Position41;
    @FXML
    private HBox Position42;
    @FXML
    private HBox Position43;
    @FXML
    private HBox Position44;
    @FXML
    private HBox Position45;
    @FXML
    private HBox Position46;
    @FXML
    private HBox Position47;
    @FXML
    private HBox Position48;
    @FXML
    private HBox Position49;
    @FXML
    private HBox Position50;
    @FXML
    private HBox Position51;
    @FXML
    private HBox Position52;
    @FXML
    private HBox Position53;
    @FXML // fx:id="HomeButton"
    private Button HomeButton; // Value injected by FXMLLoader
    @FXML // fx:id="HintButton"
    private Button HintButton; // Value injected by FXMLLoader
    @FXML // fx:id="RedoButton"
    private Button RedoButton; // Value injected by FXMLLoader
    @FXML // fx:id="UndoButton"
    private Button UndoButton; // Value injected by FXMLLoader
    @FXML // fx:id="EnemyButton"
    private Button EnemyButton; // Value injected by FXMLLoader
    @FXML // fx:id="AdvanceButton"
    private Button AdvanceButton; // Value injected by FXMLLoader
    @FXML // fx:id="ConfirmationButton"
    private Button ConfirmationButton; // Value injected by FXMLLoader
    @FXML // fx:id="LeftButton"
    private Button LeftButton; // Value injected by FXMLLoader
    @FXML // fx:id="RightButton"
    private Button RightButton; // Value injected by FXMLLoader
    @FXML // fx:id="Patch1Button"
    private Button Patch1Button; // Value injected by FXMLLoader
    @FXML // fx:id="Patch2Button"
    private Button Patch2Button; // Value injected by FXMLLoader
    @FXML // fx:id="Patch3Button"
    private Button Patch3Button; // Value injected by FXMLLoader
    @FXML
    private GridPane GameField;
    @FXML
    private Label activePlayername;
    @FXML
    private GridPane blanketField;
    @FXML
    private ScrollPane Output;
    @FXML
    private ScrollPane scrollPane;
    
    @FXML 
    private Label CurrentScore;
    @FXML
    private Label CurrentScoreValue;
    
    @FXML
    private Label ButtonCount;
    @FXML
    private Label ButtonCountValue;

    @FXML
    private Label BlanketIncome;
    @FXML
    private Label BlanketIncomeValue;

    @FXML
    private Label PatchCount;
    @FXML
    private Label PatchCountValue;

    @FXML
    private Label FullCells;
    @FXML
    private Label FullCellsValue;

    @FXML
    private Label EmptyCells;
    @FXML
    private Label EmptyCellsValue;

    @FXML
    private Label SpecialToken;
    @FXML
    private Label SpecialTokenValue;

    @FXML
    private Label HighscoreActive;
    @FXML
    private Label HighscoreActiveValue;

    @FXML
    private Label FullCellsPercentage;
    @FXML
    private Label FullCellsPercentageValue;

    @FXML
    private Label CurrentLead;
    @FXML
    private Label CurrentLeadValue;

    @FXML
    private HBox scrollPaneBox2;


    private GameController gameController;
    private List<Patch> patches;
    private List<Patch> availablePatches;
    private int currentPatchPosition;
    private String Player1;
    private String Player2;
    private boolean alreadyExecuted;

    //anker attribute
    private HashMap<String, Patch> patchImages;

    private GridPane player1BlanketField;
    private GridPane player2BlanketField;

    private double mousePositionX;
    private double mousePositionY;

    private Map<GridPane, GUIPatch> gridPaneGUIPatchMap;
    // extremer hack um den alten Patch wieder zurückzubekommen um es in der Liste zu finden wenn es rotiert/gespiegelt wird
    private GUIPatch inHandGuiPatch = null;

    /**
     * Controls gameScreen
     *
     * @param gameController = Controller of the game
     * @author Marcel
     * @TODO patches muss aus Setup übernommen werden
     */
    public GameScreenController(GameController gameController) {
        clearHint();
        this.gameController = gameController;
        //Irgendwo muss später die PatchListe aus SETUP hier übernommen werden, bitte
        patches = gameController.getGameBoardController().getAvailablePatches();
        //Patch, welches auf dem Patch1 Feld eangezeigt wird
        currentPatchPosition = 0;
        patchImages = new HashMap<>();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/GameScreen.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();

            String imageURL = "/view/texture/timeBoard.jpg";
            Image image = new Image(imageURL);
            GameField.getStyleClass().add("grid-background");
            blanketField.setGridLinesVisible(true);
            blanketField.setAlignment(Pos.CENTER);
            HomeButton.getStyleClass().add("home-background");
            HintButton.getStyleClass().add("hint-background");
            RedoButton.getStyleClass().add("redo-background");
            UndoButton.getStyleClass().add("undo-background");
            EnemyButton.getStyleClass().add("enemy-background");
            scrollPane.setFitToHeight(true);

            // Disable vertical scroll and convert to horizontal scroll
            scrollPane.addEventFilter(ScrollEvent.SCROLL, e -> {
                if (e.getDeltaY() < 0) {
                    scrollPane.setHvalue(scrollPane.getHvalue() + 0.01);
                }
                else {
                    scrollPane.setHvalue(scrollPane.getHvalue() - 0.01);
                }
                e.consume();
            });
            scrollPaneBox2 = new HBox();
            scrollPaneBox2.addEventFilter(MouseEvent.MOUSE_DRAGGED, Event::consume);

            Player1 = gameController.getGame().getGameState().getActivePlayer().getName();
            Player2 = gameController.getGame().getGameState().getPassivePlayer().getName();
            //updateWholeScreen(true);
            alreadyExecuted = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // This method is called by the FXMLLoader when initialization is complete
    @FXML
    void initialize() {
        activePlayername.setText(gameController.getGame().getGameState().getActivePlayer().getName());

        ((VBox)Output.getContent()).getChildren().add(new Label(gameController.getGame().getGameState().getActivePlayer().getName() + " begins the game."));


        initializeBlanketCells();
        loadDifferentPatches();
        updateWholeScreen(true);
        RedoButton.setDisable(true);
    }

    public void initAfterSceneAdded() {
        initInHand();
        getScene().setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.E)
                EnemyButtonPressed(null);

            if (inHandGuiPatch == null) {
                return;
            }

            GridPane gridPane = inHandGuiPatch.getGraphic();
            PatchTuple patchTuple = inHandGuiPatch.getPatchTuple();

            if (event.getCode() == KeyCode.R) {
                gridPane.setRotate(gridPane.getRotate() + 90);
                patchTuple.setPatch(patchTuple.getPatch().rotatePatch(Rotation._90));
            } else if (event.getCode() == KeyCode.Q) {
                gridPane.setRotate(gridPane.getRotate() - 90);
                patchTuple.setPatch(patchTuple.getPatch().rotatePatch(Rotation._270));
            } else if (event.getCode() == KeyCode.F) {
                if(gridPane.getRotate() % 180 == 0) {
                    gridPane.setScaleX(gridPane.getScaleX() * -1);

                }
                else {
                    gridPane.setScaleY(gridPane.getScaleY() * -1);

                }
                patchTuple.setPatch(patchTuple.getPatch().flipPatch());
            }
        });

    }

    @FXML
    void ConfirmationButtonPressed(ActionEvent event){
        gameController.getAiController().processAiMove();
        updateWholeScreen(true);
    }

    @FXML
    void AdvanceButtonPressed(ActionEvent event) {
        PlayerController playerController = gameController.getPlayerController();
        if (playerController.pass()) {
            ((VBox)Output.getContent()).getChildren().add(new Label("Place your Special Patch!"));
            loadSpecialPatch();
        } else {
            clearHint();
            updateWholeScreen(true);
        }

    }

    /**
     * Show Enemy Board and other way around
     * @param event = Event
     * @author Marcel
     */
    @FXML
    void EnemyButtonPressed(ActionEvent event) {
        if (blanketField.getStyle().equals((activeColor))){
            updateWholeScreen(false);
        }
        else{
            updateWholeScreen(true);
        }
    }

    /**
     * Shows a Hint
     * @param event = Event
     * @author Marcel
     */
    @FXML
    void HintButtonPressed(ActionEvent event) {
        Turn turn = gameController.getAiController().calculateHint();
        hintTurn = turn;
        String help = hintTurn.isPass() ? "Advance." : "Buy the highlighted patch";
        // Change Output to current Hint
        ((VBox)Output.getContent()).getChildren().add(new Label((help)));
        updateWholeScreen(true);
    }

    void highlightAdvance(boolean highlight){
        if(highlight) AdvanceButton.setStyle(highlightColor);
        else AdvanceButton.setStyle(null);
    }

    void highlightPatch(Patch patch){
        for(Map.Entry<GridPane,GUIPatch> entry :gridPaneGUIPatchMap.entrySet()){
            if(entry.getValue().getPatchTuple().getPatch().equals(patch)){
                List<Node> views = entry.getKey().getChildren();
                for(int i = 0; i < views.size(); i++){
                    ImageView patchView = (ImageView)views.get(i);
                    patchView.setImage(new Image("/view/texture/1mal1/Highlight.png"));
                }
            }
        }
    }

    void highlightPlacement(PatchTuple patchTuple){
        for(int i = 0; i < 81; i++){
            Pane tile = (Pane)blanketField.getChildren().get(i);
            boolean matrix[][] = patchTuple.getMatrixRepresentation();
            if(matrix[i%9][i/9]){
                tile.setStyle(highlightColor);
            }
        }
        System.out.println();
        patchTuple.getMatrixRepresentation();
    }

    @FXML
    void HomeButtonPressed(ActionEvent event) {
        gameController.getIoController().saveGame();
        gameController.getGUI().backToStartmenu();
    }

    /**
     * @param event = Event
     * @author Ziad
     */
    @FXML
    void RedoButtonPressed(ActionEvent event) {
        gameController.getGameStateController().redoMove();
        updateWholeScreen(true);
    }

    /**
     * @param event = Event
     * @author Ziad
     */
    @FXML
    void UndoButtonPressed(ActionEvent event) {
        gameController.getGameStateController().undoMove();
        updateWholeScreen(true);
    }

    public int getCurrentPatchPosition() {
        return currentPatchPosition;
    }

    public double calculateTopLeftXOfTileImage(Patch patch, GridPane gridPane) {
        if (calcFlip(gridPane.getScaleX())) {
            patch = patch.flipPatch();
        }

        patch = patch.rotatePatch(calcRotation(gridPane.getRotate()));

        Patch newPatch = new Patch(
                moveMatrixToTopLeft(patch.getMatrix()),
                patch.getTime(),
                patch.getCost(),
                patch.getIncome()
        );

        double patchWidth = gridPane.getWidth();
        //calc max length
        boolean[] truthRow = new boolean[5];
        for (boolean[] patchrow : newPatch.getMatrix()) {
            int indexRunner = 0;
            for (boolean patchElement : patchrow) {
                if (patchElement) {
                    truthRow[indexRunner] = true;
                }
                indexRunner++;
            }
        }

        //maxTiles
        int maxTiles = 0;
        for (boolean isTile : truthRow) {
            if (isTile) {
                maxTiles++;
            } else {
                break;
            }
        }

        double OneTileWidth = patchWidth / maxTiles;

        for (boolean[] patchrow : newPatch.getMatrix()) {
            int indexCounter = 0;
            for (boolean patchElement : patchrow) {
                if (patchElement) {
                    return OneTileWidth * indexCounter + 10;
                }
                indexCounter++;
            }
        }
        return 0;
    }

    public Rotation calcRotation(double rotationDegree) {
        switch ((int) rotationDegree % 360) {
            case 0:
                return Rotation._0;
            case 270:
                return Rotation._270;
            case 180:
                return Rotation._180;
            case 90:
                return Rotation._90;
        }
        return Rotation._0;
    }

    public boolean calcFlip(double flipped){
        return flipped < 0;
    }

    public void addFeedback(Turn turn){
        String textOfTurn = turn.isPass() ? "Advanced" : "Bought Patch";
        ((VBox)Output.getContent()).getChildren().add(new Label(textOfTurn));//TODO Display turn graphically
    }

    public void paintPlacedPatchesEnemy() {
        boolean[][] maxtrixRep = gameController.getGame().getGameState().getPassivePlayer().getBlanket().getBoolMatrixRepresentation();
        boolean[][] maxtrixRep2 = gameController.getGame().getGameState().getActivePlayer().getBlanket().getBoolMatrixRepresentation();
        int maxSize = maxtrixRep2.length;
        int counter = 0;
        for (int i1 = 0; i1 < maxSize; i1++) {
            for (int j = 0; j < maxSize; j++) {
                if (maxtrixRep2[i1][j]) {
                    blanketField.getChildren().get((j * 9) + i1).setStyle("-fx-background-color: transparent");
                    //blanketField.getChildren().get((j * 9) + i1).setVisible(false);
                }
            }
            for (int j = 0; j < maxSize; j++) {
                if (maxtrixRep[i1][j]) {
                    blanketField.getChildren().get((j * 9) + i1).setStyle("-fx-background-color: black");
                    blanketField.getChildren().get((j * 9) + i1).setVisible(true);
                }
            }
        }
    }

    public void blanketLoad(GridPane patch) {
        //Drag over event handler is used for the receiving node to allow movement
        blanketField.setOnDragOver(event -> {
            //data is dragged over to target
            //accept it only if it is not dragged from the same node
            //and if it has image data
            if (event.getGestureSource() != blanketField && event.getDragboard().hasImage()) {
                //allow for moving
                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });

        //Drag entered changes the appearance of the receiving node to indicate to the player that they can place there
        blanketField.setOnDragEntered(event -> {
            //The drag-and-drop gesture entered the target
            //show the user that it is an actual gesture target
            if (event.getGestureSource() != blanketField && event.getDragboard().hasImage()) {
                patch.getChildren().get(0).setVisible(false);
                //blanketField.setOpacity(0.7);
                //System.out.println("Drag entered");
            }
            event.consume();
        });

        //Drag exited reverts the appearance of the receiving node when the mouse is outside of the node
        blanketField.setOnDragExited(event -> {
            //mouse moved away, remove graphical cues

            blanketField.setOpacity(1);

            boolean[][] maxtrixRep = gameController.getGame().getGameState().getPassivePlayer().getBlanket().getBoolMatrixRepresentation();
            //boolean[][] maxtrixRep2 = gameController.getGame().getGameState().getPassivePlayer().getBlanket().getBoolMatrixRepresentation();
            //System.out.println(Arrays.deepToString(maxtrixRep));
            boolean[][] maxtrixRep2 = gameController.getGame().getGameState().getPassivePlayer().getBlanket().getBoolMatrixRepresentation();
            int maxSize = maxtrixRep2.length;


            int counter = 0;
            for (int i1 = 0; i1 < maxSize; i1++) {
                for (int j = 0; j < maxSize; j++) {
                    if (maxtrixRep2[i1][j] || maxtrixRep[i1][j]) {
                        blanketField.getChildren().get((j * 9) + i1).setStyle("-fx-background-color: transparent");
                        //blanketField.getChildren().get((j * 9) + i1).setVisible(false);
                    }
                }
            }


            event.consume();
        });

        //Drag dropped draws the image to the receiving node
        blanketField.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                //Data dropped
                //If there is an image on the dragboard, read it and use it
                Dragboard db = event.getDragboard();
                boolean success = false;
                Node node = (Node) event.getSource();
                event.consume();


            }
        });
    }

    private void createBlanketCells() {
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                Pane cell = new Pane();
                cell.setPrefHeight(blanketField.getPrefHeight() / 9);
                cell.setPrefWidth(blanketField.getPrefHeight() / 9);
                this.panes[x][y] = cell;
            }
        }
    }

    private boolean[][] moveMatrixToTopLeft(boolean[][] patchMatrix) {
        boolean[][] movedMatrix = new boolean[5][5];
        boolean isBroken = false;
        int wholeRowsFalse = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (patchMatrix[i][j]) {
                    isBroken = true;
                    break;
                }
            }
            if (isBroken)
                break;
            ++wholeRowsFalse;
        }

        // Move the Matrix UP
        for (int i = wholeRowsFalse; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                movedMatrix[i - wholeRowsFalse][j] = patchMatrix[i][j];
            }
        }

        boolean[][] movedMatrix2 = new boolean[5][5];
        isBroken = false;
        int wholeColsFalse = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (movedMatrix[j][i]) {
                    isBroken = true;
                    break;
                }
            }
            if (isBroken)
                break;
            ++wholeColsFalse;
        }

        // Move the Matrix LEFT
        for (int i = 0; i < 5; i++) {
            for (int j = wholeColsFalse; j < 5; j++) {
                movedMatrix2[i][j - wholeColsFalse] = movedMatrix[i][j];
            }
        }
        return movedMatrix2;
    }

    public void initializeBlanketCells() {
        Output.vvalueProperty().bind(((VBox)Output.getContent()).heightProperty());
        createBlanketCells();

        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {

                final int finalX = x;
                final int finalY = y;
                VecTwo vecTwo = new VecTwo(finalY, finalX);

                panes[finalX][finalY].setOnMouseClicked(event -> {
                    if (event.getButton() != MouseButton.PRIMARY) {
                        return;
                    }

                    //System.out.println("000000");
                    if (inHandGuiPatch == null) {
                        return;
                    }

                    double patchTupleRotation = inHandGuiPatch.getGraphic().getRotate();
                    double scaleX = inHandGuiPatch.getGraphic().getScaleX();
                    double scaleY = inHandGuiPatch.getGraphic().getScaleY();
                    double flip = 1;

                    if(scaleX == 1 && scaleY == 1){
                    }
                    else if(scaleX == -1 && scaleY == -1){
                        patchTupleRotation += 180;
                    }
                    else if(scaleX == -1 && scaleY == 1){
                        flip = -1;
                    }
                    else {
                        flip = -1;
                        patchTupleRotation += 180;
                    }

//                    System.out.println("scaleX: " + scaleX);
//                    System.out.println("scaleY: " + scaleY);
//                    System.out.println("rotation: " +patchTupleRotation);

                    Patch patch = inHandGuiPatch.getUnmodifiedPatch();
                    PatchTuple patchTuple = new PatchTuple(
                            patch,
                            calcRotation(patchTupleRotation),
                            vecTwo,
                            calcFlip(flip));

                    //System.out.println("111111");
                    if (!gameController.getPlayerController().canBuyPatch(patchTuple.getPatch())) {
                        return;
                    }
                    //System.out.println("222222");
                    if (!BlanketController.canPlacePatchTuple(gameController.getGame().getGameState().getActivePlayer().getBlanket(), patchTuple)) {
                        return;
                    }
                    //System.out.println("333333");
                    boolean extraPatch = gameController.getPlayerController().takePatchAndInsert(patchTuple);

                    clearHint();
                    clearHand();

                    if (extraPatch) {
                        loadSpecialPatch();
                    }

                    //blanketField.add(((ImageView) event.getGestureSource()), col, row); // TODO

                    updateWholeScreen(true);
                    event.consume();
                    //.setText(gameController.getGame().getGameState().getActivePlayer().getName() + " placed a patch.");
                });


                panes[finalX][finalY].setOnMouseMoved(e -> {
                    if (inHandGuiPatch == null) {
                        return;
                    }

                    //System.out.println(selectedPatch);

                    Patch selectedPatch = inHandGuiPatch.getPatchTuple().getPatch();
                    Patch movedPatch = new Patch(
                            moveMatrixToTopLeft(selectedPatch.getMatrix()),
                            selectedPatch.getTime(),
                            selectedPatch.getCost(),
                            selectedPatch.getIncome()
                    );

                    // My friend
                    //System.out.println(movedPatch);

                    int xPosShift = 0;
                    boolean shiftMore = true;


                    PatchTuple patchTuple = new PatchTuple(movedPatch, inHandGuiPatch.getPatchTuple().getRotation(), vecTwo, inHandGuiPatch.getPatchTuple().isFLipped());
                    Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();
                    boolean placeable = BlanketController.canPlacePatchTuple(blanket, patchTuple);
                    //System.out.println(patchTuple);

                    removeHighlightBlanket();
                    showHintPlacement(gameController.getGame().getGameState().getActivePlayer());

                    for (int yRow = 0; yRow < 5; yRow++) {
                        for (int xCol = 0; xCol < 5; xCol++) {

                            // Check Bounds
                            boolean xBounds = panes.length <= finalX + xCol + xPosShift || finalX + xCol + xPosShift < 0;
                            //System.out.println("xBounds: " + xBounds);
                            if (xBounds)
                                continue;

                            boolean yBounds = panes[finalX + xCol + xPosShift].length <= finalY + yRow;
                            if (yBounds)
                                continue;

                            if (movedPatch.getMatrix()[yRow][xCol]) {
                                shiftMore = false;

                                if (placeable) {
                                    panes[finalX + xCol + xPosShift][finalY + yRow].setStyle(LIGHTGREEN);
                                }
                                else {
                                    panes[finalX + xCol + xPosShift][finalY + yRow].setStyle(RED);
                                }
                            }
                            else {
                                if (shiftMore) {
                                    --xPosShift;
                                }
                            }
                        }
                    }
                });

                blanketField.add(panes[x][y], x, y);
                //System.out.println(blanketField.getNodeOrientation());
            }

        }
        updateWholeScreen(true);
        /**
         for(int i = 0; i<panes.size(); i++) {
         panes.setStyle("-fx-background-color: black");
         cell.setVisible(true);

         }*/
    }

    private void clearHint(){
        hintTurn = null;
    }

    /**
     * Return Label Output for changes
     * @author Marcel
     */
    public ScrollPane getOutput() {
        return Output;
    }

    /**
     * Return HBox Position please
     * @param position = requested position
     * @return HBox
     * @author Marcel
     */
    public HBox getPositionHBox(int position) {

        if (position == 0) {
            return Position0;
        } else if (position == 1) {
            return Position1;
        } else if (position == 2) {
            return Position2;
        } else if (position == 3) {
            return Position3;
        } else if (position == 4) {
            return Position4;
        } else if (position == 5) {
            return Position5;
        } else if (position == 6) {
            return Position6;
        } else if (position == 7) {
            return Position7;
        } else if (position == 8) {
            return Position8;
        } else if (position == 9) {
            return Position9;
        } else if (position == 10) {
            return Position10;
        } else if (position == 11) {
            return Position11;
        } else if (position == 12) {
            return Position12;
        } else if (position == 13) {
            return Position13;
        } else if (position == 14) {
            return Position14;
        } else if (position == 15) {
            return Position15;
        } else if (position == 16) {
            return Position16;
        } else if (position == 17) {
            return Position17;
        } else if (position == 18) {
            return Position18;
        } else if (position == 19) {
            return Position19;
        } else if (position == 20) {
            return Position20;
        } else if (position == 21) {
            return Position21;
        } else if (position == 22) {
            return Position22;
        } else if (position == 23) {
            return Position23;
        } else if (position == 24) {
            return Position24;
        } else if (position == 25) {
            return Position25;
        } else if (position == 26) {
            return Position26;
        } else if (position == 27) {
            return Position27;
        } else if (position == 28) {
            return Position28;
        } else if (position == 29) {
            return Position29;
        } else if (position == 30) {
            return Position30;
        } else if (position == 31) {
            return Position31;
        } else if (position == 32) {
            return Position32;
        } else if (position == 33) {
            return Position33;
        } else if (position == 34) {
            return Position34;
        } else if (position == 35) {
            return Position35;
        } else if (position == 36) {
            return Position36;
        } else if (position == 37) {
            return Position37;
        } else if (position == 38) {
            return Position38;
        } else if (position == 39) {
            return Position39;
        } else if (position == 40) {
            return Position40;
        } else if (position == 41) {
            return Position41;
        } else if (position == 42) {
            return Position42;
        } else if (position == 43) {
            return Position43;
        } else if (position == 44) {
            return Position44;
        } else if (position == 45) {
            return Position45;
        } else if (position == 46) {
            return Position46;
        } else if (position == 47) {
            return Position47;
        } else if (position == 48) {
            return Position48;
        } else if (position == 49) {
            return Position49;
        } else if (position == 50) {
            return Position50;
        } else if (position == 51) {
            return Position51;
        } else if (position == 52) {
            return Position52;
        } else if (position == 53) {
            return Position53;
        } else {
            return null;
        }
    }

    public void nameUpdate() {
        activePlayername.setText(gameController.getGame().getGameState().getActivePlayer().getName());
    }

    public void positionUpdate() {
        Player currentPlayer = gameController.getGame().getGameState().getActivePlayer();
        String currentPlayerName = currentPlayer.getName();
        int currentPlayerPosition = currentPlayer.getPos();

        Player waitingPlayer = gameController.getGame().getGameState().getPassivePlayer();
        String waitingPlayerName = waitingPlayer.getName();
        int waitingPlayerPosition = waitingPlayer.getPos();

        for (int i = 0; i <= 53; i++) {
            getPositionHBox(i).getStyleClass().remove("player-1");
            getPositionHBox(i).getStyleClass().remove("player-2");
        }
        activePlayername.getStyleClass().remove("player1color");
        activePlayername.getStyleClass().remove("player2color");
        activePlayername.getStyleClass().remove("static-text-2");

        if (currentPlayerName.equals(Player1)) {
            getPositionHBox(currentPlayerPosition).getStyleClass().add("player-1");
            getPositionHBox(waitingPlayerPosition).getStyleClass().add("player-2");
            activePlayername.getStyleClass().add("player1color");
        } else {
            getPositionHBox(currentPlayerPosition).getStyleClass().add("player-2");
            getPositionHBox(waitingPlayerPosition).getStyleClass().add("player-1");
            activePlayername.getStyleClass().add("player2color");
        }

    }

    public void availablePatchesUpdate(List<Patch> allPatches) {
        /* TODO

         //GameBoard gameBoard = gameController.getGame().getGameState().getGameBoard();
         List<Patch> patch = allPatches;
        //List<Patch> patch = gameController.getGameBoardController().getCurrentPatchList();
        scrollPane.setContent(scrollPaneBox2);
        scrollPane.setPannable(true);
        for (int i = 0; i < patch.size(); i++) {
            //System.out.println(patch.get(i).getPngString());
            GUIPatch patchImage = new GUIPatch(patch.get(i).getPngString());
            patchImage.setPatch(patch.get(i));
            ImageView imageView = patchImage.getImageView();
            imageView.setId(i + " imageview");
            patchImages.put(imageView.getId(), patch.get(i));
            imageView.setPreserveRatio(false);
            imageView.setFocusTraversable(true);

        }

        */
    }

    public void removeHighlightBlanket() {
        Arrays.stream(panes).forEach(e -> Arrays.stream(e).forEach(x -> x.setStyle(TRANSPARENT)));
    }

    public void loadSpecialPatch(){
        GUIPatch guiPatch = createGuiPatch(Constants.generateSpecialPatch());
        putInHand(guiPatch);
        disableGraphic(true);
    }

    public void loadDifferentPatches() {
        List<Patch> patches = gameController.getGameBoardController().getCurrentPatchList();
        List<GUIPatch> guiPatches = patches.stream().map(this::createGuiPatch).collect(Collectors.toList());

        HBox multiplePatches = new HBox();
        multiplePatches.setSpacing(ViewConstants.PATCHES_IN_SELECTION_SPACING);

        guiPatches.forEach(guiPatch -> {
            VBox patchOnTop = new VBox();
            patchOnTop.setBorder(new Border(new BorderStroke(Color.BLACK,
                    BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
            patchOnTop.setStyle("-fx-border-color: #d9ce07");
            patchOnTop.setPadding(new Insets(2d, 2d, 2d, 2d));
            patchOnTop.getChildren().add(guiPatch.getGraphic());
            patchOnTop.getChildren().add(new Region());
            VBox.setVgrow(patchOnTop.getChildren().get(1), Priority.ALWAYS);

            HBox filler = new HBox();
            if(gameController.getGameBoardController().getAvailablePatches().contains(guiPatch.getUnmodifiedPatch()) &&
                gameController.getPlayerController().canBuyPatch(guiPatch.getUnmodifiedPatch()) &&
                BlanketController.canPlacePatch(gameController.getGame().getGameState().getActivePlayer().getBlanket(),guiPatch.getUnmodifiedPatch())){
                patchOnTop.setStyle(LIGHTGREEN);
            }
            else{
                patchOnTop.setStyle(RED);
            }

            filler.getChildren().add(
                    constructHBox("/view/texture/1mal1/BUTTON.png",
                    guiPatch.getPatchTuple().getPatch().getIncome())
            );

            filler.getChildren().add(
                    constructHBox("/view/texture/1mal1/COST.png",
                    guiPatch.getPatchTuple().getPatch().getCost())
            );

            filler.getChildren().add(
                    constructHBox("/view/texture/1mal1/HOURGLASS.png",
                    guiPatch.getPatchTuple().getPatch().getTime())
            );
            filler.setSpacing(5d);

            patchOnTop.getChildren().add(filler);
            // Insert Whole Patch Thinge me jig here
            multiplePatches.getChildren().add(patchOnTop);
        });
        scrollPane.setContent(multiplePatches);

        gridPaneGUIPatchMap = new HashMap<>();
        for (GUIPatch guiPatch : guiPatches) {
            GridPane insertGridPane = guiPatch.getGraphic();
            insertGridPane.setId(insertGridPane.hashCode()+"gridpane");
            gridPaneGUIPatchMap.put(insertGridPane, guiPatch);
        }
    }

    private HBox constructHBox(String imageURL, int patchAttribute) {
        HBox hbox = new HBox();
        ImageView image = new ImageView(imageURL);
        image.setFitHeight(PATCH_ATTRIBUTE_SIZE);
        image.setFitWidth(PATCH_ATTRIBUTE_SIZE);
        Label label = new Label(Integer.toString(patchAttribute));
        label.setAlignment(Pos.BASELINE_LEFT);
        label.setPrefHeight(PATCH_ATTRIBUTE_SIZE);
        label.setPrefWidth(PATCH_ATTRIBUTE_SIZE);
        hbox.getChildren().add(image);
        hbox.getChildren().add(label);
        return hbox;
    }

    public GUIPatch createGuiPatch(Patch patch) {
        GridPane gridPane = new GridPane();
        PatchTuple patchTuple = new PatchTuple(patch, Rotation._0, new VecTwo(0, 0), false);
        GUIPatch guiPatch = new GUIPatch(patchTuple, gridPane);

        gridPane.setVisible(true);

        boolean[][] matrix = patch.getMatrix();
        int fixedNumber;
        if((patch.getCost()+patch.getTime()+patch.getIncome()) == 0) { fixedNumber = 33; }
        else { fixedNumber = (patch.getCost() * 5 + patch.getTime() * 7 + patch.getIncome() * 11) % 31 + 1; }
        for (int n = 0; n < 5; n++) {
            for (int m = 0; m < 5; m++) {
                if (matrix[n][m]) {
                    String imageURL = "/view/texture/1mal1/Randomizable/"+fixedNumber+".png";
                    ImageView image = new ImageView(imageURL);
                    image.setFitWidth(blanketField.getPrefHeight() / 9);
                    image.setFitHeight(blanketField.getPrefHeight() / 9);

                    gridPane.add(image, m, n);
                }
            }
        }

        //System.out.println(patch);
        gridPane.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                if (inHandGuiPatch != null) {
                    throw new IllegalStateException();
                }
                System.out.println("Selected a Patch!");
                if (gameController.getGame().getGameState().getActivePlayer().getBlanket().getPatchTuples().contains(guiPatch.getPatchTuple())) {
                    System.out.println("Patch already on a Blanket!");
                    return;
                }
                if (gameController.getGame().getGameState().getPassivePlayer().getBlanket().getPatchTuples().contains(guiPatch.getPatchTuple())) {
                    System.out.println("Patch already on a Blanket!");
                    return;
                }
                if (!gameController.getGameBoardController().getAvailablePatches().contains(patchTuple.getPatch())) {
                    return;
                }
                if(gameController.getGame().getGameState().isFinished()){
                    return;
                }
                putInHand(guiPatch);
            }
        });

        gridPane.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {

                //System.out.println("setondragover");
                if (event.getGestureSource() != gridPane &&
                        event.getDragboard().hasImage()) {
                    event.acceptTransferModes(TransferMode.NONE);

                }

                event.consume();

            }
        });

        blanketLoad(gridPane);
        gridPane.setOnDragDone(event -> {
            //System.out.println("setondragdone");
            //the drag and drop gesture has ended
            //if the data was successfully moved, clear it
            if (event.getTransferMode() == TransferMode.MOVE) {
                gridPane.setVisible(true);
                //System.out.println(patchTuple);
                //gameController.getPlayerController().takePatchAndInsert(patchTuple);
            }
            event.consume();
        });

        gridPane.setVisible(true);

        return guiPatch;
    }

    public void initInHand() {
        GameScreenController gameScreenController = this;

        getScene().setOnMouseMoved(event -> {
            if (inHandGuiPatch == null) {
                return;
            }
            if (!gameScreenController.getChildren().contains(inHandGuiPatch.getGraphic())) {
                inHandGuiPatch.getGraphic().setManaged(false); // Else position is fixed on parent
                gameScreenController.getChildren().add(inHandGuiPatch.getGraphic());
            }
            inHandGuiPatch.getGraphic().setLayoutX(event.getX());
            inHandGuiPatch.getGraphic().setLayoutY(event.getY());
        });

        getScene().setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY && !isHandPatchExtraPatch()) {
                clearHand();
                removeHighlightBlanket();
                showHintPlacement(gameController.getGame().getGameState().getActivePlayer());
            }
        });
        disableGraphic(false);
    }

    public void putInHand(GUIPatch guiPatch) {
        System.out.println("Ne Patch in Hand!");
        if(!gameController.getPlayerController().canBuyPatch(guiPatch.getPatchTuple().getPatch())) {
            return;
        }
        this.inHandGuiPatch = guiPatch;
        guiPatch.getGraphic().setDisable(true);
    }

    public void clearHand() {
        if (inHandGuiPatch == null) {
            return;
        }
        getChildren().remove(inHandGuiPatch.getGraphic());
        inHandGuiPatch.getGraphic().setDisable(false);

        this.inHandGuiPatch = null;
        updateWholeScreen(true);
        disableGraphic(false);
    }

    private void showAdvance(boolean show) {
        AdvanceButton.setVisible(show);
    }

    private void showConfirmationButton(boolean show) {
        ConfirmationButton.setVisible(show);
    }

    private void showPlacePatches(boolean show) {
        scrollPane.setDisable(!show);
    }

    private boolean isHandPatchExtraPatch(){
        if(inHandGuiPatch == null){
            return false;
        }
        return inHandGuiPatch.getUnmodifiedPatch().equals(Constants.generateSpecialPatch());
    }

    /**
     * Updated alles einmal
     * @author Marcel
     */
    public void updateWholeScreen(boolean showActivePlayerScreen){

        gameBoardChanger();
        //System.out.println("###UPDATING_WHOLE_SCREEN###");

        //generalUpdates
        activePlayername.setText(gameController.getGame().getGameState().getActivePlayer().getName());
        positionUpdate();
        availablePatches = gameController.getGameBoardController().getAvailablePatches();
        loadDifferentPatches();
        availablePatchesUpdate(gameController.getGameBoardController().getCurrentPatchList());
        highlightAdvance(false);
        paintBlanket(showActivePlayerScreen ? gameController.getGame().getGameState().getActivePlayer() : gameController.getGame().getGameState().getPassivePlayer(), showActivePlayerScreen);
        gameBoardChanger();

        if (gameController.getGameStateController().redoPossible()) RedoButton.setDisable(false);
        else RedoButton.setDisable(true);
        if (gameController.getGameStateController().undoPossible()) UndoButton.setDisable(false);
        else UndoButton.setDisable(true);


        showHintButton(gameController.getGame().getGameState().getActivePlayer().getAIInformation() == null);
        //loading active/passive playerscreen (depends on input)
        if (showActivePlayerScreen) showPlayerView(gameController.getGame().getGameState().getActivePlayer(),true);
        else showPlayerView(gameController.getGame().getGameState().getPassivePlayer(),false);

        if (gameController.getGame().getGameState().isFinished()) {
            if(!alreadyExecuted) {
                final JDialog dialog = new JDialog();
                dialog.setAlwaysOnTop(true);
                Player winner = gameController.getPlayerController().calculateWinner(gameController.getGame().getGameState());
                String winnerName = winner.getName();
                JOptionPane.showMessageDialog(dialog, "Ende! " + winnerName + " hat gewonnen.", "Beep Boop.", JOptionPane.INFORMATION_MESSAGE);
                alreadyExecuted = true;
            }
        }
    }

    private void showHintButton(boolean show){
        HintButton.setDisable(!show);
    }

    private void paintBlanket(Player player,boolean activePlayer){
        Collection<PatchTuple> patchTuples = player.getBlanket().getPatchTuples();
        for(Node tile : blanketField.getChildren()){
            try {
                ((Pane) tile).getChildren().clear();
            }
            catch(Exception e){
            }
            tile.setStyle(null);
        }
        for(PatchTuple patchTuple : patchTuples){
            renderPatchInBlanket(patchTuple);
        }

        if(activePlayer) showHintPlacement(player);
    }

    private void showHintPlacement(Player player){
        if(hintTurn == null){
            return;
        }
        if(hintTurn.isPass()){
            highlightAdvance(true);
        }
        else{
            highlightPatch(hintTurn.getBuyAndPlacePatchTuple().getPatch());
            highlightPlacement(hintTurn.getBuyAndPlacePatchTuple());
        }
        if(hintTurn.hasExtraPatch()){
            highlightPlacement(hintTurn.getExtraPatchTuple());
        }
    }

    private void renderPatchInBlanket(PatchTuple patchTuple){
        boolean[][] matrix = patchTuple.getMatrixRepresentation();
        for(int i = 0; i < blanketField.getChildren().size(); i++){
            if(matrix[i%9][(i / 9)%9]){
                try {
                    int fixedNumber;
                    if((patchTuple.getPatch().getCost()+patchTuple.getPatch().getTime()+patchTuple.getPatch().getIncome()) == 0) { fixedNumber = 33; }
                    else { fixedNumber = (patchTuple.getPatch().getCost() * 5 + patchTuple.getPatch().getTime() * 7 + patchTuple.getPatch().getIncome() * 11) % 31 + 1; }
                    ImageView image = new ImageView("/view/texture/1mal1/Randomizable/" + fixedNumber + ".png");
                    image.setFitWidth(blanketField.getPrefWidth() / 9.5);
                    image.setFitHeight(blanketField.getPrefHeight() / 10.5);
                    ((Pane) blanketField.getChildren().get(i)).getChildren().add(image);
                }
                catch(Exception e){
                }
            }
            else{
                try {
                    blanketField.getChildren().get(i).setStyle("-fx-background-color: transparent");
                }
                catch(Exception e){
                }
            }
        }
    }

    /**
     * @param player
     * @author Raphael
     */
    private void showPlayerView(Player player, boolean friendlyBoard){

        String color = player.getName().equals(gameController.getGame().getGameState().getActivePlayer().getName()) ? activeColor : passiveColor;

        //Updating Colors
        blanketField.setStyle(color); //DONT CHANGE! @Raphael wenn nötig pls!

        ButtonCount.setStyle(color);
        ButtonCount.setText("Button Count");
        ButtonCountValue.setStyle(color);
        ButtonCountValue.setText(String.valueOf(player.getButtonCount()));

        BlanketIncome.setStyle(color);
        BlanketIncome.setText("Blanket Income");
        BlanketIncomeValue.setStyle(color);
        BlanketIncomeValue.setText(String.valueOf(player.getBlanket().calcIncome()));

        PatchCount.setStyle(color);
        PatchCount.setText("Patch Count");
        PatchCountValue.setStyle(color);
        PatchCountValue.setText(String.valueOf(player.getBlanket().getBlanketAllPatchCount()));

        SpecialToken.setStyle(color);
        SpecialToken.setText("Special-Token");
        SpecialTokenValue.setStyle(color);
        SpecialTokenValue.setText(String.valueOf(player.hasSpecialToken()));

        HighscoreActive.setStyle(color);
        HighscoreActive.setText("Highscore active");
        HighscoreActiveValue.setStyle(color);
        HighscoreActiveValue.setText(String.valueOf(player.isHighscoreActivated()));

        CurrentScore.setStyle(color);
        CurrentScore.setText("Current Score");
        CurrentScoreValue.setStyle(color);
        CurrentScoreValue.setText(String.valueOf(PlayerController.calculateScore(player)));

        FullCellsPercentage.setStyle(color);
        FullCellsPercentage.setText("Filled Cells");
        FullCellsPercentageValue.setStyle(color);
        FullCellsPercentageValue.setText((int) (player.getBlanket().getBlanketAllFullCells() * 100.0 / 88) + "%");

        CurrentLead.setStyle(color);
        CurrentLead.setText("Current Lead");
        CurrentLeadValue.setStyle(color);
        CurrentLeadValue.setText(String.valueOf(PlayerController.calculateScore(player) - PlayerController.calculateScore(gameController.getGameStateController().getOtherPlayer(player))));

         activePlayername.setStyle(color);
        if(player.getName().equals(gameController.getGame().getGameState().getActivePlayer().getName())){
            activePlayername.setText(gameController.getGame().getGameState().getActivePlayer().getName());
        } else {
            activePlayername.setText(gameController.getGame().getGameState().getPassivePlayer().getName());
        }

        //Color-Updates done

        if (gameController.getGame().getGameState().getActivePlayer().getAIInformation() != null){
            showPlacePatches(false);
            showAdvance(false);
            showConfirmationButton(gameController.getGame().getGameState().getActivePlayer().getAIInformation().getSimulationSpeed() == SimulationSpeed.CONFIRMATION);
        }
        else{
            showPlacePatches(true);
            showAdvance(true);
            showConfirmationButton(false);
        }
        if(!friendlyBoard){
            showPlacePatches(false);
        }

    }

    public void disableGraphic(boolean value) {
        UndoButton.setDisable(value);
        RedoButton.setDisable(value);
        AdvanceButton.setDisable(value);
        HomeButton.setDisable(value);
        EnemyButton.setDisable(value);
        //
        if(gameController.getGame().getRedoStack().empty()) {
            RedoButton.setDisable(true);
        }
    }

    public void gameBoardChanger() {
        Player player1 = gameController.getGame().getGameState().getActivePlayer();
        Player player2 = gameController.getGame().getGameState().getPassivePlayer();

        GameField.getStyleClass().remove("grid-background");
        GameField.getStyleClass().remove("grid-background-1");
        GameField.getStyleClass().remove("grid-background-2");
        GameField.getStyleClass().remove("grid-background-3");
        GameField.getStyleClass().remove("grid-background-4");
        GameField.getStyleClass().remove("grid-background-5");

        if (player1.getPos() > 49 || player2.getPos() > 49) {
            GameField.getStyleClass().add("grid-background-5");
        } else if (player1.getPos() > 43 || player2.getPos() > 43) {
            GameField.getStyleClass().add("grid-background-4");
        } else if (player1.getPos() > 37 || player2.getPos() > 37) {
            GameField.getStyleClass().add("grid-background-3");
        } else if (player1.getPos() > 31 || player2.getPos() > 31) {
            GameField.getStyleClass().add("grid-background-2");
        } else if (player1.getPos() > 25 || player2.getPos() > 25) {
            GameField.getStyleClass().add("grid-background-1");
        } else {
            GameField.getStyleClass().add("grid-background");
        }
    }

}