package view;

import Resources.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import model.Patch;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import javafx.scene.control.Label;


import controller.GameController;
import model.*;

import javax.swing.*;

public class SetupController extends AnchorPane {

    @FXML
    private Button startButton;

    @FXML
    private Button backButton;

    @FXML // fx:id="difficultGame"
    private ChoiceBox<String> difficultAI1; // Value injected by FXMLLoader

    @FXML // fx:id="difficultGame"
    private ChoiceBox<String> difficultAI2; // Value injected by FXMLLoader

    @FXML // fx:id="speedKI"
    private ChoiceBox<String> speedAI1; // Value injected by FXMLLoader

    @FXML // fx:id="speedKI"
    private ChoiceBox<String> speedAI2; // Value injected by FXMLLoader

    @FXML // fx:id="ki1"
    private CheckBox ki1; // Value injected by FXMLLoader

    @FXML // fx:id="feedback"
    private CheckBox feedbackAI1; // Value injected by FXMLLoader

    @FXML // fx:id="feedback"
    private CheckBox feedbackAI2; // Value injected by FXMLLoader

    @FXML // fx:id="startplayerChooser"
    private ChoiceBox<String> startplayerChooser; // Value injected by FXMLLoader

    @FXML // fx:id="importCSV"
    private Button importCSV; // Value injected by FXMLLoader

    @FXML // fx:id="shuffle"
    private CheckBox shuffle; // Value injected by FXMLLoader

    @FXML // fx:id="ki2"
    private CheckBox ki2; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private TextField player1Name; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private TextField player2Name; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label difficultLabel1; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label feedbackLabel1; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label speedLabel1; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label playerName2Label; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label difficult2Label; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label playerNameLabel; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label feedback2Label; // Value injected by FXMLLoader

    @FXML // fx:id="playerName"
    private Label speed2Label; // Value injected by FXMLLoader

    private List<Patch> patchList;

    private GameController gameController;

    AIInformation ai1information;

    AIInformation ai2information;

    Player player1;

    Player player2;

    boolean ki1PressedValue;

    boolean ki2PressedValue;

    boolean shouldSshuffle;

   // String startPlayerGame;

    public SetupController(GameController gameController)
    {
        this.gameController = gameController;
        ki1PressedValue = true;
        ki2PressedValue = true;
        shouldSshuffle = false;



        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Setup.fxml"));
            loader.setRoot(this);
            loader.setController(this);

            loader.load();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void BackButtonPressed(ActionEvent event) {
        gameController.getGUI().backToStartmenu();
    }

    /**
     *
     * @param event
     * @author Reworked by Raphael
     */
    @FXML
    void StartButtonPressed(ActionEvent event){
        final JDialog dialog = new JDialog();
        dialog.setAlwaysOnTop(true);

        if (!ki1.isSelected() && !ki2.isSelected() && player1Name.getText().isEmpty() && player2Name.getText().isEmpty() ){
            JOptionPane.showMessageDialog(dialog, "Namen fehlen!", "Beep Boop.", JOptionPane.INFORMATION_MESSAGE); return;
        }
        if (!ki1.isSelected() && player1Name.getText().isEmpty()){
            JOptionPane.showMessageDialog(dialog, "Name vom ersten Spieler fehlt!", "Beep Boop.", JOptionPane.INFORMATION_MESSAGE); return;
        }
        if (!ki2.isSelected() && player2Name.getText().isEmpty()){
            JOptionPane.showMessageDialog(dialog, "Name vom zweiten Spieler fehlt!", "Beep Boop.", JOptionPane.INFORMATION_MESSAGE); return;
        }

        //Player1
        String name1 = player1Name.getText();
        boolean isKi1 = ki1.isSelected();
        Player player1;
        if (isKi1){
            player1 = new Player(ai1information.getDifficulty().getGuiName() + "_" + Constants.randomLizenz(), ai1information);
        }
        else player1 = new Player (name1, null);

        //Player2
        String name2 = player2Name.getText();
        boolean isKi2 = ki2.isSelected();
        Player player2;
        if(isKi2){
            player2 = new Player(ai2information.getDifficulty().getGuiName() + "_" + Constants.randomLizenz(), ai2information);
        }
        else player2 = new Player(name2, null);

        //Shuffle
        if(shouldSshuffle) Collections.shuffle(patchList);

        //Startplayer
        if (startplayerChooser.getValue().equals("Player2") || (startplayerChooser.getValue().equals("Random") && Math.random() > 0.5)){
            //swapping Players
            Player tmp = player1.clone();
            player1 = player2.clone();
            player2 = tmp;
        }

        if (player1.getName().equals(player2.getName())) JOptionPane.showMessageDialog(dialog, "Namen beider Spieler gleich!", "Beep Boop.", JOptionPane.INFORMATION_MESSAGE);

        //start
        if(shouldSshuffle) Collections.shuffle(patchList);
        gameController.getGameStateController().setUpGame(player1, player2, patchList);
        gameController.getGUI().setGameScreenContoller(new GameScreenController(gameController));
        gameController.getGUI().showPane(gameController.getGUI().getGameScreenController());
        gameController.getGUI().getGameScreenController().initAfterSceneAdded();
        if (gameController.getGame().getGameState().getActivePlayer().getAIInformation() != null && gameController.getGame().getGameState().getActivePlayer().getAIInformation().getSimulationSpeed() != SimulationSpeed.CONFIRMATION) {
            gameController.getGameStateController().initAITurn();
        }
    }


    @FXML
    void feedbackAI1Pressed(ActionEvent event) {
        ai1information.setProvidingFeedback(!ai1information.isProvidingFeedback());
    }

    @FXML
    void feedbackAI2Pressed(ActionEvent event) {
        ai2information.setProvidingFeedback(!ai2information.isProvidingFeedback());
    }

    @FXML
    void importCSVPressed(ActionEvent event) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Importing Patchlist...");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
            String path = fileChooser.showOpenDialog(gameController.getGUI().getWindow()).getAbsolutePath();
            List<Patch> patches = gameController.getIoController().importCSV(path);
            if (patches != null) patchList = patches;
        }
        catch(Exception e)
        {

        }
    }

    @FXML
    void ki1Pressed(ActionEvent event) {
        try {
            if (ki1PressedValue == true) {
                ai1information = new AIInformation(false, null, null);
                playerNameLabel.setVisible(false);
                player1Name.setVisible(false);
                feedbackAI1.setVisible(true);
                feedbackLabel1.setVisible(true);
                speedAI1.setVisible(true);
                speedLabel1.setVisible(true);
                difficultLabel1.setVisible(true);
                difficultAI1.setVisible(true);

                ObservableList<String> choiceAIspeed = FXCollections.observableArrayList(
                        "NOSTOP", "DELAYED","CONFIRMATION");

                speedAI1.setItems(choiceAIspeed);
                speedAI1.setValue(choiceAIspeed.get(0));

                ObservableList<String> choiceDifficult = FXCollections.observableArrayList(
                        "HARDCORE", "MISSIONARY", "SOFTCORE");

                difficultAI1.setItems(choiceDifficult);
                difficultAI1.setValue(choiceDifficult.get(0));

                ki1PressedValue = false;
            }
            else
                {
                difficultLabel1.setVisible(false);
                difficultAI1.setVisible(false);
                playerNameLabel.setVisible(true);
                player1Name.setVisible(true);
                feedbackAI1.setVisible(false);
                feedbackLabel1.setVisible(false);
                speedAI1.setVisible(false);
                speedLabel1.setVisible(false);
                ki1PressedValue = true;
                ai1information = null;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @FXML
    void ki2Pressed(ActionEvent event) {


        if (ki2PressedValue) {
            ai2information = new AIInformation(false,null,null);
            playerName2Label.setVisible(false);
            player2Name.setVisible(false);
            feedbackAI2.setVisible(true);
            feedback2Label.setVisible(true);
            speedAI2.setVisible(true);
            speed2Label.setVisible(true);
            difficult2Label.setVisible(true);
            difficultAI2.setVisible(true);

            ObservableList<String> choiceAIspeed = FXCollections.observableArrayList(
                    "NOSTOP", "DELAYED","CONFIRMATION");

            speedAI2.setItems(choiceAIspeed);
            speedAI2.setValue(choiceAIspeed.get(0));

            ObservableList<String> choiceDifficult = FXCollections.observableArrayList(
                    "HARDCORE", "MISSIONARY", "SOFTCORE");

            difficultAI2.setItems(choiceDifficult);
            difficultAI2.setValue(choiceDifficult.get(0));

            this.ki2PressedValue = false;
        }
        else
        {
            playerName2Label.setVisible(true);
            player2Name.setVisible(true);
            feedbackAI2.setVisible(false);
            feedback2Label.setVisible(false);
            speedAI2.setVisible(false);
            speed2Label.setVisible(false);
            difficult2Label.setVisible(false);
            difficultAI2.setVisible(false);
            this.ki2PressedValue = true;
            ai2information = null;
        }
    }

    @FXML
    void player1NameChange(ActionEvent event){
        String name = player1Name.getText();
        player1 = new Player(name, null);
    }

    @FXML
    void player2NameChange(ActionEvent event){
        String name = player2Name.getCharacters().toString();
        player2 = new Player(name,null);
    }

    @FXML
    void speedAI1Changed(ActionEvent event){
        String choice =  this.speedAI1.getValue();
        switch (choice)
        {
            case "CONFIRMATION":
                ai1information.setSimulationSpeed(SimulationSpeed.CONFIRMATION);
                break;

            case "DELAYED":
                ai1information.setSimulationSpeed(SimulationSpeed.DELAYED);
                break;

            case "NOSTOP":
                ai1information.setSimulationSpeed(SimulationSpeed.NOSTOP);
                break;

            default:
                break;
        }
    }

    @FXML
    void speedAI2Changed(ActionEvent event){
        String choice =  this.speedAI2.getValue();


        switch (choice) {
            case "CONFIRMATION":
                ai2information.setSimulationSpeed(SimulationSpeed.CONFIRMATION);
                break;
            case "DELAYED":
                ai2information.setSimulationSpeed(SimulationSpeed.DELAYED);
                break;
            case "NOSTOP":
                ai2information.setSimulationSpeed(SimulationSpeed.NOSTOP);
                break;
        }
    }

    @FXML
    void ai1DifficultyChanged(ActionEvent event)
    {
        String choice = difficultAI1.getValue();
        switch(choice)
        {
            case "HARDCORE":
                ai1information.setDifficulty(Difficulty.HARDCORE);
                break;
            case "SOFTCORE":
                ai1information.setDifficulty(Difficulty.SOFTCORE);
                break;
            case "MISSIONARY":
                ai1information.setDifficulty(Difficulty.MISSIONARY);
                break;

            default: break;

        }

    }

    @FXML
    void ai2DifficultyChanged(ActionEvent event)
    {
        String choice = difficultAI2.getValue();

        switch(choice)
        {
            case "HARDCORE":
                ai2information.setDifficulty(Difficulty.HARDCORE);
                break;
            case "SOFTCORE":
                ai2information.setDifficulty(Difficulty.SOFTCORE);
                break;
            case "MISSIONARY":
                ai2information.setDifficulty(Difficulty.MISSIONARY);
                break;

            default: break;

        }

    }

    @FXML
    void shufflePressed(ActionEvent event) {
        shouldSshuffle = !shouldSshuffle;
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        patchList = gameController.getIoController().importCSV();

        ObservableList<String> choiceStartplayer = FXCollections.observableArrayList(
                "Random","Player1","Player2");

        startplayerChooser.setItems(choiceStartplayer);
        startplayerChooser.setValue(choiceStartplayer.get(0));

            feedbackAI1.setVisible(false);
            feedbackAI2.setVisible(false);
            speedAI1.setVisible(false);
            speedAI2.setVisible(false);
            difficultAI1.setVisible(false);
            difficultAI2.setVisible(false);
            difficultLabel1.setVisible(false);
            difficult2Label.setVisible(false);
            feedbackLabel1.setVisible(false);
            speedLabel1.setVisible(false);
            feedback2Label.setVisible(false);
            speed2Label.setVisible(false);
            difficult2Label.setVisible(false);
    }
}