package view.texture;

import javafx.scene.layout.GridPane;
import model.Patch;
import model.PatchTuple;

public class GUIPatch {
    private final Patch patch;
    private final PatchTuple patchTuple;
    private final GridPane graphic;

    public GUIPatch(PatchTuple patchTuple, GridPane graphic) {
        this.patch = patchTuple.getPatch();
        this.patchTuple = patchTuple;
        this.graphic = graphic;
    }

    public Patch getUnmodifiedPatch() {
        return patch;
    }

    public PatchTuple getPatchTuple(){
        return patchTuple;
    }

    public GridPane getGraphic() {
        return graphic;
    }
}
