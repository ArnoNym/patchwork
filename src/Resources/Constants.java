package Resources;

import ai.anneat.main.utils.collections.Tuple;
import model.Field;
import model.Patch;
import model.PatchTuple;

import java.util.*;
import java.util.function.Function;

/**
 * @author Raphael
 */
public class Constants {
    public static final int BLANKET_ROWS_COUNT = 9;
    public static final int BLANKET_COLUMNS_COUNT = 9;
    public static final int BLANKET_MAX_EDGES_COUNT = 164;
    public static final int BLANKET_SIDE_EDGES_COUNT = 9;
    public static final int BLANKET_MAX_OUTER_EDGES_COUNT = 4 * BLANKET_SIDE_EDGES_COUNT;
    public static final int BLANKET_MAX_INNER_EDGES_COUNT = BLANKET_MAX_EDGES_COUNT - BLANKET_MAX_OUTER_EDGES_COUNT;
    public static final int BLANKET_MAX_COMPONENT_COUNT = 41;

    public static final int BUTTON_FIELD_COUNT = 9;
    public static final int EXTRA_PATCHES_FIELD_COUNT = 5; // todo check all these

    public static final int FIELDS_COUNT = 53;

    public static Patch generateSpecialPatch() {
        boolean[] col0 = {true, false, false, false, false};
        boolean[] col1 = {false, false, false, false, false};
        boolean[] col2 = {false, false, false, false, false};
        boolean[] col3 = {false, false, false, false, false};
        boolean[] col4 = {false, false, false, false, false};
        boolean[][] rows = {col0, col1, col2, col3, col4};
        return new Patch(rows, 0, 0, 0);
    }

    /**
     * @return timeBoard (only to start a new game)
     * @author Raphael
     */
    public static ArrayList<Field> generateTimeBoard() {
        ArrayList<Field> res = new ArrayList<>();
        for (int i = 0; i <= 53; i++) {
            //Income fields: 5,11,17,...53
            if (i % 6 == 5) {
                res.add(new Field(true, false));
            } else {
                //special Patches - unklare Defintion
                if (i % 6 == 2 && i >= 26) {
                    res.add(new Field(false, true));
                } else {
                    res.add(new Field(false, false));
                }
            }
        }
        return res;
    }

    /**
     * Written this way for efficiency
     *
     * @return How many IncomeButtons are left after the specified position is reached, not including the position
     * @author Leon
     */
    public static int calcIncomeButtonsLeftCount(int position) {
        return BUTTON_FIELD_COUNT - ((position + 1) / 6);
    }

    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static List<PatchTuple> shufflePatchTuples(Collection<PatchTuple> list) {
        //copying
        List<PatchTuple> tmp = new LinkedList<>();
        for (PatchTuple patchTuple : list) tmp.add(patchTuple);

        List<PatchTuple> res = new LinkedList<>();
        while (!tmp.isEmpty()) {
            res.add(tmp.remove(Constants.getRandomNumber(0, tmp.size())));
        }
        return res;
    }

    public static List<Patch> shufflePatches(Collection<Patch> list) {
        //copying
        List<Patch> tmp = new LinkedList<>();
        for (Patch patch : list) {
            tmp.add(patch);
        }

        List<Patch> res = new LinkedList<>();
        while (!tmp.isEmpty()) {
            res.add(tmp.remove(Constants.getRandomNumber(0, tmp.size())));
        }

        return res;
    }

    public static String randomLizenz(){
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 7;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }


    public static Tuple<Double, Double> statisticalAnalysis(List<Integer> integers){
        double avg = 0;
        for (int val : integers){
            avg += val;
        }
        avg /= integers.size();

        double var = 0;
        for (int val : integers){
            var += Math.pow((val - avg),2);
        }
        var /= (integers.size() - 1);

        double standardDeviation = Math.sqrt(var);

        return new Tuple<>(avg, standardDeviation);
    }
}
