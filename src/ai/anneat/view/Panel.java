package ai.anneat.view;

import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Species;
import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.NodeGene;
import ai.anneat.main.utils.Utils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    private int speciesCount;
    private Species species;
    private Agent agent;

    public Panel(int speciesCount, @Nullable Species species, @NotNull Agent agent) {
        this.speciesCount = speciesCount;
        this.species = species;
        this.agent = agent;
    }

    public void setClient(int speciesCount, @Nullable Species species, @NotNull Agent agent) {
        this.speciesCount = speciesCount;
        this.species = species;
        this.agent = agent;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D gr = (Graphics2D) g;

        g.clearRect(0,0,10000,10000);
        g.setColor(Color.black);
        g.fillRect(0,0,10000,10000);

        gr.setColor(Color.green);
        gr.setStroke(new BasicStroke(3));
        gr.drawString(Utils.toString(speciesCount, species, agent),
                (int) (0.01 * this.getWidth()),
                (int) (0.05 * this.getHeight()));

        for(ConnectionGene c: agent.getGenome().getConnections()){
            paintConnection(c, (Graphics2D) g);
        }

        for(NodeGene n: agent.getGenome().getNodes()) {
            paintNode(n, (Graphics2D) g);
        }
    }

    private void paintNode(NodeGene n, Graphics2D g){
        g.setColor(Color.gray);
        g.setStroke(new BasicStroke(3));
        int x = (int)(this.getWidth() * n.getX()) - 10;
        int y = (int)(this.getHeight() * n.getY()) - 10;
        g.drawString(""+n.getId(), x, y - 5);
        g.drawOval(x, y, 20, 20);
    }

    private void paintConnection(ConnectionGene c, Graphics2D g){
        g.setColor(c.isEnabled() ? Color.green:Color.red);
        g.setStroke(new BasicStroke(3));
        g.drawString(new String(c.getWeight() + "       ").substring(0,7),
                (int)((c.getTo().getX() + c.getFrom().getX())* 0.5 * this.getWidth()),
                (int)((c.getTo().getY() + c.getFrom().getY())* 0.5 * this.getHeight()) +15);
        g.drawLine(
                (int)(this.getWidth() * c.getFrom().getX()),
                (int)(this.getHeight() * c.getFrom().getY()),
                (int)(this.getWidth() * c.getTo().getX()),
                (int)(this.getHeight() * c.getTo().getY()));
    }

}
