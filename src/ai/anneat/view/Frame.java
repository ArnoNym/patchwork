package ai.anneat.view;

import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Species;
import ai.anneat.publish.Neat;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Optional;

public class Frame extends JFrame {

    private Neat neat;
    private SpeciesNavigator speciesNavigator;
    private Species species;
    private Agent agent;

    private Panel panel;

    public Frame(Neat neat) {
        this.neat = neat;
        evolve(0);
        nextSpecies();
        paint();
    }

    public Frame(Agent agent) {
        setClient(null, agent);
        paint();
    }

    public void setClient(@Nullable Species species, @NotNull Agent agent) {
        this.species = species;
        this.agent = agent;
    }

    @Override
    public void repaint() {
        panel.setClient(neat.getSpecies().size(), species, agent);
        super.repaint();
    }

    public void paint() throws HeadlessException {
        this.setDefaultCloseOperation(3);

        this.setTitle("NEAT");
        this.setMinimumSize(new Dimension(1100,700));
        this.setPreferredSize(new Dimension(1500,1000));

        this.setLayout(new BorderLayout());

        UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
        try {
            UIManager.setLookAndFeel(looks[3].getClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        JPanel menu = new JPanel();
        menu.setPreferredSize(new Dimension(1000,100));
        menu.setLayout(new GridLayout(1,6));

        if (neat != null) {
            addEvolveButtons(menu);
        }

        /*
        JButton buttonG = new JButton("Calculate");
        buttonG.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calculator c = new Calculator(anneat.genome);
                System.out.println(Arrays.toString(c.calculate(1,1,1,1,1,1,1,1,1,1)));
                repaint();
            }
        });
        menu.add(buttonG);
        */

        this.add(menu, BorderLayout.NORTH);

        int speciesCount = neat == null ? 0 : neat.getSpecies().size();
        this.panel = new Panel(speciesCount, species, agent);
        this.add(panel, BorderLayout.CENTER);

        this.setVisible(true);
    }

    public void addEvolveButtons(JPanel menu) {
        JButton button10 = new JButton("evolve 100 steps");
        button10.addActionListener(e -> {
            evolve(100);
            repaint();
        });
        menu.add(button10);

        JButton button3 = new JButton("evolve 10 steps");
        button3.addActionListener(e -> {
            evolve(10);
            repaint();
        });
        menu.add(button3);

        JButton buttonZ = new JButton("evolve one step");
        buttonZ.addActionListener(e -> {
            evolve(1);
            repaint();
        });
        menu.add(buttonZ);

        JButton button7 = new JButton("Next Species");
        button7.addActionListener(e -> {
            nextSpecies();
            repaint();
        });
        menu.add(button7);

        JButton buttonG = new JButton("Next Agent in Species");
        buttonG.addActionListener(e -> {
            nextAgent();
            repaint();
        });
        menu.add(buttonG);

        JButton button9 = new JButton("Show Next total");
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //setClient(agentList.get(next++));
                repaint();
            }
        });
        menu.add(button9);
    }

    public void addMutateButtons(JPanel menu) {
        JButton buttonB = new JButton("mutate weights");
        buttonB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.getGenome().mutateWeights();
                repaint();
            }
        });
        menu.add(buttonB);

        JButton buttonC = new JButton("Link mutate");
        buttonC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.getGenome().mutateNewConnection();
                repaint();
            }
        });
        menu.add(buttonC);

        JButton buttonD = new JButton("Node mutate");
        buttonD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.getGenome().mutateNewNode();
                repaint();
            }
        });
        menu.add(buttonD);

        JButton buttonE = new JButton("on/off");
        buttonE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.getGenome().mutateConnectionToggle();
                repaint();
            }
        });
        menu.add(buttonE);

        JButton buttonF = new JButton("Mutate");
        buttonF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agent.getGenome().mutate(true);
                repaint();
            }
        });
        menu.add(buttonF);
    }

    public void evolve(int steps) {
        if (steps == 1) {
            neat.evolveOneStep();
            neat.scoreAndPrintLastStep();
        } else {
            neat.evolve(steps);
        }
        this.speciesNavigator = new SpeciesNavigator(new ArrayList<>(neat.getSpecies()));
        setClient(speciesNavigator.nextSpecies().orElseThrow(), speciesNavigator.nextAgentInSpecies().orElseThrow());
    }

    public void nextSpecies() {
        Optional<Species> optionalSpecies = speciesNavigator.nextSpecies();
        if (optionalSpecies.isEmpty()) {
            System.out.println("No more species!!!");
            return;
        }
        Optional<Agent> optionalAgent = speciesNavigator.nextAgentInSpecies();
        if (optionalAgent.isEmpty()) {
            throw new IllegalStateException("Empty species!!!");
        }
        setClient(optionalSpecies.get(), optionalAgent.get());
    }

    public void nextAgent() {
        Optional<Agent> optionalAgent = speciesNavigator.nextAgentInSpecies();
        if (optionalAgent.isEmpty()) {
            nextSpecies();
            return;
        }
        setClient(species, optionalAgent.get());
    }

}
