package ai.anneat.view;

import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Species;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpeciesNavigator {
    private List<Species> speciesList;
    private int currentSpeciesIndex = 0;
    private List<Agent> agentList;
    private int currentAgentIndex = 0;

    public SpeciesNavigator(List<Species> speciesList) {
        this.speciesList = speciesList.stream()
                .sorted(Comparator.comparingDouble(species -> -species.getAllTimeAgentMaxVanillaScore()))
                .collect(Collectors.toList());
    }

    public Optional<Species> nextSpecies() {
        if (speciesList.isEmpty()) {
            throw new IllegalStateException();
        }
        if (currentSpeciesIndex == speciesList.size()) {
            currentSpeciesIndex = 0;
        }
        Species species = speciesList.get(currentSpeciesIndex++);
        agentList = species.getAgents().stream()
                .sorted(Comparator.comparingDouble(agent -> -agent.getVanillaScore()))
                .collect(Collectors.toList());
        currentAgentIndex = 0;
        return Optional.of(species);
    }

    public Optional<Agent> nextAgentInSpecies() {
        if (agentList == null || agentList.isEmpty()) {
            throw new IllegalStateException();
        }
        if (currentAgentIndex == agentList.size()) {
            currentAgentIndex = 0;
        }
        return Optional.of(agentList.get(currentAgentIndex++));
    }
}
