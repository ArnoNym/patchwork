package ai.anneat.controller;

import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.ScoreRunnable;
import ai.anneat.models.generation.Species;
import ai.anneat.models.NeatData;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.genome.Pool;
import ai.anneat.publish.settings.PerformanceSettings;
import ai.anneat.publish.settings.Settings;
import ai.anneat.publish.task.Deterministic;

import java.util.*;
import java.util.stream.Collectors;

public abstract class ScoreController {

    /**
     * @return Agents that reached the target score
     */
    public static Set<Agent> calcAgentScores(NeatData neatData) {
        List<Agent> allAgents = neatData.getGeneration().getAgentList();
        List<Agent> scoreAgents = getScoreAgents(neatData);

        List<List<Agent>> agentPartitions;
        if (neatData.getPool().getSettings().getPerformanceSettings().getThreads() == 1) {
            agentPartitions = new ArrayList<>(1);
            agentPartitions.add(scoreAgents);
        } else {
            agentPartitions = partition(scoreAgents, neatData.getSettings().getPerformanceSettings().getThreads());
        }

        List<Thread> threadList = new ArrayList<>(agentPartitions.size());
        List<ScoreRunnable> scoreRunnableList = new ArrayList<>(agentPartitions.size());

        for (List<Agent> agentPartition : agentPartitions) {
            ScoreRunnable scoreRunnable = new ScoreRunnable(agentPartition, neatData.getTask(), neatData.getTargetScore());

            Thread thread = new Thread(scoreRunnable);
            scoreRunnableList.add(scoreRunnable);

            thread.start();
            threadList.add(thread);
            //System.out.println("A Thread was Created! Partition size: "+agentPartition.size());
        }

        Set<Agent> targetScoreAgents = new HashSet<>();
        for (int i = 0; i < threadList.size(); i++) {
            Thread thread = threadList.get(i);
            try {
                thread.join();

                ScoreRunnable scoreRunnable = scoreRunnableList.get(i);
                targetScoreAgents.addAll(scoreRunnable.getReachedTargetScoreAgents());

                //System.out.println("A Thread Joined!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        punishBigSpecies(neatData.getGeneration().getOrderedSet());

        allAgents.sort(Comparator.comparingDouble(agent -> -agent.getVanillaScore()));
        assert allAgents.size() < 2 || allAgents.get(0).getVanillaScore() >= allAgents.get(1).getVanillaScore();
        for (int i = 0; i < neatData.getSettings().getEvolutionSettings().getTotalElitism(); i++) {
            Agent agent = allAgents.get(0);
            agent.setAllowKill(false);
            agent.setAllowMutate(false);
        }
        for (Species species : neatData.getGeneration().getOrderedSet()) {
            if (species.size() <= neatData.getSettings().getEvolutionSettings().getMutateALlSpeciesSize()) {
                continue;
            }
            species.getAgents().sort(Comparator.comparingDouble(agent -> -agent.getVanillaScore()));
            int to = Math.min(neatData.getSettings().getEvolutionSettings().getSpeciesElitism(), species.size());
            for (Agent agent : species.getAgents().subList(0, to)) {
                agent.setAllowKill(false);
                agent.setAllowMutate(false);
            }
        }

        double iterationBestScore = allAgents.get(0).getVanillaScore();
        if (neatData.getTask().getDeterministic().equals(Deterministic.YES_EXCEPTION) && iterationBestScore < neatData.getGeneration().getBestScore()) {
            throw new IllegalStateException("The Task is market as deterministic and throw exceptions if it isn't."
                    + " This is said exception! A previous better score could not get reached in the current generation."
                    + " [generationBestScore: "+ iterationBestScore +"], [bestScore: "+ neatData.getGeneration().getBestScore() +"]");
        } else if (iterationBestScore > neatData.getGeneration().getBestScore()) {
            neatData.getGeneration().setBestScore(iterationBestScore);
        }

        return targetScoreAgents;
    }

    private static List<Agent> getScoreAgents(NeatData neatData) {
        List<Agent> allAgents = neatData.getGeneration().getAgentList();
        if (neatData.getTask().getDeterministic().equals(Deterministic.YES_PERFORMANCE)) {
            return allAgents.stream().filter(Agent::isMutated).collect(Collectors.toList());
        } else {
            return allAgents;
        }
    }

    private static List<List<Agent>> partition(List<Agent> agents, int partitionsCount) {
        Collections.shuffle(agents); // Sometimes higher score agents take longer

        int partitionSize = agents.size() / partitionsCount;
        List<List<Agent>> partitions = new ArrayList<>(partitionsCount);
        int partitionIndex = 0;
        for (int partitionCount = 0; partitionCount < partitionsCount - 1; partitionCount++) {
            partitions.add(new ArrayList<>(agents.subList(partitionIndex, partitionIndex + partitionSize)));
            partitionIndex += partitionSize;
        }

        int i = 0;
        while (agents.size() - partitionIndex > partitionSize + 1) {
            partitions.get(i).add(agents.get(partitionIndex));
            i++;
            partitionIndex++;
        }

        partitions.add(agents.subList(partitionIndex, agents.size()));
        return partitions;
    }

    public static void punishBigSpecies(Collection<Species> speciesCollection) {
        for (Species species : speciesCollection) {
            int clientCount = species.size();
            for (Agent agent : species.getAgents()) {
                agent.setScore(agent.getVanillaScore() / clientCount);
            }
        }
    }

    public static void setSpeciesScore(OrderedSet<Species> speciesOrderedSet, boolean increaseCounter) {
        for (Species s : speciesOrderedSet) {
            s.calculateScore(increaseCounter);
        }
        speciesOrderedSet.sort(Comparator.comparingDouble(species -> -species.getScore()));
    }
}
