package ai.anneat.controller;

import ai.anneat.models.generation.Agent;

public abstract class AgentController {
    public static void cleanHistory(Agent agent) {
        agent.setAllowKill(true);
        agent.setAllowMutate(true);
        agent.setCalculator(null);
    }
}
