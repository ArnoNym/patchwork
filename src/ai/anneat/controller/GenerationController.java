package ai.anneat.controller;

import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Generation;
import ai.anneat.models.generation.Species;

import java.util.List;

public abstract class GenerationController {
    public static void createNewSpecies(Generation generation) {
        List<Agent> agents = generation.getAgentList();
        OrderedSet<Species> species = generation.getOrderedSet();
        species.clear();
        species.add(new Species(agents));
        EvolutionController.redistributeAgentsInSpecies(species);
    }
}
