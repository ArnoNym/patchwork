package ai.anneat.controller;

import ai.anneat.main.constants.Constants;
import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Generation;
import ai.anneat.models.generation.Species;
import ai.anneat.main.utils.Utils;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.main.utils.selectors.ByScoreCalculator;
import ai.anneat.main.utils.selectors.RandomSelector;
import ai.anneat.publish.settings.Settings;

import java.util.*;
import java.util.stream.Collectors;

public abstract class EvolutionController {

    /**
     * Expects rated agents and species. Returns unrated agents and species.
     */
    public static void evolve(Generation generation) {
        OrderedSet<Species> speciesOrderedSet = generation.getOrderedSet();

        kill(speciesOrderedSet);

        removeExtinctSpecies(speciesOrderedSet);

        ScoreController.setSpeciesScore(speciesOrderedSet, false);

        Map<Species, Set<Agent>> speciesNewOffspringMap = breed(speciesOrderedSet);

        mutate(speciesOrderedSet);

        setAllToAllowKillAndMutate(speciesOrderedSet);

        speciesOrderedSet.forEach(species -> {
            Set<Agent> offspring = speciesNewOffspringMap.get(species);
            if (offspring != null) {
                species.addAll(offspring);
            }
        });

        redistributeAgentsInSpecies(speciesOrderedSet);

        generation.incrementNumber();
    }

    public static void setAllToAllowKillAndMutate(Collection<Species> speciesCollection) {
        for (Species species : speciesCollection) {
            for (Agent agent : species) {
                agent.setAllowKill(true);
                agent.setAllowMutate(true);
            }
        }
    }

    public static void redistributeAgentsInSpecies(OrderedSet<Species> speciesOrderedSet) {
        Set<Agent> redistributeAgentSet = new HashSet<>();
        Set<Species> sizeBiggerHurdleSpeciesSet = new HashSet<>();

        for (Species species : speciesOrderedSet) {
            if (species.size() >= Constants.PROTECT_SPECIES_FROM_ALL_FLEEING_COUNT) {
                sizeBiggerHurdleSpeciesSet.add(species);
            }

            redistributeAgentSet.addAll(species.getAgents());
            species.reduceToRepresentative();
            redistributeAgentSet.remove(species.getRepresentative());
        }

        for (Agent c : redistributeAgentSet) {
            addToSpecies(speciesOrderedSet, c);
        }

        for (Species species : sizeBiggerHurdleSpeciesSet) {
            if (species.size() == 1) {
                species.getAgents().get(0).setAllowKill(false);
            }
        }
    }

    private static void addToSpecies(Collection<Species> speciesCollection, Agent agent) {
        boolean found = false;
        for (Species newSpecies : speciesCollection) {
            if (newSpecies.add(agent)) {
                found = true;
                break;
            }
        }
        if (!found) {
            agent.setAllowKill(false);
            Species s = new Species(agent);
            speciesCollection.add(s);
        }
    }

    public static void kill(Collection<Species> speciesCollection) {
        for (Species species : speciesCollection) {
            assert !species.isEmpty() : "Can't kill from empty species!";
            species.kill(1 - species.getAgents().get(0).getGenome().getPool().getSettings().getEvolutionSettings().getSurvivorsPercent());
        }
    }

    public static void removeExtinctSpecies(OrderedSet<Species> speciesOrderedSet) {
        Set<Species> removeSpeciesSet = speciesOrderedSet.stream().filter(Species::isEmpty).collect(Collectors.toSet());
        speciesOrderedSet.removeAll(removeSpeciesSet);
        if (!speciesOrderedSet.isEmpty() && Utils.getSettings(speciesOrderedSet).getPerformanceSettings().isPrint()) {
            System.out.println("Removed " + removeSpeciesSet.size() + " extinct species!");
        }
    }

    public static Map<Species, Set<Agent>> breed(OrderedSet<Species> speciesOrderedSet) {
        if (speciesOrderedSet.isEmpty()) {
            throw new IllegalStateException();
        }

        int parentCount = speciesOrderedSet.stream().mapToInt(Species::size).sum();
        int offspringCount = Utils.getSettings(speciesOrderedSet).getPerformanceSettings().getMaxAgentCount() - parentCount;

        if (speciesOrderedSet.size() == 1) {
            Map<Species, Set<Agent>> speciesNewOffspringMap = new HashMap<>();
            Species species = speciesOrderedSet.get(0);
            speciesNewOffspringMap.put(species, breedWithOneSpecies(species, offspringCount));
            return speciesNewOffspringMap;
        }

        RandomSelector<Species> selector = new RandomSelector<>();
        for (Species s : speciesOrderedSet) {
            if (s.isAllowedToBreed()) {
                selector.add(s, s.getScore());
            }
        }
        if (selector.isEmpty()) {
            List<Species> sortedSpecies = speciesOrderedSet.stream()
                    .sorted(Comparator.comparingDouble(species -> -species.getScore()))
                    .collect(Collectors.toList());
            Species bestSpecies = sortedSpecies.get(0);
            Species secondBestSpecies = sortedSpecies.get(1);
            assert bestSpecies.getScore() >= secondBestSpecies.getScore();
            return breedWithTwoBestSpecies(bestSpecies, secondBestSpecies, offspringCount);
        } else {
            return breedWithAllSpecies(speciesOrderedSet, offspringCount);
        }
    }

    private static Set<Agent> breedWithOneSpecies(Species species, int offspringCount) {
        return species.breed(offspringCount);
    }

    private static Map<Species, Set<Agent>> breedWithTwoBestSpecies(Species species1, Species species2, int offspringCount) {
        int secondBestSpeciesOffspringCount = offspringCount / 2;
        int bestSpeciesOffspringCount = offspringCount - secondBestSpeciesOffspringCount;
        assert bestSpeciesOffspringCount >= secondBestSpeciesOffspringCount;

        Map<Species, Set<Agent>> speciesNewOffspringMap = new HashMap<>();
        speciesNewOffspringMap.put(
                species1,
                breedWithOneSpecies(species1, bestSpeciesOffspringCount));
        speciesNewOffspringMap.put(
                species2,
                breedWithOneSpecies(species2, secondBestSpeciesOffspringCount));
        return speciesNewOffspringMap;
    }

    private static Map<Species, Set<Agent>> breedWithAllSpecies(Collection<Species> speciesCollection, int offspringCount) {
        ByScoreCalculator calc = new ByScoreCalculator(offspringCount, speciesCollection);
        if (calc.isTotalScoreZero()) {
            System.out.println("!!!WARNING!!! TOTAL SCORE OF ALL SPECIES IS ZERO!");
        }
        Map<Species, Set<Agent>> speciesNewOffspringMap = new HashMap<>();
        speciesCollection.forEach(species -> {
            Set<Agent> offspring = species.breed(calc.getCount(species.getScore()));
            speciesNewOffspringMap.put(species, offspring);
        });
        return speciesNewOffspringMap;
    }

    private static void mutate(Collection<Species> speciesCollection) {
        speciesCollection.forEach(Species::mutate);
    }
}
