package ai.anneat.publish;

import ai.anneat.controller.AgentController;
import ai.anneat.controller.GenerationController;
import ai.anneat.models.calculation.Calculator;
import ai.anneat.controller.EvolutionController;
import ai.anneat.models.NeatData;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.generation.Agent;
import ai.anneat.controller.ScoreController;
import ai.anneat.models.generation.Species;
import ai.anneat.main.utils.Utils;
import ai.anneat.publish.settings.Settings;
import ai.anneat.publish.task.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Neat implements Serializable {
    private final NeatData data;
    private final Consumer<Neat> saveFunction;

    private final Set<Agent> targetScoreAgents = new HashSet<>();

    private boolean scoreFirst;

    public Neat(@NotNull Settings settings, @NotNull Task task, @Nullable Double targetScore, @Nullable Consumer<Neat> saveFunction) {
        this(new NeatData(settings, task, targetScore), saveFunction);
        this.scoreFirst = true;
    }

    public Neat(NeatData data, @Nullable Consumer<Neat> saveFunction) {
        this.data = data;
        calcThreadCount();
        this.saveFunction = saveFunction;
        this.scoreFirst = false;
    }

    private void save() {
        if (saveFunction != null) {
            saveFunction.accept(this);
        }
    }

    public boolean isTargetScoreReached() {
        return !targetScoreAgents.isEmpty();
    }

    public void scoreAndPrintLastStep() {
        ScoreController.calcAgentScores(data);
        if (data.getPool().getSettings().getPerformanceSettings().isPrint()) {
            ScoreController.setSpeciesScore(data.getGeneration().getOrderedSet(), false);
            print();
        }
    }

    public void evolveOneStep() {
        EvolutionController.evolve(data.getGeneration());
    }

    private void calcThreadCount() {
        Settings settings = data.getSettings();

        if (settings.getPerformanceSettings().getThreads() != null) {
            return;
        }

        int actualThreadCount = Runtime.getRuntime().availableProcessors();
        int usableThreadCount = (int) Math.pow(4, (int) (Math.log(Runtime.getRuntime().availableProcessors()) / Math.log(4)));

        int threadCount = actualThreadCount;
        if (settings.getPerformanceSettings().getMaxAgentCount() < threadCount) {
            threadCount = settings.getPerformanceSettings().getMaxAgentCount();
        }

        settings.getPerformanceSettings().setThreads(threadCount);
    }

    public void evolve(int generations) {
        int targetGenerations = data.getGeneration().getNumber() + generations - 1;

        if (scoreFirst) {
            score();
            scoreFirst = false;
        }

        while (data.getGeneration().getNumber() < targetGenerations) {
            if (data.getPool().getSettings().getPerformanceSettings().isPrint()) {
                print();
            }

            save();

            if (isTargetScoreReached()) {
                return;
            }

            evolve();

            score();
        }

        save();

        if (data.getSettings().getPerformanceSettings().isPrint()) {
            print();
        }
    }

    private void evolve() {
        EvolutionController.evolve(data.getGeneration());
        data.getTask().evolvedGeneration(data.getGeneration().getAgentList());
    }

    private void score() {
        targetScoreAgents.addAll(ScoreController.calcAgentScores(data));
        ScoreController.setSpeciesScore(data.getGeneration().getOrderedSet(), true);
        data.getTask().scoredGeneration(data.getGeneration().getAgentList());
    }

    public Agent getBestAgent() {
        return data.getGeneration().getClients().stream().max(Agent::compareTo).orElseThrow();
    }

    public List<Agent> getBestAgents() {
        List<Agent> bestAgents;
        if (isTargetScoreReached()) {
            bestAgents = new ArrayList<>(targetScoreAgents);
        } else {
            bestAgents = data.getGeneration().getAgentList();
        }
        return bestAgents.stream().sorted(Agent::compareTo).collect(Collectors.toList());
    }

    public OrderedSet<Species> getSpecies() {
        return data.getGeneration().getOrderedSet();
    }

    public void print() {
        data.getGeneration().getOrderedSet().sort(Comparator.comparingDouble(species -> -species.getScore()));

        System.out.println("### "+(data.getGeneration().getNumber() - 1)+" ################################");
        for (Species species : data.getGeneration().getOrderedSet()) {
            Agent agent = species.getAgents().stream().max(Comparator.comparingDouble(Agent::getScore)).orElseThrow();
            System.out.println(Utils.toString(data.getGeneration().getOrderedSet().size(), species, agent));
        }
    }

    public int getGenerationNumber() {
        return data.getGeneration().getNumber();
    }

    public void setSettings(Settings settings) {
        data.setSettings(settings);
        calcThreadCount();
    }

    public void setTask(Task task) {
        if (data.getTask().getInputSize() != task.getInputSize()) {
            throw new IllegalArgumentException("InputSizes don't match!");
        }
        if (data.getTask().getOutputSize() != task.getOutputSize()) {
            throw new IllegalArgumentException("OutputSizes don't match!");
        }

        data.getGeneration().setBestScore(0);
        for (Agent agent : data.getGeneration().getAgentList()) {
            AgentController.cleanHistory(agent);
        }
        GenerationController.createNewSpecies(data.getGeneration());

        data.setTask(task);

        this.scoreFirst = true;
    }

    public void setTargetScore(Double targetScore) {
        data.setTargetScore(targetScore);

        targetScoreAgents.clear();
        this.scoreFirst = true;
    }

    public NeatData getData() {
        return data;
    }
}
