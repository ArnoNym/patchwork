package ai.anneat.publish;

import ai.anneat.models.calculation.Calculator;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.function.Function;

public class NeatAgent implements Serializable {
    private final Double score;
    private final Calculator calculator;
    private final Function<double[], double[]> function;

    public NeatAgent(double score, @NotNull Calculator calculator) {
        this.score = score;
        this.calculator = calculator;
        this.function = null;
    }

    public NeatAgent(@NotNull Function<double[], double[]> function) {
        this.score = null;
        this.calculator = null;
        if (!(function instanceof Serializable)) {
            throw new IllegalArgumentException("Function has to be serializable!");
        }
        this.function = function;
    }

    public Double getScore() {
        return score;
    }

    public double[] compute(double... input) {
        if (calculator != null) {
            return calculator.calculate(input);
        } else {
            assert function != null;
            return function.apply(input);
        }
    }

    public Calculator getCalculator() {
        return calculator;
    }
}
