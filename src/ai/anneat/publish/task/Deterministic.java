package ai.anneat.publish.task;

public enum Deterministic {
    YES_PERFORMANCE,
    YES_EXCEPTION,
    NO
}
