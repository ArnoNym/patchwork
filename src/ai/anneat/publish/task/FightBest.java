package ai.anneat.publish.task;

import java.io.Serializable;

@FunctionalInterface
public interface FightBest<FIGHTER> extends Serializable {
    double act(FIGHTER fighter, FIGHTER bestFighter);
}
