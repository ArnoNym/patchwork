package ai.anneat.publish.task;

import ai.anneat.main.SerializableFunction;
import ai.anneat.models.generation.Agent;

import java.util.List;

public class SoloTask extends Task {
    private final SerializableFunction<SerializableFunction<double[], double[]>, Double> task;

    public SoloTask(int inputSize, int outputSize, Deterministic deterministic, SerializableFunction<SerializableFunction<double[], double[]>, Double> task) {
        super(inputSize, outputSize, deterministic);
        this.task = task;
    }

    @Override
    public void scoredGeneration(List<Agent> agents) {

    }

    @Override
    public void evolvedGeneration(List<Agent> agents) {

    }

    @Override
    public double calculateScore(Agent agent) {
        return task.apply(agent.getCalculator().getFunction());
    }
}
