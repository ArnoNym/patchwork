package ai.anneat.publish.task;

import ai.anneat.models.generation.Agent;
import ai.anneat.publish.settings.Settings;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class ClassificationTask extends Task implements Serializable {
    private final Map<double[], Function<double[], Double>> map = new HashMap<>();

    public ClassificationTask(int inputSize, int outputSize, Deterministic deterministic) {
        super(inputSize, outputSize, deterministic);
    }

    public void put(double[] input, Function<double[], Double> outputToScoreFunction) {
        if (input.length != getInputSize()) {
            throw new IllegalArgumentException("Input size doesnt match!");
        }
        try {
            double[] testOutput = new double[getOutputSize()];
            outputToScoreFunction.apply(testOutput);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error in OutputToScoreFunction! Maybe the Input size doesnt match!");
        }
        map.put(input, outputToScoreFunction);
    }

    @Override
    public void scoredGeneration(List<Agent> agents) {

    }

    @Override
    public void evolvedGeneration(List<Agent> agents) {

    }

    @Override
    public double calculateScore(Agent agent) {
        double score = 0;
        for (Map.Entry<double[], Function<double[], Double>> entry : map.entrySet()) {
            double[] input = entry.getKey();
            Function<double[], Double> outputToScoreFunction = entry.getValue();
            score += outputToScoreFunction.apply(agent.getCalculator().getFunction().apply(input));
        }
        return score;
    }
}
