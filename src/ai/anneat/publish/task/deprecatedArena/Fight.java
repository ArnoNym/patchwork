package ai.anneat.publish.task.deprecatedArena;

import ai.anneat.main.utils.arena.Gladiator;

import java.io.Serializable;

@FunctionalInterface
public interface Fight extends Serializable {
    void act(Gladiator gladiator1, Gladiator gladiator2);
}
