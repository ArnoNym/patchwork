package ai.anneat.publish.task.deprecatedArena;

import ai.anneat.models.calculation.Calculator;
import ai.anneat.main.utils.arena.FightRecord;
import ai.anneat.main.utils.arena.FightRecords;
import ai.anneat.main.utils.arena.Gladiator;
import ai.anneat.main.utils.collections.Tuple;
import ai.anneat.models.generation.Agent;
import ai.anneat.publish.task.Deterministic;
import ai.anneat.publish.task.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class ArenaTask extends Task {
    private final int fightBestCount;
    private final Map<Calculator, Double> benchmarkGladiatorScoreMap;
    private final Fight fight;

    private List<Agent> bestAgentList = new ArrayList<>();
    private final FightRecords fightRecords = new FightRecords();

    public ArenaTask(int inputSize, int outputSize, Deterministic deterministic, int fightBestCount, Fight fight) {
        this(inputSize, outputSize, deterministic, fightBestCount, new HashMap<>(), fight);
    }

    public ArenaTask(int inputSize, int outputSize, Deterministic deterministic, int fightBestCount, Map<Calculator, Double> benchmarkGladiatorScoreMap, Fight fight) {
        super(inputSize, outputSize, deterministic);
        this.fight = fight;
        this.fightBestCount = fightBestCount;
        this.benchmarkGladiatorScoreMap = benchmarkGladiatorScoreMap;
    }

    @Override
    public void scoredGeneration(List<Agent> agents) {

    }

    @Override
    public void evolvedGeneration(List<Agent> agents) {
        for (Agent agent : agents) {
            agent.setVanillaScore(0);
        }

        agents.sort(Comparator.comparingDouble(Agent::getVanillaScore));
        this.bestAgentList = agents;

        fightRecords.clearUsedRecords();
    }

    @Override
    public double calculateScore(Agent agent) {
        for (Map.Entry<Calculator, Double> entry : benchmarkGladiatorScoreMap.entrySet()) {
            calculateScore(agent, agent.getCalculator(), null, entry.getKey(), entry.getValue());
        }

        int enemyCount = 0;
        for (Agent enemyAgent : bestAgentList) {
            if (!enemyAgent.equals(agent)) {
                calculateScore(agent, agent.getCalculator(), enemyAgent, enemyAgent.getCalculator(), null);
                enemyCount++;
            }
            if (enemyCount == fightBestCount) {
                break;
            }
        }

        return agent.getVanillaScore();
    }

    private void calculateScore(@NotNull Agent agent, @NotNull Calculator calculator,
                                @Nullable Agent enemyAgent, @NotNull Calculator enemyCalculator,
                                @Nullable Double additionalIfAgentIsWinnerScore) {
        assert !enemyCalculator.equals(calculator);

        Tuple<Gladiator, Gladiator> gladiatorTuple = calculateGladiators(calculator, enemyCalculator);
        Gladiator gladiator = gladiatorTuple.getV1();
        Gladiator enemyGladiator = gladiatorTuple.getV2();

        if (additionalIfAgentIsWinnerScore != null && gladiator.getScore() > enemyGladiator.getScore()) {
            gladiator.setScore(gladiator.getScore() + additionalIfAgentIsWinnerScore);
        }

        setScore(agent, gladiator, enemyAgent, enemyGladiator);
    }

    private Tuple<Gladiator, Gladiator> calculateGladiators(@NotNull Calculator calculator,
                                                            @NotNull Calculator enemyCalculator) {

        FightRecord fightRecord = fightRecords.getFightRecord(calculator, enemyCalculator);

        Gladiator gladiator;
        Gladiator enemyGladiator;
        if (fightRecord == null) {
            gladiator = new Gladiator(calculator);
            enemyGladiator = new Gladiator(enemyCalculator);

            fight.act(gladiator, enemyGladiator);
            fightRecords.addFightRecord(gladiator, enemyGladiator);
        } else {
            gladiator = fightRecord.getGladiator1();
            enemyGladiator = fightRecord.getGladiator2();
            if (!gladiator.getCalculator().equals(calculator)) {
                Gladiator temp = gladiator;
                gladiator = enemyGladiator;
                enemyGladiator = temp;
            }
            assert gladiator.getCalculator().equals(calculator);
            assert enemyGladiator.getCalculator().equals(enemyCalculator);
        }

        //System.out.println("Neuer eigener galdiator Score = "+gladiator.getScore());
        //System.out.println("Neuer enemy galdiator Score = "+enemyGladiator.getScore());

        return new Tuple<>(gladiator, enemyGladiator);
    }

    private void setScore(@NotNull Agent agent, @NotNull Gladiator gladiator,
                          @Nullable Agent enemyAgent, @NotNull Gladiator enemyGladiator) {
        agent.setVanillaScore(agent.getVanillaScore() + gladiator.getScore());
        //System.out.println("Neuer eigener vanillaScore = "+agent.getVanillaScore());
        agent.setScore(agent.getVanillaScore());
        if (enemyAgent != null) {
            enemyAgent.setVanillaScore(enemyAgent.getVanillaScore() + enemyGladiator.getScore());
            //System.out.println("Neuer enemy vanillaScore = "+enemyAgent.getVanillaScore());
            enemyAgent.setScore(enemyAgent.getVanillaScore());
        }
    }
}
