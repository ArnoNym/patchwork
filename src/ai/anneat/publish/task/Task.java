package ai.anneat.publish.task;

import ai.anneat.models.generation.Agent;
import ai.anneat.publish.settings.Settings;

import java.io.Serializable;
import java.util.List;

public abstract class Task implements Serializable {
    private final int inputSize;
    private final int outputSize;
    private final Deterministic deterministic;

    public Task(int inputSize, int outputSize, Deterministic deterministic) {
        if (inputSize < 0) {
            throw new IllegalArgumentException("InputSize has to be at least 0!");
        }
        if (outputSize < 1) {
            throw new IllegalArgumentException("OutputSize has to be at least 1!");
        }
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        this.deterministic = deterministic;
    }

    public abstract void scoredGeneration(List<Agent> agents);

    public abstract void evolvedGeneration(List<Agent> agents);

    public abstract double calculateScore(Agent agent);

    public int getInputSize() {
        return inputSize;
    }

    public int getOutputSize() {
        return outputSize;
    }

    public Deterministic getDeterministic() {
        return deterministic;
    }
}
