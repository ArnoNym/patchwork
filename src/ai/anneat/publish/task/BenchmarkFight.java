package ai.anneat.publish.task;

import ai.anneat.models.calculation.Calculator;

import java.io.Serializable;

public interface BenchmarkFight<BENCHMARK> extends Serializable {
    double act(Calculator active, BENCHMARK benchmarkAgent, double benchmarkScore);
}
