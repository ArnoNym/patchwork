package ai.anneat.publish.task;

import ai.anneat.models.generation.Agent;
import ai.anneat.publish.settings.Settings;

import java.io.Serializable;
import java.util.*;

public class BenchmarkTask<BENCHMARK extends Serializable> extends Task {
    private final BenchmarkFight<BENCHMARK> fight;
    private final Map<BENCHMARK, Double> benchmarkScoreMap;

    public BenchmarkTask(int inputSize, int outputSize, Deterministic deterministic, BenchmarkFight<BENCHMARK> fight, Map<BENCHMARK, Double> benchmarkScoreMap) {
        super(inputSize, outputSize, deterministic);
        this.fight = fight;
        this.benchmarkScoreMap = benchmarkScoreMap;
    }

    @Override
    public void scoredGeneration(List<Agent> agents) {

    }

    @Override
    public void evolvedGeneration(List<Agent> agents) {

    }

    @Override
    public double calculateScore(Agent agent) {
        double score = 0;
        for (Map.Entry<BENCHMARK, Double> entry : benchmarkScoreMap.entrySet()) {
            score += fight.act(agent.getCalculator(), entry.getKey(), entry.getValue());
        }
        return score;
    }
}
