package ai.anneat.publish.task;

import ai.anneat.main.SerializableSupplier;
import ai.anneat.main.utils.collections.Tuple;
import ai.anneat.main.SerializableFunction;
import ai.anneat.models.generation.Agent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Supplier;

public class FightBestTask<FIGHTER> extends Task {
    private FightBest<FIGHTER> fightBest;
    private final Supplier<FightBest<FIGHTER>> whenNewBestFightSupplier;
    private final SerializableFunction<SerializableFunction<double[], double[]>, FIGHTER> createFighterFunction;

    private Tuple<Agent, FIGHTER> best;
    private boolean newBest = false;

    public FightBestTask(int inputSize,
                         int outputSize,
                         @NotNull Deterministic deterministic,
                         @NotNull FightBest<FIGHTER> fightBest,
                         @NotNull SerializableFunction<SerializableFunction<double[], double[]>, FIGHTER> createFighterFunction,
                         @Nullable FIGHTER initialFighter,
                         @Nullable Double initialScore) {

        super(inputSize, outputSize, deterministic);
        this.fightBest = fightBest;
        this.whenNewBestFightSupplier = null;
        this.createFighterFunction = createFighterFunction;
        this.best = createInitialBest(initialFighter, initialScore);
    }

    public FightBestTask(int inputSize,
                         int outputSize,
                         @NotNull Deterministic deterministic,
                         @NotNull SerializableSupplier<FightBest<FIGHTER>> whenNewBestFightSupplier,
                         @NotNull SerializableFunction<SerializableFunction<double[], double[]>, FIGHTER> createFighterFunction,
                         @Nullable FIGHTER initialFighter,
                         @Nullable Double initialScore) {

        super(inputSize, outputSize, deterministic);
        this.fightBest = whenNewBestFightSupplier.get();
        this.whenNewBestFightSupplier = whenNewBestFightSupplier;
        this.createFighterFunction = createFighterFunction;
        this.best = createInitialBest(initialFighter, initialScore);
    }

    private Tuple<Agent, FIGHTER> createInitialBest(@Nullable FIGHTER initialFighter, @Nullable Double initialScore) {
        if (initialFighter == null) {
            if (initialScore != null) {
                throw new IllegalArgumentException("InitialScore may not be null if the initialFighter is null!");
            }
            return new Tuple<>(null, null);
        }
        if (initialScore == null) {
            initialScore = fightBest.act(initialFighter, initialFighter);
        }
        Agent agent = new Agent(null);
        agent.setVanillaScore(initialScore);
        return new Tuple<>(agent, initialFighter);
    }

    @Override
    public void scoredGeneration(List<Agent> agents) {
        assert !newBest;
        Agent otherBestAgent = agents.stream()
                .filter(agent -> !agent.equals(best.getV1()))
                .max(Comparator.comparingDouble(Agent::getVanillaScore))
                .orElseThrow();
        if (otherBestAgent.getVanillaScore() > best.getV1().getVanillaScore()) {
            best = new Tuple<>(otherBestAgent, createFighterFunction.apply(otherBestAgent.getCalculator().getFunction()));
            newBest = true;
        }
    }

    @Override
    public void evolvedGeneration(List<Agent> agents) {
        if (newBest) {
            for (Agent agent : agents) {
                agent.setCalculator(null);
                agent.setVanillaScore(0);
            }
            fightBest = whenNewBestFightSupplier.get();
            newBest = false;
            return;
        }
        if (best.getV2() == null) {
            Agent agent = agents.get(0);
            FIGHTER fighter = createFighterFunction.apply(agent.getCalculator().getFunction());
            double score = fightBest.act(fighter, fighter);
            agent.setVanillaScore(score);
            best = new Tuple<>(agent, fighter);
        }
    }

    @Override
    public double calculateScore(Agent agent) {
        return fightBest.act(createFighterFunction.apply(agent.getCalculator().getFunction()), best.getV2());
    }
}
