package ai.anneat.publish.settings;

import ai.anneat.main.constants.Constants;

import java.io.Serializable;

public class PerformanceSettings implements Serializable {
    private int maxAgentCount = Constants.MAX_CLIENT_COUNT;
    private boolean print = Constants.PRINT;
    private Integer threads = null;

    public PerformanceSettings() {
    }

    public int getMaxAgentCount() {
        return maxAgentCount;
    }

    public void setMaxAgentCount(int maxAgentCount) {
        if (maxAgentCount < 2) throw new IllegalArgumentException();
        this.maxAgentCount = maxAgentCount;
    }

    public boolean isPrint() {
        return print;
    }

    public void setPrint(boolean print) {
        this.print = print;
    }

    public Integer getThreads() {
        return threads;
    }

    public void setThreads(Integer threads) {
        if (threads != null && threads < 1) throw new IllegalArgumentException();
        this.threads = threads;
    }
}
