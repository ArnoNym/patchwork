package ai.anneat.publish.settings;

import java.io.Serializable;

public class Settings implements Serializable {
    private final PerformanceSettings performanceSettings;
    private final EvolutionSettings evolutionSettings;

    public Settings() {
        this.performanceSettings = new PerformanceSettings();
        this.evolutionSettings = new EvolutionSettings();
    }

    public Settings(PerformanceSettings performanceSettings, EvolutionSettings evolutionSettings) {
        this.performanceSettings = performanceSettings;
        this.evolutionSettings = evolutionSettings;
    }

    public PerformanceSettings getPerformanceSettings() {
        return performanceSettings;
    }

    public EvolutionSettings getEvolutionSettings() {
        return evolutionSettings;
    }
}
