package ai.anneat.publish.settings;

import ai.anneat.main.utils.functions.Functions;
import ai.anneat.main.SerializableFunction;
import ai.anneat.main.constants.Constants;
import ai.anneat.models.generation.Agent;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.main.utils.functions.PunishComplexityFunction;

import java.io.Serializable;
import java.util.function.Function;

public class EvolutionSettings implements Serializable {
    private boolean allowHiddenNodes = Constants.ALLOW_HIDDEN_NODES;

    // Species
    private double c1 = Constants.C1;
    private double c2 = Constants.C2;
    private double c3 = Constants.C3;
    private double cp = Constants.CP;
    private double survivorsPercent = Constants.SURVIVORS_PERCENT;
    private double bestMayReproducePercent = Constants.BEST_MAY_REPRODUCE_PERCENT;
    private double offspringWithOnlyOneParentPercent = Constants.OFFSPRING_WITH_ONLY_ONE_PARENT_PERCENT;
    private int allowGenerationsWithoutIncreaseInValue = Constants.ALLOW_GENERATIONS_WITHOUT_INCREASE_IN_VALUE;
    private int mutateALlSpeciesSize = Constants.MUTATE_ALL_SPECIES_SIZE;
    private int speciesElitism = Constants.SPECIES_ELITISM;
    private int totalElitism = Constants.TOTAL_ELITISM;
    public static final SerializableFunction<OrderedSet<Agent>, Double> SPECIES_SCORE_FUNCTION = Functions.SPECIES_SCORE_FUNCTION_AVERAGE;

    // Mutation
    private int bigSpeciesOfTotalPopRequired = Constants.BIG_SPECIES_OF_TOTAL_POP_REQUIRED;
    private double probabilityMutateLinkSmallPop = Constants.PROBABILITY_MUTATE_LINK_SMALL_POP;
    private double probabilityMutateLinkBigPop = Constants.PROBABILITY_MUTATE_LINK_BIG_POP;
    private double probabilityMutateNodeSmallPop = Constants.PROBABILITY_MUTATE_NODE_SMALL_POP;
    private double probabilityMutateNodeBigPop = Constants.PROBABILITY_MUTATE_NODE_BIG_POP;
    private double probabilityMutateActivateLink = Constants.PROBABILITY_MUTATE_ACTIVATE_LINK;
    private double probabilityMutateDeactivateLink = Constants.PROBABILITY_MUTATE_DEACTIVATE_LINK;
    private double probabilityMutateWeight = Constants.PROBABILITY_MUTATE_WEIGHT;
    private double probabilityMutateWeightRandom = Constants.PROBABILITY_MUTATE_WEIGHT_RANDOM;
    private double probabilityMutateWeightShift = Constants.PROBABILITY_MUTATE_WEIGHT_SHIFT;

    private double weightShiftStrength = Constants.WEIGHT_SHIFT_STRENGTH;
    private double weightRandomStrength = Constants.WEIGHT_RANDOM_STRENGTH;

    // Delete
    private double complexityPunishmentPercent = Constants.COMPLEXITY_PUNISHMENT_PERCENT;

    public SerializableFunction<OrderedSet<Agent>, Double> speciesScoreFunction = Constants.SPECIES_SCORE_FUNCTION;
    private PunishComplexityFunction punishComplexityFunction = (allAgentsMaxComplexity, agentComplexity, score) -> {
        double multi = 1 - complexityPunishmentPercent * agentComplexity / allAgentsMaxComplexity;
        assert multi >= 0;
        assert multi <= 1;
        return score * multi;
    };

    public EvolutionSettings() {
    }

    public EvolutionSettings(int maxAgentCount, boolean allowHiddenNodes, boolean print,
                    double c1, double c2, double c3, double cp,
                    double survivorsPercent, double bestMayReproducePercent, double offspringWithOnlyOneParentPercent,
                    int allowGenerationsWithoutIncreaseInValue, int mutateALlSpeciesSize, int speciesElitism,
                    int bigSpeciesOfTotalPopRequired, double probabilityMutateLinkSmallPop,
                    double probabilityMutateLinkBigPop, double probabilityMutateNodeSmallPop,
                    double probabilityMutateNodeBigPop, double probabilityMutateActivateLink,
                    double probabilityMutateDeactivateLink, double probabilityMutateWeight,
                    double probabilityMutateWeightRandom, double probabilityMutateWeightShift,
                    double weightShiftStrength, double weightRandomStrength,
                             SerializableFunction<OrderedSet<Agent>, Double> speciesScoreFunction) {
        setAllowHiddenNodes(allowHiddenNodes);
        setC1(c1);
        setC2(c2);
        setC3(c3);
        setCp(cp);
        setSurvivorsPercent(survivorsPercent);
        setBestMayReproducePercent(bestMayReproducePercent);
        setOffspringWithOnlyOneParentPercent(offspringWithOnlyOneParentPercent);
        setAllowGenerationsWithoutIncreaseInValue(allowGenerationsWithoutIncreaseInValue);
        setMutateALlSpeciesSize(mutateALlSpeciesSize);
        setSpeciesElitism(speciesElitism);
        setBigSpeciesOfTotalPopRequired(bigSpeciesOfTotalPopRequired);
        setProbabilityMutateLinkSmallPop(probabilityMutateLinkSmallPop);
        setProbabilityMutateLinkBigPop(probabilityMutateLinkBigPop);
        setProbabilityMutateNodeSmallPop(probabilityMutateNodeSmallPop);
        setProbabilityMutateNodeBigPop(probabilityMutateNodeBigPop);
        setProbabilityMutateActivateLink(probabilityMutateActivateLink);
        setProbabilityMutateDeactivateLink(probabilityMutateDeactivateLink);
        setProbabilityMutateWeight(probabilityMutateWeight);
        setProbabilityMutateWeightRandom(probabilityMutateWeightRandom);
        setProbabilityMutateWeightShift(probabilityMutateWeightShift);
        setWeightShiftStrength(weightShiftStrength);
        setWeightRandomStrength(weightRandomStrength);
        setSpeciesScoreFunction(speciesScoreFunction);
    }

    public void setAllowHiddenNodes(boolean allowHiddenNodes) {
        this.allowHiddenNodes = allowHiddenNodes;
    }

    public void setC1(double c1) {
        if (c1 < 0) throw new IllegalArgumentException();
        this.c1 = c1;
    }

    public void setC2(double c2) {
        if (c2 < 0) throw new IllegalArgumentException();
        this.c2 = c2;
    }

    public void setC3(double c3) {
        if (c3 < 0) throw new IllegalArgumentException();
        this.c3 = c3;
    }

    public void setCp(double cp) {
        if (cp < 0) throw new IllegalArgumentException();
        this.cp = cp;
    }

    public void setSurvivorsPercent(double survivorsPercent) {
        if (survivorsPercent <= 0 || survivorsPercent > 1) throw new IllegalArgumentException();
        this.survivorsPercent = survivorsPercent;
    }

    public void setBestMayReproducePercent(double bestMayReproducePercent) {
        if (bestMayReproducePercent <= 0 || bestMayReproducePercent > 1) throw new IllegalArgumentException();
        this.bestMayReproducePercent = bestMayReproducePercent;
    }

    public void setOffspringWithOnlyOneParentPercent(double offspringWithOnlyOneParentPercent) {
        if (offspringWithOnlyOneParentPercent < 0 || offspringWithOnlyOneParentPercent > 1) throw new IllegalArgumentException();
        this.offspringWithOnlyOneParentPercent = offspringWithOnlyOneParentPercent;
    }

    public void setAllowGenerationsWithoutIncreaseInValue(int allowGenerationsWithoutIncreaseInValue) {
        if (allowGenerationsWithoutIncreaseInValue <= 0) throw new IllegalArgumentException();
        this.allowGenerationsWithoutIncreaseInValue = allowGenerationsWithoutIncreaseInValue;
    }

    public void setMutateALlSpeciesSize(int mutateALlSpeciesSize) {
        if (mutateALlSpeciesSize < 1) throw new IllegalArgumentException();
        this.mutateALlSpeciesSize = mutateALlSpeciesSize;
    }

    public void setSpeciesElitism(int speciesElitism) {
        if (speciesElitism < 1) throw new IllegalArgumentException();
        this.speciesElitism = speciesElitism;
    }

    public void setTotalElitism(int totalElitism) {
        if (totalElitism < 1) throw new IllegalArgumentException();
        this.totalElitism = totalElitism;
    }

    public void setBigSpeciesOfTotalPopRequired(int bigSpeciesOfTotalPopRequired) {
        if (bigSpeciesOfTotalPopRequired < 0) throw new IllegalArgumentException();
        this.bigSpeciesOfTotalPopRequired = bigSpeciesOfTotalPopRequired;
    }

    public void setProbabilityMutateLinkSmallPop(double probabilityMutateLinkSmallPop) {
        if (probabilityMutateLinkSmallPop < 0 || probabilityMutateLinkSmallPop > 1) throw new IllegalArgumentException();
        this.probabilityMutateLinkSmallPop = probabilityMutateLinkSmallPop;
    }

    public void setProbabilityMutateLinkBigPop(double probabilityMutateLinkBigPop) {
        if (probabilityMutateLinkBigPop < 0 || probabilityMutateLinkBigPop > 1) throw new IllegalArgumentException();
        this.probabilityMutateLinkBigPop = probabilityMutateLinkBigPop;
    }

    public void setProbabilityMutateNodeSmallPop(double probabilityMutateNodeSmallPop) {
        if (probabilityMutateNodeSmallPop < 0 || probabilityMutateNodeSmallPop > 1) throw new IllegalArgumentException();
        this.probabilityMutateNodeSmallPop = probabilityMutateNodeSmallPop;
    }

    public void setProbabilityMutateNodeBigPop(double probabilityMutateNodeBigPop) {
        if (probabilityMutateNodeBigPop < 0 || probabilityMutateNodeBigPop > 1) throw new IllegalArgumentException();
        this.probabilityMutateNodeBigPop = probabilityMutateNodeBigPop;
    }

    public void setProbabilityMutateActivateLink(double probabilityMutateActivateLink) {
        if (probabilityMutateActivateLink < 0 || probabilityMutateActivateLink > 1) throw new IllegalArgumentException();
        this.probabilityMutateActivateLink = probabilityMutateActivateLink;
    }

    public void setProbabilityMutateDeactivateLink(double probabilityMutateDeactivateLink) {
        if (probabilityMutateDeactivateLink < 0 || probabilityMutateDeactivateLink > 1) throw new IllegalArgumentException();
        this.probabilityMutateDeactivateLink = probabilityMutateDeactivateLink;
    }

    public void setProbabilityMutateWeight(double probabilityMutateWeight) {
        if (probabilityMutateWeight < 0 || probabilityMutateWeight > 1) throw new IllegalArgumentException();
        this.probabilityMutateWeight = probabilityMutateWeight;
    }

    public void setProbabilityMutateWeightRandom(double probabilityMutateWeightRandom) {
        if (probabilityMutateWeightRandom < 0 || probabilityMutateWeightRandom > 1) throw new IllegalArgumentException();
        this.probabilityMutateWeightRandom = probabilityMutateWeightRandom;
    }

    public void setProbabilityMutateWeightShift(double probabilityMutateWeightShift) {
        if (probabilityMutateWeightShift < 0 || probabilityMutateWeightShift > 1) throw new IllegalArgumentException();
        this.probabilityMutateWeightShift = probabilityMutateWeightShift;
    }

    public void setWeightShiftStrength(double weightShiftStrength) {
        if (weightShiftStrength <= 0) throw new IllegalArgumentException();
        this.weightShiftStrength = weightShiftStrength;
    }

    public void setWeightRandomStrength(double weightRandomStrength) {
        if (weightRandomStrength <= 0) throw new IllegalArgumentException();
        this.weightRandomStrength = weightRandomStrength;
    }

    public void setComplexityPunishmentPercent(double complexityPunishmentPercent) {
        if (complexityPunishmentPercent < 0 || complexityPunishmentPercent > 1) throw new IllegalArgumentException();
        this.complexityPunishmentPercent = complexityPunishmentPercent;
    }

    public void setSpeciesScoreFunction(SerializableFunction<OrderedSet<Agent>, Double> speciesScoreFunction) {
        if (speciesScoreFunction == null) throw new IllegalArgumentException();
        this.speciesScoreFunction = speciesScoreFunction;
    }

    public void setPunishComplexityFunction(PunishComplexityFunction punishComplexityFunction) {
        if (punishComplexityFunction == null) throw new IllegalArgumentException();
        this.punishComplexityFunction = punishComplexityFunction;
    }

    public boolean isAllowHiddenNodes() {
        return allowHiddenNodes;
    }

    public double getC1() {
        return c1;
    }

    public double getC2() {
        return c2;
    }

    public double getC3() {
        return c3;
    }

    public double getCp() {
        return cp;
    }

    public double getSurvivorsPercent() {
        return survivorsPercent;
    }

    public double getBestMayReproducePercent() {
        return bestMayReproducePercent;
    }

    public double getOffspringWithOnlyOneParentPercent() {
        return offspringWithOnlyOneParentPercent;
    }

    public int getAllowGenerationsWithoutIncreaseInValue() {
        return allowGenerationsWithoutIncreaseInValue;
    }

    public int getMutateALlSpeciesSize() {
        return mutateALlSpeciesSize;
    }

    public int getSpeciesElitism() {
        return speciesElitism;
    }

    public static Function<OrderedSet<Agent>, Double> getSpeciesScoreFunction() {
        return SPECIES_SCORE_FUNCTION;
    }

    public PunishComplexityFunction getPunishComplexityFunction() {
        return punishComplexityFunction;
    }

    public int getBigSpeciesOfTotalPopRequired() {
        return bigSpeciesOfTotalPopRequired;
    }

    public double getProbabilityMutateLinkSmallPop() {
        return probabilityMutateLinkSmallPop;
    }

    public double getProbabilityMutateLinkBigPop() {
        return probabilityMutateLinkBigPop;
    }

    public double getProbabilityMutateNodeSmallPop() {
        return probabilityMutateNodeSmallPop;
    }

    public double getProbabilityMutateNodeBigPop() {
        return probabilityMutateNodeBigPop;
    }

    public double getProbabilityMutateActivateLink() {
        return probabilityMutateActivateLink;
    }

    public double getProbabilityMutateDeactivateLink() {
        return probabilityMutateDeactivateLink;
    }

    public double getProbabilityMutateWeight() {
        return probabilityMutateWeight;
    }

    public double getProbabilityMutateWeightRandom() {
        return probabilityMutateWeightRandom;
    }

    public double getProbabilityMutateWeightShift() {
        return probabilityMutateWeightShift;
    }

    public double getWeightShiftStrength() {
        return weightShiftStrength;
    }

    public double getWeightRandomStrength() {
        return weightRandomStrength;
    }

    public double getComplexityPunishmentPercent() {
        return complexityPunishmentPercent;
    }

    public int getTotalElitism() {
        return totalElitism;
    }
}
