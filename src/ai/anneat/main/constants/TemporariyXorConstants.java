package ai.anneat.main.constants;

import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.generation.Agent;
import ai.anneat.main.utils.functions.Functions;

import java.util.function.Function;

public class TemporariyXorConstants {
    // Performance
    public static final int MAX_NODES = (int) Math.pow(2,20);
    public static final int MAX_TRIES_TO_FIND_CONNECTION = 50;
    public static final int MAX_CLIENT_COUNT = 150;
    public static final boolean ALLOW_HIDDEN_NODES = true;
    public static final boolean PRINT = false;

    // Visual
    public static final double INPUT_X = 0.05;
    public static final double BIAS_X = INPUT_X;
    public static final double OUTPUT_X = 0.95;

    // Species
    public static final double C1 = 2, C2 = 2, C3 = 0.1;
    public static final double CP = 3;
    public static final double SURVIVORS_PERCENT = 0.8;
    public static final double BEST_MAY_REPRODUCE_PERCENT = 0.05;
    public static final double OFFSPRING_WITH_ONLY_ONE_PARENT_PERCENT = 0.25;
    public static final int ALLOW_GENERATIONS_WITHOUT_INCREASE_IN_VALUE = 30;
    public static final int MUTATE_ALL_SPECIES_SIZE = 1;
    public static final int ELITISM = 1;
    public static final Function<OrderedSet<Agent>, Double> SPECIES_SCORE_FUNCTION = Functions.SPECIES_SCORE_FUNCTION_AVERAGE;

    // Mutation
    public static final int BIG_SPECIES_OF_TOTAL_POP_REQUIRED = (int) (0.22 * MAX_CLIENT_COUNT);
    public static final double PROBABILITY_MUTATE_LINK_SMALL_POP = 0.2;
    public static final double PROBABILITY_MUTATE_LINK_BIG_POP = 0.6;
    public static final double PROBABILITY_MUTATE_NODE_SMALL_POP = 0.005;
    public static final double PROBABILITY_MUTATE_NODE_BIG_POP = 0.03;
    public static final double PROBABILITY_MUTATE_ACTIVATE_LINK = 0.05;
    public static final double PROBABILITY_MUTATE_DEACTIVATE_LINK = 0.001;
    public static final double PROBABILITY_MUTATE_WEIGHT = 0.8;
    public static final double PROBABILITY_MUTATE_WEIGHT_RANDOM = 0.1;
    public static final double PROBABILITY_MUTATE_WEIGHT_SHIFT = 0.9; // If not already random

    public static final double WEIGHT_SHIFT_STRENGTH = 0.5; //2.5
    public static final double WEIGHT_RANDOM_STRENGTH = 10;

    // Crap
    public static final int SCORE_ROUND_DIGITS = 1000;
    public static final double COMPLEXITY_PUNISHMENT_PERCENT = 0; // TODO Vielleicht sogar ganz aus dem code loeschten
}
