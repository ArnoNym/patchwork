package ai.anneat.main.utils.functions;

import ai.anneat.main.SerializableFunction;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.generation.Agent;

public abstract class Functions {
    public static double originalPaperModifiedSigmoidFunction(double x) {
        return 1d / (1 + Math.exp(-4.9*x));//1d / (1 + Math.exp(-x));
    }

    public static double sigmoidFunction(double x) {
        return 1d / (1 + Math.exp(-x));
    }

    public static double exponentialOneToTwenty(double n) {
        return Math.pow(n / 10, 3.8) + n - Math.pow(n, 0.9) + 1;
    }

    public static SerializableFunction<OrderedSet<Agent>, Double> SPECIES_SCORE_FUNCTION_MAX = (OrderedSet<Agent> agents) -> {
        return agents.stream().mapToDouble(Agent::getScore).max().orElseThrow();
    };

    public static SerializableFunction<OrderedSet<Agent>, Double> SPECIES_SCORE_FUNCTION_MAX_VANILLA = (OrderedSet<Agent> agents) -> {
        return agents.stream().mapToDouble(Agent::getVanillaScore).max().orElseThrow();
    };

    public static SerializableFunction<OrderedSet<Agent>, Double> SPECIES_SCORE_FUNCTION_AVERAGE = (OrderedSet<Agent> agents) -> {
        double v = 0;
        for (Agent c : agents) {
            double score = c.getScore();
            v += score;
        }
        return v; // No division needed because score is already divided by number of agents in species
    };
}
