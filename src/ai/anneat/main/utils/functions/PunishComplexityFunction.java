package ai.anneat.main.utils.functions;

import java.io.Serializable;

@FunctionalInterface
public interface PunishComplexityFunction extends Serializable {
    double getScore(int allAgentsMaxComplexity, int agentComplexity, double agentScore);
}
