package ai.anneat.main.utils;

import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.NodeGene;

public class NodeConnectionWrapper {
    private final ConnectionGene leftCon;
    private final NodeGene node;
    private final ConnectionGene rightCon;

    public NodeConnectionWrapper(ConnectionGene leftCon, NodeGene node, ConnectionGene rightCon) {
        this.leftCon = leftCon;
        this.node = node;
        this.rightCon = rightCon;
    }

    public ConnectionGene getLeftCon() {
        return leftCon;
    }

    public NodeGene getNode() {
        return node;
    }

    public ConnectionGene getRightCon() {
        return rightCon;
    }
}
