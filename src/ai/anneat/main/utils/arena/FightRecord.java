package ai.anneat.main.utils.arena;

public class FightRecord {
    private final Gladiator gladiator1;
    private final Gladiator gladiator2;

    public FightRecord(Gladiator gladiator1, Gladiator gladiator2) {
        this.gladiator1 = gladiator1;
        this.gladiator2 = gladiator2;
    }

    public boolean isGladiator1Winner() {
        return gladiator1.getScore() > gladiator2.getScore();
    }

    public boolean isGladiator2Winner() {
        return gladiator1.getScore() < gladiator2.getScore();
    }

    public boolean isDraw() {
        return gladiator1.getScore() == gladiator2.getScore();
    }

    public Gladiator getGladiator1() {
        return gladiator1;
    }

    public Gladiator getGladiator2() {
        return gladiator2;
    }
}
