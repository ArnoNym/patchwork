package ai.anneat.main.utils.arena;

import ai.anneat.models.calculation.Calculator;

public class Gladiator {
    private final Calculator calculator;
    private double score = 0;

    public Gladiator(Calculator calculator) {
        this.calculator = calculator;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Calculator getCalculator() {
        return calculator;
    }
}
