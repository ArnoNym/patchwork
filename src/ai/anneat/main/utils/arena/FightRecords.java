package ai.anneat.main.utils.arena;

import ai.anneat.models.calculation.Calculator;
import ai.anneat.main.utils.collections.Matrix;

import java.util.HashSet;
import java.util.Set;

public class FightRecords {
    private final Matrix<Calculator, FightRecord> matrix = new Matrix<>();
    private final Set<Calculator> usedRecords = new HashSet<>();

    public FightRecords() {

    }

    public void clearUsedRecords() {
        usedRecords.clear();
    }

    public void removeUnusedRecords() {
        matrix.removeBySingleKeysNotIn(usedRecords);
        clearUsedRecords();
    }

    public FightRecord getFightRecord(Calculator calculator1, Calculator calculator2) {
        usedRecords.add(calculator1);
        usedRecords.add(calculator2);
        return matrix.get(calculator1, calculator2);
    }

    /**
     * @param calculator1 calculator1
     * @param calculator2 calculator2
     * @return -1 if no fight recorded, 0 if draw, 1 if agent 1 is winner, 2 if agent 2 is winner
     */
    public int getWinner(Calculator calculator1, Calculator calculator2) {
        if (calculator1.equals(calculator2)) {
            throw new IllegalArgumentException();
        }
        FightRecord record = matrix.get(calculator1, calculator2);
        if (record == null) {
            return -1;
        }
        usedRecords.add(calculator1);
        usedRecords.add(calculator2);
        boolean flipped = record.getGladiator1().getCalculator().equals(calculator2);
        if ((!flipped && record.isGladiator1Winner()) || (flipped && record.isGladiator2Winner())) {
            return 1;
        } else if ((!flipped && record.isGladiator2Winner()) || (flipped && record.isGladiator1Winner())) {
            return 2;
        }
        return 0;
    }

    /*
    public void remove(Calculator calculator) {
        matrix.remove(calculator);
    }
    */

    public void addFightRecord(Gladiator g1, Gladiator g2) {
        usedRecords.add(g1.getCalculator());
        usedRecords.add(g2.getCalculator());
        matrix.put(g1.getCalculator(), g2.getCalculator(), new FightRecord(g1, g2));
    }
}
