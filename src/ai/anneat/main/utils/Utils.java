package ai.anneat.main.utils;

import ai.anneat.models.generation.Agent;
import ai.anneat.models.generation.Species;
import ai.anneat.publish.settings.Settings;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.view.Frame;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Function;

public class Utils {
    public static Settings getSettings(OrderedSet<Species> species) {
        return species.get(0).getAgents().get(0).getGenome().getPool().getSettings();
    }

    public static double getX(double fromX, double toX) {
        assert fromX < toX : "fromX="+fromX+", toX="+toX;
        double x = fromX + (toX - fromX) * (0.25 + 0.5 * Math.random());
        assert fromX < x : "fromX="+fromX+", toX="+toX+", x="+x;
        assert x < toX : "fromX="+fromX+", toX="+toX+", x="+x;
        return x;
    }

    public static double getY(double fromY, double toY) {
        double offset = 0.1 + Math.random() * 0.1;
        if (Math.random() > 0.5) {
            offset = -offset;
        }
        return (fromY + toY) / 2 + offset;
    }

    public static void show(Agent agent) {
        new Frame(agent);
        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static <E> E randomElement(List<E> list) {
        return list.get((int) (Math.random() * list.size()));
    }

    public static double shiftWeigh(double weight, double weightShiftStrength) {
        return weight + (Math.random() * 2 - 1) * weightShiftStrength;
    }

    public static double randomWeight(double weightRandomStrength) {
        return (Math.random() * 2 - 1) * weightRandomStrength;
    }

    public static double roundToTwoDecimals(double value) {
        return Math.round(value * 100) / 100d;
    }

    public static String toString(int speciesCount, @Nullable Species species, @NotNull Agent agent) {
        String popString = "[sc: "+speciesCount+"]";
        String agentString = agent
                +" [s: " + roundToTwoDecimals(agent.getScore())+"]"
                +" [vs: "+ roundToTwoDecimals(agent.getVanillaScore())+"]"
                +" [cplx: "+ roundToTwoDecimals(agent.complexity())+"]";
        if (species == null) {
            return agentString;
        } else {
            String speciesString = species
                    + " [sp s: " + roundToTwoDecimals(species.getScore()) + "]"
                    + " [ag vs: " + roundToTwoDecimals(species.getAllTimeAgentMaxVanillaScore()) + "]"
                    + " [size: " + species.size() + "]"
                    + " [nic: " + species.getScoreDidntIncreaseCounter() + "]";
            return popString + ",     " + speciesString+",     "+agentString;
        }
    }

    public static Function<double[], double[]> createRandomFunction(int outputSize) {
        return input -> {
            double[] output = new double[outputSize];
            for (int i = 0; i < outputSize; i++) {
                output[i] = Math.random();
            }
            return output;
        };
    }
}
