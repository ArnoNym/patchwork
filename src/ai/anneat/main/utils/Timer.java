package ai.anneat.main.utils;

public class Timer {
    private final String name;
    private final long startTime;
    private long endTime;
    private Long duration = null;

    private Timer(String name, long startTime) {
        this.name = name;
        this.startTime = startTime;
    }

    public static Timer start(String name) {
        return new Timer(name, System.nanoTime());
    }

    public long stop() {
        endTime = System.nanoTime();
        duration = endTime - startTime;
        return duration;
    }

    public long getNanos() {
        if (duration == null) {
            stop();
        }
        return duration;
    }

    public long getMillis() {
        return getNanos() / 1000000;
    }

    public double getSeconds() {
        return getNanos() / 1000000000d;
    }

    public void printNanos() {
        System.out.println(toString());
    }

    public void printSeconds() {
        System.out.println(name + ": "+getSeconds());
    }

    @Override
    public String toString() {
        return name + ": "+getNanos();
    }
}
