package ai.anneat.main.utils.selectors;

import ai.anneat.models.generation.Agent;

import java.util.Collection;
import java.util.stream.Collectors;

public class AgentSelector {
    private ByScoreCalculator byScoreCalculator;

    public AgentSelector(double differenceMaxPercent, int offspringCount, Collection<Agent> agents) {
        double maxScore = agents.stream().mapToDouble(Agent::getVanillaScore).max().orElseThrow();
        agents = agents.stream()
                .filter(agent -> agent.getVanillaScore() >= maxScore * (1 - differenceMaxPercent))
                .collect(Collectors.toSet());
        //byScoreCalculator = new ByScoreCalculator(offspringCount, )
    }
}
