package ai.anneat.main.utils.selectors;

import java.util.ArrayList;
import java.util.List;

public class RandomSelector<T> {

    private final List<T> objects = new ArrayList<>();
    private final List<Double> scores = new ArrayList<>();

    private double total_score = 0;

    public void add(T element, double score) {
        if (element == null) {
            throw new IllegalArgumentException();
        }
        objects.add(element);
        scores.add(score);
        total_score+=score;
    }

    public T random() {
        if (objects.isEmpty()) {
            throw new IllegalStateException();
        }
        double findAtScore = Math.random() * total_score;
        double countedScore = 0;
        for (int i = 0; i < objects.size(); i++) {
            countedScore += scores.get(i);
            if (countedScore >= findAtScore) {
                return objects.get(i);
            }
        }
        throw new IllegalStateException();
    }

    public void reset() {
        objects.clear();
        scores.clear();
        total_score = 0;
    }

    public boolean isEmpty() {
        return objects.isEmpty();
    }
}
