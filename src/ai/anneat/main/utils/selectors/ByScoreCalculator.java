package ai.anneat.main.utils.selectors;

import ai.anneat.models.generation.Species;

import java.util.Collection;

public class ByScoreCalculator {
    private int offspringCount;
    private int speciesCount;
    private double totalScore;

    public ByScoreCalculator(int offspringCount, Collection<Species> species) {
        this.offspringCount = offspringCount;
        if (species.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.speciesCount = species.size();
        this.totalScore = 0;
        for (Species s : species) {
            this.totalScore += s.getScore();
        }
        if (totalScore < 0) {
            throw new IllegalArgumentException();
        }
    }

    public boolean isTotalScoreZero() {
        return totalScore == 0;
    }

    public int getCount(double score) {
        if (speciesCount <= 0) {
            throw new IllegalArgumentException();
        }
        if (speciesCount == 1) {
            speciesCount -= 1;
            return offspringCount;
        }
        if (totalScore < 0) {
            if (Math.round(totalScore * 100000) / 100000d == 0) {
                return 0;
            } else {
                throw new IllegalStateException("score=" + score + ", totalScore=" + totalScore);
            }
        }
        int speciesOffspringCount = (int) (offspringCount * score / totalScore);

        offspringCount -= speciesOffspringCount;
        assert offspringCount >= 0;

        speciesCount -= 1;

        totalScore -= score;

        return speciesOffspringCount;
    }
}
