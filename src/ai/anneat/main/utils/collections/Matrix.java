package ai.anneat.main.utils.collections;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Matrix<K, V> implements Map<NoOrderTuple<K, K>, V> {
    private final Map<NoOrderTuple<K, K>, V> map = new HashMap<>();

    public V put(K k1, K k2, V value) {
        return put(new NoOrderTuple<>(k1, k2), value);
    }

    public V get(K k1, K k2) {
        return map.get(new NoOrderTuple<>(k1, k2));
    }

    public Set<K> getKeys() {
        Set<K> keys = new HashSet<>();
        for (Tuple<K, K> keyTuple : map.keySet()) {
            keys.add(keyTuple.getV1());
            keys.add(keyTuple.getV2());
        }
        return keys;
    }

    public boolean removeBySingleKeysNotIn(Collection<K> keys) {
        Set<K> removeKeys = getKeys();
        removeKeys.removeAll(keys);
        return removeBySingleKeys(removeKeys);
    }

    public boolean removeBySingleKeys(Collection<K> keys) {
        boolean ret = false;
        for (K key : keys) {
            ret = ret | removeBySingleKey(key);
        }
        return ret;
    }

    public boolean removeBySingleKey(K key) {
        return map.keySet().removeIf(keyTuple -> keyTuple.getV1().equals(key) || keyTuple.getV2().equals(key));
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return map.get(key);
    }

    @Nullable
    @Override
    public V put(NoOrderTuple<K, K> key, V value) {
        return map.put(key, value);
    }

    @Override
    public V remove(Object key) {
        return map.remove(key);
    }

    @Override
    public void putAll(@NotNull Map<? extends NoOrderTuple<K, K>, ? extends V> m) {
        throw new IllegalStateException();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @NotNull
    @Override
    public Set<NoOrderTuple<K, K>> keySet() {
        throw new IllegalStateException();
    }

    @NotNull
    @Override
    public Collection<V> values() {
        throw new IllegalStateException();
    }

    @NotNull
    @Override
    public Set<Entry<NoOrderTuple<K, K>, V>> entrySet() {
        throw new IllegalStateException();
    }
}
