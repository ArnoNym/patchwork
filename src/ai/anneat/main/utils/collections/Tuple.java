package ai.anneat.main.utils.collections;

import java.io.Serializable;
import java.util.Objects;

public class Tuple<V1, V2> implements Serializable {
    private final V1 v1;
    private final V2 v2;

    public Tuple(V1 v1, V2 v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public V1 getV1() {
        return v1;
    }

    public V2 getV2() {
        return v2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(v1, v2);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tuple)) return false;
        Tuple<?, ?> other = (Tuple<?, ?>) obj;
        return v1.equals(other.v1) && v2.equals(other.v2);
    }
}
