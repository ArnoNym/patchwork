package ai.anneat.main.utils.collections;

public class NoOrderTuple<V1, V2> extends Tuple<V1, V2> {

    public NoOrderTuple(V1 v1, V2 v2) {
        super(v1, v2);
    }

    @Override
    public int hashCode() {
        return getV1().hashCode() + getV2().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tuple)) return false;
        Tuple<?, ?> other = (Tuple<?, ?>) obj;
        return (getV1().equals(other.getV2()) && getV2().equals(other.getV1())) || (getV1().equals(other.getV1()) && getV2().equals(other.getV2()));
    }
}
