package ai.anneat.main.utils.collections;

import ai.anneat.models.genome.NodeGene;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class NodeNodeMap<V> implements Serializable {
    private final Map<NodeGene, Map<NodeGene, V>> map = new HashMap<>();

    public void put(NodeGene k1, NodeGene k2, V v) {
        if (k1 == null) {
            throw new IllegalArgumentException();
        }
        if (k2 == null) {
            throw new IllegalArgumentException();
        }
        if (v == null) {
            throw new IllegalArgumentException();
        }
        Map<NodeGene, V> innerMap = map.computeIfAbsent(k1, k -> new HashMap<>());
        innerMap.put(k2, v);
    }

    public Optional<V> get(NodeGene k1, NodeGene k2) {
        Map<NodeGene, V> innerMap = map.get(k1);
        if (innerMap == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(innerMap.get(k2));
    }
}
