package ai.anneat.main.utils.collections;

import ai.anneat.main.utils.Utils;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderedSet<O> implements Set<O>, Serializable {
    private final ArrayList<O> list;
    private transient Comparator<O> comparator;

    public OrderedSet() {
        this.list = new ArrayList<>();
        this.comparator = null;
    }

    public OrderedSet(Collection<? extends O> collection) {
        Set<O> set = new HashSet<>(collection);
        if (set.size() != collection.size()) {
            throw new IllegalArgumentException();
        }
        this.list = new ArrayList<>(set);
        this.comparator = null;
    }

    public OrderedSet(Comparator<O> comparator) {
        this.list = new ArrayList<>();
        this.comparator = comparator;
    }

    public OrderedSet(Collection<? extends O> collection, Comparator<O> comparator) {
        this(collection);
        this.comparator = comparator;

        list.sort(comparator);
    }

    public Comparator<O> getComparator() {
        return comparator;
    }

    public void setComparator(Comparator<O> comparator) {
        if (comparator != null) {
            this.list.sort(comparator);
        }
        this.comparator = comparator;
    }

    /**
     * @param ofLowestIndexPercent ofLowestIndexPercent
     * @throws IllegalArgumentException if ofLowestIndexPercent is smaller 0 or bigger 1
     * @return null if empty or ofLowestIndexPercent is zero. Else random element in lowest index percent rounding up
     */
    public O randomElement(double ofLowestIndexPercent) {
        if (ofLowestIndexPercent < 0 || ofLowestIndexPercent > 1) {
            throw new IllegalArgumentException();
        }
        if (isEmpty() || ofLowestIndexPercent == 0) {
            return null;
        }
        int topCount = Math.max(1, (int) Math.ceil(size() * ofLowestIndexPercent));
        int returnIndex = (int) (topCount * Math.random());
        return get(returnIndex);
    }

    /**
     * Rounds up to two
     * @param ofLowestIndexPercent ofLowestIndexPercent
     * @return Two different random elements. The first one had the lower index
     */
    public Map.Entry<O, O> randomElements(double ofLowestIndexPercent) {
        if (ofLowestIndexPercent < 0 || ofLowestIndexPercent > 1) {
            throw new IllegalArgumentException();
        }
        if (size() < 2) {
            return null;
        }
        int topCount = Math.max(2, (int) Math.ceil(size() * ofLowestIndexPercent));
        int firstReturnIndex = (int) (topCount * Math.random());
        int secondReturnIndex;
        do {
            secondReturnIndex = (int) (topCount * Math.random());
        } while (firstReturnIndex == secondReturnIndex);
        if (firstReturnIndex > secondReturnIndex) {
            int temp = firstReturnIndex;
            firstReturnIndex = secondReturnIndex;
            secondReturnIndex = temp;
        }
        return new AbstractMap.SimpleImmutableEntry<>(get(firstReturnIndex), get(secondReturnIndex));
    }

    public O randomElement() {
        if (isEmpty()) {
            return null;
        }
        O o = Utils.randomElement(list);
        assert o != null;
        return o;
    }

    public O randomElement(Predicate<? super O> filterPredicate) {
        List<O> filteredList = list.stream().filter(filterPredicate).collect(Collectors.toList());
        if (filteredList.isEmpty()) {
            return null;
        }
        O o = Utils.randomElement(filteredList);
        assert o != null;
        return o;
    }

    public void removeLast(int amount) {
        assert size() >= amount;

        int maxIndex = size() - 1;

        for (int i = maxIndex; i > maxIndex - amount; i--) {
            list.remove(size() - 1);
        }
    }

    public boolean addAll(int i, Collection<? extends O> collection) {
        throw new IllegalStateException();
    }

    public O get(int i) {
        return list.get(i);
    }

    public O set(int i, O o) {
        throw new IllegalStateException();
    }

    public void add(int i, O o) {
        throw new IllegalStateException();
    }

    public O remove(int i) {
        return list.remove(i);
    }

    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    public int lastIndexOf(Object o) {
        throw new IllegalStateException();
    }

    public ListIterator<O> listIterator() {
        return list.listIterator();
    }

    public ListIterator<O> listIterator(int i) {
        throw new IllegalStateException();
    }

    public List<O> subList(int i, int i1) {
        return list.subList(i, i1);
    }

    public void replaceAll(UnaryOperator<O> operator) {
        throw new IllegalStateException();
    }

    public void sort(Comparator<? super O> c) {
        if (comparator == null) {
            list.sort(c);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<O> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        throw new IllegalStateException();
    }

    @Override
    public boolean add(O o) {
        if (o == null) {
            throw new IllegalArgumentException();
        }
        if (contains(o)) {
            return false;
        }
        if (comparator == null) {
            return list.add(o);
        }
        for (int i = 0; i < size(); i++) {
            if (comparator.compare(o, get(i)) < 0) {
                list.add(i, o);
                return true;
            }
        }
        return list.add(o);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return list.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends O> collection) {
        for (O o : collection) {
            add(o);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return list.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new IllegalStateException();
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Spliterator<O> spliterator() {
        throw new IllegalStateException();
    }

    @Override
    public <T> T[] toArray(IntFunction<T[]> generator) {
        throw new IllegalStateException();
    }

    @Override
    public boolean removeIf(Predicate<? super O> filter) {
        throw new IllegalStateException();
    }

    @Override
    public Stream<O> stream() {
        return list.stream();
    }

    @Override
    public Stream<O> parallelStream() {
        throw new IllegalStateException();
    }

    @Override
    public void forEach(Consumer<? super O> action) {
        list.forEach(action);
    }
}
