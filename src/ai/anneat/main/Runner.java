package ai.anneat.main;

import ai.anneat.models.calculation.Calculator;
import ai.anneat.models.generation.Agent;
import ai.anneat.publish.Neat;
import ai.anneat.publish.settings.Settings;
import ai.anneat.publish.task.ClassificationTask;
import ai.anneat.publish.task.Deterministic;
import ai.anneat.view.Frame;

import java.util.function.Function;

public class Runner {
    private static double[] input1;
    private static double[] input2;
    private static double[] input3;
    private static double[] input4;
    private static Function<double[], Double> function1;
    private static Function<double[], Double> function2;
    private static Function<double[], Double> function3;
    private static Function<double[], Double> function4;
    private static ClassificationTask xor;
    private static Settings settings;
    private static Neat neat;

    public static void main(String[] args) {
        xor = new ClassificationTask(2, 1, Deterministic.YES_EXCEPTION);

        input1 = new double[]{1d, 1d};
        function1 = output -> (1 - output[0]) / 10 + (output[0] < 0.5 ? 1 : 0);
        xor.put(input1, function1);

        input2 = new double[]{1d, 0d};
        function2 = output -> output[0] / 10 + (output[0] > 0.5 ? 1 : 0);
        xor.put(input2, function2);

        input3 = new double[]{0d, 1d};
        function3 = output -> output[0] / 10 + (output[0] > 0.5 ? 1 : 0);
        xor.put(input3, function3);

        input4 = new double[]{0d, 0d};
        function4 = output -> (1 - output[0]) / 10 + (output[0] < 0.5 ? 1 : 0);
        xor.put(input4, function4);

        //evolveSettings();

        testParameters(true);
        // evolveXorFast(true, false, true);
        //evolveXorSlow();
    }

    public static void evolveXorFast(boolean showSteps, boolean slow, boolean showResults) {
        settings = new Settings();

        if (showSteps) {
            settings.getPerformanceSettings().setPrint(true);
            neat = new Neat(settings, xor, 8d, null);
            Frame frame = new Frame(neat);
            for (int i = 0; i < 1000; i++) {
                if (slow) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                frame.evolve(1);
                if (neat.isTargetScoreReached()) {
                    break;
                }
                frame.repaint();
            }
        } else {
            neat = new Neat(settings, xor, 4d, null);
            neat.evolve(1000);
        }

        Agent agent = neat.getBestAgent();
        Calculator c = agent.getCalculator();

        double[] output1 = c.calculate(input1);
        double outputNode1 = output1[0];
        double score1 = function1.apply(output1);
        double bool1 = outputNode1 < 0.5 ? 1 : 0;

        double[] output2 = c.calculate(input2);
        double outputNode2 = output2[0];
        double score2 = function2.apply(output2);
        double bool2 = outputNode2 > 0.5 ? 1 : 0;

        double[] output3 = c.calculate(input3);
        double outputNode3 = output3[0];
        double score3 = function3.apply(output3);
        double bool3 = outputNode3 > 0.5 ? 1 : 0;

        double[] output4 = c.calculate(input4);
        double outputNode4 = output4[0];
        double score4 = function4.apply(output4);
        double bool4 = outputNode4 < 0.5 ? 1 : 0;

        System.out.println("Correct output in : "+(bool1 + bool2 + bool3 + bool4)+" out of 4 cases!");
        System.out.println("(1, 1) soll: 0,   outputNode: "+outputNode1+",   score: "+score1+",   bool: "+bool1);
        System.out.println("(1, 0) soll: 1,   outputNode: "+outputNode2+",   score: "+score2+",   bool: "+bool2);
        System.out.println("(0, 1) soll: 1,   outputNode: "+outputNode3+",   score: "+score3+",   bool: "+bool3);
        System.out.println("(0, 0) soll: 0,   outputNode: "+outputNode4+",   score: "+score4+",   bool: "+bool4);

        if (showResults) new Frame(agent);
    }

    public static void testParameters(boolean show) {
        int tries = 100;
        int steps = 0;
        int notReached = 0;
        double avgComplexity = 0;
        double avgHiddenNodeCount = 0;
        int minHiddenNodeReachedCount = 0;
        for (int i = 0; i < tries; i++) {
            evolveXorFast(false, false, show);
            steps += neat.getGenerationNumber();

            Agent agent = neat.getBestAgent();
            avgComplexity += agent.complexity();
            int agentHiddenNodesCount = agent.getGenome().getNodes().size() - 2 - 1 - 1;
            avgHiddenNodeCount += agentHiddenNodesCount;
            if (agentHiddenNodesCount == 1) {
                minHiddenNodeReachedCount++;
            }

            System.out.println("steps: "+neat.getGenerationNumber());
            System.out.println("##########################################################");
            if (neat.getGenerationNumber() == 1000) {
                notReached += 1;
            }
        }
        avgComplexity /= tries;
        avgHiddenNodeCount /= tries;

        String avgSteps = "[Avg Steps: "+(steps/tries)+"]";
        String targetScoreReachedString = "[Target Score Reached: "+((1 - (double) notReached/tries)*100)+"%]";
        String minHiddenNodeCountReachedString = "[Min Hidden Nodes Reached: "+(((double) minHiddenNodeReachedCount/tries)*100)+"%]";
        String avgHiddenNodeCountString = "[Avg Hidden Nodes: "+avgHiddenNodeCount+"]";
        String avgComplexityString = "[Avg Complex: "+avgComplexity+"]";
        System.out.println(avgSteps
                + ",   " + targetScoreReachedString
                + ",   " + minHiddenNodeCountReachedString
                + ",   " + avgHiddenNodeCountString
                + ",   " + avgComplexityString);
    }

    public static void evolveXorSlow() {
        settings = new Settings();
        settings.getPerformanceSettings().setPrint(true);
        neat = new Neat(settings, xor, 8d, null);
        new Frame(neat);
    }

    /*
    public static void evolveSettings() {
        ParameterNeat parameterNeat = new ParameterNeat(xor, 8d, 100);
        parameterNeat.evolve(10);
        settings = parameterNeat.getSettings();
    }
    */
}
