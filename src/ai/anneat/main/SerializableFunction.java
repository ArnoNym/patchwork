package ai.anneat.main;

import java.io.Serializable;
import java.util.function.Function;

@FunctionalInterface
public interface SerializableFunction<T,U> extends Function<T,U>, Serializable {
}
