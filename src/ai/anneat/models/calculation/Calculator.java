package ai.anneat.models.calculation;

import ai.anneat.main.SerializableFunction;
import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.NodeGene;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Calculator implements Serializable {

    private final List<Node> inputNodes = new ArrayList<>();
    private final List<Node> hiddenNodes = new ArrayList<>();
    private final List<Node> outputNodes = new ArrayList<>();

    public Calculator(Genome g) {
        OrderedSet<NodeGene> nodeGeneList = g.getNodes();
        OrderedSet<ConnectionGene> connectionGeneList = g.getConnections();

        assert nodeGeneList.size() >= g.getPool().getInputNodes().size() +  g.getPool().getOutputNodes().size();

        HashMap<Integer, Node> nodeHashMap = new HashMap<>();

        Node biasNode = null;
        for (NodeGene nodeGene : nodeGeneList) {
            Node node = new Node(nodeGene.getX());
            nodeHashMap.put(nodeGene.getId(), node);

            if (nodeGene.isInputNode()) {
                inputNodes.add(node);
            } else if (nodeGene.isOutputNode()) {
                outputNodes.add(node);
            } else if (nodeGene.isBiasNode()) {
                assert biasNode == null;
                biasNode = node;
                biasNode.setOutput(1);
            } else {
                hiddenNodes.add(node);
            }
        }
        assert biasNode != null;

        assert inputNodes.size() == g.getPool().getInputNodes().size();
        assert outputNodes.size() == g.getPool().getOutputNodes().size();

        hiddenNodes.sort(Node::compareTo);
        assert hiddenNodes.size() < 2 || hiddenNodes.get(0).getX() <= hiddenNodes.get(1).getX();

        for (ConnectionGene c : connectionGeneList) {
            NodeGene from = c.getFrom();
            NodeGene to = c.getTo();

            Node nodeFrom = nodeHashMap.get(from.getId());
            Node nodeTo = nodeHashMap.get(to.getId());

            Connection con = new Connection(nodeFrom, nodeTo, c.getWeight(), c.isEnabled());

            nodeTo.add(con);
        }
    }

    public double[] calculate(double... input) {
        if (input.length != inputNodes.size()) {
            throw new IllegalArgumentException("Data doesnt fit! input.length="+input.length+", inputNodes.size()="+inputNodes.size());
        }
        for (int i = 0; i < input.length; i++) {
            if (input[i] < 0 || input[i] > 1) {
                //throw new IllegalArgumentException("Input "+i+" is not >=0 and <=1 but "+input[i]+"!");
            }
        }

        for (int i = 0; i < inputNodes.size(); i++) {
            inputNodes.get(i).setOutput(input[i]);
        }

        for (Node n : hiddenNodes) {
            n.calculate();
        }

        double[] output = new double[outputNodes.size()];
        for (int i = 0; i < outputNodes.size(); i++) {
            outputNodes.get(i).calculate();
            output[i] = outputNodes.get(i).getOutput();
        }
        return output;
    }

    public SerializableFunction<double[], double[]> getFunction() {
        return this::calculate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("InputNodes:\n");
        for (Node node : inputNodes) {
            sb.append(node.toString());
        }
        sb.append("HiddenNodes:\n");
        for (Node node : hiddenNodes) {
            sb.append(node.toString());
        }
        sb.append("OutputNodes:\n");
        for (Node node : outputNodes) {
            sb.append(node.toString());
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Node node : inputNodes) {
            hashCode += node.hashCode();
        }
        for (Node node : hiddenNodes) {
            hashCode += node.hashCode() * 241;
        }
        for (Node node : outputNodes) {
            hashCode += node.hashCode() * 271242;
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Calculator)) return false;
        Calculator calculator = (Calculator) obj;
        if (!(new HashSet<>(inputNodes)).equals(new HashSet<>(calculator.inputNodes))) return false;
        if (!(new HashSet<>(hiddenNodes)).equals(new HashSet<>(calculator.hiddenNodes))) return false;
        return (new HashSet<>(outputNodes)).equals(new HashSet<>(calculator.outputNodes));
    }
}
