package ai.anneat.models.calculation;

import java.io.Serializable;

public class Connection implements Serializable {

    private final Node from;
    private final Node to;
    private final double weight;
    private final boolean enabled;

    public Connection(Node from, Node to, double weight, boolean enabled) {
        assert from != null;
        assert to != null;
        assert from.getX() < to.getX();

        this.from = from;
        this.to = to;
        this.weight = weight;
        this.enabled = enabled;
    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public double getWeight() {
        return weight;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String toString() {
        return "[weight: " + weight + "], " + "[enabled: " + enabled + "]";
    }

    @Override
    public int hashCode() {
        return (int) (weight * 1000000 + (enabled ? 100000000 : 0)) + from.getWithoutConnectionsHashCode() - to.getWithoutConnectionsHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Connection)) return false;
        Connection other = (Connection) obj;
        return weight == other.weight && enabled == other.enabled;
    }
}
