package ai.anneat.models.calculation;

import ai.anneat.main.utils.functions.Functions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Node implements Comparable<Node>, Serializable {

    private final double x;
    private double output = 0;
    private final List<Connection> connections = new ArrayList<>();

    public Node(double x) {
        this.x = x;
    }

    public void calculate() {
        double s = 0;
        for (Connection c : connections) {
            if (c.isEnabled()) {
                s += c.getWeight() * c.getFrom().getOutput();
            }
        }
        output = activationFunction(s);
    }

    private double activationFunction(double x) {
        return Functions.originalPaperModifiedSigmoidFunction(x);
    }

    public boolean add(Connection connection) {
        assert equals(connection.getTo());
        return connections.add(connection);
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public double getOutput() {
        return output;
    }

    public double getX() {
        return x;
    }

    @Override
    public int compareTo(Node o) {
        return Double.compare(this.x, o.x);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[x: ").append(x).append("]\n");
        sb.append("[output: ").append(output).append("]\n");
        sb.append("Connections:\n");
        for (Connection connection : connections) {
            sb.append("[connection: ").append(connection.toString()).append("]\n");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hashCode = getWithoutConnectionsHashCode();
        for (Connection connection : connections) {
            hashCode += connection.hashCode();
        }
        return hashCode;
    }

    public int getWithoutConnectionsHashCode() {
        return (int) (x * 21929224);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Node)) return false;
        Node node = (Node) obj;
        if (x != node.x) return false;
        if (connections.size() != node.connections.size()) return false;
        return (new HashSet<>(connections)).equals(new HashSet<>(node.connections));
    }
}
