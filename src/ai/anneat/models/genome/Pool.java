package ai.anneat.models.genome;

import ai.anneat.main.constants.Constants;
import ai.anneat.main.utils.collections.NodeNodeMap;
import ai.anneat.models.genome.ConnectionGene;
import ai.anneat.models.genome.NodeGene;
import ai.anneat.publish.settings.Settings;

import java.io.Serializable;
import java.util.*;

public class Pool implements Serializable {
    private Settings settings;

    private final NodeGene biasNode;
    private final List<NodeGene> inputNodes;
    private final List<NodeGene> outputNodes;

    private final NodeNodeMap<ConnectionGene> connectionMap = new NodeNodeMap<>();
    private final NodeNodeMap<NodeGene> nodeMap = new NodeNodeMap<>();

    private int nextNodeId = 1;

    public Pool(int inputSize, int outputSize) {
        this(inputSize, outputSize, new Settings());
    }

    public Pool(int inputSize, int outputSize, Settings settings) {
        this.settings = settings;
        this.biasNode = new NodeGene(0, Constants.BIAS_X, 1 / (double)(inputSize + 1 + 1));

        List<NodeGene> inputNodes = new ArrayList<>();
        for (int i = 1; i < inputSize + 1; i++) {
            NodeGene n = new NodeGene(
                    nextNodeId(),
                    Constants.INPUT_X,
                    (i + 1) / (double)(inputSize + 1 + 1));
            inputNodes.add(n);
        }
        this.inputNodes = Collections.unmodifiableList(inputNodes);

        List<NodeGene> outputNodes = new ArrayList<>();
        for (int i = 0; i < outputSize; i++) {
            NodeGene n = new NodeGene(
                    nextNodeId(),
                    Constants.OUTPUT_X,
                    (i + 1) / (double)(outputSize + 1));
            outputNodes.add(n);
        }
        this.outputNodes = Collections.unmodifiableList(outputNodes);
    }

    // Manage connections and nodes

    public Optional<ConnectionGene> getConnectionBetween(NodeGene left, NodeGene right) {
        return connectionMap.get(left, right);
    }

    public void putConnectionBetween(ConnectionGene con) {
        connectionMap.put(con.getFrom(), con.getTo(), con);
    }

    public Optional<NodeGene> getNodeBetween(NodeGene left, NodeGene right) {
        return nodeMap.get(left, right);
    }

    public void putNodeBetween(NodeGene left, NodeGene right, NodeGene node) {
        nodeMap.put(left, right, node);
    }

    /*

        NodeGene node;
        if (optionalNode.isPresent()) {
            node = optionalNode.get();
        } else {
            node = new NodeGene(nextNodeId(), Utils.getX(left.getX(), right.getX()), Utils.getY(left.getY(), right.getY()));
            nodeMap.put(left, right, node);
        }
        ConnectionGene leftCon = getConnectionBetween(left, node);
        ConnectionGene rightCon = getConnectionBetween(node, right);
        return new NodeConnectionWrapper(leftCon, node, rightCon);
     */

    // Simple getter and setter ========================================================================================

    public int nextNodeId() {
        return nextNodeId++;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public List<NodeGene> getInputNodes() {
        return inputNodes;
    }

    public List<NodeGene> getOutputNodes() {
        return outputNodes;
    }

    public NodeGene getBiasNode() {
        return biasNode;
    }
}
