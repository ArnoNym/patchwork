package ai.anneat.models.genome;

import org.jetbrains.annotations.NotNull;

public class ConnectionGene extends Gene {
    private final NodeGene from;
    private final NodeGene to;

    private double weight;
    private boolean enabled;

    public ConnectionGene(int id, NodeGene left, NodeGene right, double weight, boolean enabled) {
        super(id);
        assert left != null;
        assert right != null;
        assert !left.equals(right);
        assert left.getX() < right.getX();
        this.from = left;
        this.to = right;
        this.weight = weight;
        this.enabled = enabled;
    }

    public ConnectionGene(int id, NodeGene left, NodeGene right) {
        this(id, left, right, 1, true);
    }

    public ConnectionGene copy() {
        return new ConnectionGene(this);
    }

    private ConnectionGene(@NotNull ConnectionGene connectionGene) {
        this(connectionGene.getId(), connectionGene.from, connectionGene.to, connectionGene.weight, connectionGene.enabled);
    }

    public NodeGene getFrom() {
        return from;
    }

    public NodeGene getTo() {
        return to;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof ConnectionGene)) return false;
        return getId() == ((Gene) o).getId();
    }

    @Override
    public String toString() {
        return "ConnectionGene{" +
                "from=" + from.getId() +
                ", to=" + to.getId() +
                ", weight=" + weight +
                ", enabled=" + enabled +
                '}';
    }
}
