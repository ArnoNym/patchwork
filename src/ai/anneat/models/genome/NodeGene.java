package ai.anneat.models.genome;

import ai.anneat.main.constants.Constants;

public class NodeGene extends Gene {

    private final double x,y;

    public NodeGene(int id, double x, double y) {
        super(id);
        this.x = x;
        this.y = y;
    }

    public boolean isBiasNode() {
        return getId() == 0;
    }

    public boolean isInputNode() {
        return x == Constants.INPUT_X && getId() != 0;
    }

    public boolean isOutputNode() {
        return x == Constants.OUTPUT_X;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof NodeGene)) return false;
        return getId() == ((Gene) o).getId();
    }
}
