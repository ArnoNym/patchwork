package ai.anneat.models.genome;

import java.io.Serializable;

public class Gene implements Comparable<Gene>, Serializable {
    private final int id;

    public Gene(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(Gene gene) {
        return Integer.compare(getId(), gene.getId());
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Gene)) return false;
        return id == ((Gene) o).getId();
    }

    public int hashCode(){
        return id;
    }

    @Override
    public String toString() {
        return "Gene{"
                + "id=" + id
                + '}';
    }
}
