package ai.anneat.models.genome;

import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.main.constants.Constants;
import ai.anneat.main.utils.NodeConnectionWrapper;
import ai.anneat.main.utils.Utils;
import ai.decisionTree.Node;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Genome implements Serializable {
    private final Pool pool;

    private final OrderedSet<ConnectionGene> connections;
    private final OrderedSet<NodeGene> nodes;

    public static Genome allInputAndOutputNodes(Pool pool) {
        return new Genome(pool);
    }

    public static Genome allInputAndOutputNodesConnected(Pool pool) {
        Genome genome = new Genome(pool);
        genome.createAlInputToOutputNodesConnections();
        return genome;
    }

    /**
     * The bias, alle Input-Nodes, all Output-Nodes will get added
     * No connections will be added
     * @param pool data
     */
    public Genome(Pool pool) {
        this.pool = pool;
        this.nodes = new OrderedSet<>(pool.getInputNodes());
        this.nodes.add(pool.getBiasNode());
        this.nodes.addAll(pool.getOutputNodes());
        this.connections = new OrderedSet<>(Comparator.comparingInt(ConnectionGene::getId));
    }

    /**
     * This is seperated from the creation of the input and output nodes because there may be genomes without
     * any of these connections (because they got replaced by a new Node with two New Connections.
     */
    public void createAlInputToOutputNodesConnections() {
        for (NodeGene inputNode : pool.getInputNodes()) {
            for (NodeGene outputNodes : pool.getOutputNodes()) {
                addNewConnection(inputNode, outputNodes, null);
            }
        }
    }

    /**
     * Bias, input and output-nodes will be fetched from data because not all connections need to be added
     * and connections may get lost when first mutating to a node and than not getting copied on crossover.
     * Which means it is possible that there are input nodes that have no connection
     *
     * @param connections Get added without any change
     * @param pool data
     */
    public Genome(Pool pool, Collection<ConnectionGene> connections) {
        this.pool = pool;
        Set<NodeGene> nodeSet = new HashSet<>();
        nodeSet.add(pool.getBiasNode());
        nodeSet.addAll(pool.getInputNodes());
        nodeSet.addAll(pool.getOutputNodes());
        for (ConnectionGene c : connections) {
            nodeSet.add(c.getFrom());
            nodeSet.add(c.getTo());
        }
        this.nodes = new OrderedSet<>(nodeSet);
        connections = connections.stream().map(ConnectionGene::copy).collect(Collectors.toSet());
        this.connections = new OrderedSet<>(connections, Comparator.comparingInt(ConnectionGene::getId));
    }

    public Genome copyWithNewObjects() {
        return new Genome(this);
    }

    private Genome(@NotNull Genome genome) {
        this.pool = genome.pool;
        this.connections = new OrderedSet<>();
        for (ConnectionGene c : genome.connections) {
            this.connections.add(c.copy());
        }
        this.nodes = new OrderedSet<>(genome.nodes);
    }

    /**
     * @return distance between this and another anneat.genome
     * @param g2 g2
     */
    public double distance(Genome g2) {
        Genome g1 = this;

        if(g1.getHighestInnovationGeneId() < g2.getHighestInnovationGeneId()) {
            Genome g = g1;
            g1 = g2;
            g2 = g;
        }

        double n = Math.max(g1.getConnections().size(), g2.getConnections().size());
        if (n == 0) {
            return 0;
        }
        if (n < 20) {
            //n = 1;
            //n = Functions.exponentialOneToTwenty(n);
        }

        int indexG1 = 0;
        int indexG2 = 0;

        int disjoint = 0;
        int similar = 0;
        double weightDiff = 0;

        while(indexG1 < g1.getConnections().size() && indexG2 < g2.getConnections().size()) {
            ConnectionGene gene1 = g1.getConnection(indexG1);
            ConnectionGene gene2 = g2.getConnection(indexG2);

            int in1 = gene1.getId();
            int in2 = gene2.getId();

            if(in1 == in2){
                // similar gene
                similar++;
                weightDiff += Math.abs(gene1.getWeight() - gene2.getWeight());
                indexG1++;
                indexG2++;
            } else if(in1 < in2) {
                // disjoint gene of a
                disjoint++;
                indexG1++;
            } else {
                // disjoint gene of b
                disjoint++;
                indexG2++;
            }
        }
        weightDiff /= Math.max(1, similar);

        int excess = g1.getConnections().size() - indexG1;

        double disjointDistance = pool.getSettings().getEvolutionSettings().getC1() * disjoint / n;
        double excessDistance = pool.getSettings().getEvolutionSettings().getC2() * excess / n;
        double weightDistance = pool.getSettings().getEvolutionSettings().getC3() * weightDiff;
        //System.out.println("disjointDistance="+disjointDistance+", excessDistance="+excessDistance+", weightDistance="+weightDistance);
        return disjointDistance + excessDistance + weightDistance;
    }

    /**
     * Creates a new anneat.genome.
     * genome1 should have the higher score.
     * Steps
     *  - take all the genes of a
     *  - if there is a anneat.genome in a that is also in b, choose randomly
     *  - do not take disjoint genes of b
     *  - take excess genes of a if they exist
     * @param genome1 Should be the anneat.genome with the higher score
     * @param genome2 Should be the anneat.genome with the lower score
     * @return A new anneat.genome
     */
    public static Genome crossOver(Genome genome1, Genome genome2) {
        Set<ConnectionGene> genes = new HashSet<>();

        int indexG1 = 0;
        int indexG2 = 0;

        while (indexG1 < genome1.getConnections().size() && indexG2 < genome2.getConnections().size()) {
            ConnectionGene gene1 = genome1.getConnections().get(indexG1);
            ConnectionGene gene2 = genome2.getConnections().get(indexG2);

            int id1 = gene1.getId();
            int id2 = gene2.getId();

            if (id1 == id2) {
                ConnectionGene con;
                if (Math.random() > 0.5) {
                    con = gene1.copy();
                } else {
                    con = gene2.copy();
                }
                con.setEnabled((gene1.isEnabled() && gene2.isEnabled())
                        || ((gene1.isEnabled() || gene2.isEnabled()) && Math.random() > 0.7)); //TODO Konstante
                genes.add(con);
                indexG1++;
                indexG2++;
            } else if (id1 < id2) {
                // disjoint gene of a
                genes.add(gene1.copy());
                indexG1++;
            } else {
                // disjoint gene of b
                genes.add(gene2.copy());
                indexG2++;
            }
        }

        while (indexG1 < genome1.getConnections().size()) {
            ConnectionGene gene1 = genome1.getConnections().get(indexG1);
            genes.add(gene1.copy());
            indexG1++;
        }
        while (indexG2 < genome1.getConnections().size()) {
            ConnectionGene gene1 = genome1.getConnections().get(indexG2);
            genes.add(gene1.copy());
            indexG2++;
        }

        return new Genome(genome1.getPool(), genes);
    }

    public void mutate(boolean bigSpecies) {
        boolean mutated = false;
        if (pool.getSettings().getEvolutionSettings().getProbabilityMutateActivateLink() > Math.random()) {
            if (mutateConnectionActivate()) mutated = true;
        }
        if (pool.getSettings().getEvolutionSettings().getProbabilityMutateDeactivateLink() > Math.random()) {
            if (mutateConnectionDeactivate()) mutated = true;
        }
        if (pool.getSettings().getEvolutionSettings().getProbabilityMutateWeight() > Math.random()) {
            mutateWeights();
            mutated = true;
        }
        if (pool.getSettings().getEvolutionSettings().isAllowHiddenNodes()) {
            double propMutateNode = bigSpecies ? pool.getSettings().getEvolutionSettings().getProbabilityMutateNodeBigPop() : pool.getSettings().getEvolutionSettings().getProbabilityMutateNodeSmallPop();
            if (propMutateNode > Math.random()) {
                if (mutateNewNodeWithAdditionalToCon()) mutated = true;
            }
        }
        double propMutateConnection = bigSpecies ? pool.getSettings().getEvolutionSettings().getProbabilityMutateLinkBigPop() : pool.getSettings().getEvolutionSettings().getProbabilityMutateLinkSmallPop();
        if (propMutateConnection > Math.random()) {
            if (mutateNewConnection().isPresent()) mutated = true;
        }
        if (!mutated) {
            mutate(bigSpecies);
        }
    }

    /*
    public void mutate(boolean bigSpecies) {
        if (pool.getSettings().getEvolutionSettings().getProbabilityMutateActivateLink() > Math.random()) {
            if (mutateConnectionActivate()) return;
        }
        if (pool.getSettings().getEvolutionSettings().getProbabilityMutateDeactivateLink() > Math.random()) {
            if (mutateConnectionDeactivate()) return;
        }
        if (pool.getSettings().getEvolutionSettings().isAllowHiddenNodes()) {
            double propMutateNode = bigSpecies ? pool.getSettings().getEvolutionSettings().getProbabilityMutateNodeBigPop() : pool.getSettings().getEvolutionSettings().getProbabilityMutateNodeSmallPop();
            if (propMutateNode > Math.random()) {
                if (mutateNewNode().isPresent()) return;
            }
        }
        double propMutateConnection = bigSpecies ? pool.getSettings().getEvolutionSettings().getProbabilityMutateLinkBigPop() : pool.getSettings().getEvolutionSettings().getProbabilityMutateLinkSmallPop();
        if (propMutateConnection > Math.random()) {
            if (mutateNewConnection().isPresent()) return;
        }
        mutateWeights();
    }
    */

    public Optional<ConnectionGene> mutateNewConnection() {
        assert nodes.size() >= 3; // At Least the bias, one in and one out
        for (int i = 0; i < Constants.MAX_TRIES_TO_FIND_CONNECTION; i++) {
            NodeGene a = nodes.randomElement();
            NodeGene b = nodes.randomElement(node -> (node.getX() != a.getX()));

            if (b == null) {
                return Optional.empty(); // All nodes have the same x value
            }

            Optional<ConnectionGene> optionalCon = addNewConnection(a, b, null);
            if (optionalCon.isPresent()) {
                return optionalCon;
            }
        }
        return Optional.empty();
    }

    /**
     * @param a One Node of the new Connection
     * @param b Other Node of the new Connection with a different x value than a
     * @param weight Weight of the new connection, set to null for random weight
     * @return Empty optional if connection already exists, Filled Optional if new connection was added
     * @throws IllegalArgumentException if the x values of the nodes are equal
     */
    public Optional<ConnectionGene> addNewConnection(NodeGene a, NodeGene b, @Nullable Double weight) {
        if (a.getX() == b.getX()) {
            throw new IllegalArgumentException();
        }

        weight = Objects.requireNonNullElseGet(
                weight,
                () -> Utils.randomWeight(pool.getSettings().getEvolutionSettings().getWeightRandomStrength()));

        if (a.getX() > b.getX()) {
            NodeGene temp = a;
            a = b;
            b = temp;
        }

        Optional<ConnectionGene> optionalCon = pool.getConnectionBetween(a, b);

        ConnectionGene con;
        if (optionalCon.isPresent()) {
            con = optionalCon.get();
            if (connections.contains(con)) {
                return Optional.empty();
            }
            con = con.copy();
            con.setWeight(weight);
            con.setEnabled(true);
        } else {
            con = new ConnectionGene(pool.nextNodeId(), a, b, weight, true);
            pool.putConnectionBetween(con);
        }

        assert !connections.contains(con);
        connections.add(con);

        return Optional.of(con);
    }

    public boolean mutateNewNodeWithAdditionalToCon() {
        Optional<NodeConnectionWrapper> optionalNodeConnectionWrapper = mutateNewNode();
        if (optionalNodeConnectionWrapper.isEmpty()) {
            return false;
        }
        NodeGene mutatedNode = optionalNodeConnectionWrapper.get().getNode();
        List<NodeGene> otherNodes = nodes.stream()
                .filter(nodeGene -> nodeGene.getX() < mutatedNode.getX())
                .collect(Collectors.toList());
        addNewConnection(Utils.randomElement(otherNodes), mutatedNode, null);
        return true;
    }

    public Optional<NodeConnectionWrapper> mutateNewNode() {
        if (connections.isEmpty()) {
            return Optional.empty();
        }
        ConnectionGene con = connections.randomElement(ConnectionGene::isEnabled);
        if (con == null) {
            return Optional.empty();
        }
        return addNewNodeWithCons(con);
    }

    public void addNode(NodeGene node) {
        nodes.add(node);
    }

    /**
     * @param con con
     * @return Empty Optional if Node and both connections already existed. Wrapper with every newly added Object.
     * Everything that wasn't newly added is null. The left connection has the weight 1 and is enabled. The second
     * connection hast the weight of the input connection but is always enabled
     */
    public Optional<NodeConnectionWrapper> addNewNodeWithCons(@NotNull ConnectionGene con) {
        assert con.isEnabled();

        NodeGene leftNode = con.getFrom();
        NodeGene rightNode = con.getTo();

        assert leftNode != null;
        assert rightNode != null;

        con.setEnabled(false);

        Optional<NodeGene> optionalNode = pool.getNodeBetween(leftNode, rightNode);
        NodeGene node;
        if (optionalNode.isPresent()) {
            node = optionalNode.get();
            /* Its still possible that the connections are missing since the nodes could have been inherited but not
            the connections */
        } else {
            node = new NodeGene(pool.nextNodeId(),
                    Utils.getX(leftNode.getX(), rightNode.getX()),
                    Utils.getY(leftNode.getY(), rightNode.getY()));
            pool.putNodeBetween(leftNode, rightNode, node);
        }

        ConnectionGene wrapperLeftCon = addNewConnection(leftNode, node, 1d).orElse(null);
        NodeGene wrapperNode = nodes.add(node) ? node : null;
        ConnectionGene wrapperRightCon = addNewConnection(node, rightNode, con.getWeight()).orElse(null);
        if (wrapperLeftCon == null && wrapperNode == null && wrapperRightCon == null) {
            return Optional.empty();
        } else {
            return Optional.of(new NodeConnectionWrapper(wrapperLeftCon, wrapperNode, wrapperRightCon));
        }
    }

    public void mutateWeights() {
        for (ConnectionGene con : connections) {
            if (con.isEnabled()) {
                if (pool.getSettings().getEvolutionSettings().getProbabilityMutateWeightRandom() > Math.random()) {
                    mutateWeightRandom(con);
                } else if (pool.getSettings().getEvolutionSettings().getProbabilityMutateWeightShift() > Math.random()) {
                    mutateWeightShift(con);
                }
            }
        }
    }

    public void mutateWeightShift(@NotNull ConnectionGene con) {
        con.setWeight(Utils.shiftWeigh(con.getWeight(), pool.getSettings().getEvolutionSettings().getWeightShiftStrength()));
    }

    public void mutateWeightRandom(@NotNull ConnectionGene con) {
        con.setWeight(Utils.randomWeight(pool.getSettings().getEvolutionSettings().getWeightRandomStrength()));
    }

    public void mutateConnectionToggle() {
        assert !connections.isEmpty();
        ConnectionGene con = connections.randomElement();
        con.setEnabled(!con.isEnabled());
    }

    public boolean mutateConnectionActivate() {
        assert !connections.isEmpty();
        ConnectionGene con = connections.randomElement(connectionGene -> !connectionGene.isEnabled());
        if (con == null) {
            return false;
        } else {
            con.setEnabled(true);
            return true;
        }
    }

    public boolean mutateConnectionDeactivate() {
        assert !connections.isEmpty();
        ConnectionGene con = connections.randomElement(ConnectionGene::isEnabled);
        if (con == null) {
            return false;
        } else {
            con.setEnabled(false);
            return true;
        }
    }

    public int getHighestInnovationGeneId() {
        return connections.isEmpty() ? 0 : connections.get(connections.size() - 1).getId();
    }

    public ConnectionGene getConnection(int index) {
        return connections.get(index);
    }
    public int complexity() {
        int connectionComplexity = (int) connections.stream().filter(ConnectionGene::isEnabled).count();
        return connectionComplexity + nodes.size() - pool.getInputNodes().size() - pool.getOutputNodes().size() - 1;
    }

    public OrderedSet<ConnectionGene> getConnections() {
        return connections;
    }

    public OrderedSet<NodeGene> getNodes() {
        return nodes;
    }

    public Pool getPool() {
        return pool;
    }
}
