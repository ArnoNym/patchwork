package ai.anneat.models.generation;

import ai.anneat.controller.EvolutionController;
import ai.anneat.models.genome.Genome;
import ai.anneat.models.genome.Pool;
import ai.anneat.publish.settings.Settings;
import ai.anneat.main.utils.collections.OrderedSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Generation implements Serializable {
    private int number;

    private double bestScore = -Double.MAX_VALUE;

    private final OrderedSet<Species> speciesOrderedSet;

    public Generation(int number, OrderedSet<Species> speciesOrderedSet) {
        this.number = number;
        this.speciesOrderedSet = speciesOrderedSet;
    }

    public static Generation init(Pool pool) {
        Generation generation = new Generation(1, new OrderedSet<>());

        // Init Species

        List<Agent> agentSet = new ArrayList<>();

        Genome musterGenome = new Genome(pool);
        musterGenome.createAlInputToOutputNodesConnections();
        Agent musterAgent = new Agent(musterGenome);
        agentSet.add(musterAgent);

        for (int i = 1; i < pool.getSettings().getPerformanceSettings().getMaxAgentCount(); i++) {
            Genome genome = new Genome(pool, musterGenome.getConnections());
            genome.mutateWeights();
            Agent agent = new Agent(genome);
            agentSet.add(agent);
        }

        generation.speciesOrderedSet.add(new Species(agentSet));

        EvolutionController.redistributeAgentsInSpecies(generation.speciesOrderedSet);

        return generation;
    }

    public Set<Agent> getClients() {
        Set<Agent> agents = new HashSet<>();
        for (Species species : speciesOrderedSet) {
            agents.addAll(species.getAgents());
        }
        return agents;
    }

    public List<Agent> getAgentList() {
        List<Agent> agents = new ArrayList<>();
        for (Species species : speciesOrderedSet) {
            agents.addAll(species.getAgents());
        }
        return agents;
    }

    public Settings getSettings() {
        if (speciesOrderedSet.isEmpty()) {
            throw new IllegalStateException();
        }
        Species species = speciesOrderedSet.get(0);
        if (species.isEmpty()) {
            throw new IllegalStateException();
        }
        return species.getAgents().get(0).getGenome().getPool().getSettings();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int incrementNumber() {
        return this.number++;
    }

    public OrderedSet<Species> getOrderedSet() {
        return speciesOrderedSet;
    }

    public double getBestScore() {
        return bestScore;
    }

    public void setBestScore(double bestScore) {
        this.bestScore = bestScore;
    }
}
