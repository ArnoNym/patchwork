package ai.anneat.models.generation;

import ai.anneat.models.calculation.Calculator;
import ai.anneat.models.genome.Genome;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class Agent implements Comparable<Agent>, Serializable {

    private final Genome genome;
    private transient Calculator calculator;

    private double score; // The score that gets modified and used for basically all anneat.calculations
    private double vanillaScore; // The score that's (indirectly) set by the user of the framework

    private boolean allowKill = true;
    private boolean allowMutate = true;

    public Agent(Genome genome) {
        this.genome = genome;
    }

    public double[] calculate(double... input) {
        return getCalculator().calculate(input);
    }

    public void mutate(boolean bigSpecies) {
        if (!allowMutate) {
            throw new IllegalStateException();
        }
        genome.mutate(bigSpecies);
        calculator = null;
    }

    public double distance(Agent other) {
        return genome.distance(other.getGenome());
    }

    public int complexity() {
        return genome.complexity();
    }

    @Override
    public int compareTo(@NotNull Agent o) {
        int c = Double.compare(vanillaScore, o.vanillaScore);
        if (c != 0) {
            return c;
        }
        return Integer.compare(o.complexity(), complexity());
    }

    public Genome getGenome() {
        return genome;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        if (score < 0) throw new IllegalArgumentException();
        this.score = score;
    }

    public void setVanillaScore(double vanillaScore) {
        if (vanillaScore < 0) throw new IllegalArgumentException();
        this.vanillaScore = vanillaScore;
    }

    public void setCalculator(Calculator calculator) {
        this.calculator = calculator;
    }

    public void setAllowKill(boolean allowKill) {
        this.allowKill = allowKill;
    }

    public void setAllowMutate(boolean allowMutate) {
        this.allowMutate = allowMutate;
    }

    public Calculator getCalculator() {
        if (calculator == null) calculator = new Calculator(genome);
        return calculator;
    }

    public boolean isMutated() {
        return calculator == null;
    }

    public double getVanillaScore() {
        return vanillaScore;
    }

    public boolean isAllowKill() {
        return allowKill;
    }

    public boolean isAllowMutate() {
        return allowMutate;
    }
}
