package ai.anneat.models.generation;

import ai.anneat.main.constants.Constants;
import ai.anneat.publish.task.Task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ScoreRunnable implements Runnable {
    private final Collection<Agent> agents;
    private final Task task;
    private final Double targetScore;

    private final Set<Agent> reachedTargetScoreAgents = new HashSet<>();
    private double bestReachedScore = -Double.MAX_VALUE;

    public ScoreRunnable(Collection<Agent> agents, Task task, Double targetScore) {
        this.agents = agents;
        this.task = task;
        this.targetScore = targetScore;
    }

    @Override
    public void run() {
        for (Agent agent : agents) {
            double score = Math.round(task.calculateScore(agent) * Constants.SCORE_ROUND_DIGITS) / (double) Constants.SCORE_ROUND_DIGITS;
            agent.setVanillaScore(score);
            agent.setScore(score);
            if (bestReachedScore < score) {
                bestReachedScore = score;
            }
            if (targetScore != null && score >= targetScore) {
                reachedTargetScoreAgents.add(agent);
            }
        }
    }

    public double getBestReachedScore() {
        return bestReachedScore;
    }

    public Set<Agent> getReachedTargetScoreAgents() {
        return reachedTargetScoreAgents;
    }
}

