package ai.anneat.models.generation;

import ai.anneat.main.utils.collections.OrderedSet;
import ai.anneat.models.genome.Genome;
import ai.anneat.main.utils.Utils;
import ai.anneat.publish.settings.EvolutionSettings;
import ai.anneat.publish.settings.Settings;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Species implements Set<Agent>, Serializable {
    private final Settings settings;

    private final OrderedSet<Agent> agents;
    private Agent representative;

    private double score;
    private double allTimeAgentMaxVanillaScore = -Double.MAX_VALUE;
    private int scoreDidntIncreaseCounter = 0;

    public Species(Agent representative) {
        this.agents = new OrderedSet<>();
        this.agents.add(representative);
        this.representative = representative;
        this.settings = representative.getGenome().getPool().getSettings();
    }

    public Species(List<Agent> agents) {
        if (agents.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.agents = new OrderedSet<>(agents);
        this.representative = Utils.randomElement(agents);
        this.settings = representative.getGenome().getPool().getSettings();
    }

    public void reduceToRepresentative() {
        Agent newRepresentative = agents.stream().max(Comparator.comparingDouble(Agent::getVanillaScore)).orElseThrow();
        //assert newRepresentative == agents.stream().max(Comparator.comparingDouble(Agent::getScore)).orElseThrow();
        //Agent newRepresentative = agents.randomElement();
        agents.clear();
        representative = newRepresentative;
        agents.add(newRepresentative);
    }

    public boolean add(@NotNull Agent agent) {
        double distance = agent.distance(representative);
        if (distance < representative.getGenome().getPool().getSettings().getEvolutionSettings().getCp()) {
            if (!agents.add(agent)) {
                throw new IllegalArgumentException();
            }
            return true;
        }
        return false;
    }

    public void forceAdd(@NotNull Agent agent) {
        if (!agents.add(agent)) {
            throw new IllegalArgumentException();
        }
    }

    public void calculateScore(boolean increaseCounter) {
        score = EvolutionSettings.getSpeciesScoreFunction().apply(agents);

        double allTimeAgentMaxVanillaScore = representative.getVanillaScore();
        //assert !agents.isEmpty();
        //assert allTimeAgentMaxVanillaScore == agents.stream().mapToDouble(Agent::getVanillaScore).max().orElseThrow();
        if (allTimeAgentMaxVanillaScore > this.allTimeAgentMaxVanillaScore) {
            this.allTimeAgentMaxVanillaScore = allTimeAgentMaxVanillaScore;
            scoreDidntIncreaseCounter = 0;
        } else {
            if (increaseCounter) {
                scoreDidntIncreaseCounter++;
            }
        }
    }

    /**
     * Tries to kill as many agents as specified with the percentage.
     * If more than one agent is present one Agent will always survive.
     * Does not kill agents that do not allowKill.
     *
     * @param percentage of worst agents to kill
     */
    public void kill(double percentage) {
        assert percentage >= 0 && percentage <= 1;

        if (isEmpty()) {
            return;
        }

        agents.sort(Comparator.comparingDouble(agent -> -agent.getVanillaScore()));

        List<Agent> killAgents = agents.stream()
                .filter(Agent::isAllowKill)
                .collect(Collectors.toList());

        if (killAgents.isEmpty()) {
            return;
        }

        int amount = Math.min(agents.size(), (int) Math.ceil(percentage * agents.size()));

        int index = Math.max(Math.min(1, agents.size() - 1), killAgents.size() - amount);
        killAgents = killAgents.subList(index, killAgents.size());

        if (agents.get(0).getGenome().getPool().getSettings().getPerformanceSettings().isPrint()) {
            System.out.println("Species ::: Killed " + killAgents.size() + " of " + agents.size() + " Agents!");
        }

        removeAll(killAgents);

        /*
        System.out.println("Killed Agent: ");
        for (Agent killedAgent : killAgents) {
            System.out.println(killedAgent);
        }
        System.out.println("\n");
        */
    }

    public Agent remove(int i) {
        Agent agent = agents.get(i);
        remove(agents.get(i));
        return agent;
    }

    public boolean remove(Agent agent) {
        boolean removed = agents.remove(agent);
        if (agents.isEmpty()) {
            representative = null;
        } else {
            representative = agents.randomElement();
        }
        return removed;
    }

    @Override
    public boolean removeAll(Collection<?> removeAgents) {
        if (removeAgents.contains(representative)) {
            setRepresentative(agents.randomElement(agent -> !removeAgents.contains(agent)));
        }
        return agents.removeAll(removeAgents);
    }

    public boolean isAllowedToBreed() {
        return scoreDidntIncreaseCounter <= settings.getEvolutionSettings().getAllowGenerationsWithoutIncreaseInValue();
    }

    public Set<Agent> breed(int offspringCount) {
        double singleParentPercent = settings.getEvolutionSettings().getOffspringWithOnlyOneParentPercent();
        boolean bigSpecies = isBigSpecies();
        if (agents.isEmpty()) {
            throw new IllegalStateException();
        }
        Set<Agent> offspring = new HashSet<>();
        if (agents.size() == 1) {
            for (int i = 0; i < offspringCount; i++) {
                offspring.add(breedWithOneParent(bigSpecies));
            }
        } else {
            for (int i = 0; i < offspringCount; i++) {
                if (singleParentPercent > Math.random()) {
                    offspring.add(breedWithOneParent(bigSpecies));
                } else {
                    offspring.add(breedWithTwoParents());
                }
            }
        }
        return offspring;
    }
    
    public Agent breedWithTwoParents() {
        if (agents.size() < 2) {
            throw new IllegalStateException();
        }

        Map.Entry<Agent, Agent> entry = agents.randomElements(settings.getEvolutionSettings().getBestMayReproducePercent()); // todo einen Selector einfuehren, der die ebsten bevorzugt
        Agent c1 = entry.getKey();
        Agent c2 = entry.getValue();

        if (c1.getScore() > c2.getScore()) {
            return new Agent(Genome.crossOver(c1.getGenome(), c2.getGenome()));
        } else {
            return new Agent(Genome.crossOver(c2.getGenome(), c1.getGenome()));
        }
    }

    public boolean isBigSpecies() {
        return size() >= settings.getEvolutionSettings().getBigSpeciesOfTotalPopRequired();
    }

    public Agent breedWithOneParent(boolean bigSpecies) {
        Agent parent = agents.randomElement();
        Agent offspring = new Agent(parent.getGenome().copyWithNewObjects());
        offspring.mutate(bigSpecies);
        return offspring;
    }

    public void mutate() {
        agents.sort(Comparator.comparingDouble(agent -> -agent.getVanillaScore()));
        assert agents.size() < 2 || agents.get(0).getVanillaScore() >= agents.get(1).getVanillaScore();

        List<Agent> mutateAgents = agents.stream().filter(Agent::isAllowMutate).collect(Collectors.toList());

        if (mutateAgents.isEmpty()) {
            return;
        }

        int from = Math.min(mutateAgents.size() - 1, settings.getEvolutionSettings().getSpeciesElitism() + 1);
        mutateAgents = mutateAgents.subList(from, mutateAgents.size());

        boolean bigSpecies = isBigSpecies();
        for (Agent c : mutateAgents) {
            c.mutate(bigSpecies);
        }
    }

    public Agent getBestAgent() {
        return agents.stream().max(Comparator.comparingDouble(Agent::getScore)).orElseThrow();
    }

    public void setRepresentative(Agent representative) {
        if (representative != null && !agents.contains(representative)) {
            agents.add(representative);
        }
        this.representative = representative;
    }

    public int size() {
        return agents.size();
    }

    public boolean isEmpty() {
        return agents.isEmpty();
    }

    public Agent getRepresentative() {
        return representative;
    }

    public double getScore() {
        return score;
    }

    public OrderedSet<Agent> getAgents() {
        return agents;
    }

    public int getScoreDidntIncreaseCounter() {
        return scoreDidntIncreaseCounter;
    }

    public double getAllTimeAgentMaxVanillaScore() {
        return allTimeAgentMaxVanillaScore;
    }

    @Override
    public boolean contains(Object o) {
        return agents.contains(o);
    }

    @NotNull
    @Override
    public Iterator<Agent> iterator() {
        return agents.iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return agents.toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] ts) {
        return agents.toArray(ts);
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof Agent) {
            return remove((Agent) o);
        } else {
            return false;
        }
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> collection) {
        return agents.containsAll(collection);
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends Agent> collection) {
        boolean ret = agents.addAll(collection);
        if (representative == null) {
            representative = agents.randomElement();
        }
        return ret;
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> collection) {
        throw new IllegalStateException();
    }

    @Override
    public void clear() {
        agents.clear();
        representative = null;
    }
}
