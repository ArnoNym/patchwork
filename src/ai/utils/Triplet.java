package ai.utils;

import model.Blanket;
import model.GameState;
import model.PatchTuple;

public class Triplet {
    private final Blanket blanket;
    private final PatchTuple patchTuple;
    private final boolean extraTupleReached;

    public Triplet(Blanket blanket, PatchTuple patchTuple, boolean extraTupleReached) {
        this.blanket = blanket;
        this.patchTuple = patchTuple;
        this.extraTupleReached = extraTupleReached;
    }

    public Blanket getBlanket() {
        return blanket;
    }

    public PatchTuple getPatchTuple() {
        return patchTuple;
    }

    public boolean isExtraTupleReached() {
        return extraTupleReached;
    }
}
