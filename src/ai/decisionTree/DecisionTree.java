package ai.decisionTree;

import ai.agents.GameStateAgent;
import ai.agents.NeatPatchworkAgent;
import ai.AiConstants;
import ai.anneat.main.utils.collections.Tuple;
import ai.training.utils.DecoderUtils;
import ai.training.utils.TrainingUtils;
import controller.GameController;
import controller.PlayerController;
import model.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * The DecisionTree tries to look into the future and predict the best move
 * based on future GameStates.
 * @author Florian
 */
public class DecisionTree {
    private final GameStateAgent agent;
    private final GameState initGameState;
    public final String callerName;
    private final GameController gameController;
    /**
     * indices:
     * 0 = Pass
     * 1 = BuyFirst
     * 2 = BuySecond
     * 3 = BuyThird
     */
    private final Node[] rootNodes;

    /**
     * Create a new DecisionTree with the first iteration of decisions
     * @param agent The agent, that should evaluate the next move
     * @param gameState The State, which the next move should be taken in. Gets destroyed
     * @author Florian
     */
    public DecisionTree(GameStateAgent agent, GameState gameState) {
        this.agent = agent;
        this.initGameState = gameState;
        this.callerName = gameState.getActivePlayer().getName();
        this.gameController = new GameController(initGameState);

        Node parent = new Node(null, null, false,0,callerName);
        createChildren(initGameState, parent);
        this.rootNodes = parent.getChildren();
    }

    // Node Creation ===================================================================================================

    /**
     * Calculates the next possible moves given a gameState
     * @param protectedGameState the gameState the next moves should be calculated off of
     * @param move the previous move, right before the calculated moves
     * @author Florian
     */
    private void createChildren(GameState protectedGameState, Node move){
        Set<Node> nodes = new HashSet<>();
        nodes.add(createNode(protectedGameState.clone(), Move.PASS).orElseThrow());
        createNode(protectedGameState.clone(), Move.BUY_FIRST).ifPresent(nodes::add);
        createNode(protectedGameState.clone(), Move.BUY_SECOND).ifPresent(nodes::add);
        createNode(protectedGameState.clone(), Move.BUY_THIRD).ifPresent(nodes::add);
        move.addChildren(nodes.toArray(new Node[0]));
    }

    /**
     * @param destroyGameState will stay intact
     * @author Leon
     */
    @NotNull
    private Optional<Node> createNode(@NotNull GameState destroyGameState, @NotNull Move move) {
        gameController.getGame().setGameState(destroyGameState);
        GameState gameState = agent.executeMove(gameController, move).map(Tuple::getV1).orElse(null);

        if (gameState == null) {
            return Optional.empty();
        }

        double rating;

        if (gameState.isFinished()) {
            Player winner = PlayerController.calculateWinner(gameState);
            if (winner == null) {
                rating = 0.5;
            } else {
                rating = winner.getName().equals(callerName) ? 1 : 0; // TODO Vielleicht ist eine 1 zu viel
            }
        } else {
            rating = agent.rate(gameState,callerName);
        }

        return Optional.of(new Node(move, gameState, gameState.isFinished(), rating, callerName));
    }

    // Compute =========================================================================================================

    /**
     * Compute calculates the accurate ratings for each possible move within the give constraints.
     * @param max_depth The nax searching depth of the decisionTree. After reaching this depth, evaluation of the subtree halts.
     *                  If this is 0, the max_depth is unbound and not taken into consideration.
     * @param max_time Timebound in seconds. After reaching this timebound, calcualation stops.
     * @author Florian
     */
    public void compute(int max_depth, float max_time) {
        //if (1==1) return; // TODO dsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

        long endTime = System.currentTimeMillis() + (long) max_time * 1000;

        boolean progress = true;
        int iteration = 0;
        String out = "";
        try {
            while (System.currentTimeMillis() < endTime && progress) {
                progress = computeNextIteration(max_depth);
                iteration++;
            }
            out = progress ? "Stopped manually." : "Out of time.";
        } catch (StackOverflowError e) {
            out = "StackOverflow... Ran out of memory";
        }

        if (AiConstants.PRINT_DECISION_TREE) {
            System.out.println("-------------------------DecisionTree STATS-------------------------");
            System.out.println("Steps calculated: " + iteration + ", Reason for stopping: " + out);
            Node deepest = rootNodes[0];
            Node flattest = rootNodes[0];
            double total = 0;
            for(Node move : rootNodes){
                if(move.depth() > deepest.depth())  deepest = move;
                if(move.depth() < flattest.depth())  flattest = move;
                total += move.getRating();
            }
            double deepProb = 0;
            double flatProb = 0;
            if(total != 0) {
                deepProb = (deepest.getRating() / total) * 100;
                flatProb = (flattest.getRating() / total) * 100;
            }
            System.out.println("Deepest Search: " + deepest.depth() + " layers. Properties => (Rating = " + deepest.getRating() + ", Move = " + deepest.getMove() + ", Probability: " + deepProb + "%, Fully evaluated: " + deepest.isFinished() + ")");
            System.out.println("Flattest search " + flattest.depth() + " layers. Properties => (Rating = " + flattest.getRating() + ", Move = " + flattest.getMove() + ", Probability: " + flatProb + "%, Fully evaluated: " + flattest.isFinished() + ")");
            System.out.println("--------------------------------------------------------------------");
        }
    }

    /**
     * Calculate the next iteration of moves and update the Nodes accordingly
     * @param max_depth The max searching depth of the decisionTree. After reaching this depth, evaluation of the subtree halts.
     *                  If this is 0, the max_depth is unbound and not taken into consideration.
     * @return true if there is progress being made by iterating further
     * @author Florian
     */
    private boolean computeNextIteration(int max_depth){
        // Orders the moves according to their ratings (weighted by their certainty) in descending order
        Arrays.sort(rootNodes);

        // All subTrees lead to us loosing
        if (Arrays.stream(rootNodes).allMatch(move -> move.getRating() == 0)) {
            if (AiConstants.PRINT_DECISION_TREE) {
                System.out.println("LOST! Stop evaluating...");
            }
            return false;
        }

        for (Node node : rootNodes) {
            // One subTree leads to us winning 100%
            if (node.isFinished()) {
                if (AiConstants.PRINT_DECISION_TREE) {
                    System.out.println("WON! Stop evaluating...");
                }
                return false;
            }

            if (!node.fullyEvaluated(max_depth)) {
                evaluateNextStep(node);
                return true;
            }
        }

        return false;
    }

    /**
     * Calculate the next step and improve Rating accuracy
     * @param node The current node in the game
     * @author Florian
     */
    private void evaluateNextStep(Node node) {
        GameState resultingGameState = node.getGameState();

        // This subTree doesn't need any further evaluation
        if (node.getRating() == 0 || node.getRating() == 1) {
            return;
        }

        if (node.isLeaf()) {
            // Calculate node after THIS node

            createChildren(resultingGameState,node);
        } else {
            //Calculate later node and update this one

            Node[] chosenMoves = node.getChildren();

            Arrays.sort(chosenMoves);
            assert chosenMoves.length == 1 || chosenMoves[0].getRatingModifiedByDepth() >= chosenMoves[1].getRatingModifiedByDepth()
                    : "chosenMoves.length="+chosenMoves.length+", chosenMoves[0]="+chosenMoves[0].getRatingModifiedByDepth()+", chosenMoves[1]="+chosenMoves[1].getRatingModifiedByDepth();

            boolean done = false;
            for (Node chosenMove : chosenMoves) {
                if (!chosenMove.isFinished()) {
                    evaluateNextStep(chosenMove);
                    done = true;
                    break;
                }
            }
            if (!done) {
                return;
            }
        }

        // Update the ratings of this node and adjusting it according to its children
        node.updateRatings();
    }

    /**
     * @return The Turn that gives us the highest chance of winning
     * @author Florian
     */
    public Turn getBestTurn() {
        Node moveNode = null;
        double maxRating = -Double.MAX_VALUE;
        for (Node node : rootNodes) {
            if (node.getRating() > maxRating) {
                maxRating = node.getRating();
                moveNode = node;
            }
        }

        assert moveNode != null;

        gameController.getGame().setGameState(initGameState.clone());
        return agent.executeMove(gameController, moveNode.getMove()).orElseThrow().getV2();
    }
}
