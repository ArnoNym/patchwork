package ai.decisionTree;

public class MaxDepthReachedException extends RuntimeException{
    public MaxDepthReachedException(){
        super();
    }
    public MaxDepthReachedException(String message){
        super(message);
    }
    public MaxDepthReachedException(String message, Throwable cause){
        super(message, cause);
    }
    public MaxDepthReachedException(Throwable cause){
        super(cause);
    }
}
