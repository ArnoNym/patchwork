package ai.decisionTree;

import model.PatchTuple;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Way to communicate what a player did during a turn.
 *
 * @author Leon
 */
public class Turn {
    private final PatchTuple buyAndPlacePatchTuple;
    private final PatchTuple extraPatchTuple;

    /**
     * Creates a Turn object to communicate what a player did during his turn.
     * The turn does not get Executed by calling this method!
     *
     * @param extraPatch if during the pass Action the player reached an extraPatch
     * @return A Turn Object that contains the parameters
     * @author Leon
     */
    public static Turn createPass(@Nullable PatchTuple extraPatch) {
        return new Turn(null, extraPatch);
    }

    /**
     * Creates a Turn object to communicate what a player did during his turn.
     * The turn does not get Executed by calling this method!
     *
     * @param buyAndPlacePatchTuple the Tuple the player decided to buy and place
     * @param extraPatch if the player reached an extraPatch
     * @return A Turn Object that contains the parameters
     * @author Leon
     */
    public static Turn create(@Nullable PatchTuple buyAndPlacePatchTuple, @Nullable PatchTuple extraPatch) {
        return new Turn(buyAndPlacePatchTuple, extraPatch);
    }

    /**
     * @author Leon
     */
    private Turn(@Nullable PatchTuple buyAndPlacePatchTuple, @Nullable PatchTuple extraPatchTuple) {
        this.buyAndPlacePatchTuple = buyAndPlacePatchTuple;
        this.extraPatchTuple = extraPatchTuple;
    }

    /**
     * @return true if during this turn the player passed, else false
     * @author Leon
     */
    public boolean isPass() {
        return buyAndPlacePatchTuple == null;
    }

    /**
     * @return true if an extraToken was placed during the turn, else false
     * @author Leon
     */
    public boolean hasExtraPatch() {
        return extraPatchTuple != null;
    }

    /**
     * @return the buyPatchTuple
     * @author Leon
     */
    public PatchTuple getBuyAndPlacePatchTuple() {
        return buyAndPlacePatchTuple;
    }

    /**
     * @return the extraPatchTuple
     * @author Leon
     */
    public PatchTuple getExtraPatchTuple() {
        return extraPatchTuple;
    }

    /**
     * @author Leon
     */
    @Override
    public String toString() {
        String st = "";
        st += buyAndPlacePatchTuple;
        st += "\n";
        st += extraPatchTuple;
        return st;
    }
}
