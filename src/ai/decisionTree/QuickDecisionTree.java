package ai.decisionTree;

import ai.agents.GameStateAgent;
import ai.agents.NeatPatchworkAgent;
import ai.anneat.main.utils.collections.Tuple;
import controller.BlanketController;
import controller.GameController;
import model.GameState;
import model.Patch;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class QuickDecisionTree {
    private final GameStateAgent agent;
    private final GameState initialGameState;
    private final String playerName;
    private final GameController gameController;

    public QuickDecisionTree(GameStateAgent agent, GameState gameState) {
        this.agent = agent;
        this.initialGameState = gameState;
        this.playerName = gameState.getActivePlayer().getName();
        this.gameController = new GameController(gameState);
    }

    @NotNull
    public Turn calcTurn() {
        List<Tuple<Turn, Double>> turns = new ArrayList<>(4);
        calcTurn(Move.PASS).ifPresent(turns::add);
        calcTurn(Move.BUY_FIRST).ifPresent(turns::add);
        calcTurn(Move.BUY_SECOND).ifPresent(turns::add);
        calcTurn(Move.BUY_THIRD).ifPresent(turns::add);

        List<Tuple<Turn, Double>> bestTurns = new ArrayList<>(4);
        double bestRating = 0;
        for (Tuple<Turn, Double> tuple : turns) {
            double rating = tuple.getV2();
            if (rating == bestRating) {
                bestTurns.add(tuple);
            } else if (rating > bestRating) {
                bestRating = rating;
                bestTurns.clear();
                bestTurns.add(tuple);
            }
        }

        if (bestTurns.size() == 1) {
            return bestTurns.get(0).getV1();
        } else {
            return bestTurns.stream()
                    .min(Comparator.comparingInt(triplet -> triplet.getV2().hashCode()))
                    .orElseThrow()
                    .getV1();
        }
    }

    public Optional<Tuple<Turn, Double>> calcTurn(Move move) {
        gameController.getGame().setGameState(initialGameState.clone());

        Turn turn;
        if (move.equals(Move.PASS)) {
            turn = agent.computePass(gameController);
        } else {
            Patch patch;
            switch (move) {
                case BUY_FIRST:
                    patch = gameController.getGameBoardController().getAvailablePatches().get(0);
                    break;
                case BUY_SECOND:
                    patch = gameController.getGameBoardController().getAvailablePatches().get(1);
                    break;
                case BUY_THIRD:
                    patch = gameController.getGameBoardController().getAvailablePatches().get(2);
                    break;
                default:
                    throw new IllegalStateException();
            }
            if (gameController.getPlayerController().canBuyPatch(patch)) {
                turn = agent.computeBuyAndPlacePatchTurn(gameController, patch);
            } else {
                turn = null;
            }
            if (turn == null) {
                return Optional.empty();
            }
        }

        double rating = agent.rate(gameController.getGame().getGameState(), playerName);
        return Optional.of(new Tuple<>(turn, rating));
    }
}

