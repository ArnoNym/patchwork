package ai.decisionTree;

import ai.anneat.main.utils.collections.Tuple;
import model.GameState;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class Node implements Comparable<Node> {

    private final Move move;
    private final GameState gameState;
    private boolean finished;
    private double rating;
    private final String callerName;
    /**
     * indices:
     * 0 = Pass
     * 1 = BuyFirst
     * 2 = BuySecond
     * 3 = BuyThird
     */
    private Node[] children;

    public Node(Move move, GameState gameState, boolean finished, double rating, String callerName) {
        this.move = move;
        this.gameState = gameState;
        this.finished = finished;
        this.rating = rating;
        assert !Double.isNaN(rating);

        this.callerName = callerName;
        children = new Node[4];
    }


    /**
     * A Node can either have for children or none. Therefore
     * checking for one children to exists is sufficent
     * @return true if the Node has no children
     * @author Florian
     */
    public boolean isLeaf(){
        return children[0] == null;
    }

    /**
     * A Node can either have for children or none.
     * @param children the children of the current Node
     * @throws IllegalArgumentException if the number of provided children does not equal 4.
     * @author Florian
     */
    public void addChildren(Node[] children){
        this.children = children;
    }

    /**
     * Wether or not a subTree needs further evaluation in order to be complete
     * @param max_depth The maximum depth up to which the subTree should be constructed
     * @return true if there is some Path in this subTree that still needs to be evaluated
     * @author Florian
     */
    public boolean fullyEvaluated(int max_depth) {
        //every Path has been calculated
        try {
            return finished(max_depth, 0);
        }
        //max_depth is reached
        catch (MaxDepthReachedException e) {
            finished = true;
            return true;
        }
    }

    /**
     * Wether or not a subTree needs further evaluation in order to be complete
     * @return true if there is some Path in this subTree that still needs to be evaluated
     * @author Florian
     */
    private boolean finished(int maxDepth, int currentDepth) throws MaxDepthReachedException {
        // Max depth reached.. Stop evaluating this branch
        if (maxDepth > 0 && (maxDepth <= currentDepth)) {
            throw new MaxDepthReachedException();
        }

        // Node has a defined finish
        if (rating == 0 || rating == 1) {
            finished = true;
            return isFinished();
        }

        // This is the last calculated node; evaluate only if this needs further evaluation
        if (isLeaf() || finished) return isFinished();

        // Every child is evaluated. No need to continue this node
        for (Node child : children) {
            if (!child.finished(maxDepth, currentDepth + 1)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Computes the depth of the subTree
     * @return the depth of this subTree
     * @author Florian
     */
    public int depth() {
        if (isLeaf()) return 0;
        return 1 + Collections.max(Arrays.stream(children).map(Node::depth).collect(Collectors.toList()));
    }

    /**
     * @author Florian, Leon
     */
    public void updateRatings() {
        if (isLeaf()) {
            throw new IllegalArgumentException("Supplied Node must not be a leaf!");
        }

        double[] ratings = new double[children.length];
        for (int i = 0, childrenLength = children.length; i < childrenLength; i++) {
            ratings[i] = children[i].getRating();
        }
        if(gameState.getActivePlayer().getName().equals(callerName)){
            this.rating = Arrays.stream(ratings).max().orElseThrow();
        }
        else{
            this.rating = Arrays.stream(ratings).average().orElseThrow();
        }

        if(rating == 0 || rating == 1){
            finished = true;
        }

    }

    @Override
    public int compareTo(Node other) {
        double diff = getRatingModifiedByDepth() - other.getRatingModifiedByDepth();
        if (diff == 0) {
            return 0;
        }
        return diff < 0 ? 1 : -1;
    }

    public double getRatingModifiedByDepth() {
        return getRating() / (depth() + 1);
    }
    
    @Override
    public String toString(){
        String move = "Move: " + getMove() + ", ";
        String rating = "Rating: " + getRating() + ", ";
        String finished = "Certain Outcome: " + isFinished() + "";

        /*TODO
        String children1 = "";
        String children2 = "";
        String children3 = "";
        String children4 = "";

        if(!isLeaf()){
            children1 = "\nChildren 1: {" + getChildren()[0].toString() + "}\n";
            children2 = "Children 2: {" + getChildren()[1].toString() + "}\n";
            children3 = "Children 3: {" + getChildren()[2].toString() + "}\n";
            children4 = "Children 4: {" + getChildren()[3].toString() + "}\n";
        }
        */
        return "";
    }

    public Move getMove() {
        return move;
    }

    public GameState getGameState(){
        return gameState;
    }

    public double getRating() {
        return rating;
    }

    public Node[] getChildren(){
        return children;
    }

    public boolean isFinished(){
        return finished;
    }
}
