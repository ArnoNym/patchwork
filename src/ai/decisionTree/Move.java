package ai.decisionTree;

public enum Move {
    PASS,
    BUY_FIRST,
    BUY_SECOND,
    BUY_THIRD
}
