package ai.hardcoded.benchmarks;

import Resources.Constants;
import ai.agents.PatchworkAgent;
import ai.decisionTree.Turn;
import controller.BlanketController;
import controller.GameController;
import controller.PlayerController;
import model.Blanket;
import model.GameState;
import model.Patch;
import model.PatchTuple;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AlwaysBuyFirstFit implements PatchworkAgent {
    @Override
    public Turn calcTurn(GameState gameState) {
        GameController gameController = new GameController(gameState);
        PlayerController playerController = gameController.getPlayerController();
        List<Patch> patches = gameController.getGameBoardController().getAvailablePatches();
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean specialPatchFound = false;
        PatchTuple patchTuple = null;

        if (gameState.getActivePlayer().getButtonCount() != 0) {
            for (Patch patch : patches) {
                if (playerController.canBuyPatch(patch)) {
                    Collection<PatchTuple> collection = BlanketController.calculatePlacements(blanket, patch);
                    if (!collection.isEmpty()) {
                        patchTuple = collection.stream().min(Comparator.comparingInt(PatchTuple::hashCode)).orElseThrow();
                        specialPatchFound = playerController.takePatchAndInsert(patchTuple, false, false);
                        break;
                    }
                }
            }
        }

        if (patchTuple == null) {
            specialPatchFound = playerController.pass();
        }

        PatchTuple specialPatchTuple = null;
        if (specialPatchFound) {
            Collection<PatchTuple> collection = BlanketController.calculatePlacements(blanket, Constants.generateSpecialPatch());
            specialPatchTuple = collection.stream().min(Comparator.comparingInt(PatchTuple::hashCode)).orElseThrow();
        }

        return Turn.create(patchTuple, specialPatchTuple);
    }
}
