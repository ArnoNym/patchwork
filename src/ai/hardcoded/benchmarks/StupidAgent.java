package ai.hardcoded.benchmarks;

import Resources.Constants;
import ai.agents.PatchworkAgent;
import ai.anneat.main.utils.collections.Tuple;
import ai.decisionTree.Turn;
import ai.training.utils.MatrixUtils;
import controller.BlanketController;
import controller.GameController;
import controller.PlayerController;
import model.*;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class StupidAgent implements PatchworkAgent {
    private double surfaceFactor = -0.1;
    private double spreadFactor = -0.1;
    private double sizeFactor = 0.5;
    private double incomeFactor = 1;
    private double costFactor = -0.1;
    private double timeFactor = -0.3;


    @Override
    public Turn calcTurn(GameState gameState) {
        GameController gameController = new GameController(gameState);
        PlayerController playerController = gameController.getPlayerController();

        Collection<Patch> availablePatches = gameController.getGameBoardController().getAvailablePatches();

        Collection<PatchTuple> patchTuples = new LinkedList<>();
        for (Patch patch : availablePatches){
            if (playerController.canBuyPatch(patch)){
                patchTuples.addAll(BlanketController.calculatePlacements(gameController.getGame().getGameState().getActivePlayer().getBlanket(), patch));
            }
        }

        boolean specialPatchFound;
        PatchTuple patchTuple = null;


        if(!patchTuples.isEmpty()){
            Tuple<PatchTuple, Double> bestChoice = bestPatchTuple(
                    gameController.getGame().getGameState().getActivePlayer().getBlanket(),
                    gameController.getGame().getGameState().getActivePlayer().getPos(),
                    patchTuples);
            Double value = bestChoice.getV2();

            //System.out.println("VALUE: " + value);

            if(value >  0){
                //System.out.println("Buy");
                patchTuple = bestChoice.getV1();
                specialPatchFound = playerController.takePatchAndInsert(patchTuple, false, false);
            }
            else{
                //System.out.println("Pass");
                patchTuple = null;
                specialPatchFound = playerController.pass();
            }
        }
        else{
            //System.out.println("Pass");
            specialPatchFound = playerController.pass();
        }


        PatchTuple specialPatchTuple = null;
        if(specialPatchFound){
            Patch specialPatch = Constants.generateSpecialPatch();
            if (BlanketController.canPlacePatch(gameController.getGame().getGameState().getActivePlayer().getBlanket(), specialPatch)){
                specialPatchTuple = bestFit(gameController.getGame().getGameState().getActivePlayer().getBlanket(), specialPatch);
            } else {
                //System.out.println(gameController.getGame().getGameState().getActivePlayer().getBlanket());
                throw new IllegalStateException("THIS CASE SHOULD NOT BE POSSIBLE!");
            }
        }

        //if (specialPatchFound) System.out.println("SpecialPatchFoundFromStupidAgent");

        //System.out.println(Turn.create(patchTuple, specialPatchTuple));
        return Turn.create(patchTuple, specialPatchTuple);
    }

    private Tuple<PatchTuple, Double> bestPatchTuple (Blanket blanket, int pos, Collection<PatchTuple> patchTuples){
        PatchTuple res = patchTuples.iterator().next();
        Double max = patchValue(blanket, pos, res).getV1();
        for (PatchTuple patchTuple : patchTuples){
            double compareVal = patchValue(blanket, pos, patchTuple).getV1();
            if (compareVal > max){
                max = compareVal;
                res = patchTuple;
            }
        }
        return new Tuple<>(res, max);
    }

    private Tuple<Double, String> patchValue(Blanket blanket, int pos, PatchTuple patchTuple){
        double surface = MatrixUtils.calcEdges(BlanketController.insertPatchTuple(blanket,patchTuple).getBoolMatrixRepresentation());
        double spread = meanAbsEuclideanDistance(BlanketController.insertPatchTuple(blanket, patchTuple));
        double size = patchTuple.getVectorList().size();
        double income = patchTuple.getPatch().getIncome();
        double cost = patchTuple.getPatch().getCost();
        double time = patchTuple.getPatch().getTime();

        int blanketSize = blanket.getPatchTuples().size();

        double surfaceSummand = surfaceFactor * surface * 1.0 / (Math.sqrt(size + blanketSize * blanketSize)); //maybe works a bit
        double spreadSummand  = spreadFactor * spread / (1 + 2 * (pos / (double) Constants.FIELDS_COUNT));   //weird normalisation, spreadSummand should be somewhere between [1.5, 2.5]
        double sizeSummand    = sizeFactor * size;
        double incomeSummand  = incomeFactor * income * (Constants.calcIncomeButtonsLeftCount(pos) / 9.0);
        double costSummand    = costFactor * cost;
        double timeSummand    = timeFactor * time;

        double val = surfaceSummand + spreadSummand + sizeSummand + incomeSummand + costSummand + timeSummand;
        String valString = "[ (" + surfaceSummand + spreadSummand + ", " + sizeSummand + ", " + incomeSummand + ", " + costSummand + ", " + timeSummand + ")\t=>\t" + val + " ], ";
        return new Tuple<>(val, valString);
    }

    public static double meanAbsEuclideanDistance(Blanket blanket){
        List<VecTwo> allVectors = new LinkedList<>();
        double res = 0;
        for (PatchTuple patchTuple : blanket.getPatchTuples()){
            allVectors.addAll(patchTuple.getVectorList());
        }
        int count = 0;
        for (VecTwo vec1 : allVectors){
            for (VecTwo vec2 : allVectors){
                res += VecTwo.euclidanDistance(vec1, vec2);
                count++;
            }
        }
        return res / count;
    }

    private static PatchTuple bestFit(Blanket blanket, Patch patch){
        Collection<PatchTuple> patchTuples = BlanketController.calculatePlacements(blanket, patch);
        return bestFit(blanket, patchTuples); //todo shuffle patchTuples?
    }

    private static PatchTuple bestFit(Blanket blanket, Collection<PatchTuple> patchTuples){
        PatchTuple res = null;
        for(PatchTuple patchTuple : patchTuples){
            res = betterFit(blanket, patchTuple, res);
        }
        return res;
    }

    private static PatchTuple betterFit(Blanket blanket, PatchTuple pt1, PatchTuple pt2){
        if (pt1 == null) return pt2;
        if (pt2 == null) return pt1;

        double aed1 = meanAbsEuclideanDistance(BlanketController.insertPatchTuple(blanket, pt1));
        double aed2 = meanAbsEuclideanDistance(BlanketController.insertPatchTuple(blanket, pt2));

        if (aed1 < aed2){
            return pt1;
        }
        else return pt2;
    }


}
