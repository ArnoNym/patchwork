package ai.hardcoded.benchmarks;

import Resources.Constants;
import ai.agents.PatchworkAgent;
import ai.decisionTree.Turn;
import controller.BlanketController;
import controller.GameController;
import controller.PlayerController;
import model.Blanket;
import model.GameState;
import model.Patch;
import model.PatchTuple;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class BuyFirstAgent implements PatchworkAgent {

    @Override
    public Turn calcTurn(GameState gameState) {
        GameController gameController = new GameController(gameState);
        PlayerController playerController = gameController.getPlayerController();

        List<Patch> patches = gameController.getGameBoardController().getAvailablePatches();
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean specialPatchFound = false;
        PatchTuple patchTuple = null;

        if (gameState.getActivePlayer().getButtonCount() != 0) {
            for (Patch patch : patches) {
                if (playerController.canBuyPatch(patch)) {
                    patchTuple = BlanketController.calculateFirstPlacement(blanket, patch);
                    if (patchTuple != null) {
                        specialPatchFound = playerController.takePatchAndInsert(patchTuple, false, false);
                        break;
                    }
                }
            }
        }

        PatchTuple specialPatchTuple = null;
        if (specialPatchFound) {
            specialPatchTuple = BlanketController.calculateFirstPlacement(blanket, Constants.generateSpecialPatch());
        }

        return Turn.create(patchTuple, specialPatchTuple);
    }
}
