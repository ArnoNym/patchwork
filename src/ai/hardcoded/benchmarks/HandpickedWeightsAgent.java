package ai.hardcoded.benchmarks;

import Resources.Constants;
import ai.agents.GameStateAgent;
import ai.agents.PatchworkAgent;
import ai.anneat.main.utils.collections.Tuple;
import ai.decisionTree.Turn;
import ai.training.utils.DecoderUtils;
import controller.GameBoardController;
import controller.GameController;
import controller.GameStateController;
import model.GameState;
import model.Patch;
import model.PatchTuple;
import model.Player;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HandpickedWeightsAgent implements PatchworkAgent {
    @Override
    public Turn calcTurn(GameState gameState) {
        return null;
    }
    /*
    private final GameStateAgent gameStateAgent;

    public HandpickedWeightsAgent(GameStateAgent gameStateAgent) {
        this.gameStateAgent = gameStateAgent;
    }

    @Override
    public Turn calcTurn(GameState gameState) {
        Set<Tuple<Double, Turn>> tuples = new HashSet<>();

        System.out.println();
        System.out.println("################# NEW TURN #######################");

        tuples.add(pass(gameState.clone()));

        List<Patch> availablePatches = GameBoardController.getAvailablePatches(gameState.getGameBoard());
        if (availablePatches.size() > 0) {
            Tuple<Double, Turn> tuple = place(gameState.clone(), availablePatches.get(0));
            if (tuple != null) {
                tuples.add(tuple);
            }
        }
        if (availablePatches.size() > 1) {
            Tuple<Double, Turn> tuple = place(gameState.clone(), availablePatches.get(1));
            if (tuple != null) {
                tuples.add(tuple);
            }
        }
        if (availablePatches.size() > 2) {
            Tuple<Double, Turn> tuple = place(gameState.clone(), availablePatches.get(2));
            if (tuple != null) {
                tuples.add(tuple);
            }
        }

        return tuples.stream().max(Comparator.comparingDouble(Tuple::getV1)).orElseThrow().getV2();
    }

    public Tuple<Double, Turn> pass(GameState gameState) {
        String name = gameState.getActivePlayer().getName();
        GameController gameController = new GameController(gameState);
        Turn pass;
        if (gameController.getPlayerController().pass()) {
            pass = Turn.createPass(gameStateAgent.calcBestExtraPatchTuple(gameController.getGameStateController().getPlayerByName(name).getBlanket()));
        } else {
            pass = Turn.createPass(null);
        }
        Player player = gameController.getGameStateController().getPlayerByName(name);
        Player enemyPlayer = GameStateController.getOtherPlayerName(gameState, name);
        System.out.println();
        System.out.println("Score for Pass:");
        System.out.println(player.getBlanket().toString());
        return new Tuple<>(score(player, enemyPlayer), pass);
    }

    public Tuple<Double, Turn> place(GameState gameState, Patch patch) {
        String name = gameState.getActivePlayer().getName();
        GameController gameController = new GameController(gameState);

        if (!gameController.getPlayerController().canBuyPatch(patch)) {
            return null;
        }

        PatchTuple patchTuple = gameStateAgent.calcBestPatchTuple(gameController.getGameStateController().getPlayerByName(name).getBlanket(), patch);
        if (patchTuple == null) {
            return null;
        }

        boolean extraPatch = gameController.getPlayerController().takePatchAndInsert(patchTuple);
        PatchTuple extraPatchTuple = null;
        if (extraPatch) {
            extraPatchTuple = gameStateAgent.calcBestPatchTuple(gameController.getGameStateController().getPlayerByName(name).getBlanket(), Constants.generateSpecialPatch());
            gameController.getPlayerController().takePatchAndInsert(extraPatchTuple);
        }

        Player player = gameController.getGameStateController().getPlayerByName(name);
        Player enemyPlayer = GameStateController.getOtherPlayerName(gameState, name);
        System.out.println();
        System.out.println("Score for Patch:");
        System.out.println(player.getBlanket().toString());
        return new Tuple<>(score(player, enemyPlayer), Turn.create(patchTuple, extraPatchTuple));
    }

    public double score(Player player, Player enemyPlayer) {
        boolean[][] matrix = player.getBlanket().getBoolMatrixRepresentation();

        double incomeScore = 2 * DecoderUtils.decodeIncome(player);
        double passButtonsScore = 0.1 * DecoderUtils.decodePassButtons(player, enemyPlayer);
        double timeScore = 0.8 * DecoderUtils.decodeTime(player);
        double victoryPointsScore = 1 * DecoderUtils.decodeVictoryPoints(player);
        double buttonsScore = 0.95 * DecoderUtils.decodeButtons(player);
        double massScore = 2 * DecoderUtils.decodeMass(matrix);
        double specialPatch = 5 *  DecoderUtils.decodeSpecialPatch(player.hasSpecialToken());
        double blanketScore = 5 * DecoderUtils.decodeBlanketRating(gameStateAgent.rate(player.getBlanket()));
        double score = incomeScore + timeScore + buttonsScore + massScore + specialPatch + blanketScore;

        System.out.println("incomeScore: "+incomeScore);
        System.out.println("passButtonsScore: "+passButtonsScore);
        System.out.println("timeScore: "+timeScore);
        System.out.println("victoryPointsScore: "+victoryPointsScore);
        System.out.println("buttonsScore: "+buttonsScore);
        System.out.println("massScore: "+massScore);
        System.out.println("specialPatch: "+specialPatch);
        System.out.println("blanketScore: "+blanketScore);
        System.out.println("score: "+score);

        return score;
    }

     */
}
