package ai.hardcoded.raphael;

import controller.GameController;

public abstract class RaphaelAI {

    public abstract String toString();
    public abstract String doTurn();
    public abstract void setGameController(GameController gameController);

}
