package ai.hardcoded.raphael;

import java.util.List;

public class Util {

    public static boolean allDead(List<Thread> threads) {
        for (Thread t : threads){
            if (t.isAlive()) return false;
        }
        return true;
    }

    public static void sleep(long ms) {
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }


    public static double [] getRandomVector(int length){
        double [] res = new double [length];
        for (int i = 0; i < length; i++){
            res[i] = Math.random() * 2 - 1;
        }
        return res;
    }



}
