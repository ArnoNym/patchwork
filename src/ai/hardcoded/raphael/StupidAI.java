package ai.hardcoded.raphael;

import Resources.Constants;
import ai.anneat.main.utils.collections.Tuple;
import ai.training.utils.MatrixUtils;
import controller.*;
import model.*;

import java.util.*;

public class StupidAI extends RaphaelAI{
    private GameController gameController;

    double surfaceFactor;
    double spreadFactor;
    double sizeFactor;
    double incomeFactor;
    double costFactor;
    double timeFactor;


    public StupidAI(double surfaceFactor, double spreadFactor, double sizeFactor, double incomeFactor, double costFactor, double timeFactor){
        this.surfaceFactor = surfaceFactor;
        this.spreadFactor = spreadFactor;
        this.sizeFactor = sizeFactor;
        this.incomeFactor = incomeFactor;
        this.costFactor = costFactor;
        this.timeFactor = timeFactor;
    }

    public StupidAI(GameController gameController){
        this.gameController = gameController;
        surfaceFactor = -0.1;
        spreadFactor = -0.4;
        sizeFactor = 1;
        incomeFactor = 0.9;
        costFactor = -0.3;
        timeFactor = -0.3;
    }

    private double [] vectest = {
            -1, -0.95, -0.9, -0.85, -0.8, -0.75, -0.7, -0.65, -0.6, -0.55, -0.5, -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, 0,
            0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1
    };

    public StupidAI(){
        this.surfaceFactor = getRandom(vectest);
        this.spreadFactor = getRandom(vectest);
        this.sizeFactor = getRandom(vectest);
        this.incomeFactor = getRandom(vectest);
        this.costFactor = getRandom(vectest);
        this.timeFactor = getRandom(vectest);
    }


    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    public String toString(){
        String res = "StupidAI - [";
        res += surfaceFactor + ", ";
        res += spreadFactor + ", ";
        res += sizeFactor + ", ";
        res += incomeFactor + ", ";
        res += costFactor + ", ";
        res += timeFactor;

        return res + "]";
    }

    public String doTurn(){
        String str = ""; //todo

        Collection<Patch> availablePatches = gameController.getGameBoardController().getAvailablePatches();

        PlayerController playerController = gameController.getPlayerController();

        Collection<PatchTuple> patchTuples = new LinkedList<>();
        for (Patch patch : availablePatches){
            if (playerController.canBuyPatch(patch)){
                patchTuples.addAll(BlanketController.calculatePlacements(gameController.getGame().getGameState().getActivePlayer().getBlanket(), patch));
            }
        }

        boolean specialPatchFound;

        if(!patchTuples.isEmpty()){
            Tuple<PatchTuple, Double> bestChoice = bestPatchTuple(
                    gameController.getGame().getGameState().getActivePlayer().getBlanket(),
                    gameController.getGame().getGameState().getActivePlayer().getPos(),
                    patchTuples);
            PatchTuple patchTuple = bestChoice.getV1();
            Double value = bestChoice.getV2();

            if(value >  0){
                specialPatchFound = playerController.takePatchAndInsert(patchTuple, false, false);
            }
            else{
                specialPatchFound = playerController.pass();
            }
        }
        else{
            specialPatchFound = playerController.pass();
        }

        if(specialPatchFound){
            Patch specialPatch = Constants.generateSpecialPatch();
            if (BlanketController.canPlacePatch(gameController.getGame().getGameState().getActivePlayer().getBlanket(), specialPatch)){
                PatchTuple bestChoiceSpecialPatch = bestFit(gameController.getGame().getGameState().getActivePlayer().getBlanket(), specialPatch);
                playerController.takePatchAndInsert(bestChoiceSpecialPatch, false, false);
            }
        }
        return str;
    }

    private Tuple<PatchTuple, Double> bestPatchTuple (Blanket blanket, int pos, Collection<PatchTuple> patchTuples){
        PatchTuple res = patchTuples.iterator().next();
        Double max = patchValue(blanket, pos, res).getV1();
        //String str  = patchValue(blanket, pos, res).getV2();
        //System.out.print("#\t" + str);
        for (PatchTuple patchTuple : patchTuples){
            double compareVal = patchValue(blanket, pos, patchTuple).getV1();
            if (compareVal > max){
                max = compareVal;
                res = patchTuple;
                //str = patchValue(blanket, pos, res).getV2();
            }
        }
        //System.out.println("\t|\t" + str);

        return new Tuple<>(res, max);
    }

    private Tuple<Double, String> patchValue(Blanket blanket, int pos, PatchTuple patchTuple){
        double surface = MatrixUtils.calcEdges(BlanketController.insertPatchTuple(blanket,patchTuple).getBoolMatrixRepresentation());
        double spread = meanAbsEuclideanDistance(BlanketController.insertPatchTuple(blanket, patchTuple));
        double size = patchTuple.getVectorList().size();
        double income = patchTuple.getPatch().getIncome();
        double cost = patchTuple.getPatch().getCost();
        double time = patchTuple.getPatch().getTime();

        int blanketSize = blanket.getPatchTuples().size();

        double surfaceSummand = surfaceFactor * surface * 1.0 / (Math.sqrt(size + blanketSize * blanketSize)); //maybe works a bit
        double spreadSummand  = spreadFactor * spread / (1 + 2 * (pos / (double) Constants.FIELDS_COUNT));   //weird normalisation, spreadSummand should be somewhere between [1.5, 2.5]
        double sizeSummand    = sizeFactor * size;
        double incomeSummand  = incomeFactor * income * (Constants.calcIncomeButtonsLeftCount(pos) / 9.0);
        double costSummand    = costFactor * cost;
        double timeSummand    = timeFactor * time;

        double val = surfaceSummand + spreadSummand + sizeSummand + incomeSummand + costSummand + timeSummand;
        String valString = "[ (" + surfaceSummand + spreadSummand + ", " + sizeSummand + ", " + incomeSummand + ", " + costSummand + ", " + timeSummand + ")\t=>\t" + val + " ], ";
        return new Tuple<>(val, valString);
    }


    public static double meanAbsEuclideanDistance(Blanket blanket){
        Set<VecTwo> allVectors = new HashSet<>();
        double res = 0;
        for (PatchTuple patchTuple : blanket.getPatchTuples()){
            allVectors.addAll(patchTuple.getVectorList());
        }
        int count = 0;
        for (VecTwo vec1 : allVectors){
            for (VecTwo vec2 : allVectors){
                res += VecTwo.euclidanDistance(vec1, vec2);
                count++;
            }
        }
        return res / count;
    }

    private static PatchTuple bestFit(Blanket blanket, Patch patch){
        Collection<PatchTuple> patchTuples = BlanketController.calculatePlacements(blanket, patch);
        return bestFit(blanket, patchTuples); //todo shuffle patchTuples?
    }

    private static PatchTuple bestFit(Blanket blanket, Collection<PatchTuple> patchTuples){
        PatchTuple res = null;
        for(PatchTuple patchTuple : patchTuples){
            res = betterFit(blanket, patchTuple, res);
        }
        return res;
    }

    private static PatchTuple betterFit(Blanket blanket, PatchTuple pt1, PatchTuple pt2){
        if (pt1 == null) return pt2;
        if (pt2 == null) return pt1;

        double aed1 = meanAbsEuclideanDistance(BlanketController.insertPatchTuple(blanket, pt1));
        double aed2 = meanAbsEuclideanDistance(BlanketController.insertPatchTuple(blanket, pt2));

        if (aed1 < aed2){
            return pt1;
        }
        else return pt2;
    }

    public static double getRandom(double [] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

}
