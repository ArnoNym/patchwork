package ai.hardcoded.raphael;

import Resources.Constants;
import controller.BlanketController;
import controller.GameController;
import controller.PlayerController;
import model.Blanket;
import model.Patch;
import model.PatchTuple;

import java.util.Collection;
import java.util.List;

public class GreedyFirstFit extends RaphaelAI{
    private GameController gameController;

    public GreedyFirstFit(GameController gameController){
        this.gameController = gameController;
    }

    public GreedyFirstFit() {}

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    public String toString(){
        return "GreedyFirstFit";
    }

    public String doTurn(){
        String str = ""; //todo

        List<Patch> patches = gameController.getGameBoardController().getAvailablePatches();
        Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();

        boolean specialPatchFound = false;
        boolean patchFound = false;

        PlayerController playerController = gameController.getPlayerController();

        for (Patch patch : patches){
            if (playerController.canBuyPatch(patch)){
                if (BlanketController.canPlacePatch(blanket, patch)){
                    patchFound = true;
                    Collection<PatchTuple> collection = BlanketController.calculatePlacements(blanket, patch);
                    PatchTuple pt = collection.iterator().next();
                    specialPatchFound = playerController.takePatchAndInsert(pt);
                    break;
                }
            }
        }
        if (!patchFound){
            specialPatchFound = playerController.pass();
        }
        if (specialPatchFound){
            Collection<PatchTuple> collection = BlanketController.calculatePlacements(blanket, Constants.generateSpecialPatch());
            PatchTuple pt = collection.iterator().next();
            playerController.takePatchAndInsert(pt);
        }
        return str;
    }

}
