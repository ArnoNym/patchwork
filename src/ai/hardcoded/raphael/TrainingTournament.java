package ai.hardcoded.raphael;

import Resources.Constants;
import ai.anneat.main.utils.collections.Tuple;
import controller.GameController;
import controller.GameStateController;
import controller.IOController;
import controller.PlayerController;
import model.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class TrainingTournament {
    private GameController gameController;
    int countPlayer1Wins;
    int countPlayer2Wins;

    public TrainingTournament(GameController gameController){
        this.gameController = gameController;
        this.countPlayer1Wins = 0;
        this.countPlayer2Wins = 0;
    }

    public void run(RaphaelAI player1, RaphaelAI player2, boolean show){
        setup();
        playMatch(player1, player2, show);
    }

    private void setup(){
        GameStateController gameStateController = this.gameController.getGameStateController();
        IOController io = new IOController(null);
        Player stupidAi = new Player("Player1", null);
        Player greedyAi = new Player("Player2", null);
        gameStateController.setUpGame(stupidAi, greedyAi, Constants.shufflePatches(io.importCSV()));
        //gameStateController.setUpGame(stupidAi, greedyAi, io.importCSV());
    }

    private void playMatch(RaphaelAI player1, RaphaelAI player2, boolean show){
        while(!gameController.getGame().getGameState().isFinished()){
            if (gameController.getGame().getGameState().getActivePlayer().getName().equals("Player1")){
                player1.doTurn();
            }
            else{
                player2.doTurn();
            }
        }
        if(show)System.out.println(gameController.getGame().getGameState());

        //Getting the winner
        Player active = gameController.getGame().getGameState().getActivePlayer();
        int activeScore = PlayerController.calculateScore(active);
        Player passive = gameController.getGame().getGameState().getPassivePlayer();
        int passiveScore = PlayerController.calculateScore(passive);

        if(activeScore > passiveScore){
            if (active.getName().equals("Player1")){
                if(show)System.out.print("Gamescore: " + "(P1 = " + activeScore + ", P2 = " + passiveScore + ")\t");
                countPlayer1Wins++;
            }
            else{
                if(show)System.out.print("Gamescore: " + "(P1 = " + passiveScore + ", P2 = " + activeScore + ")\t");
                countPlayer2Wins++;
            }
        }
        else{
           if (passive.getName().equals("Player1")){
               if(show)System.out.print("Gamescore: " + "(P1 = " + passiveScore + ", P2 = " + activeScore + ")\t");
               countPlayer1Wins++;
           }
           else{
               if(show)System.out.print("Gamescore: " + "(P1 = " + activeScore + ", P2 = " + passiveScore + ")\t");
               countPlayer2Wins++;
           }
        }
        //System.out.println(gameController.showHighscore());
        if(show)System.out.println("(" + countPlayer1Wins + ", " + countPlayer2Wins + ")");
    }

    public static RaphaelAI runTournament(int iterations){
        Stack<RaphaelAI> stillParticipating = new Stack<>();
        for (int i = 0; i < Math.pow(2, iterations); i++){
            stillParticipating.push(new StupidAI());
            //stillParticipating.push(new GreedyFirstFit());
        }

        List<Thread> threads = new LinkedList<>();
        int iteration = 1;

        while (!stillParticipating.isEmpty() && stillParticipating.size() != 1){
            System.out.println("*** ROUND #" + iteration++ + " WITH " + stillParticipating.size() + " PARTICIPANTS ***");
            Stack<RaphaelAI> winnersOfIteration = new Stack<>();
            for (int i = 0; i < Math.min(8, stillParticipating.size() / 2); i++) {
                Stack<RaphaelAI> finalStillParticipating = stillParticipating;
                int matchesThisRound = stillParticipating.size() / 2;
                Thread thread = new Thread(() -> {
                    int gameCount = 0;
                    int rounds =  Math.max(4, 128/matchesThisRound);
                    while(true){
                        RaphaelAI player1;
                        RaphaelAI player2;
                        synchronized (finalStillParticipating){
                            if (finalStillParticipating.isEmpty()){
                                break;
                            }
                            player1 = finalStillParticipating.pop();
                            player2 = finalStillParticipating.pop();
                        }
                        RaphaelAI winner = playMatches(player1, player2, rounds, false);
                        synchronized (winnersOfIteration){
                            winnersOfIteration.push(winner);
                        }
                        gameCount++;
                    }
                    System.out.println("FINISHING THREAD AFTER " + gameCount + " GAMES WITH " + rounds + " ROUNDS!");
                });
                threads.add(thread);
                thread.start();
            }
            while (!Util.allDead(threads)){
                Util.sleep(1000);
            }
            stillParticipating = winnersOfIteration;
        }
        return stillParticipating.pop();
    }

    private static RaphaelAI playMatches(RaphaelAI ai1, RaphaelAI ai2, int matchCount, boolean show){
        GameController gameController = new GameController();
        TrainingTournament trainingTournament = new TrainingTournament(gameController);
        ai1.setGameController(gameController);
        ai2.setGameController(gameController);

        if (false && matchCount >= 64){
            System.out.println("PLAYER1: " + ai1);
            System.out.println("PLAYER2: " + ai2);
        }

        if(show)System.out.println("(ai1, ai2)");
        for (int i = 0; i < matchCount/2; i++){
            trainingTournament.run(ai1, ai2, show);
        }

        if(show)System.out.println("\nswapping sides");
        int ai1Wins = trainingTournament.countPlayer1Wins;
        int ai2Wins = trainingTournament.countPlayer2Wins;

        trainingTournament.countPlayer1Wins = 0;
        trainingTournament.countPlayer2Wins = 0;

        if(show)System.out.println("(ai2, ai1)");
        for (int i = 0; i < matchCount/2; i++){
            trainingTournament.run(ai2, ai1, show);
        }

        ai1Wins += trainingTournament.countPlayer2Wins;
        ai2Wins += trainingTournament.countPlayer1Wins;

        if(show)System.out.println(ai1Wins + " | " + ai2Wins);

        return ai1Wins >= ai2Wins ? ai1 : ai2;
    }

    public static Tuple<Double, Double> validate(RaphaelAI winner, int n){
        GameController gameController = new GameController();
        Training training = new Training(gameController);
        winner.setGameController(gameController);

        for (int i = 0; i < n; i++){
            training.run(winner);
        }
        Tuple<Double, Double> tuple = Constants.statisticalAnalysis(training.scores);
        playMatches(winner, winner, 2, true);
        return tuple;
    }

    public static void apply(int rounds){
        RaphaelAI winner = runTournament(rounds);
        System.out.println("THE WINNER IS: " + winner);
        System.out.println("VALIDATING HIS PERFORMANCE...");
        Tuple<Double, Double> tuple = validate(winner, 200);
        System.out.println("(" + tuple.getV1() + ", " + tuple.getV2() + ")");
        System.out.println("*******DONE*******");
    }

    public static void main(String [] args){
        /*System.out.println(playMatches(
                //new StupidAI(-0.1, -0.3, 0.7, 0.95, -0.3, -0.1),
                new StupidAI(-0.1, -0.1, 0.5, 1.0, -0.1, -0.3),
                new StupidAI(-0.1, -0.1, 0.5, 1.0, -0.1, -0.3),
                //new StupidAI(-0.15, 0.0, 0.7, 0.9, -0.25, 0.0),
                2, true));
        */

        for(int i = 0; i < 5; i++){
            apply(10);
        }

    }
}
