package ai.hardcoded.raphael;

import Resources.Constants;
import ai.anneat.main.utils.collections.Tuple;
import controller.*;
import model.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Training {
    GameController gameController;
    List<Integer> scores;
    List<Integer> greedyAiScores;
    String stupidAI = "stupidAI";

    public Training (GameController gameController){
        this.gameController = gameController;
        scores = new LinkedList<>();
        greedyAiScores = new LinkedList<>();
    }

    public static void main(String [] args){
        apply(10, -12.5);
    }

    public static double[] apply(int iterations, double threshold){
        double [] doubles = {1, 0.21024946083257579, -0.5073072735299076, -0.9738027969850156, -0.7342049654937466, 0.29558799291167515, -0.23220857484295498};
        for (int i = 0; i < iterations; i++){
            System.out.println("\n##########################################\nEVALUATING: " + Arrays.toString(doubles) + " ...");
            Tuple<Double, Double> bestYet = stressTest(doubles, 500);
            System.out.println("(" + bestYet.getV1() + ", " + bestYet.getV2() + ")" );
            if (bestYet.getV1() - bestYet.getV2() >= threshold){
                break;
            }
            System.out.println("\n##########################################\nEVALUATING COMPLETED");
            System.out.println("GETTING NEXT CANDIDATES...");
            doubles = findBestVectorMultiThreading(doubles, 8, 250, 2);
        }
        System.out.println("\n\n\n BEST GUESS:" + Arrays.toString(doubles));
        return doubles;
    }

    public static double[] findBestVectorMultiThreading(double[] vec,int threadNr ,int triesPerThread, int stressTests){
        List<Thread> threads = new LinkedList<>();
        List<Tuple<double[], Tuple<Double, Double>>> threadResults = new LinkedList<>();
        Tuple<Double, Double> bestTuple = stressTest(vec,250);

        for (int i = 0; i < Math.min(8, threadNr); i++){
            Thread thread = new Thread(() -> {
                double [] vect = findBestVector(triesPerThread, stressTests);
                Tuple<Double, Double> tup = stressTest(vect, 250);
                threadResults.add(new Tuple<>(vect, tup));
            });
            threads.add(thread);
            thread.start();
        }

        while (!Util.allDead(threads)){
            Util.sleep(1000);
        }
        System.out.println("DONE\tCHECKING RESULTS...");
        System.out.println("Previous BEST TUPLE (" + bestTuple.getV1() + ", " + bestTuple.getV2() + ")\t\tPrevious BEST VECTOR " + Arrays.toString(vec));

        for (Tuple <double[],Tuple<Double, Double>> tuple : threadResults){
            double[] tmp = tuple.getV1();
            Tuple<Double, Double> compareTuple = tuple.getV2();
            if ((compareTuple.getV1() - compareTuple.getV2()) > (bestTuple.getV1() - bestTuple.getV2())){
                bestTuple = compareTuple;
                vec = tmp;
                System.out.println("NEW BEST TUPLE (" + bestTuple.getV1() + ", " + bestTuple.getV2() + ")\t\tNEW BEST VECTOR " + Arrays.toString(vec));
            }
        }

        System.out.println("FINISHING...");
        System.out.println("NEW BEST TUPLE (" + bestTuple.getV1() + ", " + bestTuple.getV2() + ")\t\tNEW BEST VECTOR " + Arrays.toString(vec));
        return vec;
    }

    private static double [] findBestVector(int tries, int stressTests){
        double[] vec = Util.getRandomVector(6);
        Tuple<Double, Double> bestTuple = stressTest(vec,stressTests);

        for (int times = 0; times < tries - 1; times++){
            double[] tmp = Util.getRandomVector(7);
            Tuple<Double, Double> compareTuple = stressTest(tmp,stressTests);
            if ( (compareTuple.getV1() - compareTuple.getV2()) > (bestTuple.getV1() - bestTuple.getV2()) ){
                bestTuple = compareTuple;
                vec = tmp;
                System.out.println("NEW BEST TUPLE (" + bestTuple.getV1() + ", " + bestTuple.getV2() + ")\t\tNEW BEST VECTOR " + Arrays.toString(vec));
            }
        }
        return vec;
    }

    public static Tuple<Double, Double> stressTest(double[] vec,int n){
        Training training = new Training(new GameController());
        for (int i = 0; i < n; i++){
            training.run(vec);
        }
        Tuple<Double, Double> tuple = Constants.statisticalAnalysis(training.scores);
        return tuple;
    }

    public void run(double [] vec){
        if (vec.length != 6) throw new IllegalStateException("falsche Arraylänge");
        setup();
        StupidAI stupidAI = new StupidAI(vec[0] , vec[1], vec[2], vec[3], vec[4], vec[5]);
        stupidAI.setGameController(gameController);
        playGame(stupidAI);
    }

    public void run(RaphaelAI raphaelAI){
        setup();
        playGame(raphaelAI);
    }

    public void setup(){
        GameStateController gameStateController = this.gameController.getGameStateController();
        IOController io = new IOController(null);
        Player player1 = new Player(stupidAI, null);
        Player player2 = new Player("player2", null);

        gameStateController.setUpGame(player1, player2, Constants.shufflePatches(io.importCSV()));
    }

    public void playGame(RaphaelAI raphaelAI){
        while(!gameController.getGame().getGameState().isFinished()){
            raphaelAI.doTurn();
        }
        Player player1 = gameController.getGame().getGameState().getActivePlayer();
        Player player2 = gameController.getGame().getGameState().getPassivePlayer();

        scores.add(PlayerController.calculateScore(player1));
        scores.add(PlayerController.calculateScore(player2));
    }
}
