package ai;

import controller.GameController;

public class AITurnThread implements Runnable{
        GameController gameController;

        public AITurnThread(GameController gameController){
            this.gameController = gameController;
        }
        @Override
        public void run() {
            gameController.getAiController().processAiMove();
        }
}

