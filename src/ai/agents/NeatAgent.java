package ai.agents;

import ai.anneat.main.SerializableFunction;

import java.io.Serializable;

public class NeatAgent implements Serializable {
    private final SerializableFunction<double[], double[]> function;

    public NeatAgent(SerializableFunction<double[], double[]> serializableFunction) {
        this.function = serializableFunction;
    }

    public double[] compute(double[] input) {
        return function.apply(input);
    }
}
