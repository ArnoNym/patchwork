package ai.agents;

import ai.decisionTree.DecisionTree;
import ai.decisionTree.QuickDecisionTree;
import ai.decisionTree.Turn;
import ai.AiConstants;
import model.*;
import org.jetbrains.annotations.NotNull;

/**
 * AI-Agent that can play the game by using a neat agent to rate the gameState
 * @author Leon
 */
public class NeatPatchworkAgent implements PatchworkAgent {
    private final GameStateAgent gameStateAgent;

    public NeatPatchworkAgent(GameStateAgent gameStateAgent) {
        this.gameStateAgent = gameStateAgent;
    }

    /**
     * @param gameState is a copy of the currentGameState. Gets destroyed.
     * @return the turn, calculated by the ai with the difficulty in the players aiInformation
     */
    @Override
    @NotNull
    public Turn calcTurn(@NotNull GameState gameState) {
        boolean useCompute = isUsingDecisionTree(gameState.getActivePlayer().getAIInformation().getDifficulty());
        if (useCompute) {
            DecisionTree decisionTree = new DecisionTree(gameStateAgent, gameState);
            decisionTree.compute(AiConstants.LOOKAHEAD_MAX_STEPS, AiConstants.LOOKAHEAD_MAX_SECS);
            return decisionTree.getBestTurn();
        } else {
            QuickDecisionTree quickDecisionTree = new QuickDecisionTree(gameStateAgent, gameState);
            return quickDecisionTree.calcTurn();
        }
    }

    public static boolean isUsingDecisionTree(@NotNull Difficulty difficulty) {
        if (!AiConstants.USE_DECISION_TREE) {
            return false;
        }
        switch (difficulty) {
            case HARDCORE:
                return true;
            case MISSIONARY:
            case SOFTCORE:
            default:
                return false;
        }
    }
}
