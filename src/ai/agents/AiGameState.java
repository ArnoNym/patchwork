package ai.agents;

import controller.GameStateController;
import controller.PlayerController;
import model.Blanket;
import model.GameState;
import model.Player;
import org.jetbrains.annotations.NotNull;

public class AiGameState {
    private final GameState gameState;
    private final String playerName;

    public AiGameState(@NotNull GameState gameState, @NotNull String playerName) {
        this.gameState = gameState;
        this.playerName = playerName;
    }

    protected AiGameState copy() {
        return new AiGameState(gameState.clone(), playerName);
    }

    public GameState getGameState() {
        return gameState;
    }

    public Player getPlayer() {
        return GameStateController.getPlayerByName(gameState, playerName);
    }

    public Player getEnemyPlayer() {
        return GameStateController.getOtherPlayerName(gameState, playerName);
    }
}
