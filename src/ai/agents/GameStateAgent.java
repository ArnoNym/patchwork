package ai.agents;

import Resources.Constants;
import ai.anneat.main.utils.collections.Tuple;
import ai.anneat.main.SerializableFunction;
import ai.decisionTree.Move;
import ai.decisionTree.Turn;
import ai.training.utils.BlanketDecoderUtils;
import ai.training.utils.DecoderUtils;
import controller.BlanketController;
import controller.GameController;
import controller.GameStateController;
import model.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class GameStateAgent extends NeatAgent {

    public GameStateAgent(SerializableFunction<double[], double[]> serializableFunction) {
        super(serializableFunction);
    }

    /**
     * This Method executes a give move and returns the resulting GameState.
     * If the move is illegal or impossible, it returns null
     *
     * @param destroyGameController Game State will get Destroyed.
     * @param move The move that should be executed
     * @return The GameState that resulted by executing the move on the old GameState; Null for illegal or impossible moves
     * @author Florian
     */
    @NotNull
    public Optional<Tuple<GameState, Turn>> executeMove(GameController destroyGameController, Move move) {
        List<Patch> availablePatches = destroyGameController.getGameBoardController().getAvailablePatches();

        Turn turn;
        if (move.equals(Move.PASS)) {
            turn = computePass(destroyGameController);
        } else {
            Patch patch;
            switch (move) {
                case BUY_FIRST:
                    if (availablePatches.size() < 1) {
                        return Optional.empty();
                    }
                    patch = availablePatches.get(0);
                    break;
                case BUY_SECOND:
                    if (availablePatches.size() < 2) {
                        return Optional.empty();
                    }
                    patch = availablePatches.get(1);
                    break;
                case BUY_THIRD:
                    if (availablePatches.size() < 3) {
                        return Optional.empty();
                    }
                    patch = availablePatches.get(2);
                    break;
                default:
                    throw new IllegalStateException();
            }
            if (!destroyGameController.getPlayerController().canBuyPatch(patch)) {
                return Optional.empty();
            }
            turn = computeBuyAndPlacePatchTurn(destroyGameController, patch);
        }

        if (turn == null) {
            //The Patch cannot be placed
            return Optional.empty();
        } else {
            return Optional.of(new Tuple<>(destroyGameController.getGame().getGameState(), turn));
        }
    }

    // Turn ============================================================================================================

    /**
     * Computes for the activePlayer
     *
     * @param gameController Destroys GameState
     * @author Leon
     */
    @Nullable
    public Turn computeBuyAndPlacePatchTurn(@NotNull GameController gameController, @NotNull Patch patch) {
        AiGameState aiGameState = new AiGameState(
                gameController.getGame().getGameState(),
                gameController.getGame().getGameState().getActivePlayer().getName());

        PatchTuple patchTuple = calcBestPatchTuple(
                aiGameState.copy(),
                patch);
        if (patchTuple == null) {
            return null;
        }

        boolean foundExtraPatchTuple = gameController.getPlayerController().takePatchAndInsert(patchTuple, false, false);
        PatchTuple extraPatchTuple;
        if (foundExtraPatchTuple) {
            extraPatchTuple = calcBestExtraPatchTuple(aiGameState);
            if (extraPatchTuple == null) {
                System.out.println(aiGameState.getPlayer().getBlanket());
                throw new IllegalStateException("Extra Patch Tuple can not be placed!");
            }
        } else {
            extraPatchTuple = null;
        }

        return Turn.create(patchTuple, extraPatchTuple);
    }

    /**
     * Computes for the activePlayer
     *
     * @param gameController Destroys GameState
     * @author Leon
     */
    public Turn computePass(@NotNull GameController gameController) {
        PatchTuple extraPatchTuple = null;
        AiGameState aiGameState = new AiGameState(
                gameController.getGame().getGameState(),
                gameController.getGame().getGameState().getActivePlayer().getName());
        if (gameController.getPlayerController().pass()) {
            extraPatchTuple = calcBestExtraPatchTuple(aiGameState);
            if (extraPatchTuple == null) {
                throw new IllegalStateException("Extra Patch Tuple can not be placed!");
            }
        }
        return Turn.createPass(extraPatchTuple);
    }

    // PatchTuple ======================================================================================================

    /**
     * @param aiGameState gets destroyed.
     * @author Leon
     */
    @Nullable
    public PatchTuple calcBestExtraPatchTuple(@NotNull AiGameState aiGameState) {
        Patch extraPatch = Constants.generateSpecialPatch();
        return calcBestPatchTuple(aiGameState, extraPatch);
    }

    /**
     * @param aiGameState gets destroyed
     * @author Leon
     */
    @Nullable
    public PatchTuple calcBestPatchTuple(@NotNull AiGameState aiGameState, @NotNull Patch patch) {
        Collection<Tuple<Blanket, PatchTuple>> tuples = BlanketController.calculateAllPossibleFollowupBlankets(
                aiGameState.getPlayer().getBlanket(),
                patch);
        if (tuples.isEmpty()) {
            //If there is no way to place the Patch on the current gameBoard
            return null;
        }
        return calcBestPatchTuple(aiGameState, tuples);
    }

    /**
     * @param aiGameState gets destroyed
     * @author Leon
     */
    @NotNull
    private PatchTuple calcBestPatchTuple(AiGameState aiGameState, @NotNull Collection<Tuple<Blanket, PatchTuple>> tuples) {
        assert !tuples.isEmpty();

        Set<PatchTuple> bestPatchTuples = new HashSet<>();
        double bestRating = -Double.MAX_VALUE;
        for (Tuple<Blanket, PatchTuple> tuple : tuples) {
            aiGameState.getPlayer().setBlanket(tuple.getV1());
            double rating = rate(aiGameState);
            if (bestRating < rating) {
                bestRating = rating;
                bestPatchTuples.clear();
                bestPatchTuples.add(tuple.getV2());
            } else if (bestRating == rating) {
                bestPatchTuples.add(tuple.getV2());
            }
        }
        assert !bestPatchTuples.isEmpty();

        if (bestPatchTuples.size() == 1) {
            return bestPatchTuples.iterator().next();
        } else {
            return bestPatchTuples.stream()
                    .min(Comparator.comparingInt(PatchTuple::hashCode))
                    .orElseThrow();
        }
    }

    // Rate ============================================================================================================

    public double rate(@NotNull GameState gameState, @NotNull String playerName) {
        double[] input = DecoderUtils.decodeGameState(new AiGameState(gameState, playerName));
        return compute(input)[0];
    }

    public double rateAsOpponent(@NotNull GameState gameState, @NotNull String playerName) {
        Player opponent = GameStateController.getOtherPlayerName(gameState, playerName);
        return rate(gameState, opponent.getName());
    }

    public double rate(@NotNull AiGameState aiGameState) {
        double[] input = DecoderUtils.decodeGameState(aiGameState);
        return compute(input)[0];
    }
}
