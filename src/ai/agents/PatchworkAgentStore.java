package ai.agents;

import model.Difficulty;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class PatchworkAgentStore {
    private final PatchworkAgent patchworkAgent;
    private final Map<Difficulty, PatchworkAgent> patchworkAgentMap;

    public PatchworkAgentStore(PatchworkAgent patchworkAgent) {
        this.patchworkAgent = patchworkAgent;
        this.patchworkAgentMap = null;
    }

    public PatchworkAgentStore(Map<Difficulty, PatchworkAgent> agentMap) {
        this.patchworkAgent = null;
        this.patchworkAgentMap = agentMap;
    }

    public PatchworkAgent getAgent(@Nullable Difficulty difficulty) {
        if (difficulty == null) {
            if (patchworkAgent == null) {
                throw new IllegalStateException();
            }
            return patchworkAgent;
        } else {
            if (patchworkAgentMap == null) {
                throw new IllegalStateException();
            }
            return patchworkAgentMap.get(difficulty);
        }
    }
}

