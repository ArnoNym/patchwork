package ai.agents;

import ai.decisionTree.Turn;
import model.GameState;

import java.io.Serializable;

public interface PatchworkAgent extends Serializable {
    /**
     * @param gameState is a copy of the currentGameState
     * @return a turn object that describes the turn the agent wants to take
     * @author Leon
     */
    Turn calcTurn(GameState gameState);
}
