package ai;

import ai.agents.PatchworkAgent;
import ai.training.save.AiSaveController;
import ai.training.trainings.TurnBenchmarkTraining;
import model.Difficulty;

import java.util.HashMap;
import java.util.Map;

public class AiConstants {
    public static final String BLANKET_AGENT_NAME = "2021_02_10_0319";
    public static final String TURN_AGENT_NAME = TurnBenchmarkTraining.AGENT_MAP_SAVE_NAME;
    public static final String AGENT_MAP = TurnBenchmarkTraining.AGENT_MAP_SAVE_NAME;

    public static void main(String[] args) {
        Map<Difficulty, PatchworkAgent> map = new HashMap<>();

        /*
        BlanketAgent blanketAgent = new BlanketAgent(AiSaveController.loadBlanketFunction(BLANKET_AGENT_NAME));
        NeatAgent neatAgent = new NeatAgent(AiSaveController.loadTurnBenchmarkFunction(TURN_AGENT_NAME));
        map.put(Difficulty.HARDCORE, new NeatPatchworkAgent(blanketAgent, neatAgent));

        map.put(Difficulty.MISSIONARY, new StupidAgent());

        map.put(Difficulty.SOFTCORE, new BuyFirstAgent());
        */

        //aiSaveController.saveAgentMao(map);
        AiSaveController.saveAgentMao(AiSaveController.loadAgentMap(AGENT_MAP));
    }

    public static final boolean PRINT_DECISION_TREE = false;

    public static boolean USE_DECISION_TREE = true;
    public static boolean PLACE_PATCH_AND_EXTRA_PATCH_AT_ONCE = false;

    public static final boolean RATE_ENEMY_MOVES = true;
    public static final int LOOKAHEAD_MAX_STEPS = 2;
    public static final float LOOKAHEAD_MAX_SECS = 8f;

    public static final String TRAINING_PLAYER_1_NAME = "AI_1";
    public static final String TRAINING_PLAYER_2_NAME = "AI_2";
}
