package ai.training.settings;

import ai.anneat.publish.settings.Settings;

public class TurnBenchmarkSettings extends Settings {

    public TurnBenchmarkSettings() {
        super();

        getPerformanceSettings().setMaxAgentCount(30);
        getPerformanceSettings().setPrint(true);
        getPerformanceSettings().setThreads(null);

        getEvolutionSettings().setAllowHiddenNodes(true);

        /*
        getEvolutionSettings().setC1(2);
        getEvolutionSettings().setC2(2);
        getEvolutionSettings().setC3(1);
        getEvolutionSettings().setCp(4);

        getEvoSet().setSurvivorsPercent(0.8);
        getEvoSet().setBestMayReproducePercent(0.05);
        getEvoSet().setOffspringWithOnlyOneParentPercent(0.25);
        getEvoSet().setAllowGenerationsWithoutIncreaseInValue(30);
        getEvoSet().setMutateALlSpeciesSize(1);
        getEvoSet().setElitism(1);
        */
        /*
        getEvolutionSettings().setBigSpeciesOfTotalPopRequired((int) (0.16 * getPerformanceSettings().getMaxAgentCount()));

        getEvolutionSettings().setProbabilityMutateActivateLink(0.05);
        getEvolutionSettings().setProbabilityMutateDeactivateLink(0.01);
        getEvolutionSettings().setProbabilityMutateLinkSmallPop(0.08);
        getEvolutionSettings().setProbabilityMutateLinkBigPop(0.4);
        getEvolutionSettings().setProbabilityMutateNodeSmallPop(0.04);
        getEvolutionSettings().setProbabilityMutateNodeBigPop(0.2);
        getEvolutionSettings().setProbabilityMutateWeight(0.6);
        getEvolutionSettings().setProbabilityMutateWeightRandom(0.1);
        getEvolutionSettings().setProbabilityMutateWeightShift(0.9);

        getEvolutionSettings().setWeightShiftStrength(1);
        getEvolutionSettings().setWeightRandomStrength(5);
        */
    }
}
