package ai.training.settings;

import ai.anneat.publish.settings.Settings;

public class IterationSettings extends Settings {

    public IterationSettings() {
        super();

        getPerformanceSettings().setMaxAgentCount(10);
        getPerformanceSettings().setPrint(true);
        getPerformanceSettings().setThreads(null);

        getEvolutionSettings().setAllowHiddenNodes(true);

        getEvolutionSettings().setC1(1);
        getEvolutionSettings().setC2(1);
        getEvolutionSettings().setC3(1);
        getEvolutionSettings().setCp(3);
        /*
        getEvoSet().setSurvivorsPercent(0.8);
        getEvoSet().setBestMayReproducePercent(0.05);
        getEvoSet().setOffspringWithOnlyOneParentPercent(0.25);
        getEvoSet().setAllowGenerationsWithoutIncreaseInValue(30);
        getEvoSet().setMutateALlSpeciesSize(1);
        getEvoSet().setElitism(1);
        */
        getEvolutionSettings().setBigSpeciesOfTotalPopRequired((int) (0.16 * getPerformanceSettings().getMaxAgentCount()));

        getEvolutionSettings().setProbabilityMutateLinkSmallPop(0.1);
        getEvolutionSettings().setProbabilityMutateLinkBigPop(0.8);
        getEvolutionSettings().setProbabilityMutateNodeSmallPop(0.01);
        getEvolutionSettings().setProbabilityMutateNodeBigPop(0.2);
        getEvolutionSettings().setProbabilityMutateActivateLink(0.05);
        getEvolutionSettings().setProbabilityMutateDeactivateLink(0.001);
        getEvolutionSettings().setProbabilityMutateWeight(0.9);
        getEvolutionSettings().setProbabilityMutateWeightRandom(0.1);
        getEvolutionSettings().setProbabilityMutateWeightShift(0.9);

        getEvolutionSettings().setWeightShiftStrength(0.5);
        getEvolutionSettings().setWeightRandomStrength(3);
    }
}
