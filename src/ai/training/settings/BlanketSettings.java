package ai.training.settings;

import ai.anneat.publish.settings.Settings;

public class BlanketSettings extends Settings {
    public BlanketSettings() {
        super();

        getPerformanceSettings().setMaxAgentCount(150);
        getPerformanceSettings().setPrint(true);
        getPerformanceSettings().setThreads(null);

        getEvolutionSettings().setAllowHiddenNodes(true);

        getEvolutionSettings().setC1(2);
        getEvolutionSettings().setC2(2);
        getEvolutionSettings().setC3(0.1);
        getEvolutionSettings().setCp(3);

        getEvolutionSettings().setSurvivorsPercent(0.8);
        getEvolutionSettings().setBestMayReproducePercent(0.05);
        getEvolutionSettings().setOffspringWithOnlyOneParentPercent(0.25);
        getEvolutionSettings().setAllowGenerationsWithoutIncreaseInValue(30);
        getEvolutionSettings().setMutateALlSpeciesSize(1);
        getEvolutionSettings().setTotalElitism(1);
        getEvolutionSettings().setSpeciesElitism(1);

        getEvolutionSettings().setBigSpeciesOfTotalPopRequired((int) (0.16 * getPerformanceSettings().getMaxAgentCount()));

        getEvolutionSettings().setProbabilityMutateLinkSmallPop(0.08);
        getEvolutionSettings().setProbabilityMutateLinkBigPop(0.8);
        getEvolutionSettings().setProbabilityMutateNodeSmallPop(0.03);
        getEvolutionSettings().setProbabilityMutateNodeBigPop(0.3);
        getEvolutionSettings().setProbabilityMutateActivateLink(0.05);
        getEvolutionSettings().setProbabilityMutateDeactivateLink(0.001);
        getEvolutionSettings().setProbabilityMutateWeight(1);
        getEvolutionSettings().setProbabilityMutateWeightRandom(0.1);
        getEvolutionSettings().setProbabilityMutateWeightShift(0.9);

        getEvolutionSettings().setWeightShiftStrength(1);
        getEvolutionSettings().setWeightRandomStrength(5);
    }
}
