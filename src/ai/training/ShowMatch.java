package ai.training;

import ai.agents.GameStateAgent;
import ai.agents.NeatPatchworkAgent;
import ai.agents.PatchworkAgent;
import ai.decisionTree.Turn;
import ai.hardcoded.benchmarks.StupidAgent;
import ai.AiConstants;
import ai.training.save.AiSaveController;
import ai.training.utils.TrainingUtils;
import controller.GameController;
import controller.GameStateController;
import controller.IOController;
import model.GameState;
import model.Patch;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ShowMatch {
    private  static IOController ioController;

    public static void main(String[] args) {
        AiConstants.USE_DECISION_TREE = true;

        ioController = new IOController(null);

        PatchworkAgent trainedAgent = new NeatPatchworkAgent(new GameStateAgent(AiSaveController.loadTurnBenchmarkFunction(AiConstants.TURN_AGENT_NAME)));

        start(
                trainedAgent,//trainedAgent,//new HandpickedWeightsAgent(new BlanketAgent(AiSaveController.loadBlanketFunction(BlanketTraining.BLANKET_AGENT_SAVE_NAME))),
                new StupidAgent(),
                true,
                null);
    }

    public static void start(PatchworkAgent agent1, PatchworkAgent agent2, boolean print, @Nullable String playerName) {
        AiConstants.USE_DECISION_TREE = true;
        List<Patch> patches = ioController.importCSV();

        GameController gameController = new GameController(agent1, agent2, patches);


        List<Patch> patches0 = TrainingUtils.resortPatches_0(ioController.importCSV());
        GameController gameController0 = new GameController(agent1, agent2, patches0);

        List<Patch> patches1 = TrainingUtils.resortPatches_1(ioController.importCSV());
        GameController gameController1 = new GameController(agent1, agent2, patches1);

        List<Patch> patches2 = TrainingUtils.resortPatches_2(ioController.importCSV());
        GameController gameController2 = new GameController(agent1, agent2, patches2);

        List<Patch> patches3 = TrainingUtils.resortPatches_3(ioController.importCSV());
        GameController gameController3 = new GameController(agent1, agent2, patches3);


        play(gameController, print, playerName);
        play(gameController0, print, playerName);
        play(gameController1, print, playerName);
        play(gameController2, print, playerName);
        play(gameController3, print, playerName);

    }

    public static void play(GameController gameController, boolean print, @Nullable String playerName) {
        while (!gameController.getGame().getGameState().isFinished()) {
            if (print && shallPrint(gameController.getGame().getGameState(), playerName)) {
                System.out.println("######################################################################");
                System.out.println("##### GameState ######");
                System.out.println(gameController.getGame().getGameState().toString());
                System.out.println("##### Turn ######");
            }

            Turn turn = gameController.getAiController().doTurn();

            if (print && shallPrint(gameController.getGame().getGameState(), playerName)) {
                System.out.println(turn.toString());
                System.out.println();
            }
        }
        if (print && shallPrint(gameController.getGame().getGameState(), playerName)) {
            System.out.println("######################################################################");
            System.out.println("##### Finished GameState ######");
            System.out.println(gameController.getGame().getGameState().toString());
        }
    }

    private static boolean shallPrint(GameState gameState, @Nullable String name) {
        return name == null || gameState.getActivePlayer().equals(GameStateController.getPlayerByName(gameState, name));
    }
}
