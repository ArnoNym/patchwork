package ai.training.trainings;

public class SoloTraining {
    /*
}
    private static final String AGENT_MAP_SAVE_NAME = "agentMap";
    private static final String NEAT_DATE_SAVE_NAME = "neatData";

    private static boolean PRINT = false;
    private static final int GENERATIONS = 100;

    public static void main(String[] args) {
        PatchworkAgent bestPatchworkAgent = newTrain();//continueTrain();//

        IOController ioController = new IOController(null);
        HashMap<Difficulty, PatchworkAgent> agentMap = new HashMap<>();
        agentMap.put(Difficulty.HARDCORE, bestPatchworkAgent);
        ioController.saveAgentMapFromPath(agentMap,"resources/"+AGENT_MAP_SAVE_NAME+".ser");
    }

    public static PatchworkAgent newTrain() {
        List<Patch> patches = TrainingUtils.createPatches();

        Task task = new SoloTask(19, 1, Deterministic.YES_PERFORMANCE, function -> {
            PatchworkAgent patchworkAgent = new NeatPatchworkAgent(new NeatAgent(function));
            return getScore(patchworkAgent, patches);
        });

        Settings settings = new TrainingSettings();
        Neat neat = new Neat(settings, task,1040d);

        PatchworkAgent patchworkAgent = train(neat, patches);

        IOController ioController = new IOController(null);
        ioController.saveNeatDataToPath("resources/"+ NEAT_DATE_SAVE_NAME +".ser", neat.getData());

        return patchworkAgent;
    }

    public static PatchworkAgent continueTrain() {
        IOController ioController = new IOController(null);
        NeatData neatData = ioController.loadNeatFromPath("resources/neat_1.ser");

        neatData.setSettings(new TrainingSettings());

        List<Patch> patches = TrainingUtils.createPatches();

        PatchworkAgent patchworkAgent = train(new Neat(neatData), patches);

        ioController.saveNeatDataToPath("resources/neat_1.ser", neatData);

        return patchworkAgent;
    }

    public static PatchworkAgent train(Neat neat, List<Patch> patches) {
        neat.evolve(GENERATIONS);

        PRINT = true;
        ai.anneat.models.generation.Agent bestAgent = neat.getBestAgent();
        PatchworkAgent patchworkAgent = new NeatPatchworkAgent(new NeatAgent(bestAgent.getCalculator().getFunction()));
        double score = getScore(patchworkAgent, patches);
        System.out.println("Score: "+score);
        new Frame(neat.getBestAgent());
        return patchworkAgent;
    }

    public static double getScore(PatchworkAgent patchworkAgent, List<Patch> patches) {
        //Timer timer = Timer.start("getScore");

        GameController gameController = new GameController(patchworkAgent, new ArrayList<>(patches));
        assert gameController.getGame().getGameState().getActivePlayer().getName().equals(AiConstants.TRAINING_PLAYER_1_NAME);

        ShowMatch.play(gameController, PRINT);


        //timer.printNanos();

        return 1000 + (PlayerController.calculateScore(gameController.getGame().getGameState().getActivePlayer()) + PlayerController.calculateScore(gameController.getGame().getGameState().getPassivePlayer())) / 2d;

        /*
        Player aiPlayer1 = gameController.getGame().getGameState().getActivePlayer();
        if (aiPlayer1.getName().equals(AiConstants.TRAINING_PLAYER_1_NAME)) {
            return 1000 + PlayerController.calculateScore(aiPlayer1);
        } else {
            Player aiPlayer2 = gameController.getGame().getGameState().getPassivePlayer();
        }
        * TODO Score ausprobieren, der nur sagt wie hoch die Siegchance ist
        *   Dazu alle Einschaetzungen entlang des Weges ufsummieren. Je hoher die des Gewinners von der
        *   des Verleieres entfern ist desto besser*
    }
    */
}
