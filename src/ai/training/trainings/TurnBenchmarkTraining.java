
package ai.training.trainings;

import ai.agents.GameStateAgent;
import ai.agents.NeatPatchworkAgent;
import ai.agents.PatchworkAgent;
import ai.anneat.main.utils.collections.Tuple;
import ai.anneat.models.NeatData;
import ai.anneat.models.generation.Agent;
import ai.anneat.publish.Neat;
import ai.anneat.publish.settings.Settings;
import ai.anneat.publish.task.BenchmarkTask;
import ai.anneat.publish.task.BenchmarkFight;
import ai.anneat.publish.task.Deterministic;
import ai.anneat.publish.task.Task;
import ai.anneat.view.Frame;
import ai.hardcoded.benchmarks.BuyFirstAgent;
import ai.AiConstants;
import ai.hardcoded.benchmarks.StupidAgent;
import ai.training.settings.TurnBenchmarkSettings;
import ai.training.utils.DecoderUtils;
import ai.training.ShowMatch;
import ai.training.utils.TrainingUtils;
import ai.training.save.AiSaveController;
import controller.GameController;
import controller.GameStateController;
import controller.IOController;
import controller.PlayerController;
import model.Difficulty;
import model.GameState;
import model.Patch;
import model.Player;

import java.util.*;

public class TurnBenchmarkTraining {
    private static final String NEAT_DATE_LOAD_NAME = "2021_02_11_0940";

    private static final String NEAT_DATE_SAVE_NAME = "2021_02_11_0940";
    public static final String AGENT_MAP_SAVE_NAME = NEAT_DATE_SAVE_NAME;
    public static final String FUNCTION_SAVE_NAME = NEAT_DATE_SAVE_NAME;

    private static boolean PRINT = false;
    private static final int GENERATIONS = 1000;

    private static final int HURDLE_BATCH_SIZE = 50;
    private static final int MAX_GAME_COUNT = 10 * HURDLE_BATCH_SIZE;
    private static final int STARTING_HURDLE = 180;
    private static final int INITIAL_HURDLE = 240;
    private static final int PER_HURDLE_INCREMENT = 0;

    private static final boolean DETERMINISTIC = true;
    private static final double PATCHES_SWITCH_PERCENT = 0.1d;
    private static final boolean SHOW_BEST = true;
    private static final boolean SET_TASK = false;
    private static final boolean NEW_TRAIN = false;

    private static final List<Patch> patches = (new IOController(null)).importCSV();

    public static void main(String[] args) {
        AiConstants.USE_DECISION_TREE = true;
        train(NEW_TRAIN);
    }

    public static void train(boolean newTrain) {
        Neat neat = newTrain ? newNeat() : continueNeat();

        neat.evolve(GENERATIONS);

        save(neat);

        show(neat);
    }

    public static Neat newNeat() {
        Settings settings = new TurnBenchmarkSettings();
        Task task = createTask(patches);
        return new Neat(settings, task,null, TurnBenchmarkTraining::showAndSave);
    }

    public static Neat continueNeat() {
        NeatData neatData = AiSaveController.loadTurnBenchmarkNeatData(NEAT_DATE_LOAD_NAME);
        if (neatData == null) {
            throw new IllegalStateException();
        }

        Neat neat = new Neat(neatData, TurnBenchmarkTraining::showAndSave);

        neat.setSettings(new TurnBenchmarkSettings());

        if (SET_TASK) {
            neat.setTask(createTask(patches));
        }

        return neat;
    }

    public static void showAndSave(Neat neat) {
        save(neat);

        Agent agent = neat.getBestAgent();

        if (SHOW_BEST) {
            new Frame(agent);
            ShowMatch.play(
                    new GameController(
                            new NeatPatchworkAgent(new GameStateAgent(agent.getCalculator().getFunction())),
                            new StupidAgent(),
                            patches),
                    true,
                    null);
        }
    }

    public static void show(Neat neat) {
        PRINT = true;

        ai.anneat.models.generation.Agent bestAgent = neat.getBestAgent();

        double score = neat.getData().getTask().calculateScore(bestAgent);

        assert score == bestAgent.getVanillaScore();

        System.out.println("Score: "+score);

        new Frame(neat.getBestAgent());

        PRINT = false;
    }

    public static void save(Neat neat) {
        Agent agent = neat.getBestAgent();

        AiSaveController.saveTurnBenchmarkFunction(agent.getCalculator().getFunction(), FUNCTION_SAVE_NAME);

        AiSaveController.saveTurnBenchmarkNeatData(neat.getData(), NEAT_DATE_SAVE_NAME);

        PatchworkAgent patchworkAgent = new NeatPatchworkAgent(new GameStateAgent(agent.getCalculator().getFunction()));
        Map<Difficulty, PatchworkAgent> agentMap = new HashMap<>();
        agentMap.put(Difficulty.HARDCORE, patchworkAgent);
        agentMap.put(Difficulty.MISSIONARY, new StupidAgent());
        agentMap.put(Difficulty.SOFTCORE, new BuyFirstAgent());
        AiSaveController.saveAgentMao(agentMap, AGENT_MAP_SAVE_NAME);
    }

    public static BenchmarkTask<PatchworkAgent> createTask(List<Patch> patches) {
        List<List<Patch>> patchesLists = TrainingUtils.resortedPatches(patches, MAX_GAME_COUNT);

        BenchmarkFight<PatchworkAgent> fight = createFight(patches, patchesLists);

        Map<PatchworkAgent, Double> patchworkAgentWinScoreMap = createPatchworkAgentWinScoreMap();

        return new BenchmarkTask<>(
                DecoderUtils.DECODE_GAME_STATE_LENGTH,
                1,
                DETERMINISTIC ? Deterministic.YES_PERFORMANCE : Deterministic.NO,
                fight,
                patchworkAgentWinScoreMap) {
            @Override
            public void scoredGeneration(List<Agent> agents) {
                super.scoredGeneration(agents);
                if (!DETERMINISTIC) {
                    int smallPatchesCount = (int) (MAX_GAME_COUNT * PATCHES_SWITCH_PERCENT);

                    for (int i = 0; i < smallPatchesCount; i++) {
                        int randomInt = TrainingUtils.random(0, patchesLists.size() - 1);
                        patchesLists.remove(randomInt);
                    }

                    List<List<Patch>> smallPatchesLists = TrainingUtils.resortedPatches(patches, smallPatchesCount);
                    patchesLists.addAll(smallPatchesLists);
                }
            }
        };
    }

    public static Map<PatchworkAgent, Double> createPatchworkAgentWinScoreMap() {
        Map<PatchworkAgent, Double> patchworkAgentWinScoreMap = new HashMap<>();
        patchworkAgentWinScoreMap.put(new StupidAgent(), 100d);
        return patchworkAgentWinScoreMap;
    }

    public static BenchmarkFight<PatchworkAgent> createFight(List<Patch> patches, List<List<Patch>> patchesLists) {
        return (active, benchmarkAgent, benchmarkScore) -> {
            PatchworkAgent scorePatchworkAgent = new NeatPatchworkAgent(new GameStateAgent(active.getFunction()));

            double score = calcScore(
                    patches,
                    scorePatchworkAgent,
                    benchmarkAgent,
                    true,
                    benchmarkScore).getV2();
            if (score < STARTING_HURDLE) {
                return score;
            }

            double hurdle = INITIAL_HURDLE;
            for (int c = 0; c < MAX_GAME_COUNT; c += HURDLE_BATCH_SIZE) {
                double addScore = 0;
                for (int j = c; j < c + HURDLE_BATCH_SIZE; j++) {
                    if (j % 2 == 1) {
                        addScore += calcScore(
                                patchesLists.get(j),
                                scorePatchworkAgent,
                                benchmarkAgent,
                                true,
                                benchmarkScore).getV2();
                    } else {
                        addScore += calcScore(
                                patchesLists.get(j),
                                benchmarkAgent,
                                scorePatchworkAgent,
                                false,
                                benchmarkScore).getV2();
                    }
                }
                score += addScore;

                if (addScore / HURDLE_BATCH_SIZE < hurdle) {
                    break;
                }

                hurdle += PER_HURDLE_INCREMENT;
            }
            //System.out.println("HurdlesTaken: "+hurdlesTaken);
            return score;
        };
    }

    public static Tuple<Boolean, Double> calcScore(List<Patch> patches, PatchworkAgent patchworkAgent1, PatchworkAgent patchworkAgent2, boolean forPlayerOne, double score) {
        GameController gameController = new GameController(
                patchworkAgent1,
                patchworkAgent2,
                patches);
        ShowMatch.play(gameController, PRINT, null);
        if (forPlayerOne) {
            return calcScore(gameController, AiConstants.TRAINING_PLAYER_1_NAME, score);
        } else {
            return calcScore(gameController, AiConstants.TRAINING_PLAYER_2_NAME, score);
        }
    }

    public static Tuple<Boolean, Double> calcScore(GameController gameController, String scoreForName, double benchmarkScore) {
        GameState gameState = gameController.getGame().getGameState();
        Player player = GameStateController.getPlayerByName(gameState, scoreForName);
        /*
        Player enemyPlayer = GameStateController.getOtherPlayerName(gameState, scoreForName);

        double scoreDiff = PlayerController.calculateScore(player) - PlayerController.calculateScore(enemyPlayer);

        if (player.equals(PlayerController.calculateWinner(gameState))) {
            return new Tuple<>(true, 1000 + scoreDiff + benchmarkScore);
        } else {
            return new Tuple<>(false, 1000 + scoreDiff);
        }
        */
        if (player.equals(PlayerController.calculateWinner(gameState))) {
            return new Tuple<>(true, 200 + PlayerController.calculateScore(player) + benchmarkScore);
        } else {
            return new Tuple<>(false, 200d + PlayerController.calculateScore(player));
        }
    }
}
