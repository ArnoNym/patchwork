package ai.training.trainings;

import ai.AiConstants;
import ai.agents.GameStateAgent;
import ai.anneat.main.utils.collections.Tuple;
import ai.anneat.models.generation.Agent;
import ai.anneat.publish.Neat;
import ai.anneat.publish.task.Deterministic;
import ai.anneat.publish.task.SoloTask;
import ai.anneat.publish.task.Task;
import ai.anneat.view.Frame;
import ai.decisionTree.Turn;
import ai.training.save.AiSaveController;
import ai.training.settings.BlanketSettings;
import ai.training.utils.BlanketDecoderUtils;
import ai.training.utils.MatrixUtils;
import ai.training.utils.TrainingUtils;
import controller.BlanketController;
import controller.IOController;
import model.*;

import java.util.*;

public class BlanketTraining {
    /*
    private static final String BLANKET_AGENT_LOAD_NAME = "2021_02_10_0319";
    private static final String BLANKET_AGENT_SAVE_NAME = "2021_02_10_0319";

    public static final double DERIVATION_MULT = 0.1d;
    public static final double RATINGS_AROUND_ZERO_FIVE_MULT = 0.2d;
    public static final double DIFFERENT_RATINGS_MULT = 0.2d;
    public static final int BLANKET_COUNT = 500;
    public static final Double TARGET_SCORE = null;
    public static final double FIRST_HURDLE_VICTORY_POINTS = 140;
    public static final double FIFTY_PERCENT_HURDLE_VICTORY_POINTS = 140;

    public static final int GENERATIONS = 100;

    public static final List<Patch> patches = (new IOController(null)).importCSV();

    private static final boolean DETERMINISTIC = false;
    private static final double PATCHES_SWITCH_PERCENT = 0.1d;
    private static final boolean SHOW_BEST = true;
    private static final boolean SET_TASK = true;
    private static final boolean NEW_TRAIN = false;

    public static void main(String[] args) {
        AiConstants.USE_COMPUTE = false;
        AiConstants.PLACE_PATCH_AND_EXTRA_PATCH_AT_ONCE = false;
        train(NEW_TRAIN);

        /*
        NeatData neatData = AiSaveController.loadBlanketNeatData(BLANKET_AGENT_LOAD_NAME);
        neatData.setTask(null);
        AiSaveController.saveBlanketNeatData(neatData, BLANKET_AGENT_SAVE_NAME);
        *

        /*
        List<List<Patch>> patchesLists = new ArrayList<>();
        patchesLists.add((new IOController(null)).importCSV());
        show(new BlanketAgent(AiSaveController.loadBlanketFunction(BLANKET_AGENT_SAVE_NAME)), patchesLists);
        *
    }

    public static void train(boolean newTrain) {
        Set<Integer> extraPatchAfterPatchIndex = new HashSet<>();
        initExtraPatchesPatchIndexSet(extraPatchAfterPatchIndex);
        List<List<Patch>> patchesLists = TrainingUtils.resortedPatches((new IOController(null)).importCSV(), BLANKET_COUNT);

        Neat neat = newTrain ? newNeat(patchesLists, extraPatchAfterPatchIndex) : continueNeat(patchesLists, extraPatchAfterPatchIndex);

        neat.evolve(GENERATIONS);

        save(neat);

        show(neat.getBestAgent(), patchesLists, extraPatchAfterPatchIndex);
    }

    public static Neat newNeat(List<List<Patch>> patchesLists, Set<Integer> extraPatchAfterPatchIndex) {
        List<Tuple<List<Patch>, Integer>> tupleLists = TrainingUtils.addSpecialTokenIndices(patchesLists);

        Task task = createTask(tupleLists, extraPatchAfterPatchIndex);
        return new Neat(new BlanketSettings(), task, TARGET_SCORE, BlanketTraining::save);
    }

    public static Neat continueNeat(List<List<Patch>> patchesLists, Set<Integer> extraPatchAfterPatchIndex) {
        List<Tuple<List<Patch>, Integer>> tupleLists = TrainingUtils.addSpecialTokenIndices(patchesLists);

        Neat neat = new Neat(AiSaveController.loadBlanketNeatData(BLANKET_AGENT_LOAD_NAME), BlanketTraining::save);
        neat.setSettings(new BlanketSettings());
        if (SET_TASK) {
            Task task = createTask(tupleLists, extraPatchAfterPatchIndex);
            neat.setTask(task);
        }
        return neat;
    }

    public static void save(Neat neat) {
        Agent bestAgent = neat.getBestAgent();
        AiSaveController.saveBlanketNeatData(neat.getData(), BLANKET_AGENT_SAVE_NAME);
        AiSaveController.saveBlanketFunction(bestAgent.getCalculator().getFunction(), BLANKET_AGENT_SAVE_NAME);

        if (SHOW_BEST) {
            show(bestAgent);
        }
    }

    public static void show(Agent agent) {
        List<List<Patch>> patchesLists = new ArrayList<>();
        patchesLists.add(patches);
        Set<Integer> extraPatchAfterPatchIndex = new HashSet<>();
        initExtraPatchesPatchIndexSet(extraPatchAfterPatchIndex);
        show(agent, patchesLists, extraPatchAfterPatchIndex);
    }

    public static void show(Agent agent,
                            List<List<Patch>> patchesLists,
                            Set<Integer> extraPatchAfterPatchIndex) {
        new Frame(agent);
        GameStateAgent gameStateAgent = new GameStateAgent(agent.getCalculator().getFunction());
        show(gameStateAgent, patchesLists, extraPatchAfterPatchIndex);
    }

    public static void show(GameStateAgent gameStateAgent, List<List<Patch>> patchesLists,
                            Set<Integer> extraPatchAfterPatchIndex) {
        List<Tuple<List<Patch>, Integer>> tupleList = TrainingUtils.addSpecialTokenIndices(patchesLists);
        for (Tuple<List<Patch>, Integer> tuple : tupleList) {
            System.out.println("###########################################################################");
            System.out.println("###########################################################################");
            System.out.println("###########################################################################");
            calcScore(gameStateAgent, tuple, extraPatchAfterPatchIndex, true, true);
        }
    }

    private static Task createTask(List<Tuple<List<Patch>, Integer>> tupleList,
                                   Set<Integer> extraPatchAfterPatchIndex) {
        return new SoloTask(BlanketDecoderUtils.DECODE_BLANKET_LENGTH,
                1,
                DETERMINISTIC ? Deterministic.YES_PERFORMANCE : Deterministic.NO,
                function -> {
            GameStateAgent gameStateAgent = new GameStateAgent(function);
            double[] scores = new double[tupleList.size()];
            double score = 0;
            int scoresIndex = 0;
            for (Tuple<List<Patch>, Integer> tuple : tupleList) {
                scores[scoresIndex] = calcScore(gameStateAgent, tuple, extraPatchAfterPatchIndex, false, false).getV2();
                if (scoresIndex == 0) {
                    if (scores[0] < FIRST_HURDLE_VICTORY_POINTS) {
                        break;
                    }
                    score += 1000;
                } else if (scoresIndex == BLANKET_COUNT / 2) {
                    double fiftyPercentScore = Arrays.stream(scores).sum() / (scoresIndex + 1);
                    if (fiftyPercentScore < FIFTY_PERCENT_HURDLE_VICTORY_POINTS) {
                        break;
                    }
                    score += 1000;
                }
                scoresIndex++;
            }
            score += Arrays.stream(scores).sum() / tupleList.size();
            score += DERIVATION_MULT * (100 - TrainingUtils.deviation(scores));
            return score;
        }) {
            @Override
            public void scoredGeneration(List<Agent> agents) {
                super.scoredGeneration(agents);
                if (!DETERMINISTIC) {
                    int smallPatchesCount = (int) (BLANKET_COUNT * PATCHES_SWITCH_PERCENT);

                    for (int i = 0; i < smallPatchesCount; i++) {
                        int randomInt = TrainingUtils.random(0, tupleList.size() - 1);
                        tupleList.remove(randomInt);
                    }

                    tupleList.addAll(TrainingUtils.addSpecialTokenIndices(TrainingUtils.resortedPatches(patches, smallPatchesCount)));

                    initExtraPatchesPatchIndexSet(extraPatchAfterPatchIndex);
                }
            }
        };
    }

    public static Tuple<Double, Double> calcScore(GameStateAgent gameStateAgent,
                                                  Tuple<List<Patch>, Integer> tuple,
                                                  Set<Integer> extraPatchAfterPatchIndex,
                                                  boolean showSteps,
                                                  boolean showResult) {
        tuple = new Tuple<>(new ArrayList<>(tuple.getV1()), tuple.getV2());

        Blanket blanket = new Blanket();
        int boughtPatches = 0;

        Integer specialTokenScore = null;

        Set<Double> ratings = new HashSet<>();

        List<Patch> patches = tuple.getV1();
        int patchIndex1 = 0;
        while (true) {
            int patchIndex2 = (patchIndex1+1) % patches.size();
            int patchIndex3 = (patchIndex1+2) % patches.size();

            Patch patch1 = patches.get(patchIndex1);
            Patch patch2 = patches.get(patchIndex2);
            Patch patch3 = patches.get(patchIndex3);

            Patch patch;
            if (AiConstants.PLACE_PATCH_AND_EXTRA_PATCH_AT_ONCE) {
                boolean placeExtraPatch = extraPatchAfterPatchIndex.contains(boughtPatches);

                Turn turn = gameStateAgent.calcBestPatchTupleAndExtraPatch(
                        blanket,
                        patch1, patch2, patch3,
                        placeExtraPatch);
                if (turn == null) {
                    break;
                }

                assert turn.getBuyAndPlacePatchTuple() != null;
                assert (placeExtraPatch && turn.getExtraPatchTuple() != null) || (!placeExtraPatch && turn.getExtraPatchTuple() == null);

                blanket.addPatchTuple(turn.getBuyAndPlacePatchTuple());
                if (showSteps) {
                    System.out.println(blanket.toString());
                }
                if (turn.getExtraPatchTuple() != null) {
                    assert placeExtraPatch;
                    blanket.addPatchTuple(turn.getExtraPatchTuple());
                    if (showSteps) {
                        System.out.println(blanket.toString());
                    }
                }

                patch = turn.getBuyAndPlacePatchTuple().getPatch();
            } else {
                PatchTuple patchTuple = gameStateAgent.calcBestPatchTuple(blanket, patch1, patch2, patch3);
                if (patchTuple == null) {
                    break;
                }

                blanket.addPatchTuple(patchTuple);
                if (showSteps) {
                    System.out.println(blanket.toString());
                }

                if (extraPatchAfterPatchIndex.contains(boughtPatches)) {
                    PatchTuple extraPatchTuple = gameStateAgent.calcBestExtraPatchTuple(blanket);
                    if (extraPatchTuple == null) {
                        break;
                    }
                    blanket.addPatchTuple(extraPatchTuple);
                    if (showSteps) {
                        System.out.println(blanket.toString());
                    }
                }

                patch = patchTuple.getPatch();
            }

            if (patch2.equals(patch)) {
                patchIndex1 = patchIndex2;
            } else if (patch3.equals(patch)) {
                patchIndex1 = patchIndex3;
            }
            patches.remove(patchIndex1);
            patchIndex1 = (patchIndex1 + 1) % patches.size();

            if (specialTokenScore == null && patchIndex1 < tuple.getV2() && BlanketController.specialTokenReady(blanket.getBoolMatrixRepresentation())) {
                specialTokenScore = 7;
            }

            ratings.add(gameStateAgent.rate(blanket));

            boughtPatches++;
        }

        double massScore = 2 * MatrixUtils.calcMass(blanket.getBoolMatrixRepresentation());
        specialTokenScore = specialTokenScore == null ? 0 : specialTokenScore;
        double comparableRatingsScore = RATINGS_AROUND_ZERO_FIVE_MULT
                * (0.5 -  Math.abs(0.5 - Math.abs(ratings.stream().mapToDouble(Double::doubleValue).average().orElseThrow())));
        double differentRatingsScore = DIFFERENT_RATINGS_MULT * TrainingUtils.deviation(ratings);

        if (showResult) {
            System.out.println(blanket.toString());
            System.out.println("MassScore: "+massScore);
            System.out.println("SpecialTokenScore: "+specialTokenScore);
            System.out.println("ComparableRatingsScore: "+comparableRatingsScore);
            System.out.println("DifferentRatingsScore: "+differentRatingsScore);
        }

        return new Tuple<>(massScore + specialTokenScore, massScore + specialTokenScore + comparableRatingsScore + differentRatingsScore);
    }

    public static void initExtraPatchesPatchIndexSet(Set<Integer> extraPatchesPatchIndexSet) {
        extraPatchesPatchIndexSet.clear();
        while (extraPatchesPatchIndexSet.size() < 5) {
            extraPatchesPatchIndexSet.add(TrainingUtils.random(2, patches.size()-1));
        }
    }
    */
}
