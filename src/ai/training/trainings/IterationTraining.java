package ai.training.trainings;

import ai.AiConstants;
import ai.agents.GameStateAgent;
import ai.agents.NeatAgent;
import ai.agents.NeatPatchworkAgent;
import ai.agents.PatchworkAgent;
import ai.anneat.models.NeatData;
import ai.anneat.models.generation.Agent;
import ai.anneat.publish.Neat;
import ai.anneat.publish.task.*;
import ai.anneat.view.Frame;
import ai.hardcoded.benchmarks.BuyFirstAgent;
import ai.hardcoded.benchmarks.StupidAgent;
import ai.training.ShowMatch;
import ai.training.save.AiSaveController;
import ai.training.settings.IterationSettings;
import ai.training.utils.DecoderUtils;
import ai.training.utils.TrainingUtils;
import controller.GameController;
import controller.IOController;
import controller.PlayerController;
import model.Difficulty;
import model.GameState;
import model.Patch;
import model.Player;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class IterationTraining {
    /*
    public static final int PATCH_LISTS_COUNT = 4;
    public static final int GENERATIONS = 1000;
    public static final Double INITIAL_SCORE = 3900d;

    private static boolean PRINT = false;
    public static final String AGENT_MAP_LOAD_NAME = "resources/agentMap";
    public static final String AGENT_MAP_SAVE_NAME = "resources/agentMap";
    public static final String NEAT_DATE_LOAD_NAME = "neatData";
    public static final String NEAT_DATE_SAVE_NAME = "neatData";

    public static String THIS_ITERATION_NAME(){
        return "_iteration_" + (iteration % 5);
    }
    public static String LAST_ITERATION_NAME(){
        return "_iteration_" + ((iteration-1)% 5);
    }
    public static int iteration = 0;

    public static Neat neat;

    public static void main(String[] args) {
        AiConstants.USE_COMPUTE = false;
        GameStateAgent gameStateAgent = new GameStateAgent(AiSaveController.loadBlanketFunction(AiConstants.BLANKET_AGENT_NAME));
        AtomicReference<Agent> bestAgent = new AtomicReference<>();

        newTrain(gameStateAgent, bestAgent);//continueTrain();//
    }

    public static void newTrain(GameStateAgent gameStateAgent, AtomicReference<Agent> bestAgent) {
        neat = new Neat(
                new IterationSettings(),
                createTask(gameStateAgent),
                null, neat -> save(neat.getData(), gameStateAgent, bestAgent));

        train(neat);

        IOController.save(neat.getData(),NEAT_DATE_SAVE_NAME + THIS_ITERATION_NAME());
    }

    public static void continueTrain(GameStateAgent gameStateAgent, AtomicReference<Agent> bestAgent) {
        NeatData neatData = AiSaveController.loadIterationNeatData(NEAT_DATE_LOAD_NAME + LAST_ITERATION_NAME());
        if (neatData == null) {
            throw new IllegalStateException();
        }

        neat = new Neat(neatData, neat -> save(neat.getData(), gameStateAgent, bestAgent));
        neat.setSettings(new IterationSettings());
        neat.setTask(createTask(gameStateAgent));

        train(neat);

        IOController.save(neatData,NEAT_DATE_SAVE_NAME + THIS_ITERATION_NAME());
    }

    public static void save(NeatData neatData, GameStateAgent gameStateAgent, AtomicReference<Agent> bestAgent) {
        Agent currentBestAgent = neatData.getGeneration().getAgentList().stream().max(Comparator.comparingDouble(Agent::getVanillaScore)).orElseThrow();
        if (currentBestAgent.equals(bestAgent.get())) {
            System.out.println("No improvement...");
            if(bestAgent.get() != null) {
                System.out.println("Target Score: " + bestAgent.get().getVanillaScore());
            }
            System.out.println();
            return;
        }
        System.out.println("New best Agent!");
        if(bestAgent.get() != null) {
            System.out.println("Old Score: " + bestAgent.get().getVanillaScore());
        }
        bestAgent.set(currentBestAgent);
        System.out.println("New Score: " + bestAgent.get().getVanillaScore());

        System.out.println();
        System.out.println("---------------------------------------------------");
        System.out.println("Calculating Iteration " + iteration + "...");
        System.out.println("---------------------------------------------------");

        PatchworkAgent bestPatchworkAgent = new NeatPatchworkAgent(gameStateAgent, new NeatAgent(neat.getBestAgent().getCalculator().getFunction()));

        Map<Difficulty, PatchworkAgent> agentMap = iteration == 0 ? new HashMap<>() : IOController.load(AGENT_MAP_LOAD_NAME + LAST_ITERATION_NAME());
        agentMap.put(Difficulty.HARDCORE, bestPatchworkAgent);
        agentMap.put(Difficulty.MISSIONARY, bestPatchworkAgent);
        agentMap.put(Difficulty.SOFTCORE, new BuyFirstAgent());

        IOController.save(agentMap, AGENT_MAP_SAVE_NAME + THIS_ITERATION_NAME());
        AiSaveController.saveIterationNeatData(neat.getData(), NEAT_DATE_SAVE_NAME + THIS_ITERATION_NAME());

        iteration++;
    }

    public static void train(Neat neat) {
        neat.evolve(GENERATIONS);
        new Frame(neat.getBestAgent());
    }

    public static FightBestTask<PatchworkAgent> createTask(GameStateAgent gameStateAgent) {
        return new FightBestTask<>(
                DecoderUtils.DECODE_GAME_STATE_LENGTH,
                1,
                Deterministic.YES_PERFORMANCE,
                IterationTraining::createFight,
                function -> new NeatPatchworkAgent(gameStateAgent, new NeatAgent(function)),
                new StupidAgent(),
                INITIAL_SCORE);
    }

    public static FightBest<PatchworkAgent> createFight() {
        List<List<Patch>> patchesLists = TrainingUtils.resortedPatches((new IOController(null)).importCSV(), PATCH_LISTS_COUNT);
        assert patchesLists.size() == PATCH_LISTS_COUNT;

        return (fighter, bestFighter) -> {
            double[] scores = new double[2 * patchesLists.size()];
            int scoresIndex = 0;

            for (List<Patch> patchesList : patchesLists) {
                GameController gameController = new GameController(fighter, bestFighter, new ArrayList<>(patchesList));
                ShowMatch.play(gameController, PRINT, null);
                scores[scoresIndex++] = calcScoreForPlayerName(gameController.getGame().getGameState(), AiConstants.TRAINING_PLAYER_1_NAME);
                boolean mayContinue = mayContinue(gameController.getGame().getGameState(), AiConstants.TRAINING_PLAYER_1_NAME);

                gameController = new GameController(bestFighter, fighter, new ArrayList<>(patchesList));
                ShowMatch.play(gameController, PRINT, null);
                scores[scoresIndex++] = calcScoreForPlayerName(gameController.getGame().getGameState(), AiConstants.TRAINING_PLAYER_2_NAME);
                mayContinue = mayContinue || mayContinue(gameController.getGame().getGameState(), AiConstants.TRAINING_PLAYER_2_NAME);

                if (!mayContinue) {
                    break;
                }
            }

            double score = Arrays.stream(scores).sum();
            score -= TrainingUtils.deviation(scores);

            return score;
        };
    }

    public static double calcScoreForPlayerName(@NotNull GameState gameState, @NotNull String playerName) {
        Player activePlayer = gameState.getActivePlayer();
        double score = 0;
        if (activePlayer.getName().equals(playerName)) {
            score += 1000 + PlayerController.calculateScore(activePlayer);
        } else {
            Player player2 = gameState.getPassivePlayer();
            score += 1000 + PlayerController.calculateScore(player2);
        }
        return score;
    }

    public static boolean mayContinue(@NotNull GameState gameState, @NotNull String playerName) {
        Player winner = PlayerController.calculateWinner(gameState);
        if (winner == null) {
            return true;
        }
        return playerName.equals(winner.getName());
    }
    */
}

