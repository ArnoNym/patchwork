package ai.training.utils;

import ai.AiConstants;
import ai.agents.PatchworkAgent;
import ai.anneat.main.utils.collections.Tuple;
import ai.training.ShowMatch;
import controller.GameController;
import controller.IOController;
import controller.PlayerController;
import model.*;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class TrainingUtils {

    /**
     * @author Florian
     */
    public static double deviation(double... values) {
        if (values.length == 0) {
            throw new IllegalArgumentException();
        }
        double average = Arrays.stream(values).sum() / values.length;
        double sum = 0;
        for (double value : values) {
            sum += Math.pow(value - average, 2);
        }
        return Math.sqrt(sum / values.length);
    }

    /**
     * @author Florian
     */
    public static double deviation(Collection<Double> doubles) {
        double average = doubles.stream().mapToDouble(Double::doubleValue).average().orElseThrow();
        double sum = 0;
        for (Double value : doubles) {
            sum += Math.pow(value - average, 2);
        }
        return Math.sqrt(sum / doubles.size());
    }

    /**
     * @return A list of lists of patch tuples that are ordered randomly.
     * No two patchLists have the same order.
     * The initial patchList is not included.
     * @author Leon
     */
    public static List<List<Patch>> resortedPatches(List<Patch> patchList, int count) {
        Set<List<Patch>> resortedPatchesSet = new HashSet<>();
        resortedPatchesSet.add(patchList);
        List<List<Patch>> resortedPatches = new ArrayList<>();
        while (resortedPatches.size() < count) {
            patchList = new ArrayList<>(patchList);
            Collections.shuffle(patchList);
            if (resortedPatchesSet.add(patchList)) {
                resortedPatches.add(patchList);
            }
        }
        return resortedPatches;
    }

    public static List<Tuple<List<Patch>, Integer>> addSpecialTokenIndices(List<List<Patch>> patchesLists) {
        return patchesLists.stream()
                .map(pl -> new Tuple<>(pl, random(0, pl.size()-1)))
                .collect(Collectors.toList());
    }

    public static int random(int low, int high) {
        return (new Random()).nextInt(high-low) + low;
    }

    public static List<Patch> resortPatches_(List<Patch> patches) {
        return patches;
    }

    public static List<Patch> resortPatches_0(List<Patch> patches) {
        List<Patch> reversedPatches = new ArrayList<>(patches);
        Collections.reverse(reversedPatches);
        return reversedPatches;
    }

    public static List<Patch> resortPatches_1(List<Patch> patches) {
        patches = new ArrayList<>(patches);
        List<Patch> addPatches = new ArrayList<>();
        addPatches.add(patches.remove(2));
        addPatches.add(patches.remove(3));
        addPatches.add(patches.remove(3));
        addPatches.add(patches.remove(6));
        addPatches.add(patches.remove(9));
        addPatches.add(patches.remove(9));
        addPatches.add(patches.remove(9));
        addPatches.add(patches.remove(10));
        patches.addAll(addPatches);
        return patches;
    }

    public static List<Patch> resortPatches_2(List<Patch> patches) {
        patches = new ArrayList<>(patches);
        List<Patch> addPatches = new ArrayList<>();
        addPatches.add(patches.remove(1));
        addPatches.add(patches.remove(1));
        addPatches.add(patches.remove(1));
        addPatches.add(patches.remove(5));
        addPatches.add(patches.remove(5));
        addPatches.add(patches.remove(8));
        addPatches.add(patches.remove(8));
        addPatches.add(patches.remove(11));
        addPatches.add(patches.remove(11));
        addPatches.add(patches.remove(11));
        patches.addAll(addPatches);
        return patches;
    }

    public static List<Patch> resortPatches_3(List<Patch> patches) {
        patches = new ArrayList<>(patches);
        List<Patch> addPatches = new ArrayList<>();
        addPatches.add(patches.remove(1));
        addPatches.add(patches.remove(7));
        addPatches.add(patches.remove(7));
        addPatches.add(patches.remove(7));
        addPatches.add(patches.remove(7));
        addPatches.add(patches.remove(9));
        addPatches.add(patches.remove(9));
        addPatches.add(patches.remove(13));
        addPatches.add(patches.remove(13));
        addPatches.add(patches.remove(13));
        addPatches.add(patches.remove(14));
        addPatches.add(patches.remove(14));
        patches.addAll(addPatches);
        return patches;
    }

    public static List<Patch> createPatches() {
        IOController io = new IOController(null);
        List<Patch> patches = io.importCSV();
        Collections.shuffle(patches);
        return patches;
    }

    public static List<List<Patch>> createPatchesLists() {
        List<Patch> patches = createPatches();
        List<List<Patch>> patchesList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            List<Patch> shuffledPatches = new ArrayList<>(patches);
            Collections.shuffle(shuffledPatches);
            patchesList.add(shuffledPatches);
        }
        return patchesList;
    }

    /*
    public static List<Tuple<GameState, GameState>> createClassifications() {
        List<Tuple<GameState, GameState>> classifications = new ArrayList<>();
        classifications.add()
    }

    public static Tuple<GameState, GameState> createClassification_1() {
        GameState gameState = new GameState()
    }
    */

    public static Boolean isAgentOneWinner(List<Patch> patches, PatchworkAgent patchworkAgent1, PatchworkAgent patchworkAgent2) {
        GameController gameController = new GameController(
                patchworkAgent1,
                patchworkAgent2,
                patches);
        ShowMatch.play(gameController, false, null);
        Player winner = PlayerController.calculateWinner(gameController.getGame().getGameState());
        if (winner == null) {
            return null;
        }
        return AiConstants.TRAINING_PLAYER_1_NAME.equals(winner.getName());
    }

    public static Tuple<Integer, Integer> calcWinsDrawsTuple(List<List<Patch>> patchLists, PatchworkAgent patchworkAgent1, PatchworkAgent patchworkAgent2) {
        int wins = 0;
        int draws = 0;
        for (List<Patch> patches : patchLists) {
            Boolean agentOneWins = TrainingUtils.isAgentOneWinner(patches, patchworkAgent1, patchworkAgent2);
            if (agentOneWins == null) {
                draws++;
            } else if (agentOneWins) {
                wins++;
            }
            agentOneWins = TrainingUtils.isAgentOneWinner(patches, patchworkAgent2, patchworkAgent1);
            if (agentOneWins == null) {
                draws++;
            } else if (!agentOneWins) {
                wins++;
            }
        }
        return new Tuple<>(wins, draws);
    }

    public static double meanAbsEuclideanDistanceBetweenPatches(Blanket blanket) {
        Set<VecTwo> allVectors = new HashSet<>();
        double res = 0;
        for (PatchTuple patchTuple : blanket.getPatchTuples()){
            int rowSum = 0;
            int colSum = 0;
            for (VecTwo vecTwo : patchTuple.getVectorList()) {
                rowSum += vecTwo.getRow();
                colSum += vecTwo.getCol();
            }
            rowSum /= patchTuple.getVectorList().size();
            colSum /= patchTuple.getVectorList().size();
            allVectors.add(new VecTwo(rowSum, colSum));
        }
        int count = 0;
        for (VecTwo vec1 : allVectors){
            for (VecTwo vec2 : allVectors){
                res += VecTwo.euclidanDistance(vec1, vec2);
                count++;
            }
        }
        return res / count;
    }
}
