package ai.training.utils;

import java.util.Arrays;

public class MatrixUtils {
    public static int calcEdges(boolean[][] matrix) {
        return calcInnerEdges(matrix) + calcOuterEdges(matrix);
    }

    public static int calcInnerEdges(boolean[][] matrix) {
        int rowsCount = matrix.length;
        int columnCount = matrix[0].length;

        int edges = 0;
        for (boolean[] row : matrix) {
            boolean previous = row[0];
            for (int colIndex = 1; colIndex < columnCount; colIndex++) {
                boolean current = row[colIndex];
                if (previous != current) {
                    edges++;
                }
                previous = current;
            }
        }
        for (int colIndex = 0; colIndex < columnCount; colIndex++) {
            boolean previous = matrix[0][colIndex];
            for (int rowIndex = 1; rowIndex < rowsCount; rowIndex++) {
                boolean current = matrix[rowIndex][colIndex];
                if (previous != current) {
                    edges++;
                }
                previous = current;
            }
        }
        return edges;
    }

    public static int calcOuterEdges(boolean[][] matrix) {
        int rowsCount = matrix.length;
        int columnCount = matrix[0].length;

        int edges = 0;
        for (boolean[] row : matrix) {
            if (!row[0]) edges++;
            if (!row[columnCount-1]) edges++;
        }
        for (int colIndex = 0; colIndex < columnCount; colIndex++) {
            if (!matrix[0][colIndex]) edges++;
            if (!matrix[rowsCount-1][colIndex]) edges++;
        }
        return edges;
    }

    public static int calcTopOuterEdges(boolean[][] matrix) {
        int columnCount = matrix[0].length;
        int edges = 0;
        for (int colIndex = 0; colIndex < columnCount; colIndex++) {
            if (!matrix[0][colIndex]) edges++;
        }
        return edges;
    }

    public static int calcBottomOuterEdges(boolean[][] matrix) {
        int rowsCount = matrix.length;
        int columnCount = matrix[0].length;
        int edges = 0;
        for (int colIndex = 0; colIndex < columnCount; colIndex++) {
            if (!matrix[rowsCount-1][colIndex]) edges++;
        }
        return edges;
    }

    public static int calcLeftOuterEdges(boolean[][] matrix) {
        int edges = 0;
        for (boolean[] row : matrix) {
            if (!row[0]) edges++;
        }
        return edges;
    }

    public static int calcRightOuterEdges(boolean[][] matrix) {
        int columnCount = matrix[0].length;
        int edges = 0;
        for (boolean[] row : matrix) {
            if (!row[columnCount-1]) edges++;
        }
        return edges;
    }

    public static int calcAvgLeftHeight(boolean[][] matrix) {
        int height = 0;
        for (boolean[] row : matrix) {
            for (boolean value : row) {
                if (value) {
                    height++;
                } else {
                    break;
                }
            }
        }
        return height / matrix.length;
    }

    public static int calcAvgLeftBumpiness(boolean[][] matrix) {
        int bumpiness = 0;
        int lastHeight = 0;
        for (int i = 0; i < matrix.length; i++) {
            boolean[] row = matrix[i];

            int currentHeight = 0;
            for (boolean value : row) {
                if (value) {
                    currentHeight++;
                } else {
                    break;
                }
            }
            if (i != 0) {
                bumpiness += Math.abs(lastHeight - currentHeight);
            }
            lastHeight = currentHeight;
        }
        return bumpiness / (9 - 2);
    }

    public static int calcMass(boolean[][] matrix) {
        int mass = 0;
        for (boolean[] row : matrix) {
            for (boolean value : row) {
                mass += value ? 1 : 0;
            }
        }
        return mass;
    }

    public static int calcMass(boolean[][] matrix, int startRowIndex, int startColIndex, int rowCount, int colCount) {
        int mass = 0;
        for (int rowIndex = startRowIndex; rowIndex < startRowIndex + rowCount; rowIndex++) {
            boolean[] row = matrix[rowIndex];
            for (int colIndex = startColIndex; colIndex < startColIndex + colCount; colIndex++) {
                if (row[colIndex]) {
                    mass += 1;
                }
            }
        }
        return mass;
    }

    public static boolean[][] copy(boolean[][] matrix) {
        return Arrays.stream(matrix).map(boolean[]::clone).toArray(boolean[][]::new);
    }

    public static int calcCloseTo7Times7(boolean[][] matrix) {
        int rowCount = matrix.length - 7 + 1;
        int colCount = matrix[0].length - 7 + 1;

        assert rowCount >= 1 && colCount >= 1;

        int maxMass = 0;
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            for (int colIndex = 0; colIndex < colCount; colIndex++) {
                maxMass = Math.max(maxMass, calcMass(matrix, rowIndex, colIndex, 7, 7));
            }
        }
        return maxMass;
    }

    public static int calcCloseTo7Times7InCorner(boolean[][] matrix) {
        int rowCount = matrix.length - 7 + 1;
        int colCount = matrix[0].length - 7 + 1;
        assert rowCount >= 1 && colCount >= 1;
        return calcMass(matrix, 0, 0, 7, 7);
    }

    public static int calcEmptyComponents(boolean[][] matrix) {
        for (boolean[] row : matrix) {
            int columnCount = row.length;
            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                row[columnIndex] = !row[columnIndex];
            }
        }
        return calcFullComponents(matrix);
    }

    public static int calcFullComponents(boolean[][] matrix) {
        int connectedTrues = 0;
        for (int rowIndex = 0; rowIndex < matrix.length; rowIndex++) {
            boolean[] row = matrix[rowIndex];
            int columnCount = row.length;
            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                if (row[columnIndex]) {
                    connectedTrues += 1;
                    setConnectedTrueToFalse(rowIndex, columnIndex, matrix);
                }
            }
        }
        return connectedTrues;
    }

    private static void setConnectedTrueToFalse(int first, int second, boolean[][] matrix) {
        if (second < 0 || first < 0 || first >= matrix.length) {
            return;
        }
        boolean[] inner = matrix[first];
        if (second >= inner.length) {
            return;
        }
        if (inner[second]) {
            inner[second] = false;
            setConnectedTrueToFalse(first + 1, second, matrix);
            setConnectedTrueToFalse(first, second + 1, matrix);
            setConnectedTrueToFalse(first - 1, second, matrix);
            setConnectedTrueToFalse(first, second - 1, matrix);
        }
    }

    public static void print(boolean[][] matrix) {
        StringBuilder s = new StringBuilder();
        for (boolean[] row : matrix) {
            for (boolean b : row) {
                s.append(b ? "O " : "- ");
            }
            s.append("\n");
        }
        System.out.println(s);
    }

    public static int calcFieldOne(boolean[][] matrix) {
        int mass = 0;
        for (boolean[] row : matrix) {
            for (int i = 0; i < 6; i++) {
                if (row[i]) {
                    mass++;
                }
            }
        }
        return mass / (matrix.length * 6);
    }

    public static int calcFieldTwo(boolean[][] matrix) {
        int mass = 0;
        for (int j = 5; j < matrix.length; j++) {
            boolean[] row = matrix[j];
            for (int i = 6; i < 9; i++) {
                if (row[i]) {
                    mass++;
                }
            }
        }
        return mass / (4 * 3);
    }
}
