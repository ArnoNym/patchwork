package ai.training.utils;

import Resources.Constants;
import ai.AiConstants;
import ai.agents.AiGameState;
import controller.IOController;
import controller.PlayerController;
import model.*;
import org.jetbrains.annotations.NotNull;

import java.lang.invoke.ConstantBootstraps;
import java.util.ArrayList;
import java.util.List;

public abstract class DecoderUtils {
    public static final int DECODE_GAME_STATE_LENGTH = decodeGameState(
            new AiGameState(createDummyGameState(), AiConstants.TRAINING_PLAYER_1_NAME)).length;

    public static final int MAX_INCOME = 30;
    public static final int MAX_SCORE = 60;
    public static final int MIN_SCORE = -162;
    public static final int MAX_BLANKET_MASS = 81;

    /**
     * @return decoded gameState
     * @author Leon
     */
    public static double[] decodeGameState(@NotNull AiGameState aiGameState) {
        List<Double> input = new ArrayList<>();

        Player player = aiGameState.getPlayer();
        Player enemyPlayer = aiGameState.getEnemyPlayer();

        input.add(decodePassButtons(player, enemyPlayer));

        input.add(decodeIncome(player));

        input.add(decodeTime(player));

        input.add(decodeVictoryPoints(player));

        input.add(decodeButtons(player));

        BlanketDecoderUtils.decodeBlanket(input, player.getBlanket(), player.hasSpecialToken());

        return listToArray(input);
    }

    // =================================================================================================================

    public static double decodeButtons(Player player) {
        return clampBetweenZeroAndOne(player.getButtonCount(), 0, MAX_BLANKET_MASS); // Intentional same as BlanketMass
    }

    public static double decodeMass(boolean[][] matrix) {
        return clampBetweenZeroAndOne(MatrixUtils.calcMass(matrix), 0, MAX_BLANKET_MASS);
    }

    public static double decodeVictoryPoints(Player player) {
        return clampBetweenZeroAndOne(PlayerController.calculateScore(player), MIN_SCORE, MAX_SCORE);
    }

    public static double decodeNearSpecialPatch(boolean[][] matrix) {
        return MatrixUtils.calcCloseTo7Times7(matrix);
    }

    public static double decodeNearSpecialPatchInCorner(boolean[][] matrix) {
        return MatrixUtils.calcCloseTo7Times7InCorner(matrix);
    }

    public static double decodeSpecialPatch(boolean hasSpecialPatch) {
        if (hasSpecialPatch) {
            return 1; // Already has active patch
        } else {
            return 0; // Enemy has ExtraPatch
        }
    }

    public static double decodeTime(Player player) {
        return PlayerController.calcTime(player) / (Constants.FIELDS_COUNT - 1d);
    }

    public static double decodeIncome(Player player) {
        return (double) PlayerController.calcIncomeButtonsLeftCount(player) * player.getBlanket().calcIncome() / MAX_BLANKET_MASS; // Intentional same as BlanketMass
    }

    public static double decodePassButtons(Player player, Player enemyPlayer) {
        int position = player.getPos();
        int enemyPosition = enemyPlayer.getPos();
        if (position <= enemyPosition) {
            return (double) PlayerController.calcPassButtons(player, enemyPlayer) / MAX_BLANKET_MASS; // Intentional same as BlanketMass
        } else {
            return 0;
        }
    }

    public static double clampBetweenZeroAndOne(double value, double min, double max) {
        assert !Double.isNaN(value);
        assert !Double.isNaN(min);
        assert !Double.isNaN(max);
        double below = max - min;
        assert below > 0;
        return (value - min) / (max - min);
    }

    /*
    public static double decodeScoreWithOnlyPass(Player player) {
        double score = decodeScore(player);
        score +=
    }
    */

    // =================================================================================================================

    /**
     * @author Leon
     */
    public static double[] listToArray(@NotNull List<Double> list) {
        assert !list.isEmpty();
        double[] array = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
            assert !Double.isNaN(array[i]);
        }
        return array;
    }

    public static GameState createDummyGameState() {
        Player player1 = new Player(AiConstants.TRAINING_PLAYER_1_NAME, new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP));
        Player player2 = new Player(AiConstants.TRAINING_PLAYER_2_NAME, new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP));
        return new GameState(player1, player2, new GameBoard((new IOController(null)).importCSV()));
    }
}
