package ai.training.utils;

import Resources.Constants;
import ai.hardcoded.raphael.StupidAI;
import controller.BlanketController;
import model.*;

import java.util.ArrayList;
import java.util.List;

public class BlanketDecoderUtils {

    /**
     * @author Leon
     */
    public static void decodeBlanket(List<Double> input, Blanket blanket, boolean hasSpecialToken) {
        boolean[][] matrix = blanket.getBoolMatrixRepresentation();

        input.add(decodeEuclideanDistance(blanket));

        input.add(decodeNearSpecialPatch(matrix));
        input.add(decodeSpecialPatch(hasSpecialToken));

        input.add(decodeEmptyComponents(matrix));

        input.add(decodeInnerEdges(matrix));

        input.add(decodeOuterEdges(matrix));

        input.add(decodeLeftHeight(matrix));

        input.add(decodeLeftBumpiness(matrix));

        input.add(decodeMass(matrix));
    }

    // =================================================================================================================

    public static double decodeField1(boolean[][] matrix) {
        return MatrixUtils.calcFieldOne(matrix);
    }

    public static double decodeField2(boolean[][] matrix) {
        return MatrixUtils.calcFieldTwo(matrix);
    }

    public static double decodeLeftHeight(boolean[][] matrix) {
        return (double) MatrixUtils.calcAvgLeftHeight(matrix) / 9;
    }

    public static double decodeLeftBumpiness(boolean[][] matrix) {
        return (double) MatrixUtils.calcAvgLeftBumpiness(matrix) / 9;
    }

    public static double decodeMass(boolean[][] matrix) {
        return (double) MatrixUtils.calcMass(matrix) / DecoderUtils.MAX_BLANKET_MASS;
    }

    public static double decodeEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcEdges(matrix) / Constants.BLANKET_MAX_EDGES_COUNT;
    }

    public static double decodeInnerEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcInnerEdges(matrix) / Constants.BLANKET_MAX_INNER_EDGES_COUNT;
    }

    public static double decodeOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcOuterEdges(matrix) / Constants.BLANKET_MAX_OUTER_EDGES_COUNT;
    }

    public static double decodeTopOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcTopOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeLeftOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcLeftOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeRightOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcRightOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeBottomOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcBottomOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeEmptyComponents(boolean[][] matrix) {
        return (double) MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)) / Constants.BLANKET_MAX_COMPONENT_COUNT;
    }

    public static double decodeFullComponents(boolean[][] matrix) {
        return (double) MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)) / Constants.BLANKET_MAX_COMPONENT_COUNT;
    }

    public static double decodeMiddleField(boolean[][] matrix) {
        return (matrix[matrix.length / 2][matrix.length / 2] ? 1d : 0) / Constants.BLANKET_MAX_COMPONENT_COUNT;
    }

    public static void decodeEdges(List<Double> input, boolean[][] matrix) {
        int first = 0;
        int last = matrix.length - 1;
        input.add((matrix[last][last] ? 1d : 0));
        input.add((matrix[last][first] ? 1d : 0));
        input.add((matrix[first][last] ? 1d : 0));
        input.add((matrix[first][first] ? 1d : 0));
    }

    public static double decodeNearSpecialPatch(boolean[][] matrix) {
        return (double) MatrixUtils.calcCloseTo7Times7(matrix) / (7*7);
    }

    public static double decodeSpecialPatch(boolean hasSpecialPatch) {
        if (hasSpecialPatch) {
            return 1; // Already has active patch
        } else {
            return 0; // Enemy has ExtraPatch
        }
    }

    public static double decodeEuclideanDistance(Blanket blanket) {
        double meanAbsEuclideanDistance = StupidAI.meanAbsEuclideanDistance(blanket);
        if (Double.isNaN(meanAbsEuclideanDistance)) {
            return 0;
        }
        return clampBetweenZeroAndOne(meanAbsEuclideanDistance, 1, 12);
    }

    public static double decodeEuclideanDistanceBetweenPatches(Blanket blanket) {
        double meanAbsEuclideanDistance = TrainingUtils.meanAbsEuclideanDistanceBetweenPatches(blanket);
        if (Double.isNaN(meanAbsEuclideanDistance)) {
            return 0;
        }
        return clampBetweenZeroAndOne(meanAbsEuclideanDistance, 1, 12);
    }

    public static double clampBetweenZeroAndOne(double value, double min, double max) {
        assert !Double.isNaN(value);
        assert !Double.isNaN(min);
        assert !Double.isNaN(max);
        double below = max - min;
        assert below > 0;
        return (value - min) / (max - min);
    }
}
