package ai.training.utils;

import Resources.Constants;
import ai.hardcoded.raphael.StupidAI;
import controller.BlanketController;
import controller.IOController;
import controller.PlayerController;
import model.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class DecoderUtilsSuggestion {
    public static final int DECODE_GAME_STATE_LENGTH = decodeGameState(createDummyGameState(), 0).length;
    public static final int DECODE_BLANKET_LENGTH = decodeBlanket(createDummyGameState().getActivePlayer().getBlanket()).length;

    public static final int MAX_INCOME = 30;
    public static final int MAX_SCORE = 60;
    public static final int MIN_SCORE = -81;
    public static final int MAX_BUTTON_COUNT = 100;

    // gameState =======================================================================================================

    /**
     * @return decoded gameState
     * @author Leon
     */
    public static double[] decodeGameState(@NotNull GameState gameState, double blanketRating) {
        List<Double> input = new ArrayList<>();

        Player player = gameState.getActivePlayer();
        boolean[][] matrix = player.getBlanket().getBoolMatrixRepresentation();
        Player enemyPlayer = gameState.getPassivePlayer();

        input.add(decodePassButtons(player, enemyPlayer));

        input.add(decodeIncome(player));

        input.add(decodeTime(player));

        input.add(decodeButtons(player));

        input.add(decodeMass(matrix));

        input.add(blanketRating);

        return listToArray(input);
    }

    /**
     * @return decoded blanket
     * @author Leon
     */
    public static double[] decodeBlanket(Blanket blanket) {
        List<Double> input = new ArrayList<>();

        boolean[][] matrix = blanket.getBoolMatrixRepresentation();

        input.add(decodeEuclideanDistance(blanket));

        boolean specialTokenReady = BlanketController.specialTokenReady(matrix);
        input.add(decodeSpecialPatch(specialTokenReady ? true : null, matrix));

        //input.add(decodeFullComponents(matrix));

        input.add(decodeEmptyComponents(matrix));

        input.add(decodeInnerEdges(matrix));

        input.add(decodeOuterEdges(matrix));
        /*
        input.add(decodeTopOuterEdges(matrix));
        input.add(decodeBottomOuterEdges(matrix));
        input.add(decodeLeftOuterEdges(matrix));
        input.add(decodeRightOuterEdges(matrix));
        */

        //input.add(decodeMiddleField(matrix));

        return listToArray(input);
    }

    // =================================================================================================================

    public static double decodeButtons(Player player) {
        return (double) player.getButtonCount() / MAX_BUTTON_COUNT;
    }

    public static double decodeMass(boolean[][] matrix) {
        return (double) MatrixUtils.calcMass(matrix) / (9 * 9);
    }

    public static double decodeEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcEdges(matrix) / Constants.BLANKET_MAX_EDGES_COUNT;
    }

    public static double decodeInnerEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcInnerEdges(matrix) / Constants.BLANKET_MAX_INNER_EDGES_COUNT;
    }

    public static double decodeOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcOuterEdges(matrix) / Constants.BLANKET_MAX_OUTER_EDGES_COUNT;
    }

    public static double decodeTopOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcTopOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeLeftOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcLeftOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeRightOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcRightOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeBottomOuterEdges(boolean[][] matrix) {
        return (double) MatrixUtils.calcBottomOuterEdges(matrix) / Constants.BLANKET_SIDE_EDGES_COUNT;
    }

    public static double decodeEmptyComponents(boolean[][] matrix) {
        return (double) MatrixUtils.calcEmptyComponents(MatrixUtils.copy(matrix)) / Constants.BLANKET_MAX_COMPONENT_COUNT;
    }

    public static double decodeFullComponents(boolean[][] matrix) {
        return (double) MatrixUtils.calcFullComponents(MatrixUtils.copy(matrix)) / Constants.BLANKET_MAX_COMPONENT_COUNT;
    }

    public static double decodeMiddleField(boolean[][] matrix) {
        return (matrix[matrix.length / 2][matrix.length / 2] ? 1d : 0) / Constants.BLANKET_MAX_COMPONENT_COUNT;
    }

    public static void decodeEdges(List<Double> input, boolean[][] matrix) {
        int first = 0;
        int last = matrix.length - 1;
        input.add((matrix[last][last] ? 1d : 0));
        input.add((matrix[last][first] ? 1d : 0));
        input.add((matrix[first][last] ? 1d : 0));
        input.add((matrix[first][first] ? 1d : 0));
    }

    public static double decodeScore(Player player) {
        return clampBetweenZeroAndOne(PlayerController.calculateScore(player), MIN_SCORE, MAX_SCORE);
    }

    public static double decodeSpecialPatch(Boolean hasSpecialPatch, boolean[][] matrix) {
        if (hasSpecialPatch == null) {
            return (double) MatrixUtils.calcCloseTo7Times7(matrix) / (7*7);
        } else {
            if (hasSpecialPatch) {
                return 1; // Already has active patch
            } else {
                return -1; // Enemy has ExtraPatch
            }
        }
    }

    public static double decodeTime(Player player) {
        return (double) PlayerController.calcTime(player) / (Constants.FIELDS_COUNT - 1);
    }

    public static double decodeIncome(Player player) {
        return (double) PlayerController.calcIncomeButtonsLeftCount(player)
                / Constants.BUTTON_FIELD_COUNT
                * player.getBlanket().calcIncome()
                / MAX_INCOME;
    }

    public static double decodePassButtons(Player player, Player enemyPlayer) {
        int position = player.getPos();
        int enemyPosition = enemyPlayer.getPos();
        if (position <= enemyPosition) {
            return (double) PlayerController.calcPassButtons(player, enemyPlayer) / (Constants.FIELDS_COUNT - 1);
        } else {
            return 0;
        }
    }

    public static double decodeEuclideanDistance(Blanket blanket) {
        double meanAbsEuclideanDistance = StupidAI.meanAbsEuclideanDistance(blanket);
        if (Double.isNaN(meanAbsEuclideanDistance)) {
            return 1;
        }
        return clampBetweenZeroAndOne(StupidAI.meanAbsEuclideanDistance(blanket), 1.4, 12);
    }

    public static double clampBetweenZeroAndOne(double value, double min, double max) {
        assert !Double.isNaN(value);
        assert !Double.isNaN(min);
        assert !Double.isNaN(max);
        double below = max - min;
        assert below > 0;
        return (value - min) / (max - min);
    }

    // =================================================================================================================

    /**
     * @author Leon
     */
    private static double[] listToArray(@NotNull List<Double> list) {
        assert !list.isEmpty();
        double[] array = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
            assert !Double.isNaN(array[i]);
        }
        return array;
    }

    private static GameState createDummyGameState() {
        Player player1 = new Player("a", new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP));
        Player player2 = new Player("b", new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP));
        return new GameState(player1, player2, new GameBoard((new IOController(null)).importCSV()));
    }
}
