package ai.training.save;

import ai.agents.PatchworkAgent;
import ai.anneat.main.SerializableFunction;
import ai.anneat.models.NeatData;
import controller.IOController;
import model.Difficulty;

import java.util.Map;

public abstract class AiSaveController {
    private static String getBlanketFunctionPath(String name) {
        return "resources/blanket_function_"+name+".ser";
    }
    private static String getBlanketNeatDataPath(String name) {
        return "resources/blanket_neatData_"+name+".ser";
    }

    private static String getIterationNeatDataPath(String name) {
        return "resources/iteration_neatData_"+name+".ser";
    }

    private static String getTurnBenchmarkNeatDataPath(String name) {
        return "resources/turn_benchmark_neatData_"+name+".ser";
    }
    private static String getTurnBenchmarkFunctionPath(String name) {
        return "resources/turn_benchmark_function_"+name+".ser";
    }

    private static String getAgentMapPath(String name) {
        return "resources/agentMap_"+name+".ser";
    }

    //==================================================================================================================

    public static void saveIterationNeatData(NeatData neatData, String name) {
        IOController.save(neatData, getIterationNeatDataPath(name));
    }
    public static NeatData loadIterationNeatData(String name) {
        return IOController.load(getIterationNeatDataPath(name));
    }

    public static void saveBlanketFunction(SerializableFunction<double[], double[]> blanketFunction, String name) {
        IOController.save(blanketFunction, getBlanketFunctionPath(name));
    }
    public static SerializableFunction<double[], double[]> loadBlanketFunction(String name) {
        return IOController.load(getBlanketFunctionPath(name));
    }

    public static void saveBlanketNeatData(NeatData neatData, String name) {
        IOController.save(neatData, getBlanketNeatDataPath(name));
    }

    public static NeatData loadBlanketNeatData(String name) {
        return IOController.load(getBlanketNeatDataPath(name));
    }

    public static void saveTurnBenchmarkNeatData(NeatData neatData, String name) {
        IOController.save(neatData, getTurnBenchmarkNeatDataPath(name));
    }
    public static NeatData loadTurnBenchmarkNeatData(String name) {
        return IOController.load(getTurnBenchmarkNeatDataPath(name));
    }

    public static void saveTurnBenchmarkFunction(SerializableFunction<double[], double[]> function, String name) {
        IOController.save(function, getTurnBenchmarkFunctionPath(name));
    }
    public static SerializableFunction<double[], double[]> loadTurnBenchmarkFunction(String name) {
        return IOController.load(getTurnBenchmarkFunctionPath(name));
    }

    public static void saveAgentMao(Map<Difficulty, PatchworkAgent> agentMap, String name) {
        IOController.save(agentMap, getAgentMapPath(name));
    }
    public static Map<Difficulty, PatchworkAgent> loadAgentMap(String name) {
        return IOController.load(getAgentMapPath(name));
    }

    public static void saveAgentMao(Map<Difficulty, PatchworkAgent> agentMap) {
        IOController.save(agentMap, "resources/agentMap");
    }
    public static Map<Difficulty, PatchworkAgent> loadAgentMap() {
        return IOController.load("resources/agentMap");
    }
}
