package ai.training.save;

import ai.agents.NeatPatchworkAgent;
import ai.anneat.models.NeatData;
import model.GameState;

import java.io.Serializable;
import java.util.function.Function;

public class AiSave implements Serializable {
    private Function<GameState, double[]> decoderUtils;
    private NeatData neatData;
    private NeatPatchworkAgent neatPatchworkAgent;

    public AiSave(Function<GameState, double[]> decoderUtils, NeatData neatData, NeatPatchworkAgent neatPatchworkAgent) {
        this.decoderUtils = decoderUtils;
        this.neatData = neatData;
        this.neatPatchworkAgent = neatPatchworkAgent;
    }

    public Function<GameState, double[]> getDecoderUtils() {
        return decoderUtils;
    }

    public void setDecoderUtils(Function<GameState, double[]> decoderUtils) {
        this.decoderUtils = decoderUtils;
    }

    public NeatData getNeatData() {
        return neatData;
    }

    public void setNeatData(NeatData neatData) {
        this.neatData = neatData;
    }

    public NeatPatchworkAgent getNeatPatchworkAgent() {
        return neatPatchworkAgent;
    }

    public void setNeatPatchworkAgent(NeatPatchworkAgent neatPatchworkAgent) {
        this.neatPatchworkAgent = neatPatchworkAgent;
    }
}
