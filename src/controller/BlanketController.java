package controller;
import Resources.Constants;
import ai.AiConstants;
import ai.anneat.main.utils.collections.Tuple;
import ai.decisionTree.Turn;
import model.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Controls the blanket
 * @author Raphael
 */
public class BlanketController {
	private GameController gameController;

	/**
	 * Default-Constructor
	 * @param gameController the main-GameController
	 */
	public BlanketController(GameController gameController){
		this.gameController = gameController;
	}

	/**
	 * Inserts the given patchTuple into the current blanket of the active player.
	 * @param patchTuple to insert.
	 * @author Raphael
	 */
	public void placePatchTuple(PatchTuple patchTuple){
		Blanket blanket = gameController.getGame().getGameState().getActivePlayer().getBlanket();
		if (canPlacePatchTuple(blanket, patchTuple)) { // TODO LEISTUNGSFRESSER
			blanket.getPatchTuples().add(patchTuple);
		}
	}

	/**
	 * Checks if more than 1 possible future Placements exist.
	 * @param blanket blanket in which the Patch shall be inserted.
	 * @param patch  patch to place.
	 * @return true if placeable
	 * @author Raphael
	 */
	public static boolean canPlacePatch(Blanket blanket, Patch patch) {
		return !calculatePlacements(blanket, patch).isEmpty();
	}

	/**
	 * @param patch for which all possible placements will get calculated.
	 * @return All possible placements of the provided patch as a Collection of PatchTuples.
	 * @author Raphael(, Leon)
	 */
	public static Collection<PatchTuple> calculatePlacements(Blanket blanket, Patch patch) {
		Collection<PatchTuple> res = new ArrayList<>();

		/*
		boolean [] boolArr;
		Rotation[] rotationArr;
		if (patch.equals(Constants.generateSpecialPatch())) { // It is an extra Patch
			boolArr = new boolean[]{true};
			rotationArr = new Rotation[]{Rotation._0};
		} else {
			boolArr = new boolean[]{true,false};
			rotationArr = Rotation.values();
		}
		*/

		for (Rotation rotation : Rotation.values()){
			for (boolean bool : new boolean[]{true,false}){
				Patch tmp;
				if (bool){
					tmp = patch.flipPatch();
					tmp = tmp.rotatePatch(rotation);
				}
				else{
					tmp = patch.rotatePatch(rotation);
				}
				Tuple<Integer, Integer> tpl = dimOfPatch(tmp); //efficiency
				int rowMinus = tpl.getV1();
				int colMinus = tpl.getV2();
				for (int row = 0; row < 9 - rowMinus; row++){
					for (int col = 0; col < 9 - colMinus; col++){
						PatchTuple patchTuple = new PatchTuple(patch.clone(), rotation, new VecTwo(row,col), bool);
						if (canPlacePatchTuple(blanket, patchTuple)) res.add(patchTuple);
					}
				}
			}
		}
		return res;
	}

	/**
	 * @param blanket -
	 * @param patch for which all possible placements will get calculated.
	 * @return first placeable PatchTuple
	 * @author Florian, Leon
	 */
	@Nullable
	public static PatchTuple calculateFirstPlacement(@NotNull Blanket blanket, @NotNull Patch patch) {
		boolean [] boolArr = {true, false};
		for (Rotation rotation : Rotation.values()) {
			for (boolean bool : boolArr) {
				Patch tmp;
				if (bool) {
					tmp = patch.flipPatch();
					tmp = tmp.rotatePatch(rotation);
				} else {
					tmp = patch.rotatePatch(rotation);
				}
				Tuple<Integer, Integer> tpl = dimOfPatch(tmp); // efficiency
				int rowMinus = tpl.getV1();
				int colMinus = tpl.getV2();
				for (int row = 0; row < 9 - rowMinus; row++) {
					for (int col = 0; col < 9 - colMinus; col++) {
						PatchTuple patchTuple = new PatchTuple(patch.clone(), rotation, new VecTwo(row, col), bool);
						if (canPlacePatchTuple(blanket, patchTuple)) {
							return patchTuple;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param blanket blanket in which the Patch shall be inserted.
	 * @param patch  patch to place.
	 * @return Collection of (Blanket, PatchTuple)-Tuples without any duplicated Blankets.
	 * @author Raphael
	 */
	public static Collection<Tuple<Blanket, PatchTuple>> calculateAllPossibleFollowupBlankets(Blanket blanket, Patch patch) {
		List<Tuple<Blanket, PatchTuple>> res = new LinkedList<>();
		Collection<PatchTuple> tuples = calculatePlacements(blanket, patch);
		for (PatchTuple patchTup : tuples){
			BlanketWrapper wrapper = new BlanketWrapper(insertPatchTuple(blanket, patchTup), patchTup);
			res.add(wrapper);
		}
		return res;
	}

	/**
	 * @param blanket blanket in which the Patch shall be inserted.
	 * @param patch patch to place.
	 * @param placeExtraPatch if an extra patch has to be placed as well.
	 * @return Collection of (Blanket, PatchTuple)-Tuples without any duplicated Blankets. Both the patch and the extra
	 * patch get placed in every possible combination.
	 * @author Leon
	 */
	public static Collection<Tuple<Blanket, Turn>> calculateAllPossibleFollowupBlankets(Blanket blanket, Patch patch, boolean placeExtraPatch) {
		if (!placeExtraPatch) {
			return calculateAllPossibleFollowupBlankets(blanket, patch).stream()
					.map(tuple -> new Tuple<>(tuple.getV1(), Turn.create(tuple.getV2(), null)))
					.collect(Collectors.toList());
		}

		Patch extraPatch = Constants.generateSpecialPatch();

		Collection<Tuple<Blanket, Turn>> all = new ArrayList<>();
		all.addAll(calculateAllPossibleFollowupBlankets(blanket, patch, extraPatch));
		all.addAll(calculateAllPossibleFollowupBlankets(blanket, extraPatch, patch));
		return all;
	}

	/**
	 * @author Leon
	 */
	public static Collection<Tuple<Blanket, Turn>> calculateAllPossibleFollowupBlankets(Blanket blanket, Patch patch1, Patch patch2) {
		Collection<Tuple<Blanket, Turn>> all = new ArrayList<>();
		Collection<Tuple<Blanket, PatchTuple>> placements1 = calculateAllPossibleFollowupBlankets(blanket, patch1);
		for (Tuple<Blanket, PatchTuple> tuple1 : placements1) {
			Collection<Tuple<Blanket, PatchTuple>> placements2 = calculateAllPossibleFollowupBlankets(tuple1.getV1().clone(), patch2);
			for (Tuple<Blanket, PatchTuple> tuples2 : placements2) {
				all.add(new Tuple<>(tuples2.getV1(), Turn.create(tuple1.getV2(), tuples2.getV2())));
			}
		}
		return all;
	}

	/**
	 * Inserts patchTuple into the blanket
	 * @param blanket blanket in which the Patch shall be inserted.
	 * @param patchTuple  patchTuple to place.
	 * @return new Blanket-Object
	 * @author Raphael
	 */
	public static Blanket insertPatchTuple(Blanket blanket, PatchTuple patchTuple){
		Blanket res = blanket.clone();
		res.getPatchTuples().add(patchTuple);
		return res;
	}

	/**
	 * @param blanket blanket to check placeability for.
	 * @param patchTuple patchTuple which is ment to be inserted.
	 * @return true if patch can be placed.
	 * @author Raphael
	 */
	public static boolean canPlacePatchTuple(Blanket blanket, PatchTuple patchTuple){
		for(VecTwo vecTwo : patchTuple.getVectorList()){
			if (!vecTwo.validVec2()){
				return false;
			}
		}
		return !overlap(blanket, patchTuple);
	}

	/**
	 * @param patchTuple the patch to insert
	 * @param blanket current blanket
	 * @return true if overlap exists
	 * @author Raphael
	 */
	private static boolean overlap (Blanket blanket, PatchTuple patchTuple){
		boolean [][] patchRepr = patchTuple.getMatrixRepresentation();
		boolean [][] blanketRepr = blanket.getBoolMatrixRepresentation();
		for (int row = 0; row < 9; row++){
			for (int col = 0; col < 9; col++){
				if (patchRepr[row][col] && blanketRepr [row][col]) return true;
			}
		}
		return false;
	}

	/**
	 * @return true if 7x7 subMatrix is fully filled
	 * @author Raphael
	 */
	public boolean specialTokenReady(){
		return specialTokenReady(gameController.getGame().getGameState().getActivePlayer().getBlanket().getBoolMatrixRepresentation());
	}

	/**
	 * @return true if 7x7 subMatrix is fully filled
	 * @author Raphael
	 */
	public static boolean specialTokenReady(boolean [][] matrix){
		for (int row = 0; row < 3; row++){
			for (int col = 0; col < 3; col++){
				if (specialTokenReady(matrix, row,col)){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param row startRow
	 * @param col startCol
	 * @return true if all Vals in 7x7-subMatrix are true
	 * @author Raphael
	 */
	private static boolean specialTokenReady(boolean [][] matrix, int row, int col){
		for (int rowShift = 0; rowShift < 7; rowShift++){
			for (int colShift = 0; colShift < 7; colShift++){
				if (!matrix [row+rowShift] [col+colShift]){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Calculates the outer Dimensions of a patch.
	 * @param patch
	 * @return
	 * @author Raphael
	 */
	private static Tuple<Integer, Integer> dimOfPatch(Patch patch){
		int xMin, xMax, yMin, yMax;
		xMin = xMax = yMin = yMax = 0;
		for (VecTwo vecTwo : patch.getVectorList()){
			xMin = Math.min(xMin, vecTwo.getRow());
			xMax = Math.max(xMax, vecTwo.getRow());
			yMin = Math.min(yMin, vecTwo.getCol());
			yMax = Math.max(yMax, vecTwo.getCol());
		}
		return new Tuple<>(xMax - xMin, yMax - yMin);
	}
}
