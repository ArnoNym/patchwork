package controller;

import ai.AITurnThread;
import javafx.application.Platform;
import model.*;

import java.util.List;

/**
 * controls the gamestate
 * @author Raphael
 */
public class GameStateController {
	private GameController gameController;

	/**
	 * Default-Constructor
	 * @param gameController the main-GameController
	 */
	public GameStateController(GameController gameController){
		this.gameController = gameController;
	}

	/**
	 * Reverts the last move done.
	 * @author Raphael
	 */
	public void undoMove() {
		gameController.getGame().getGameState().getActivePlayer().setHighscoreActivated(false);
		Game game = gameController.getGame();
		if (!undoPossible()){
			throw new IllegalStateException("undoMove() called when undoStack was empty");
		}
		GameState newOldState = game.getUndoStack().pop();
		game.getRedoStack().push(game.getGameState());
		game.setGameState(newOldState);
	}

	/**
	 * @return Player with the specified name
	 * @author Leon
	 */
	public Player getPlayerByName(String name) {
		return getPlayerByName(gameController.getGame().getGameState(), name);
	}

	/**
	 * @return Player with the specified name
	 * @author Leon
	 */
	public static Player getPlayerByName(GameState gameState, String name) {
		if (name.equals(gameState.getActivePlayer().getName())) {
			return gameState.getActivePlayer();
		} else if (name.equals(gameState.getPassivePlayer().getName())) {
			return gameState.getPassivePlayer();
		} else {
			throw new IllegalStateException();
		}
	}

	public Player getOtherPlayer(Player player){
		Player active = gameController.getGame().getGameState().getActivePlayer();
		if (player.equals(active)){
			return gameController.getGame().getGameState().getPassivePlayer();
		}
		else{
			return gameController.getGame().getGameState().getActivePlayer();
		}
	}

	/**
	 * @return Player with the specified name
	 * @author Leon
	 */
	public static Player getOtherPlayerName(GameState gameState, String name) {
		if (name.equals(gameState.getActivePlayer().getName())) {
			assert !name.equals(gameState.getPassivePlayer().getName());
			return gameState.getPassivePlayer();
		} else if (name.equals(gameState.getPassivePlayer().getName())) {
			assert !name.equals(gameState.getActivePlayer().getName()) : "Looking for "+name
					+", activePlayer.name: " +gameState.getActivePlayer().getName()
					+", passivePlayer.name: " +gameState.getPassivePlayer().getName();
			return gameState.getActivePlayer();
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * Redoes the last undone Move.
	 * @author Raphael
	 */
	public void redoMove() {
		Game game = gameController.getGame();
		if (!redoPossible())
			throw new IllegalStateException("redoMove() called when redoStack was empty");
		GameState newState = game.getRedoStack().pop();
		game.getUndoStack().push(game.getGameState());
		game.setGameState(newState);
	}

	/**
	 * Checks if undoStack isn´t empty.
	 * @return true if possible
	 * @author Raphael
	 */
	public boolean undoPossible(){
		return !gameController.getGame().getUndoStack().empty() && !gameController.getGame().getGameState().isFinished();
	}

	/**
	 * Checks if redoStack isn't empty.
	 * @return true if possible.
	 * @author Raphael
	 */
	public boolean redoPossible(){
		return !gameController.getGame().getRedoStack().empty();
	}

	/**
	 * Initializes a new game/match.
	 * @param startingPlayer player to start the game.
	 * @param otherPlayer second Player.
	 * @param patchesList list of patches on the gameboard
	 * @author Raphael
	 */
	public void setUpGame(Player startingPlayer, Player otherPlayer, List<Patch> patchesList) {
		GameBoard gameBoard = new GameBoard(patchesList);
		GameState newGame = new GameState(startingPlayer, otherPlayer, gameBoard);
		gameController.getGame().setGameState(newGame);
		gameController.getGame().getUndoStack().clear();
		gameController.getGame().getRedoStack().clear();
	}

	public void initAITurn(){
		if(gameController.getGame().getGameState().getActivePlayer().getAIInformation() != null) {
			Platform.runLater(new AITurnThread(gameController));
		}
	}

	/**
	 * ending a game/match. Processed in a few steps:
	 * 1. calculation the scores.
	 * 2. updating the highscores (only if activated).
	 * 3. resetting game (gameState = null, undoStack.clear, redoStack.clear).
	 * 4. passing the results to the GUI to be shown on screen.
	 *
	 * @author Raphael
	 */
	public void finishGame() {
		Game game = gameController.getGame();
		GameState endState = game.getGameState();
		Player active = endState.getActivePlayer();
		Player passive = endState.getPassivePlayer();
		if (!endState.isFinished()){
			throw new IllegalStateException("finishGame was called when the game wasnt actually finished!");
		}

		int activeScore = PlayerController.calculateScore(active);
		int passiveScore = PlayerController.calculateScore(passive);

		//updating highscore
		updateHighScoreForName(passive, passiveScore);
		updateHighScoreForName(active, activeScore);

		//resetting game
		//game.setGameState(null);
		game.getUndoStack().clear();
		game.getRedoStack().clear();
	}

	/**
	 * If the Player has HighScoreActivated save the best score for the given player
	 * @author Kevin
	 */
	private void updateHighScoreForName(Player player, int score) {
		if (player.isHighscoreActivated()){
			if (gameController.getGame().getHighscore().containsKey(player.getName())) {
				int savedScore = gameController.getGame().getHighscore().get(player.getName());
				if (savedScore < score)
					gameController.getGame().getHighscore().put(player.getName(), score);
			}
			else {
				gameController.getGame().getHighscore().put(player.getName(), score);
			}
		}
	}

	/**
	 * Swaps the roles of the players. Now its the previously passive player´s turn.
	 * @author Raphael
	 */
	public void swapPlayerRoles(){
		GameState gameState = gameController.getGame().getGameState();
		Player tmp = gameState.getActivePlayer();
		gameState.setActivePlayer(gameState.getPassivePlayer());
		gameState.setPassivePlayer(tmp);
	}

	/**
	 * @return if the specialToken (which the first player who fills up a 7x7 field gets and which counts for
	 * victory points) is still available
	 * @author Leon
	 */
	public static boolean isSpecialTokenAvailable(GameState gameState) {
		return !gameState.getActivePlayer().hasSpecialToken() && !gameState.getPassivePlayer().hasSpecialToken();
	}
}
