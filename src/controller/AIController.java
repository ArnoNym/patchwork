package controller;

import ai.agents.PatchworkAgentStore;
import ai.agents.PatchworkAgent;
import ai.decisionTree.Turn;
import model.*;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Optional;

/**
 * @author Florian, Leon
 */
public class AIController {
	private final GameController gameController;
	private final PatchworkAgentStore patchworkAgentStore;

	/**
	 * @param gameController which is used to interact with the rest of the program
	 * @author Leon
	 */
	public AIController(GameController gameController) {
		this.gameController = gameController;
		IOController ioController = new IOController(null);

		Optional<Map<Difficulty, PatchworkAgent>> optionalAgentMap = ioController.loadAgentMapFromPath("resources/agentMap.ser");
		if (optionalAgentMap.isEmpty()) {
			System.out.println("WARNING! Could not find AI-AgentMap!");
			this.patchworkAgentStore = null;
		} else {
			this.patchworkAgentStore = new PatchworkAgentStore(optionalAgentMap.get());
		}
	}

	/**
	 * @param gameController which is used to interact with the rest of the program
	 * @param agentMap with the agents for different difficulties
	 * @author Leon
	 */
	public AIController(GameController gameController, Map<Difficulty, PatchworkAgent> agentMap) {
		this.gameController = gameController;
		this.patchworkAgentStore = new PatchworkAgentStore(agentMap);
	}

	/**
	 * @param gameController which is used to interact with the rest of the program
	 * @param patchworkAgent which is used for all difficulties
	 * @author Leon
	 */
	public AIController(GameController gameController, PatchworkAgent patchworkAgent) {
		this.gameController = gameController;
		this.patchworkAgentStore = new PatchworkAgentStore(patchworkAgent);
	}

	/**
	 * @return a turn object that includes all information to display a hint by the ai for the player
	 * @author Leon
	 */
	public Turn calculateHint() {
		gameController.getGame().getGameState().getActivePlayer().setHighscoreActivated(false);
		return calcTurn(Difficulty.HARDCORE);
	}

	/**
	 * Executes the turn the ai thinks is best
	 *
	 * @return the executed turn
	 * @author Leon
	 */
	public Turn doTurn() {
		AIInformation aiInformation = gameController.getGame().getGameState().getActivePlayer().getAIInformation();
		if (aiInformation == null) {
			throw new IllegalStateException("The active player is no Ai!");
		}
		Difficulty difficulty = aiInformation.getDifficulty();
		Turn turn = calcTurn(difficulty);
		gameController.act(turn);
		return turn;
	}

	public void doTurn(Turn turn) {
		gameController.act(turn);
	}

	@NotNull
	private Turn calcTurn(Difficulty difficulty) {
		GameState clonedGameState = gameController.getGame().getGameState().clone();
		if (clonedGameState.getActivePlayer().getAIInformation() == null) {
			clonedGameState.getActivePlayer().setAIInformation(new AIInformation(false, difficulty, SimulationSpeed.NOSTOP));
		}
		return patchworkAgentStore.getAgent(difficulty).calcTurn(clonedGameState);
	}

	public void processAiMove() {
		String name = gameController.getGame().getGameState().getActivePlayer().getName();
		AIInformation aiInfo = gameController.getGame().getGameState().getActivePlayer().getAIInformation();

		while(gameController.getGame().getGameState().getActivePlayer().getName().equals(name) && !gameController.getGame().getGameState().isFinished()) {
			System.out.println("\n\n" + gameController.getGame().getGameState().getActivePlayer().getName() + " does turn at" + gameController.getGame().getGameState().getActivePlayer().getPos());
			if (aiInfo.getSimulationSpeed().equals(SimulationSpeed.DELAYED)) {
				System.out.println("here: " + name + " " +  gameController.getGame().getGameState().getActivePlayer().getName() + " pos " + gameController.getGame().getGameState().getActivePlayer().getPos());
				try {
					Thread.sleep((2 + (long) Math.random() * 5) *1000);
				} catch (InterruptedException ex) {
				}
			}

			Turn turn = doTurn();
			if(gameController.getGameStateController().getPlayerByName(name).getAIInformation().getSimulationSpeed() != SimulationSpeed.CONFIRMATION){
				gameController.getGUI().getGameScreenController().updateWholeScreen(true);
			}
			if (aiInfo.isProvidingFeedback()) {
				gameController.getGUI().getGameScreenController().addFeedback(turn);
			}
			if (name.equals(gameController.getGame().getGameState().getActivePlayer().getName())){
				System.out.println(gameController.getGame().getGameState().getActivePlayer().getName() + " did turn at" + gameController.getGame().getGameState().getActivePlayer().getPos());
			}
			else System.out.println(gameController.getGame().getGameState().getPassivePlayer().getName() + " did turn at" + gameController.getGame().getGameState().getPassivePlayer().getPos());
		}
	}
}
