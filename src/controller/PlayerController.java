package controller;

import Resources.Constants;
import javafx.application.Platform;
import model.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * controls the Player
 * @author Raphael
 */
public class PlayerController {
	private GameController gameController;

	/**
	 * Default-Constructor
	 * @param gameController the main-GameController
	 */
	public PlayerController(GameController gameController){
		this.gameController = gameController;
	}

	/**
	 * Turn A: The activePlayer moves past the inactivePlayer (or onto the last field if inactivePlayer is already finished).
	 * While moving forward each field is evaluated separately. For each field the player gains one Button.
	 * When the pass is fully processed finishPlayerTurn() is called, if there was no specialPatch on the last field he arrived at.
	 *
	 * Before the pass is processed the gameState is pushed on top of the undoStack, so that the move can be undone.
	 * @return true, if a specialPatch was found, else false.
	 * @author Raphael
	 */
	public boolean pass() {
		gameController.nextGameState(gameController.getGame().getGameState().clone());

		GameState gameState = gameController.getGame().getGameState();
		GameBoard gameBoard = gameState.getGameBoard();
		Player activePlayer = gameState.getActivePlayer();
		Player passivePlayer = gameState.getPassivePlayer();

		boolean specialPatchFound = false;

		int buttonCount = calcPassButtons(activePlayer, passivePlayer);

		while(activePlayer.getPos() < passivePlayer.getPos() + 1 && activePlayer.getPos() < gameBoard._LASTPOS){
			incrementActivePlayerPos();
			specialPatchFound = processField() || specialPatchFound;
		}

		activePlayer.setButtonCount(activePlayer.getButtonCount() + buttonCount);

		if (!specialPatchFound) {
			finishPlayerTurn();
		}

		return specialPatchFound;
	}

	/**
	 * @author Leon
	 */
	public static int calcPassButtons(Player activePlayer, Player passivePlayer) {
		return Math.max(0, Math.min(passivePlayer.getPos() + 1, Constants.FIELDS_COUNT) - activePlayer.getPos());
	}

	/**
	 * @author Leon
	 */
	public boolean takePatchAndInsert(PatchTuple toInsert) {
		return takePatchAndInsert(toInsert, true, true);
	}

	/**
	 * Turn B: The activePlayer chooses to insert a PatchTuple.
	 * This is processed in a number of steps:
	 * 1. The Patch is bought from the GameBoard
	 * 2. The Player is moved forward according to the timeCost of the patch. The field of each step is processed individually.
	 * 3. The Patch is placed into the activePlayer´s blanket.
	 * 4. The activePlayer´s Blanket is checked for a 7x7 fully-filled SubMatrix: if found, he gets the specialPatch.
	 * 5. If there was no specialPatch on the activePlayer´s way and activePos greater passivePos finishPlayerTurn() is called.
	 *
	 * Should also be used for placing the specialPatch
	 *
	 * Before takePatchAndInsert is processed the gameState is pushed on top of the undoStack, so that the move can be undone.
	 * @param toInsert A fitting PatchTuple - to check whether it fits use BlanketController-methods!
	 * @return true, if a specialPatch was found, else false.
	 * @author Raphael, (Leon)
	 */
	public boolean takePatchAndInsert(PatchTuple toInsert, boolean checkBuy, boolean checkPlacement) {
		gameController.nextGameState(gameController.getGame().getGameState().clone());
		Player activePlayer = gameController.getGame().getGameState().getActivePlayer();
		Player inactivePlayer = gameController.getGame().getGameState().getPassivePlayer();
		Patch patch = toInsert.getPatch();

		if (checkBuy && !canBuyPatch(patch)) {
			return false;
		} else {
			assert canBuyPatch(patch);
		}
		if (checkPlacement && !BlanketController.canPlacePatch(activePlayer.getBlanket(), patch)) {
			return false;
		} else {
			assert BlanketController.canPlacePatch(activePlayer.getBlanket(), patch);
		}

		boolean extraPatchFound = false;

		//buying Patch if it isn't an extraPatch
		buyPatch(toInsert.getPatch());

		//placing patch and check for specialToken. Before incrementPlayerPosition so that the income of the patch gets included if we walk over an income-button!
		gameController.getBlanketController().placePatchTuple(toInsert);
		if (GameStateController.isSpecialTokenAvailable(gameController.getGame().getGameState()) && gameController.getBlanketController().specialTokenReady()) {
			activePlayer.setSpecialToken(true);
		}

		//moving Player
		int inactivePos = inactivePlayer.getPos();
		int steps = 0;
		int time = patch.getTime();
		int lastPos = gameController.getGame().getGameState().getGameBoard()._LASTPOS;

		while (steps < time && activePlayer.getPos() < lastPos){
			incrementActivePlayerPos();
			extraPatchFound = processField() || extraPatchFound;
			steps++;
		}

		//finishing move
		if (!extraPatchFound && (activePlayer.getPos() > inactivePos || activePlayer.getPos() == lastPos)){
			finishPlayerTurn();
		}

		return extraPatchFound;
	}

	/**
	 * @author Leon
	 */
	public boolean isPassingExtraPatch(Patch patch) {
		gameController.nextGameState(gameController.getGame().getGameState().clone());
		Player active = gameController.getGame().getGameState().getActivePlayer();

		int activePos = active.getPos();
		int steps = 0;
		int time = patch.getTime();
		int lastPos = gameController.getGame().getGameState().getGameBoard()._LASTPOS;

		while (steps < time && activePos < lastPos){
			incrementActivePlayerPos();
			activePos++;
			if (processField()) {
				return true;
			}
			steps++;
		}

		return false;
	}

	/**
	 * Calculates the Player with the most points given a gameState
	 * @param gameState the gameState, the winner is playing on
	 * @return the winner of the game gameState
	 * @author Florian
	 */
	@Nullable
	public static Player calculateWinner(@NotNull GameState gameState){
		int activePlayerScore = calculateScore(gameState.getActivePlayer());
		int passivePlayerScore = calculateScore(gameState.getPassivePlayer());
		if (activePlayerScore > passivePlayerScore) {
			return gameState.getActivePlayer();
		} else if (activePlayerScore < passivePlayerScore) {
			return gameState.getPassivePlayer();
		} else {
			return null;
		}
	}

	/**
	 * Removes the Patch from the gameBoard, subtracts patch-cost from player-buttonCount and moves the Meeple.
	 * Does nothing if the patch is a specialPatch.
	 * @param patch patch to be placed
	 * @author Raphael
	 */
	private void buyPatch(Patch patch) {
		int size = gameController.getGame().getGameState().getGameBoard().getPatches().size();
		if (!patch.equals(Constants.generateSpecialPatch())) {
			changeButtonCount(-patch.getCost());
			//updating GameBoard: 1.finding the patchPos 2.removing Patch from the Gameboard 3.moving Meeple to where the removed patch has been
			GameBoard gameBoard = gameController.getGame().getGameState().getGameBoard();
			int meeplePos = gameBoard.getMeeplePos();
			int patchPos = meeplePos;
			List<Patch> patches = gameBoard.getPatches();

			while (!patches.get(patchPos % patches.size()).equals(patch)) {
				int two = 2;
				if ((patchPos - meeplePos) > two) {
					System.out.println(patchPos);
					System.out.println(meeplePos);
					throw new IllegalStateException("cant find Patch in patchList");
				}
				patchPos++;
			}
			gameController.getGameBoardController().removePatchAtIndex(patchPos % patches.size());
			gameBoard.setMeeplePos(patchPos % size);
		}
	}

	/**
	 * Inspects the currently visited field (By the active player of course).
	 * If the field holds a button (gamelogic-convention - In reality the button lies between this field and its predecessor), then the income generated by the activePlayers blanket is added to his balance (buttonCount).
	 * If the field holds a specialPatch, the specialPatch is removed and the processField() notifies the calling-Function by returning true.
	 * @return true if the checked field held a specialPatch when the activePlayer entered it.
	 * @author Raphael
	 */
	private boolean processField(){
		GameBoard gameBoard = gameController.getGame().getGameState().getGameBoard();
		Player active = gameController.getGame().getGameState().getActivePlayer();
		Field field = gameBoard.getFields().get(active.getPos());
		boolean specialPatchFound = false;
		if (field.isButtonField()){
			changeButtonCount(active.getBlanket().calcIncome());
		}
		if (field.isSpecialPatch()){
			field.setSpecialPatch(false);
			specialPatchFound = true;
		}
		return specialPatchFound;
	}

	/**
	 * Calculates the player's score. Should be called at the end of a game only, since midgame this value doesn't mean shit.
	 * @param player player for whom the score shall be computed.
	 * @return Case-Of [[if player.hasSpecialToken then Sum (Diff (player.balance, Prod (2, player.blanket.emptySpaces), 7)] [if True then (Diff (player.balance, Prod (2, player.blanket.emptySpaces)]]
	 * @author Raphael
	 */
	public static int calculateScore(Player player) {
		int countEmptySpaces = 0;
		int buttonCount = player.getButtonCount();
		boolean [][] matrix = player.getBlanket().getBoolMatrixRepresentation();
		for (boolean [] row : matrix){
			for (boolean bool : row){
				if (!bool) countEmptySpaces++;
			}
		}
		return player.hasSpecialToken() ? (buttonCount - 2 * countEmptySpaces + 7) : (buttonCount - 2 * countEmptySpaces);
	}

	/**
	 * @param patch patch to check buyability for.
	 * @return true if possible
	 * @author Raphael
	 */
	public boolean canBuyPatch(Patch patch) {
		return gameController.getGame().getGameState().getActivePlayer().getButtonCount() >= patch.getCost();
	}

	/**
	 * F'r whom t may conc'rn, the factious playeth'r shall most c'rtainly beest hath moved on the game board anoth'r position into the right direction.
	 * @author Raphael
	 */
	private void incrementActivePlayerPos(){
		Player active = gameController.getGame().getGameState().getActivePlayer();
		active.setPos(active.getPos() + 1);
	}

	/**
	 * @param amt Positive or negative amount of Buttons to be added to the current buttonCount of the active Player.
	 * @author Raphael
	 */
	private void changeButtonCount(int amt){
		Player active = gameController.getGame().getGameState().getActivePlayer();
		active.setButtonCount(amt + active.getButtonCount());
	}

	/**
	 * Finishes the current Turn. Therefore the roles of the players are being swapped (active -> passive & vice versa).
	 * If the game is found to be completely finished finishGame() is called.
	 * @author Raphael
	 */
	private void finishPlayerTurn() {
		GameState gameState = gameController.getGame().getGameState();
		if (gameState.isFinished()) {
			gameController.getGameStateController().finishGame();
		} else {
			gameController.getGameStateController().swapPlayerRoles();
			if (gameController.getGUI() == null) {
				return;
			}
			if (gameState.getActivePlayer().getAIInformation() == null) {
				return;
			}
			if (gameState.getActivePlayer().getAIInformation().getSimulationSpeed().equals(SimulationSpeed.CONFIRMATION)) {
				return;
			}
			Platform.runLater(new ai.AITurnThread(gameController));
		}
	}

	/**
	 * @param player the time is calculated for
	 * @return time of the player
	 * @author Raphael
	 */
	public static int calcTime(Player player) {
		return (Constants.FIELDS_COUNT - 1) - player.getPos();
	}

	/**
	 * @return How many IncomeButtons will the specified player walk over until the game is over
	 * @author Leon
	 */
	public static int calcIncomeButtonsLeftCount(Player player) {
		return Constants.calcIncomeButtonsLeftCount(player.getPos());
	}
}
