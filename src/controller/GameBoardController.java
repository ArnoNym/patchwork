package controller;

import Resources.Constants;
import model.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * controls the gameboard
 * @author Raphael
 */
public class GameBoardController {
	private final GameController gameController;

	/**
	 * Default-Constructor
	 * @param gameController the main-GameController
	 */
	public GameBoardController(GameController gameController) {
		this.gameController = gameController;
	}

	/**
	 * @return List of leq 3 Patches
	 * @author Raphael
	 */
	public List<Patch> getAvailablePatches() {
		return getAvailablePatches(gameController.getGame().getGameState().getGameBoard());
	}

	/**
	 * @return List of leq 3 Patches
	 * @author Raphael(, Leon)
	 */
	public static List<Patch> getAvailablePatches(GameBoard gameBoard) {
		List<Patch> allLeftOverPatches = gameBoard.getPatches();
		int meeplePos = gameBoard.getMeeplePos();

		LinkedList<Patch> res = new LinkedList<>();
		int three = 3;
		if (allLeftOverPatches.size() < three){
			res.addAll(allLeftOverPatches);
		}
		else{
			for (int pos = 0; pos < 3; pos++){
				res.add(allLeftOverPatches.get((meeplePos + pos) % allLeftOverPatches.size()));
			}
		}
		return res;
	}

	/**
	 * @return
	 * @author Raphael
	 */
	public List<Patch> getCurrentPatchList(){
		GameBoard gameBoard = gameController.getGame().getGameState().getGameBoard();
		List<Patch> allLeftOverPatches = gameBoard.getPatches();
		int meeplePos = gameBoard.getMeeplePos();

		LinkedList<Patch> res = new LinkedList<>();

		for (int i = 0; i < allLeftOverPatches.size(); i++){
			res.add(allLeftOverPatches.get((meeplePos + i) % allLeftOverPatches.size()));
		}
		return res;
	}

	/**
	 * @param index index of the Patch that shall be removed.
	 * @author Raphael
	 */
	public void removePatchAtIndex(int index){
		gameController.getGame().getGameState().getGameBoard().getPatches().remove(index);
	}


	/**
	 * @return how many extra patches are left on the game board
	 * @author Leon
	 */
	public static int calcExtraPatchesLeft(GameBoard gameBoard) {
		return (int) gameBoard.getFields().stream().filter(Field::isSpecialPatch).count();
	}
}
