package controller;

import ai.agents.PatchworkAgent;
import ai.decisionTree.Turn;
import ai.AiConstants;
import model.*;
import view.GUI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * controls the game
 * @author Leon, Raphael, Marcel
 */
public class GameController {
	private GUI gui;
	private final AIController aiController;
	private final BlanketController blanketController;
	private final GameBoardController gameBoardController;
	private final GameStateController gameStateController;
	private final IOController ioController;
	private final PlayerController playerController;
	private boolean musik = false;
	private Game game;


	/**
	 * No-Args-Constructor. Needed because we now have two constructors with only one parameter which means we cant pass
	 * null as a single argument.
	 *
	 * @author Leon
	 */
	public GameController() {
		this.aiController = new AIController(this);
		this.blanketController = new BlanketController(this);
		this.gameBoardController = new GameBoardController(this);
		this.gameStateController = new GameStateController(this);
		this.ioController = new IOController(this);
		this.playerController = new PlayerController(this);
		this.game = new Game();
		this.gui = null;
		this.musik=true;
	}

	/**
	 * GUI-Constructor
	 * @param gui gui
	 * @author Leon
	 */
	public GameController(GUI gui) {
		this.aiController = new AIController(this);
		this.blanketController = new BlanketController(this);
		this.gameBoardController = new GameBoardController(this);
		this.gameStateController = new GameStateController(this);
		this.ioController = new IOController(this);
		this.playerController = new PlayerController(this);
		this.game = new Game();
		this.gui = gui;
	}

	/**
	 * Ai lookahead constructor. Creates a GameController with a new game that is stripped of everything that's
	 * unnecessary for the ai to make its choice while also setting everything up to make it easier to use for
	 * a look into possible future moves.
	 *
	 * @param gameState whose copy will get used by this controller
	 * @author Leon
	 */
	public GameController(final GameState gameState) {
		this.aiController = null;
		this.blanketController = new BlanketController(this);
		this.gameBoardController = new GameBoardController(this);
		this.gameStateController = new GameStateController(this);
		this.ioController = null;
		this.playerController = new PlayerController(this);

		AIInformation aiInformation = new AIInformation(false, null, SimulationSpeed.NOSTOP);

		gameState.getActivePlayer().setAIInformation(aiInformation);
		gameState.getPassivePlayer().setAIInformation(aiInformation);

		this.game = new Game();
		game.setGameState(gameState);
	}

	/**
	 * Ai training constructor
	 * @param patchworkAgent patchworkAgent
	 * @param patches patches
	 * @author Leon
	 */
	public GameController(PatchworkAgent patchworkAgent, List<Patch> patches) {
		this.aiController = new AIController(this, patchworkAgent);
		this.blanketController = new BlanketController(this);
		this.gameBoardController = new GameBoardController(this);
		this.gameStateController = new GameStateController(this);
		this.ioController = null;
		this.playerController = new PlayerController(this);

		AIInformation aiInformation = new AIInformation(false, null, SimulationSpeed.NOSTOP);
		Player player1 = new Player(AiConstants.TRAINING_PLAYER_1_NAME, aiInformation);
		Player player2 = new Player(AiConstants.TRAINING_PLAYER_2_NAME, aiInformation);

		GameState newGameState = new GameState(player1, player2, new GameBoard(patches));

		this.game = new Game();
		game.setGameState(newGameState);
	}

	/**
	 * Ai training constructor
	 * @param agent1 agent1
	 * @param agent2 agent2
	 * @param patches patches
	 * @author Leon
	 */
	public GameController(PatchworkAgent agent1, PatchworkAgent agent2, List<Patch> patches) {
		Map<Difficulty, PatchworkAgent> agentMap = new HashMap<>();
		agentMap.put(Difficulty.HARDCORE, agent1);
		agentMap.put(Difficulty.MISSIONARY, agent2);
		this.aiController = new AIController(this, agentMap);
		this.blanketController = new BlanketController(this);
		this.gameBoardController = new GameBoardController(this);
		this.gameStateController = new GameStateController(this);
		this.ioController = null;
		this.playerController = new PlayerController(this);

		AIInformation aiInformation1 = new AIInformation(false, Difficulty.HARDCORE, SimulationSpeed.NOSTOP);
		Player player1 = new Player(AiConstants.TRAINING_PLAYER_1_NAME, aiInformation1);
		AIInformation aiInformation2 = new AIInformation(false, Difficulty.MISSIONARY, SimulationSpeed.NOSTOP);
		Player player2 = new Player(AiConstants.TRAINING_PLAYER_2_NAME, aiInformation2);

		GameState newGameState = new GameState(player1, player2, new GameBoard(patches));

		this.game = new Game();
		game.setGameState(newGameState);
	}

	/**
	 * The activePlayer takes the specified turn.
	 *
	 * @param turn that contains all the necessary information for the player to take a turn
	 * @author Leon
	 */
	public void act(Turn turn) {
		boolean hasToPlaceExtraPatch;
		if (turn.isPass()) {
			hasToPlaceExtraPatch = playerController.pass();
		} else {
			hasToPlaceExtraPatch = playerController.takePatchAndInsert(turn.getBuyAndPlacePatchTuple(), false, false);
		}
		assert hasToPlaceExtraPatch == turn.hasExtraPatch();
		if (turn.hasExtraPatch()) {
			playerController.takePatchAndInsert(turn.getExtraPatchTuple(), false, false);
		}
	}

	/**
	 * Adds the current game state to the undo stack and sets a new gameState.
	 * @param newGameState New gameState to be set as current gameState.
	 * @author Leon
	 */
	public void nextGameState(GameState newGameState) {
		game.getRedoStack().clear();
		game.getUndoStack().push(game.getGameState());
		game.setGameState(newGameState);
	}

	/**
	 * Format: 	Name0 | Score0
	 * 			Name1 | Score1
	 * 			...
	 * @return String Representation of the Highscore.
	 * @author Raphael
	 */
	public String showHighscore() {
		StringBuilder res = new StringBuilder();
		Map<String, Integer> highscore = game.getHighscore();
		for (Map.Entry<String, Integer> entry : highscore.entrySet()){
			res.append(entry.getKey()).append(" | ").append(entry.getValue().toString()).append("\n");
		}
		return res.toString();
	}

	/**
	 * @return game
	 */
	public Game getGame() {
		return game;
	}
	
	/**
	 * Used by the IoController to load a game
	 * @param game The game to be set
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * @return the AiController
	 * @author Leon
	 */
	public AIController getAiController() {
		return aiController;
	}

	/**
	 * @return the BlanketController
	 * @author Leon
	 */
	public BlanketController getBlanketController() {
		return blanketController;
	}

	/**
	 * @return the GameBoardController
	 * @author Leon
	 */
	public GameBoardController getGameBoardController() {
		return gameBoardController;
	}

	/**
	 * @return the GameStateController
	 * @author Leon
	 */
	public GameStateController getGameStateController() {
		return gameStateController;
	}

	/**
	 * @return the IOController
	 * @author Leon
	 */
	public IOController getIoController() {
		return ioController;
	}

	/**
	 * @return the PlayerController
	 * @author Leon
	 */
	public PlayerController getPlayerController() {
		return playerController;
	}

	/**
	 * @return Graphic User Interface
	 * @author Marcel
	 */
	public GUI getGUI() {
		return gui;
	}

	/**
	 *get Musik
	 */
	public boolean isMusik() {
		return musik;
	}

	/**
	 *set Musik
	 */
	public void setMusik(boolean musik) {
		this.musik = musik;
	}
}
