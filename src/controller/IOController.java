package controller;

import ai.agents.PatchworkAgent;
import ai.anneat.models.NeatData;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVReader;

import model.Difficulty;
import model.Game;
import model.Patch;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Controller used to handle IO interactions
 */
public class IOController {
    private GameController gameController;

    /**
     * Constructs a IOController
     *
     * @param gameController The main gameController
     */
    public IOController(GameController gameController) {
        this.gameController = gameController;
    }

    /**
     * @return a list of patches
     * returns null if the operation has failed.
     * @author Long
     * imports the default csv file
     */
    public List<Patch> importCSV() {
        String filepath = "resources/patchwork-pieces.csv";
        return importCSV(filepath);
    }

    /**
     * imports a custom csv file
     * returns null if the operation has failed.
     *
     * @param filepath filepatch
     * @return a list of patches
     * @author Long
     */
    public List<Patch> importCSV(String filepath) {
        LinkedList<Patch> patchList = new LinkedList<>();
        File file = new File(filepath);
        try {
            // Create an object of file reader class with CSV file as a parameter.
            FileReader filereader = new FileReader(file);

            CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

            CSVReader csvReader = new CSVReaderBuilder(filereader)
                    .withCSVParser(parser)
                    .withSkipLines(1) //skip headline
                    .build();

            // Read all data at once
            List<String[]> allData = csvReader.readAll();

            //kinda scuffed tbh
            for (String[] row : allData) {
                if (row[0].length() < 5 || row[0].length() % 5 != 0 || row[0].length() > 25) {
                    return null;
                }

                boolean[][] boolMatrix = convertStringRowToBooleanMatrix(row[0]);

                if (boolMatrix == null) {
                    return null;
                }

                row = Arrays.copyOfRange(row, 1, row.length);
                Patch patch = new Patch(boolMatrix, //patch
                        Integer.parseInt(row[1]), //time
                        Integer.parseInt(row[0]), //cost
                        Integer.parseInt(row[2])); //income

//                System.out.println(patch.toString());
//                System.out.println(Arrays.deepToString(row));
//                System.out.println();

                patchList.add(patch);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Patch p = findSmallestPatch(patchList);
        int indexOfSmallestPatch = patchList.indexOf(p);
        ArrayList<Patch> returnList = new ArrayList<>();

        returnList.addAll(patchList.subList(indexOfSmallestPatch + 1, patchList.size()));
        returnList.addAll(patchList.subList(0, indexOfSmallestPatch + 1));
        System.out.println(returnList.size());
        return returnList;
    }

    /**
     * @author Long
     * @param patchList the List
     * @return the smallest Patch in the List
     */
    private Patch findSmallestPatch(List<Patch> patchList) {
        boolean[][] matrix1 = new boolean[5][5];
        matrix1[0][0] = true;
        matrix1[0][1] = true;
        boolean[][] matrix2 = new boolean[5][5];
        matrix2[0][0] = true;
        matrix2[1][0] = true;

        for (Patch p : patchList) {
            if (Arrays.deepEquals(p.getMatrix(), matrix1) || Arrays.deepEquals(p.getMatrix(), matrix2))
                return p;
        }
        return patchList.get(0);
    }

    /**
     * converts a >=5 character long character array into a 5*5 matrix
     *
     * @param matrixString coded matrix String for Patch
     * @return a 5*5 boolean 2D array, null if the csv contains errors
     */
    private boolean[][] convertStringRowToBooleanMatrix(String matrixString) {
        boolean[][] boolMatrix = new boolean[5][5];
        char[] charArray = matrixString.toCharArray();
        int kVar = 0;
        for (int i = 0; i < charArray.length / 5; i++) {
            for (int j = 0; j < 5; j++) {
                switch (charArray[kVar++]) {
                    case 'X':
                        boolMatrix[i][j] = true;
                        break;
                    case '-':
                        boolMatrix[i][j] = false; //redundant
                        break;
                    default:
                        System.out.println("error I guess.");
                        return null;
                }
            }
        }
        return boolMatrix;
    }

    /**
     * @author Kevin
     * Overloaded method. Saves the game to directory of the .jar
     */
    public void saveGame() {
        saveGame(System.getProperty("user.dir") + "\\save.ser");
    }

    /**
     * @param path The path to save the game file to
     * @author Kevin
     */
    public void saveGame(String path) {
        if (path == null)
            throw new IllegalArgumentException();
        // Starting serialization
        // Order is important
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(gameController.getGame());
            out.close();

            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @return The loaded Game from the directory of the .jar
     * @author Kevin
     * Overloaded method. Returns the Game loaded by the .ser file
     */
    public Game loadGame() {
        return loadGame(System.getProperty("user.dir") + "\\save.ser");
    }

    /**
     * @param path The path to the location of the save file
     * @return The loaded Game
     * @author Kevin
     */
    public Game loadGame(String path) {
        if (path == null)
            throw new IllegalArgumentException();

        System.out.println("Loading from: " + path);

        Game game = null;
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream inputStream = new ObjectInputStream(fileIn);
            game = (Game) inputStream.readObject();
            inputStream.close();
            fileIn.close();
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("No save file found. Returning null");
        }

        return game;
    }

    /**
     * @param path The path to save the agent to. Needs to end in .ser
     * @param neatData The NeatData Object that needs to be saved
     * @author Leon
     */
    public void saveNeatDataToPath(@NotNull String path, @NotNull NeatData neatData) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(neatData);
            out.close();

            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param path Path of the .ser file
     * @return The loaded NeatData Object
     * @author Leon
     */
    public NeatData loadNeatFromPath(@NotNull String path) {
        NeatData neatData = null;
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream inputStream = new ObjectInputStream(fileIn);
            neatData = (NeatData) inputStream.readObject();
            inputStream.close();
            fileIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return neatData;
    }

    /**
     * Loads the agent map from the given path
     *
     * @param path Path of the .ser file
     * @return The loaded Agentmap
     * @author Long + Kevin
     */
    public Optional<Map<Difficulty, PatchworkAgent>> loadAgentMapFromPath(String path) {
        if (path == null)
            throw new IllegalArgumentException();
        Map<Difficulty, PatchworkAgent> agentMap = null;
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream inputStream = new ObjectInputStream(fileIn);
            agentMap = (Map<Difficulty, PatchworkAgent>) inputStream.readObject();
            //System.out.println("wqqq");
            inputStream.close();
            fileIn.close();
        } catch (Exception e) {
            System.out.println("Failed loading agentMap");
            return Optional.empty();
        }

        return Optional.of(agentMap);
    }

    /**
     * saves the agent map from the given path
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * @AIGruppe
     * WICHTIG: um die Map serializable zu bekommen müsst ihr die NeatAgent.function
     * typecasten als (Function<double[], double[]> & Serializable)
     * Im IOController befindet sich ein Beispiel
     * ist die einfachste Lösung. Sollte es nicht klappen oder es nicht möglich sein, sagt bescheid.
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     *
     * @param path Path of the .ser file, den path müsst ihr selber angeben
     * @return the success of the operation as boolean
     * @author Long + Kevin
     */
    public void saveAgentMapFromPath(Map<Difficulty, PatchworkAgent> agentMap, String path) {
        if (path == null || agentMap == null) {
            throw new IllegalArgumentException();
        }
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(agentMap);
            out.close();

            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @author Leon
     */
    public static <O> void save(@NotNull O object, @NotNull String path) {
        try {
            System.out.println("SAVING TO: " + path);
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(object);
            out.close();
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @author Leon
     */
    @Nullable
    public static <O> O load(@NotNull String path) {
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream inputStream = new ObjectInputStream(fileIn);
            O object = (O) inputStream.readObject();
            inputStream.close();
            fileIn.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
